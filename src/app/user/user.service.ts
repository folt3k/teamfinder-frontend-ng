import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  RegistrationDataAvailabilityRO,
  RegistrationDataAvailabilityParams,
  CreateUserDto,
  UpdateUserDto,
  ChangePasswordDto,
  RemindPasswordDto,
  UnsubscribeFromMailNotificationsDto,
  RecoverPasswordDto,
} from './user.interface';
import { User, UserMailNotificationSettings } from '../_shared/models';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  checkRegistrationDataAvailability(
    params: RegistrationDataAvailabilityParams
  ): Observable<RegistrationDataAvailabilityRO> {
    return this.http.get<RegistrationDataAvailabilityRO>('/users/register/check-availability', {
      params,
    });
  }

  register(body: CreateUserDto): Observable<void> {
    return this.http.post<void>('/users/register', body);
  }

  update(body: UpdateUserDto): Observable<User> {
    return this.http.put<User>('/users', body);
  }

  remove(): Observable<void> {
    return this.http.delete<void>('/users');
  }

  changePassword(body: ChangePasswordDto): Observable<void> {
    return this.http.put<void>('/users/change-password', body);
  }

  remindPassword(body: RemindPasswordDto): Observable<void> {
    return this.http.post<void>('/users/forgot-password', body);
  }

  recoverPassword(body: RecoverPasswordDto): Observable<void> {
    return this.http.post<void>('/users/recover-password', body);
  }

  updateMailNotificationSetting(
    body: UserMailNotificationSettings
  ): Observable<UserMailNotificationSettings> {
    return this.http.put<UserMailNotificationSettings>('/users/mail-notification-settings', body);
  }

  unsubscribeFromMailNotifications(body: UnsubscribeFromMailNotificationsDto): Observable<void> {
    return this.http.put<void>('/users/mail-notification-settings/unsubscribe', body);
  }
}
