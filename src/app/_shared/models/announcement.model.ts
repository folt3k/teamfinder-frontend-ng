import { UserProfile } from './user-profile.model';
import { BaseEntity, GameType } from '../interfaces';
import { GameMode } from './mode.model';
import { GameRole } from './role.models';

export interface Announcement extends BaseEntity {
  author: UserProfile;
  type: GameType;
  desc: string;
  disabled: boolean;
  members: AnnouncementMember[];
  modes: GameMode[];
  roles: GameRole[];
}

export interface AnnouncementMember {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  member: UserProfile;
}
