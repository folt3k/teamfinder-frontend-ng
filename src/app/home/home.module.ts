import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { SharedModule } from '../_shared/shared.module';
import { JumbotronComponent } from './jumbotron/jumbotron.component';
import { AboutComponent } from './about/about.component';
import { AvailableGamesComponent } from './available-games/available-games.component';
import { FeedbacksComponent } from './feedbacks/feedbacks.component';
import { LastTeamsComponent } from './last-teams/last-teams.component';
import { TeamModule } from '../team/team.module';
import { ProfileModule } from '../profile/profile.module';
import { FeedbacksModule } from '../feedbacks/feedbacks.module';
import { CockpitModule } from '../cockpit/cockpit.module';

@NgModule({
  declarations: [
    HomeComponent,
    JumbotronComponent,
    AboutComponent,
    AvailableGamesComponent,
    FeedbacksComponent,
    LastTeamsComponent,
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    TeamModule,
    ProfileModule,
    FeedbacksModule,
    CockpitModule,
  ],
})
export class HomeModule {}
