import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
  Inject,
} from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState } from 'src/app/app.interface';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'tf-global-chat-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent implements OnInit {
  @Input() activeUsersCount: number = 0;
  @Input() isMaximized: boolean = false;
  @Input() unseenMessagesCount: number = 0;
  @Output() closed = new EventEmitter();
  @Output() maximized = new EventEmitter();

  constructor(private store: Store<AppState>, @Inject(DOCUMENT) private document: Document) {}

  ngOnInit(): void {}

  close(): void {
    this.closed.emit();
  }

  maximize(event: Event): void {
    event.stopPropagation();
    this.maximized.emit();
  }
}
