import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AnnouncementDetailsComponent } from './details.component';
import { AnnouncementDetailsRoutingModule } from './details-routing.module';
import { EntityDetailsModule } from '../../entity-details/entity-details.module';

@NgModule({
  declarations: [AnnouncementDetailsComponent],
  imports: [
    CommonModule,
    AnnouncementDetailsRoutingModule,
    EntityDetailsModule,
  ]
})
export class AnnouncementDetailsModule { }
