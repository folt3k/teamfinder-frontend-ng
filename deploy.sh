#!/bin/bash

ssh root@77.55.212.56 << 'ENDSSH'
pm2 delete node
pm2 delete main
pm2 list all

cd /var/www/teamfinder-frontend

cp src/assets/pages/maintenance.html ./maintenance.html
mv maintenance.html maintenance.enabled.html
service nginx restart

git stash
git fetch
git checkout master
git pull
npm install
npm run build:ssr
pm2 start node /var/www/teamfinder-frontend/dist/teamfinder-chat-frontend/server/main.js
pm2 list all
pm2 restart all

mv maintenance.enabled.html  maintenance.html

service nginx restart
