import { AppState } from '../../app.interface';
import { featureKey } from './constants';
import { createSelector } from '@ngrx/store';

export const selectFeature = (state: AppState) => state[featureKey];

export const selectCurrentGame = createSelector(selectFeature, (state) => state.selected);

export const selectGameHistory = createSelector(selectFeature, (state) => state.gamesHistory);
