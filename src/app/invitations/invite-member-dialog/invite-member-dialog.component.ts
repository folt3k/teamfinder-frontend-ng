import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map, tap, withLatestFrom } from 'rxjs/operators';

import { Team } from '../../team';
import { AppState } from '../../app.interface';
import { currentProfile } from '../../auth/_reducer';
import { UserProfile } from '../../_shared/models';
import { selectDetails } from '../../entity-details/_reducer';
import { sendInvitation } from '../_reducer';
import { DialogRef } from '../../_shared/poppy/dialog';

@Component({
  selector: 'tf-invite-member-dialog',
  templateUrl: './invite-member-dialog.component.html',
  styleUrls: ['./invite-member-dialog.component.scss'],
})
export class InviteMemberDialogComponent implements OnInit {
  team: FormControl;
  currentUserOwnedTeams$: Observable<Team[]>;
  invitedProfile: UserProfile;

  constructor(private store: Store<AppState>, private dialogRef: DialogRef) {}

  ngOnInit(): void {
    this.currentUserOwnedTeams$ = this.getCurrentUserOwnedTeams$();
    this.team = new FormControl();
  }

  onSubmit(): void {
    this.store.dispatch(
      sendInvitation({ body: { profileId: this.invitedProfile.id, teamId: this.team.value } })
    );
    this.dialogRef.close();
  }

  private getCurrentUserOwnedTeams$(): Observable<Team[]> {
    return this.store.select(currentProfile).pipe(
      withLatestFrom(this.store.select(selectDetails)),
      filter(([profile]) => !!profile),
      tap(([_, details]) => {
        setTimeout(() => {
          this.invitedProfile = details as UserProfile;
        });
      }),
      map(([profile, details]) =>
        profile.ownedTeams.filter((team) => !team.members.find((member) => member.profile.id === details.id))
      )
    );
  }
}
