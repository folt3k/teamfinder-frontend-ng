import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { RouterReducerState } from '@ngrx/router-store';
import { Router } from '@angular/router';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { of } from 'rxjs';

import { AppState } from '../../app.interface';
import {
  changeCurrentGame,
  fetchLolGamesHistory,
  fetchLolGamesHistoryFailed,
  fetchLolGamesHistorySuccess,
} from './actions';
import { GAMES } from '../../_shared/common';
import { isHomeURLWithGamePrefix, isURLWithGamePrefix } from '../../_helpers';
import { GamesApiService } from '../_services/games-api.service';
import { selectDetails } from '../../entity-details/_reducer';
import { selectGameHistory } from './selectors';

@Injectable()
export class GameEffects {
  changeCurrentGame$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(changeCurrentGame),
        withLatestFrom(this.store.select((state) => state.router)),
        tap(([action, router]) => {
          this.onGameChange(action.gameId, router);
        })
      ),
    { dispatch: false }
  );

  fetchLolGamesHistory$ = createEffect(() =>
    this.actions$.pipe(
      ofType(fetchLolGamesHistory),
      withLatestFrom(this.store.select(selectGameHistory), this.store.select(selectDetails)),
      switchMap(([_, gamesHistory, details]) =>
        this.gamesApiService
          .fetchLolGamesHistory(details.game_metadata.accountId, {
            page: gamesHistory.page + 1,
            summonerServer: details.gameAccountServer,
          })
          .pipe(
            map((data) => fetchLolGamesHistorySuccess({ data })),
            catchError((err) => of(fetchLolGamesHistoryFailed(err)))
          )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private router: Router,
    private gamesApiService: GamesApiService
  ) {}

  private onGameChange(gameId: number, router: RouterReducerState<any>): void {
    const url = router?.state?.url;
    const game = GAMES.find((g) => g.value === gameId);
    const canMoveToHomeWithGamePrefix = url === '/' || isHomeURLWithGamePrefix(url);

    if (canMoveToHomeWithGamePrefix) {
      window.history.pushState('', '', game.slashRouterPrefix);
      return;
    }

    const isCurrentPathIncludesGamePrefix = isURLWithGamePrefix(url);

    if (isCurrentPathIncludesGamePrefix) {
      const urlGamePrefix = router.state.root.firstChild.params.gamePrefix;

      if (urlGamePrefix !== game.routerPrefix) {
        const newUrl = url.replace(urlGamePrefix, game.routerPrefix).split('?')[0];
        this.router.navigate([newUrl]);
      }
    }
  }
}
