import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import * as fromNotifications from './_reducer';
import { CompactListComponent } from './compact-list/compact-list.component';
import { ItemComponent } from './item/item.component';
import { SharedModule } from '../_shared/shared.module';

@NgModule({
  declarations: [CompactListComponent, ItemComponent],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromNotifications.featureKey, fromNotifications.reducer),
    EffectsModule.forFeature([fromNotifications.NotificationsEffects]),
    SharedModule,
  ],
  exports: [CompactListComponent, ItemComponent],
})
export class NotificationsModule {}
