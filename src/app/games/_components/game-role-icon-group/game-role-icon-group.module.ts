import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopoverModule } from '@ng-poppy/lib';

import { GameRoleIconGroupComponent } from './game-role-icon-group.component';

@NgModule({
  declarations: [GameRoleIconGroupComponent],
  imports: [CommonModule, PopoverModule],
  exports: [GameRoleIconGroupComponent],
})
export class GameRoleIconGroupModule {}
