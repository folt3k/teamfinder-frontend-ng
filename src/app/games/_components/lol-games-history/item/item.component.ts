import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

import { LolGamesHistoryItem } from '../../../games.interface';

@Component({
  selector: 'tf-lol-games-history-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LolGamesHistoryItemComponent implements OnInit {
  @Input() game: LolGamesHistoryItem;

  constructor() {}

  get queueName(): string {
    const queue = this.game.queue;

    if (+queue === 420) {
      return 'Rankingowa solo/duet';
    }
    if (+queue === 440) {
      return 'Rankingowa Flex';
    }

    return '';
  }

  get kdaScoreLabel(): string {
    const kda = this.game.summonerPlayer.kda;

    return kda.d === 0 ? 'Perfect KDA' : `${((kda.k + kda.a) / kda.d).toFixed(2)}:1 KDA`;
  }

  ngOnInit(): void {}
}
