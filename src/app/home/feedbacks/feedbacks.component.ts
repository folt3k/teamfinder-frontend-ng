import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Feedback, UserProfile } from '../../_shared/models';
import { ProfileService } from '../../profile/profile.service';
import { FeedbacksService } from '../../feedbacks/feedbacks.service';

@Component({
  selector: 'tf-home-feedbacks',
  templateUrl: './feedbacks.component.html',
  styleUrls: ['./feedbacks.component.scss'],
})
export class FeedbacksComponent implements OnInit {
  users$: Observable<UserProfile[]>;
  feedbacks$: Observable<Feedback[]>;

  constructor(private profileService: ProfileService, private feedbacksService: FeedbacksService) {}

  ngOnInit(): void {
    this.users$ = this.profileService.fetchLatest().pipe(map((data) => data.results));
    this.feedbacks$ = this.feedbacksService.fetchAll({ perPage: 8 }).pipe(map((data) => data.results));
  }
}
