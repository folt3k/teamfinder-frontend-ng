import { AfterViewInit, Component, ContentChildren, QueryList, TemplateRef } from '@angular/core';
import { TabComponent } from './tab/tab.component';

interface Tab {
  label: string;
  content: TemplateRef<any>;
  active: boolean;
}

@Component({
  selector: 'tf-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent implements AfterViewInit {
  @ContentChildren(TabComponent) tabsList: QueryList<TabComponent>;

  tabs: Tab[] = [];

  constructor() {}

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.tabs = this.tabsList.toArray().map((tab) => ({
        label: tab.label,
        content: tab.content,
        active: tab.active,
      }));
    });
  }

  onTabClick(clickedTab: Tab): void {
    this.tabs = this.tabs.map((tab) => ({ ...tab, active: tab === clickedTab }));
  }
}
