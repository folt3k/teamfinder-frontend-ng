import { GameRank } from '../models';

export interface BaseEntity {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  entityType?: EntityType;
  logoUrl?: string;
  name?: string;
  username?: string;
  rank?: GameRank;
  ranks?: GameRank[];
  desc?: string;
  [key: string]: any;
}

export enum EntityType {
  USER = 'user',
  TEAM = 'team',
  ANNOUNCEMENT = 'game-announcement',
}

export const getEndpointPrefixByEntityType = (type: EntityType) => {
  switch (type) {
    case EntityType.TEAM:
      return 'teams';
    case EntityType.ANNOUNCEMENT:
      return 'game-announcements';
    default:
      return '';
  }
};
