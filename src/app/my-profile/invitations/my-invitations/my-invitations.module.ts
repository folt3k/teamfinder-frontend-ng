import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyProfileMyInvitationsComponent } from './my-invitations.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../../../_shared/shared.module';
import { MyProfileInvitationListModule } from '../list/list.module';

@NgModule({
  declarations: [MyProfileMyInvitationsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: MyProfileMyInvitationsComponent,
      },
    ]),
    SharedModule,
    MyProfileInvitationListModule,
  ],
})
export class MyProfileMyInvitationsModule {}
