import { BaseSelectOption } from '../_shared/interfaces';

export interface CreateAnnouncementDto {
  desc: string;
  rank: BaseSelectOption;
  modes: BaseSelectOption[];
  roles: BaseSelectOption[];
}

export interface UpdateAnnouncementDto {
  desc: string;
  rank: BaseSelectOption;
  modes: BaseSelectOption[];
  roles: BaseSelectOption[];
}
