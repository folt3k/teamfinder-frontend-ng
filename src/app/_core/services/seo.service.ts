import { Injectable } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';
import { get } from 'lodash';

import { seoTranslations } from '../../_shared/common/seo-translations';
import { GameType } from '../../_shared/interfaces';
import { getGameByType } from '../../games';

export interface SeoData {
  title?: string;
  description?: string;
  meta?: Array<{ property: string; content: string }>;
}

@Injectable({
  providedIn: 'root',
})
export class SeoService {
  constructor(private title: Title, private meta: Meta) {}

  setSeo(path: string, gameType?: GameType): void {
    try {
      const _data: SeoData = get(seoTranslations, path);
      const data: SeoData = this.prepareData(_data, gameType);

      if (data.title) {
        this.setTitle(data.title);
      }
      this.setMeta(data);
    } catch (e) {}
  }

  setTitle(title: string): void {
    this.title.setTitle(title);
  }

  setMeta(data: SeoData): void {
    this.meta.removeTag("name='description'");
    if (data.meta) {
      data.meta.forEach((item) => {
        this.meta.removeTag(`property='${item.property}'`);
      });
    }
    this.meta.addTags([
      ...(data.description ? [{ name: 'description', content: data.description }] : []),
      ...(data.meta ? [...data.meta.map((m) => ({ property: m.property, content: m.content }))] : []),
    ]);
  }

  private prepareData(data: SeoData, gameType?: GameType): SeoData {
    const gameName = gameType ? getGameByType(gameType).displayName : null;
    const translate = (text: string) => this.translate(text, { gameName });

    return {
      ...(data.title ? { title: translate(data.title) } : null),
      ...(data.description ? { description: translate(data.description) } : null),
      ...(data.meta
        ? [...data.meta.map((m) => ({ property: m.property, content: translate(m.content) }))]
        : []),
    };
  }

  private translate(text: string, { gameName }): string {
    return text.replace(/<% game %>/g, gameName);
  }
}
