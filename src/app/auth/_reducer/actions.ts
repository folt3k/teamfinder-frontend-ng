import { createAction, props } from '@ngrx/store';

import { types } from './constants';
import { AccountActivationRO, LoginRO, UserMeRO } from '../auth.interface';
import { ErrorRO } from 'src/app/_shared/interfaces';

export const init = createAction(types.Init, props<{ token: string }>());

export const login = createAction(types.Login, props<{ email: string; password: string }>());
export const loginSuccess = createAction(types.LoginSuccess, props<LoginRO>());
export const loginFailed = createAction(types.LoginFailed, props<ErrorRO>());

export const logout = createAction(types.Logout);
export const logoutSuccess = createAction(types.LogoutSuccess);

export const me = createAction(types.Me);
export const meSuccess = createAction(types.MeSuccess, props<UserMeRO>());
export const meFailed = createAction(types.MeFailed, props<ErrorRO>());

export const activateAccount = createAction(types.AccountActivation);
export const activateAccountSuccess = createAction(
  types.AccountActivationSuccess,
  props<AccountActivationRO>()
);
export const activateAccountFailed = createAction(types.AccountActivationFailed, props<ErrorRO>());

export const clearError = createAction(types.ClearError);

export const finalAuthorization = createAction(types.FinalAuthorization);
