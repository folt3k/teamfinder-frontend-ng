import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AllTeamsRoutingModule } from './all-routing.module';
import { AllTeamsComponent } from './all.component';
import { EntitiesScreenModule } from '../../entities-screen/entities-screen.module';

@NgModule({
  declarations: [AllTeamsComponent],
  imports: [CommonModule, AllTeamsRoutingModule, EntitiesScreenModule],
})
export class AllTeamsModule {}
