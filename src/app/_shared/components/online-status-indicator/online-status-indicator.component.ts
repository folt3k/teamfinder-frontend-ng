import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'tf-online-status-indicator',
  templateUrl: './online-status-indicator.component.html',
  styleUrls: ['./online-status-indicator.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OnlineStatusIndicatorComponent implements OnInit {
  @Input() isOnline: boolean = false;
  @Input() showLabel: boolean = false;

  constructor() {}

  ngOnInit(): void {}
}
