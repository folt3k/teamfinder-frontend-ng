export const featureKey = 'profile';

export const types = {
  Update: '[Profile] Update profile',
  UpdateSuccess: '[Profile] Update profile success',
  UpdateFailed: '[Profile] Update profile failed',

  UpdateGameMetadata: '[Profile] Update profile game metadata',
  UpdateGameMetadataSuccess: '[Profile] Update profile game metadata success',
  UpdateGameMetadataFailed: '[Profile] Update profile game metadata failed',
};
