import { takeUntil, filter } from 'rxjs/operators';
import { Subject } from 'rxjs';
import {
  Component,
  Input,
  ChangeDetectionStrategy,
  OnDestroy,
  OnInit,
  ChangeDetectorRef,
} from '@angular/core';
import { ErrorState } from 'src/app/_reducers';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.interface';

@Component({
  selector: 'tf-error-message',
  templateUrl: './error-message.component.html',
  styleUrls: ['./error-message.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ErrorMessageComponent implements OnInit, OnDestroy {
  @Input() reducerKey: string;

  error: ErrorState | null;
  private destroy = new Subject();

  constructor(private store: Store<AppState>, private cdr: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.store
      .select((state) => state[this.reducerKey].error as ErrorState)
      .pipe(
        takeUntil(this.destroy),
        filter((error) => !!error)
      )
      .subscribe((error) => {
        this.error = error;
        this.cdr.detectChanges();
      });
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }
}
