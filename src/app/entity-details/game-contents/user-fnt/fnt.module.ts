import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FntDetailsContentComponent } from './fnt.component';
import { FntDetailsContentService } from './fnt.service';
import { SharedModule } from '../../../_shared/shared.module';
import { GamesModule } from '../../../games/games.module';

@NgModule({
  declarations: [FntDetailsContentComponent],
  imports: [CommonModule, SharedModule, GamesModule],
  exports: [FntDetailsContentComponent],
  providers: [FntDetailsContentService],
})
export class FntDetailsContentModule {}
