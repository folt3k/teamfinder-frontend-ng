import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FooterComponent } from './footer.component';
import { SharedModule } from '../../../_shared/shared.module';
import { GamesModule } from '../../../games/games.module';

@NgModule({
  declarations: [FooterComponent],
  imports: [CommonModule, SharedModule, GamesModule],
  exports: [FooterComponent],
})
export class FooterModule {}
