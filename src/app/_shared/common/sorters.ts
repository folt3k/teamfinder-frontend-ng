export const DEFAULT_PER_PAGE = '36';

export const perPageOptions = [
  {
    label: '18',
    value: 18,
  },
  {
    label: '36',
    value: 36,
  },
  {
    label: '72',
    value: 72,
  },
];

export const orderOptions = [
  {
    label: 'od najnowszego',
    value: 'desc',
  },
  {
    label: 'od najstarszego',
    value: 'asc',
  },
];
