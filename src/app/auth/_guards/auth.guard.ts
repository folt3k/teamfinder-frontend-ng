import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, CanActivate, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { map, first, withLatestFrom } from 'rxjs/operators';

import { AppState } from 'src/app/app.interface';
import { isLoggedIn } from '../_reducer';
import { STATIC_URLS } from '../../_shared/common';
import { updateUI } from '../../_reducers/ui';

@Injectable()
export class AuthGuard implements CanLoad, CanActivate {
  constructor(private router: Router, private route: ActivatedRoute, private store: Store<AppState>) {}

  canLoad(route: Route): Observable<boolean> {
    return this.isLoggedIn(route.path);
  }

  canActivate(): Observable<boolean> {
    return this.isLoggedIn();
  }

  private isLoggedIn(guardPath?: string): Observable<boolean> {
    return this.store.select(isLoggedIn).pipe(
      withLatestFrom(this.store.select((state) => state.router)),
      first(),
      map(([logged, router]) => {
        if (!logged) {
          const redirectPath: string = guardPath ? `/${guardPath}` : router.state.url;
          this.store.dispatch(updateUI({ redirectPath }));
          this.router.navigate([STATIC_URLS['login'].slashPath]);

          return false;
        }
        return true;
      })
    );
  }
}
