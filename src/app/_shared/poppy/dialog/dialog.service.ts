import { ComponentFactoryResolver, ComponentRef, Injectable, Injector, Type } from '@angular/core';

import { DialogComponent } from './dialog.component';
import { LayerService } from '../layer/layer.service';
import { LayerAppendOptions } from '../layer/layer.model';
import { DIALOG_CONFIG, DIALOG_DATA } from './dialog.token';
import { ActiveDialog, DialogOptions, DialogUniqueId } from './dialog.interface';
import { DialogRef } from './dialog-ref';

@Injectable({
  providedIn: 'root',
})
export class DialogService {
  private activeDialogs: ActiveDialog[] = [];

  constructor(private cfr: ComponentFactoryResolver, private layerService: LayerService) {}

  open<T>(component: Type<T>, options: DialogOptions = {}): DialogRef {
    const dialogRef = this.layerService.appendToBody(
      DialogComponent,
      new LayerAppendOptions({ overlay: true }),
      Injector.create([
        {
          provide: DIALOG_CONFIG,
          useValue: {
            dialogId: this.activeDialogs.length + 1,
            service: this,
          },
        },
      ])
    );

    dialogRef.changeDetectorRef.detectChanges();

    const contentFactory = this.cfr.resolveComponentFactory(component);
    const contentRef = dialogRef.instance.container.createComponent(
      contentFactory,
      null,
      Injector.create([
        {
          provide: DialogRef,
          useValue: dialogRef.instance,
        },
        {
          provide: DIALOG_DATA,
          useValue: options.data,
        },
      ])
    );

    setTimeout(() => {
      dialogRef.instance.afterShow.emit();
    });

    this.activeDialogs.push({
      dialogId: this.activeDialogs.length + 1,
      uniqueId: options.uniqueId,
      dialogRef,
      contentRef,
    });

    document.body.classList.add('dialog-open');

    return dialogRef.instance;
  }

  remove<T>(handler: ComponentRef<DialogComponent> | DialogUniqueId, options: { payload?: any } = {}): void {
    let dialogRef: ComponentRef<DialogComponent>;

    if (handler instanceof ComponentRef) {
      dialogRef = handler;
    } else {
      dialogRef = this.activeDialogs.find((dialog) => dialog.uniqueId === handler).dialogRef;
    }

    this.activeDialogs = this.activeDialogs.filter((dialog) => dialog.dialogRef !== dialogRef);
    this.layerService.removeFromBody(dialogRef);
    dialogRef.instance.afterClose.emit(options.payload);
    dialogRef.destroy();

    if (!this.activeDialogs.length) {
      document.body.classList.remove('dialog-open');
    }
  }

  getActiveDialog(dialogId: number): ActiveDialog {
    return this.activeDialogs.find((dialog) => dialog.dialogId === dialogId);
  }
}
