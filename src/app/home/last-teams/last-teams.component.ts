import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Team } from '../../team';
import { TeamService } from '../../team/team.service';

@Component({
  selector: 'tf-home-last-teams',
  templateUrl: './last-teams.component.html',
  styleUrls: ['./last-teams.component.scss'],
})
export class LastTeamsComponent implements OnInit {
  results$: Observable<Team[]>;

  constructor(private teamsService: TeamService) {}

  ngOnInit(): void {
    this.results$ = this.teamsService.fetchLatest().pipe(map((data) => data.results));
  }
}
