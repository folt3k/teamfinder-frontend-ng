export const featureKey = 'chat';

export const types = {
  FetchPrivateRooms: '[Chat] Fetch private rooms',
  FetchPrivateRoomsSuccess: '[Chat] Fetch private rooms success',
  FetchPrivateRoomsFailed: '[Chat] Fetch private rooms failed',

  FetchTeamRooms: '[Chat] Fetch team rooms',
  FetchTeamRoomsSuccess: '[Chat] Fetch team rooms success',
  FetchTeamRoomsFailed: '[Chat] Fetch team rooms failed',

  ReceiveNewTeamRoom: '[Chat] Receive new team room',

  ReceiveNewTeamMember: '[Chat] Receive new team member',

  FetchMoreMessages: '[Chat] Fetch more messages',
  FetchMoreMessagesSuccess: '[Chat] Fetch more messages success',

  SendRoomMessage: '[Chat] Send room message',
  SendRoomMessageSuccess: '[Chat] Send room message success',

  ReceiveMessage: '[Chat] Receive message',

  ReceiveTeamMessage: '[Chat] Receive team message',

  MarkRoomAsRead: '[Chat] Mark room as read',

  ReceiveNewConversation: '[Chat] Receive new conversation',
  StartNewConversation: '[Chat] Start new conversation',
  CreateNewConversation: '[Chat] Create new conversation',
  CreateNewConversationSuccess: '[Chat] Create new conversation success',
  ResetWasNewConversationCreated: '[Chat] Reset WasNewConversationCreated field',

  OpenConversation: '[Chat] Open conversation',

  AddRoomWindow: '[Chat] Add room window',
  CloseRoomWindow: '[Chat] Close room window',
  MinimizeRoomWindow: '[Chat] Minimize room window',
  MaximizeRoomWindow: '[Chat] Maximize room window',
};
