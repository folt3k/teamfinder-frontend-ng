import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, filter, first, map, mergeMap, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { AppState } from '../../app.interface';
import * as actions from './actions';
import { addRoomWindow, fetchPrivateRooms, startNewConversation } from './actions';
import { PrivateChatService } from '../_services/private-chat.service';
import { serializePrivateChatRooms, serializeTeamChatRooms } from '../_models/chat-room.model';
import { currentProfile, currentProfileId, currentUser } from '../../auth/_reducer';
import { PrivateChatSocketService } from '../_services/private-chat-ws.service';
import {
  selectNewConversationReceiver,
  selectPrivateRooms,
  selectRoom,
  selectRoomMessagesCount,
} from './selectors';
import { AlertService } from '../../_shared/components/alert/alert.service';
import { SoundService, SoundType } from '../../_core/services';
import { closeChat } from '../../global-chat/_reducer';
import { TeamChatService } from '../_services/team-chat.service';
import { TeamChatSocketService } from '../_services/team-chat-ws.service';
import { ChatType } from '../chat.inerface';

@Injectable()
export class ChatEffects {
  fetchPrivateRooms$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.fetchPrivateRooms),
      withLatestFrom(this.store.select(currentProfile)),
      switchMap(([action, profile]) =>
        this.privateChatService.fetchRooms().pipe(
          map((data) =>
            actions.fetchPrivateRoomsSuccess({
              results: serializePrivateChatRooms(data, { currentProfile: profile }),
            })
          ),
          catchError((err) => of(actions.fetchPrivateRoomsFailed(err)))
        )
      )
    )
  );

  fetchTeamRooms$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.fetchTeamRooms),
      withLatestFrom(this.store.select(currentProfile)),
      switchMap(([action, profile]) =>
        this.teamChatService.fetchRooms().pipe(
          map((data) =>
            actions.fetchTeamRoomsSuccess({
              results: serializeTeamChatRooms(data.results, { currentProfile: profile }),
              unreadMessages: data.unreadMessages,
            })
          ),
          catchError((err) => {
            return of(actions.fetchTeamRoomsFailed(err));
          })
        )
      )
    )
  );

  fetchMoreMessages$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.fetchMoreMessages),
        mergeMap((action) =>
          of(action).pipe(
            withLatestFrom(
              this.store.select(selectRoomMessagesCount, { roomId: action.roomId, chatType: action.chatType })
            )
          )
        ),
        map(([action, messagesCount]) => {
          if (action.chatType === ChatType.PRIVATE) {
            this.privateChatSocketService.fetchMoreMessages({ page: messagesCount, roomId: action.roomId });
          } else {
            this.teamChatSocketService.fetchMoreMessages({ page: messagesCount, roomId: action.roomId });
          }
        })
      ),
    { dispatch: false }
  );

  sendRoomMessage$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.sendRoomMessage),
        withLatestFrom(this.store.select(currentUser)),
        tap(([{ roomId, chatType, content }, user]) => {
          if (user.user.banned) {
            this.alertService.definedAlerts.userBanned.show();
          } else {
            if (chatType === ChatType.PRIVATE) {
              this.privateChatSocketService.sendRoomMessage({
                roomId,
                content,
                senderId: user.profile.id,
              });
            } else {
              this.teamChatSocketService.sendRoomMessage({
                roomId,
                content,
                senderId: user.profile.id,
              });
            }
          }
        })
      ),
    { dispatch: false }
  );

  markRoomAsRead$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.markRoomAsRead),
        withLatestFrom(this.store.select(currentProfileId)),
        tap(([{ roomId, chatType }, profileId]) => {
          if (chatType === ChatType.PRIVATE) {
            this.privateChatSocketService.markRoomAsRead({ roomId, memberId: profileId });
          } else {
            this.teamChatSocketService.markRoomAsRead({ roomId, memberId: profileId });
          }
        })
      ),
    { dispatch: false }
  );

  receiveMessage$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.receiveMessage),
        mergeMap((action) =>
          of(action).pipe(
            withLatestFrom(
              this.store.select(selectRoom, { roomId: action.roomId, chatType: action.chatType })
            )
          )
        ),
        filter(([_, room]) => room),
        tap(([_, room]) => {
          this.soundService.play(SoundType.NEW_MESSAGE);

          if (!room.active) {
            this.store.dispatch(addRoomWindow({ roomId: room.id, chatType: room.chatType }));
          }
        })
      ),
    { dispatch: false }
  );

  receiveNewConversation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.receiveNewConversation),
      tap(() => this.store.dispatch(fetchPrivateRooms())),
      switchMap(() =>
        this.actions$.pipe(
          ofType(actions.fetchPrivateRoomsSuccess),
          first(),
          map((data) => {
            this.soundService.play(SoundType.NEW_MESSAGE);
            return addRoomWindow({ roomId: data.results[0].id, chatType: data.results[0].chatType });
          })
        )
      )
    )
  );

  createNewConversation$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.createNewConversation),
        withLatestFrom(this.store.select(currentUser), this.store.select(selectNewConversationReceiver)),
        filter(([a, u, r]) => !!r && !!u?.user),
        tap(([action, user, receiver]) => {
          if (user.user.banned) {
            this.alertService.definedAlerts.userBanned.show();
          } else {
            this.privateChatSocketService.createNewConversation({
              content: action.content,
              senderId: user.profile.id,
              receiverId: receiver.id,
            });
          }
        })
      ),
    { dispatch: false }
  );

  createNewConversationSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.createNewConversationSuccess),
      map(() => fetchPrivateRooms())
    )
  );

  openConversation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.openConversation),
      withLatestFrom(this.store.select(selectPrivateRooms)),
      map(([{ receiver }, rooms]) => {
        const room = rooms.find((room) => room.memberProfile.id === receiver.id);

        if (room) {
          return addRoomWindow({ roomId: room.id, chatType: room.chatType });
        }

        return startNewConversation({ receiver });
      })
    )
  );

  openChatWindow$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.addRoomWindow, actions.startNewConversation),
      map(() => closeChat())
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private privateChatService: PrivateChatService,
    private privateChatSocketService: PrivateChatSocketService,
    private teamChatService: TeamChatService,
    private teamChatSocketService: TeamChatSocketService,
    private alertService: AlertService,
    private soundService: SoundService
  ) {}
}
