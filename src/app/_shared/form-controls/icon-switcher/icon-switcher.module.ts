import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IconSwitcherComponent } from './icon-switcher.component';

@NgModule({
  declarations: [IconSwitcherComponent],
  imports: [CommonModule],
  exports: [IconSwitcherComponent],
})
export class IconSwitcherModule {}
