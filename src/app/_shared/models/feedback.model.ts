import { UserProfile } from './user-profile.model';
import { Team } from '../../team';
import { Announcement } from './announcement.model';
import { Comment } from './comment.model';

export interface Feedback {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  action: FeedbackAction;
  senderUser?: UserProfile;
  senderTeam?: Team;
  receiverUser?: UserProfile;
  receiverTeam?: Team;
  receiverGameAnnouncement?: Announcement;
  comment?: Comment;
}

export enum FeedbackAction {
  INVITED = 1,
  TEAM_REQUESTED,
  JOINED,
  TEAM_CREATED,
  USER_CREATED,
  TEAM_COMMENTED,
  GAME_ANNOUNCEMENT_COMMENTED,
  GAME_ANNOUNCEMENT_CREATED,
}
