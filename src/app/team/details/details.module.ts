import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TeamDetailsComponent } from './details.component';
import { TeamDetailsRoutingModule } from './details-routing.module';
import { EntityDetailsModule } from '../../entity-details/entity-details.module';

@NgModule({
  declarations: [TeamDetailsComponent],
  imports: [
    CommonModule,
    TeamDetailsRoutingModule,
    EntityDetailsModule,
  ]
})
export class TeamDetailsModule { }
