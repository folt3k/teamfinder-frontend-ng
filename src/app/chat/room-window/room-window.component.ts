import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { BehaviorSubject, fromEvent, Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, first, map, skip, takeUntil } from 'rxjs/operators';

import { PrivateChatRoom, TeamChatRoom } from '../_models/chat-room.model';
import { AppState } from '../../app.interface';
import {
  createNewConversation,
  fetchMoreMessages,
  fetchMoreMessagesSuccess,
  markRoomAsRead,
  messagesLoading,
  receiveMessage,
  selectRoomMessages,
  selectRoomsMemberIds,
  sendRoomMessage,
  sendRoomMessageSuccess,
  startNewConversation,
  types,
} from '../_reducer';
import { UserProfile } from '../../_shared/models';
import { ChatMessage } from '../_models/chat-message.model';
import { isMobile, memoize } from '../../_helpers';
import { ChatType } from '../chat.inerface';

@Component({
  selector: 'tf-chat-room-window',
  templateUrl: './room-window.component.html',
  styleUrls: ['./room-window.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RoomWindowComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('listEl') listEl: ElementRef;
  @ViewChild('formEl') formEl: ElementRef;
  @ViewChild('alertEl') alertEl: ElementRef;

  @Input() room: PrivateChatRoom | TeamChatRoom;
  @Output() closed = new EventEmitter<void>();
  @Output() minimized = new EventEmitter<void>();

  messages: ChatMessage[] = [];
  show$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  excludedSuggestedUserIds$: Observable<number[]>;
  messagesLoading: boolean = false;
  canDisplayNewMessagesAlert: boolean = false;
  isMobile: boolean = false;
  private currentMessagesCount: number = 0;
  private destroy$ = new Subject();

  constructor(
    private store: Store<AppState>,
    private actions: Actions,
    private ngZone: NgZone,
    private cdr: ChangeDetectorRef
  ) {}

  get isPrivateChat(): boolean {
    return this.room.chatType === ChatType.PRIVATE;
  }

  get isTeamChat(): boolean {
    return this.room.chatType === ChatType.TEAM;
  }

  ngOnInit(): void {
    if (this.room.id) {
      this.listenOnRoomEvents();
    }
    if (!this.room.id) {
      this.excludedSuggestedUserIds$ = this.store.select(selectRoomsMemberIds);
    }
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.isMobile = isMobile();
    });
    this.onViewInit();
    this.listenOnListScroll();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onSubmitMessage(content: string): void {
    if (this.room?.id) {
      this.store.dispatch(sendRoomMessage({ roomId: this.room.id, chatType: this.room.chatType, content }));
    } else {
      this.store.dispatch(createNewConversation({ content }));
      this.actions.pipe(ofType(types.FetchPrivateRoomsSuccess), first()).subscribe(() => {
        setTimeout(() => {
          this.listenOnRoomEvents();
        });
      });
    }
  }

  onInputFocus(): void {
    if (this.room) {
      this.markRoomAsRead();
    }
  }

  onUserForNewConversationSelect(user: UserProfile): void {
    this.room = {
      ...this.room,
      memberProfile: user,
    };

    this.store.dispatch(startNewConversation({ receiver: user }));
  }

  onClickMember(): void {
    if (this.isMobile) {
      this.close();
    }
  }

  close(): void {
    this.closed.emit();
  }

  minimize(): void {
    this.isMobile ? this.close() : this.minimized.emit();
  }

  goToNewMessages(): void {
    this.scrollToBottom();
    this.canDisplayNewMessagesAlert = false;

    if (this.room) {
      this.markRoomAsRead();
    }
  }

  trackByFn(index: number, item: ChatMessage): number {
    return item.id;
  }

  @memoize()
  messageSender(message: ChatMessage): UserProfile {
    let room: PrivateChatRoom | TeamChatRoom;

    switch (this.room.chatType) {
      case ChatType.PRIVATE:
        room = this.room as PrivateChatRoom;
        return room.host.id === message.senderId ? room.host : room.member;
      case ChatType.TEAM:
        room = this.room as TeamChatRoom;
        return room.team.members.find((m) => m.profile.id === message.senderId)?.profile;
    }
  }

  private onViewInit(): void {
    if (this.room?.id && !this.room.wasInitialMessagesFetched) {
      this.fetchMoreMessages();
      this.actions.pipe(ofType(fetchMoreMessagesSuccess), first()).subscribe(() => {
        setTimeout(() => {
          this.currentMessagesCount = this.messages.length;
          this.scrollToBottom();
          this.show$.next(true);
        });
      });
    } else {
      setTimeout(() => {
        this.scrollToBottom();
        this.show$.next(true);
      });
    }
  }

  private listenOnRoomEvents(): void {
    this.listenOnMessagesChange();
    this.listenOnFetchMoreMessagesSuccessAction();
    this.listenOnNewMessageActions();

    this.store
      .select(messagesLoading, { roomId: this.room.id, chatType: this.room.chatType })
      .subscribe((loading) => {
        setTimeout(() => {
          this.messagesLoading = loading;
          this.cdr.detectChanges();
        });
      });
  }

  private listenOnMessagesChange(): void {
    this.store
      .select(selectRoomMessages, { roomId: this.room.id, chatType: this.room.chatType })
      .pipe(
        distinctUntilChanged((prev, curr) => prev.length === curr.length),
        takeUntil(this.destroy$)
      )
      .subscribe((messages) => {
        setTimeout(() => {
          this.messages = messages;
          this.cdr.detectChanges();
        });
      });
  }

  private listenOnFetchMoreMessagesSuccessAction(): void {
    this.actions
      .pipe(
        ofType(fetchMoreMessagesSuccess),
        filter((action) => action.roomId === this.room.id),
        skip(1),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        this.cdr.detectChanges();
        setTimeout(() => {
          this.onFetchMoreMessages();
        });
      });
  }

  private listenOnNewMessageActions(): void {
    this.actions
      .pipe(
        ofType(sendRoomMessageSuccess, receiveMessage),
        filter((action) => action.roomId === this.room.id),
        map((action) => action.type),
        takeUntil(this.destroy$)
      )
      .subscribe((actionType) => {
        if (actionType === types.ReceiveMessage) {
          if (this.isScrolledToBottom()) {
            setTimeout(() => {
              this.scrollToBottom();
            });
          } else {
            this.showNewMessagesAlert();
          }
        } else {
          setTimeout(() => {
            this.scrollToBottom();
          });
        }
      });
  }

  private listenOnListScroll(): void {
    this.ngZone.runOutsideAngular(() => {
      fromEvent(this.listEl.nativeElement, 'scroll')
        .pipe(debounceTime(50), takeUntil(this.destroy$))
        .subscribe(() => {
          if (this.isScrolledToTop()) {
            this.fetchMoreMessages();
          }
          if (this.isScrolledToBottom()) {
            this.canDisplayNewMessagesAlert = false;
            this.cdr.detectChanges();
          }
        });
    });
  }

  private fetchMoreMessages(): void {
    this.store.dispatch(fetchMoreMessages({ roomId: this.room.id, chatType: this.room.chatType }));
  }

  private markRoomAsRead(): void {
    this.store.dispatch(markRoomAsRead({ roomId: this.room.id, chatType: this.room.chatType }));
  }

  private onFetchMoreMessages(): void {
    const offset = this.messages.length - this.currentMessagesCount;
    const items = [...this.listEl.nativeElement.childNodes].filter(
      (el: HTMLElement) => el.localName === 'tf-chat-message'
    );
    const item = items[offset];

    this.currentMessagesCount = this.messages.length;

    if (item) {
      this.listEl.nativeElement.scrollTop = item.offsetTop - 45;
    }
  }

  private showNewMessagesAlert(): void {
    const isListOverflowed = this.listEl.nativeElement.scrollHeight > this.listEl.nativeElement.clientHeight;

    if (isListOverflowed && !this.isScrolledToBottom()) {
      const formBounds = this.formEl.nativeElement.getBoundingClientRect();
      this.canDisplayNewMessagesAlert = true;

      setTimeout(() => {
        this.alertEl.nativeElement.style.bottom = `${formBounds.height}px`;
      });
    }
  }

  private scrollToBottom(): void {
    this.listEl.nativeElement.scrollTop = this.listEl.nativeElement.scrollHeight;
  }

  private isScrolledToTop(): boolean {
    return this.listEl.nativeElement.scrollTop === 0;
  }

  private isScrolledToBottom(): boolean {
    const listScrollTop = Math.round(+this.listEl.nativeElement.scrollTop);
    const listOffset = this.listEl.nativeElement.scrollHeight - this.listEl.nativeElement.offsetHeight;
    const diff = listScrollTop - listOffset;

    return diff < 5 && diff >= -5;
  }
}
