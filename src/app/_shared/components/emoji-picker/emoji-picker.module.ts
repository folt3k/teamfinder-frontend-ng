import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { PopoverModule } from '@ng-poppy/lib';

import { EmojiPickerComponent } from './emoji-picker.component';

@NgModule({
  declarations: [EmojiPickerComponent],
  imports: [CommonModule, PopoverModule, ScrollingModule],
  exports: [EmojiPickerComponent],
})
export class EmojiPickerModule {}
