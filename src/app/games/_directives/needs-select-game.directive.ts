import { Directive, HostListener, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { first } from 'rxjs/operators';

import { AppState } from '../../app.interface';
import { selectCurrentGame } from '../_reducer';
import { DialogService } from '../../_shared/poppy/dialog';
import { GamePickerDialogComponent } from '../_components/game-picker-dialog/game-picker-dialog.component';

@Directive({
  selector: '[tfNeedsSelectGame]',
})
export class NeedsSelectGameDirective {
  @Input() tfNeedsSelectGame: string;

  @HostListener('click')
  onClick(): void {
    this.store
      .select(selectCurrentGame)
      .pipe(first())
      .subscribe((game) => {
        const link = this.tfNeedsSelectGame;
        if (game) {
          this.router.navigate([`${game.slashRouterPrefix}/${link}`]);
        } else {
          this.dialogService.open(GamePickerDialogComponent, { data: { link } });
        }
      });
  }

  constructor(private store: Store<AppState>, private router: Router, private dialogService: DialogService) {}
}
