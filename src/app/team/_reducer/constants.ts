export const featureKey = 'team';

export const types = {
  Create: '[Team] Create team',
  CreateSuccess: '[Team] Create team success',
  CreateFailed: '[Team] Create team failed',

  Update: '[Team] Update team',
  UpdateSuccess: '[Team] Update team success',
  UpdateFailed: '[Team] Update team failed',

  Remove: '[Team] Remove team',
  RemoveSuccess: '[Team] Remove team success',
  RemoveFailed: '[Team] Remove team failed',

  FetchDetails: '[Team] Fetch team details',
  FetchDetailsSuccess: '[Team] Fetch team details success',
  FetchDetailsFailed: '[Team] Fetch team details failed',

  RemoveMember: '[Team] Remove team member',
  RemoveMemberSuccess: '[Team] Remove team member success',
  RemoveMemberFailed: '[Team] Remove team member failed',

  LeaveTeam: '[Team] Leave team',
};
