import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'tf-headline',
  templateUrl: './headline.component.html',
  styleUrls: ['./headline.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeadlineComponent {
  @Input() title: string;
  @Input() subtitle: string;
  @Input() align: 'left' | 'right' | 'center' = 'left';

  constructor() {}
}
