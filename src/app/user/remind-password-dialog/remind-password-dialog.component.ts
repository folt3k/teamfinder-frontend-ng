import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { UserService } from '../user.service';

@Component({
  selector: 'tf-remind-password-dialog',
  templateUrl: './remind-password-dialog.component.html',
  styleUrls: ['./remind-password-dialog.component.scss'],
})
export class RemindPasswordDialogComponent implements OnInit {
  form: FormGroup;
  success: boolean = false;
  submitting: boolean = false;

  constructor(private fb: FormBuilder, private usersService: UserService) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
    });
  }

  onSubmit(): void {
    this.submitting = true;
    this.usersService.remindPassword({ email: this.form.controls.email.value }).subscribe(() => {
      this.success = true;
      this.submitting = false;
    });
  }
}
