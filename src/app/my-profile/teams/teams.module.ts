import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MyProfileTeamsComponent } from './teams.component';
import { SharedModule } from '../../_shared/shared.module';
import { TeamModule } from '../../team/team.module';

@NgModule({
  declarations: [MyProfileTeamsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: MyProfileTeamsComponent,
      },
    ]),
    SharedModule,
    TeamModule,
  ],
})
export class MyProfileTeamsModule {}
