import { createReducer, on, Action } from '@ngrx/store';

import * as actions from './actions';
import { BaseReducerState, clearError, getErrorState } from '../../_reducers';
import { Pagination } from '../../_shared/interfaces';
import { logout, meSuccess } from '../../auth/_reducer/actions';
import { updateUserNetworkStatus } from '../../user/_reducer/actions';
import { isMobile } from '../../_helpers';
import { ChatType } from '../chat.inerface';
import { createTeamSuccess } from '../../team/_reducer';

export interface State {
  rooms: {
    results: any[];
    pagination: Pagination;
  } & BaseReducerState;
  roomWindowIds: Array<{ value: number | null; type: ChatType }>;
  unreadMessagesCount: number;
}

export const initialState: State = {
  rooms: {
    results: [],
    pagination: null,
    loading: false,
    error: null,
  },
  roomWindowIds: [],
  unreadMessagesCount: 0,
};

const getNewConversation = (state: State) => state.rooms.results.find((room) => !room.id);

const _reducer = createReducer(
  initialState,

  on(actions.fetchPrivateRooms, actions.fetchTeamRooms, (state) => ({
    ...state,
    rooms: {
      ...state.rooms,
      error: clearError(),
      loading: true,
    },
  })),
  on(actions.fetchPrivateRoomsSuccess, (state, { results }) => ({
    ...state,
    rooms: {
      results: getNewConversation(state)
        ? [
            { ...results[0], active: true, maximized: true, wasNewConversationCreated: true },
            ...state.rooms.results.slice(1),
            ...results.slice(1).filter((room) => !state.rooms.results.find((r) => r.id === room.id)),
          ].sort((a, b) => (a.lastMessageDate > b.lastMessageDate ? -1 : 1))
        : [
            ...state.rooms.results,
            ...results.filter((room) => !state.rooms.results.find((r) => r.id === room.id)),
          ].sort((a, b) => (a.lastMessageDate > b.lastMessageDate ? -1 : 1)),
      pagination: null,
      loading: false,
      error: null,
    },
    roomWindowIds: state.roomWindowIds.map((id) =>
      id.value === null ? { value: results[0].id, type: results[0].chatType } : id
    ),
  })),
  on(actions.fetchTeamRoomsSuccess, (state, { results, unreadMessages }) => ({
    ...state,
    unreadMessagesCount: state.unreadMessagesCount + unreadMessages,
    rooms: {
      results: [...state.rooms.results, ...results].sort((a, b) =>
        a.lastMessageDate > b.lastMessageDate ? -1 : 1
      ),
      pagination: null,
      loading: false,
      error: null,
    },
    roomWindowIds: state.roomWindowIds.map((id) =>
      id.value === null ? { value: results[0].id, type: results[0].chatType } : id
    ),
  })),
  on(actions.fetchPrivateRoomsFailed, (state, payload) => ({
    ...state,
    rooms: {
      ...state.rooms,
      error: getErrorState(payload),
      loading: false,
    },
  })),
  on(actions.markRoomAsRead, (state, { roomId, chatType }) => {
    let currentRoomUnreadCount = 0;

    return {
      ...state,
      rooms: {
        ...state.rooms,
        results: state.rooms.results.map((r) => {
          if (r.id === roomId && r.chatType === chatType) {
            currentRoomUnreadCount =
              r.chatType === ChatType.PRIVATE
                ? r[`${r.currentProfileSenderType}UnreadCount`]
                : r.unreadMessages;
            return {
              ...r,
              ...(r.chatType === ChatType.PRIVATE
                ? { [`${r.currentProfileSenderType}UnreadCount`]: 0 }
                : { unreadMessages: 0 }),
            };
          }
          return r;
        }),
      },
      unreadMessagesCount: state.unreadMessagesCount - currentRoomUnreadCount,
    };
  }),
  on(actions.receiveNewConversation, (state) => ({
    ...state,
    unreadMessagesCount: state.unreadMessagesCount + 1,
  })),
  on(actions.startNewConversation, (state, { receiver }) => ({
    ...state,
    rooms: {
      ...state.rooms,
      results: [
        {
          id: null,
          active: true,
          maximized: true,
          messages: { results: [] },
          memberProfile: receiver,
          chatType: ChatType.PRIVATE,
          getCurrentProfileUnreadCount: () => 0,
        },
        ...state.rooms.results.filter((room) => room.id),
      ],
    },
    roomWindowIds: [
      { value: null, type: ChatType.PRIVATE },
      ...(isMobile() ? [] : [...state.roomWindowIds.filter((id) => !!id.value)]),
    ],
  })),
  // ROOM WINDOWS
  on(actions.addRoomWindow, (state, { roomId, chatType }) => {
    return {
      ...state,
      rooms: {
        ...state.rooms,
        results: state.rooms.results.map((r) =>
          r.id === roomId && r.chatType === chatType ? { ...r, active: true, maximized: true } : r
        ),
      },
      roomWindowIds: !state.roomWindowIds.find((id) => id.value === roomId && id.type === chatType)
        ? [
            { value: roomId, type: chatType },
            ...(isMobile() ? [] : [...state.roomWindowIds.filter((id) => id.value)]),
          ]
        : state.roomWindowIds,
    };
  }),

  on(actions.closeRoomWindow, (state, { roomId, chatType }) => {
    return {
      ...state,
      rooms: {
        ...state.rooms,
        results: state.rooms.results.map((r) => {
          if (r.id === roomId && r.chatType === chatType) {
            return { ...r, active: false, maximized: false };
          }
          return r;
        }),
      },
      roomWindowIds: state.roomWindowIds.filter((id) => !(id.value === roomId && id.type === chatType)),
    };
  }),

  on(actions.maximizeRoomWindow, (state, { roomId, chatType }) => {
    return {
      ...state,
      rooms: {
        ...state.rooms,
        results: state.rooms.results.map((r) => {
          if (r.id === roomId && r.chatType === chatType) {
            return { ...r, maximized: true };
          }
          return r;
        }),
      },
    };
  }),

  on(actions.minimizeRoomWindow, (state, { roomId, chatType }) => {
    return {
      ...state,
      rooms: {
        ...state.rooms,
        results: state.rooms.results
          .filter((r) => (roomId === null ? r.id !== roomId && r.chatType !== chatType : true))
          .map((r) => {
            if (r.id === roomId && r.chatType === chatType) {
              return { ...r, maximized: false };
            }
            return r;
          }),
      },
      roomWindowIds: roomId === null ? state.roomWindowIds.filter((id) => !!id.value) : state.roomWindowIds,
    };
  }),

  on(actions.sendRoomMessageSuccess, (state, payload) => ({
    ...state,
    rooms: {
      ...state.rooms,
      results: state.rooms.results.map((r) => {
        if (r.id === payload.room && r.chatType === payload.chatType) {
          return {
            ...r,
            messages: {
              ...r.messages,
              results: [...r.messages.results, payload],
            },
          };
        }

        return r;
      }),
    },
  })),

  on(actions.receiveMessage, (state, payload) => ({
    ...state,
    unreadMessagesCount: state.unreadMessagesCount + 1,
    rooms: {
      ...state.rooms,
      results: state.rooms.results.map((r) => {
        if (r.id === payload.roomId && r.chatType === payload.chatType) {
          return {
            ...r,
            ...(r.chatType === ChatType.PRIVATE
              ? { [`${payload.senderType}UnreadCount`]: r[`${payload.senderType}UnreadCount`] + 1 }
              : { unreadMessages: r.unreadMessages + 1 }),
            messages: {
              ...r.messages,
              results: [...r.messages.results, payload.message],
            },
          };
        }

        return r;
      }),
    },
  })),

  on(actions.fetchMoreMessages, (state, { roomId, chatType }) => ({
    ...state,
    rooms: {
      ...state.rooms,
      results: state.rooms.results.map((r) => {
        if (r.id === roomId && r.chatType === chatType) {
          return {
            ...r,
            wasInitialMessagesFetched: true,
            messages: {
              ...r.messages,
              loading: true,
              error: false,
            },
          };
        }

        return r;
      }),
    },
  })),

  on(actions.fetchMoreMessagesSuccess, (state, payload) => ({
    ...state,
    rooms: {
      ...state.rooms,
      results: state.rooms.results.map((r) => {
        if (r.id === payload.roomId && r.chatType === payload.chatType) {
          const roomMessages = r.messages.results;
          const hasInitialOneMessage =
            roomMessages.length === 1 &&
            !!payload.results.length &&
            roomMessages[0].id === payload.results[payload.results.length - 1].id;

          return {
            ...r,
            wasInitialMessagesFetched: true,
            messages: {
              pagination: payload.pagination,
              results: [...payload.results, ...(!hasInitialOneMessage ? [...r.messages.results] : [])],
              loading: false,
              error: false,
            },
          };
        }

        return r;
      }),
    },
  })),
  on(actions.receiveNewTeamRoom, (state, payload) => ({
    ...state,
    rooms: {
      ...state.rooms,
      results: [payload.data, ...state.rooms.results],
    },
  })),
  on(actions.receiveNewTeamMember, (state, payload) => ({
    ...state,
    rooms: {
      ...state.rooms,
      results: state.rooms.results.map((r) => {
        if (r.team && r.team.id === payload.teamId) {
          return {
            ...r,
            team: {
              ...r.team,
              members: [payload.member, ...r.team.members],
            },
          };
        }

        return r;
      }),
    },
  })),
  // OTHER ACTIONS
  on(createTeamSuccess, (state, payload) => ({
    ...state,
    rooms: {
      ...state.rooms,
      results: [payload.chatRoom, ...state.rooms.results],
    },
  })),
  on(meSuccess, (state, payload) => ({
    ...state,
    unreadMessagesCount: payload.metadata.counters.unreadPrivateMessages,
  })),
  on(logout, (state) => ({
    ...state,
    rooms: {
      ...state.rooms,
      results: [],
    },
    roomWindowIds: [],
  })),
  on(updateUserNetworkStatus, (state, { profile, isOnline }) => ({
    ...state,
    rooms: {
      ...state.rooms,
      results: state.rooms.results
        .filter((r) => r.id)
        .map((r) => {
          if (r.chatType === ChatType.PRIVATE) {
            return r.memberProfile.id === profile.id
              ? { ...r, memberProfile: { ...r.memberProfile, isOnline } }
              : r;
          } else {
            return {
              ...r,
              team: {
                ...r.team,
                members: r.team.members.map((m) =>
                  m.profile.id === profile.id ? { ...m, profile: { ...m.profile, isOnline } } : m
                ),
              },
            };
          }
        }),
    },
  }))
);

export function reducer(state: State, action: Action) {
  return _reducer(state, action);
}
