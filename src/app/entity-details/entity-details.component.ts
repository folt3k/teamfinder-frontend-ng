import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { filter, first, mapTo, skip, switchMap, takeUntil } from 'rxjs/operators';

import { EntityDetailsService } from './entity-details.service';
import { EntityDetailsAction, EntityType } from './entity-details.interface';
import { Store } from '@ngrx/store';
import { AppState } from '../app.interface';
import { BaseEntity } from '../_shared/interfaces';
import { error, loading, selectDetails } from './_reducer';

@Component({
  selector: 'tf-entity-details',
  templateUrl: './entity-details.component.html',
  styleUrls: ['./entity-details.component.scss'],
})
export class EntityDetailsComponent implements OnInit, OnDestroy {
  @Input() type: EntityType;

  data: BaseEntity;
  actions$: Observable<EntityDetailsAction[]>;
  loading$: Observable<boolean>;
  notFound$: Observable<boolean>;
  private destroy$ = new Subject();

  constructor(
    private service: EntityDetailsService,
    private store: Store<AppState>,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.init();
    this.store
      .select(selectDetails)
      .pipe(
        takeUntil(this.destroy$),
        filter((details) => !!details)
      )
      .subscribe((data) => {
        this.data = data;
        this.actions$ = this.service.prepareActions();
      });

    if (this.route.snapshot.fragment === 'edycja') {
      this.showEditModal();
    }

    this.loading$ = this.store.select(loading);
    this.notFound$ = this.store.select(error).pipe(
      filter((err) => err.statusCode === 404),
      mapTo(true)
    );
    this.route.params.pipe(takeUntil(this.destroy$), skip(1)).subscribe(() => {
      this.init();
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  private init(): void {
    this.service.init(
      this.type,
      this.route.snapshot.paramMap.get('identifier'),
      this.route.snapshot.paramMap.get('gamePrefix')
    );
    this.service.fetchDetails();
  }

  private showEditModal(): void {
    this.store
      .select(selectDetails)
      .pipe(
        takeUntil(this.destroy$),
        filter((details) => !!details),
        first(),
        switchMap(() => this.service.prepareActions().pipe(first()))
      )
      .subscribe((actions) => {
        const editAction = actions.find((a) => a.label === 'Edytuj');

        if (editAction) {
          editAction.callback();
        }
      });
  }
}
