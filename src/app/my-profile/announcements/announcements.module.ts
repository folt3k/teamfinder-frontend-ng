import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MyProfileAnnouncementsComponent } from './announcements.component';
import { SharedModule } from '../../_shared/shared.module';
import { AnnouncementsModule } from '../../announcements/announcements.module';

@NgModule({
  declarations: [MyProfileAnnouncementsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: MyProfileAnnouncementsComponent,
      },
    ]),
    SharedModule,
    AnnouncementsModule,
  ],
})
export class MyProfileAnnouncementsModule {}
