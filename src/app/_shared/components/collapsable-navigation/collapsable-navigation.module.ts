import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CollapsableNavigationComponent } from './collapsable-navigation.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [CollapsableNavigationComponent],
  imports: [CommonModule, RouterModule],
  exports: [CollapsableNavigationComponent],
})
export class CollapsableNavigationModule {}
