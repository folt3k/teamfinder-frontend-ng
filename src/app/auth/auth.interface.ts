import { UserProfile, User } from '../_shared/models';

export interface LoginBody {
  email: string;
  password: string;
}

export interface LoginRO {
  token: string;
}

export interface UserMeRO {
  currentProfile: UserProfile;
  metadata: {
    counters: {
      undisplayedNotifications: number;
      unreadGlobalChatMessages: number;
      unreadPrivateMessages: number;
    };
  };
  otherProfiles: UserProfile[];
  user: User;
}

export interface AccountActivationBody {
  token: string;
}

export interface AccountActivationRO {
  token: string;
  redirectUrl?: string;
}
