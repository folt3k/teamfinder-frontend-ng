import { ChangeDetectionStrategy, Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

import { PrivateChatRoom, TeamChatRoom } from '../_models/chat-room.model';
import { ChatType } from '../chat.inerface';

@Component({
  selector: 'tf-chat-room-window-launcher',
  templateUrl: './room-window-launcher.component.html',
  styleUrls: ['./room-window-launcher.component.scss'],
})
export class RoomWindowLauncherComponent implements OnInit {
  @Input() room: PrivateChatRoom | TeamChatRoom;

  @Output() closed = new EventEmitter();
  @Output() maximized = new EventEmitter();

  constructor() {}

  get panelClasses(): { [key: string]: boolean } {
    const unreadCount =
      this.room.chatType === ChatType.TEAM
        ? this.room.unreadMessages
        : this.room.getCurrentProfileUnreadCount();
    return {
      'chat-room-window-launcher--active': unreadCount > 0,
    };
  }

  get isPrivateChat(): boolean {
    return this.room.chatType === ChatType.PRIVATE;
  }

  get isTeamChat(): boolean {
    return this.room.chatType === ChatType.TEAM;
  }

  get unreadMessagesCount(): number {
    return this.isPrivateChat ? this.room.getCurrentProfileUnreadCount() : this.room.unreadMessages;
  }

  ngOnInit(): void {}

  maximize(): void {
    this.maximized.emit();
  }

  close(event: Event): void {
    event.stopPropagation();
    this.closed.emit();
  }
}
