import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'tf-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoaderComponent {
  @Input() label: string;
  @Input() size: 'xs' | 'sm' | 'lg' = 'sm';

  constructor() {}

  get panelClasses(): { [key: string]: boolean } {
    return {
      [`loader--${this.size}`]: true,
    };
  }
}
