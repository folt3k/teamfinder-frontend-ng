import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';
import { GAMES } from '../_shared/common';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  ...GAMES.map((game) => ({
    path: ':gamePrefix',
    component: HomeComponent,
  })),
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
