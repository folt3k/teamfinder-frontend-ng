export interface GameMode {
  id: number;
  name: string;
  value: string;
  label: string;
  iconUrl: string;
}
