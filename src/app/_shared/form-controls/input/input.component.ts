import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectionStrategy,
  forwardRef,
  Input,
  ChangeDetectorRef,
  Optional,
  Host,
  SkipSelf,
  Output,
  EventEmitter,
} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, ControlContainer, NG_VALIDATORS } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { CustomFormControl } from '../custom-form-control';

@Component({
  selector: 'tf-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => InputComponent),
      multi: true,
    },
  ],
})
export class InputComponent extends CustomFormControl implements OnInit, ControlValueAccessor {
  @Input() type: 'text' | 'email' | 'password' = 'text';
  @Input() mode: 'input' | 'textarea' = 'input';
  @Input() value: string = '';
  @Output() changed = new EventEmitter<string>();
  @Output() keypressed = new EventEmitter<KeyboardEvent>();

  _type: 'text' | 'email' | 'password' = 'text';
  pending$: Observable<boolean>;

  constructor(
    cdr: ChangeDetectorRef,
    @Optional()
    @Host()
    @SkipSelf()
    controlContainer: ControlContainer
  ) {
    super(cdr, controlContainer);
  }

  ngOnInit(): void {
    this._type = this.type;
    super.ngOnInit();
    this.pending$ = this.control?.statusChanges.pipe(map((status) => status === 'PENDING'));
  }

  writeValue(value: string): void {
    this.value = value || '';
    this.cdr.detectChanges();
  }

  registerOnChange(fn: () => {}): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => {}): void {
    this.propagateTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
    this.cdr.detectChanges();
  }

  onChange(event: Event): void {
    const target: HTMLInputElement = event.target as HTMLInputElement;

    this.value = target.value;
    this.cdr.detectChanges();
    this.propagateChange(target.value);
    this.changed.emit(this.value);
  }

  onBlur(): void {}

  onFocus(): void {
    this.propagateTouched();
    this.cdr.detectChanges();
  }

  onKeyPress(event: KeyboardEvent): void {
    this.keypressed.emit(event);
  }

  togglePasswordVisibility(): void {
    this._type = this._type === 'password' ? 'text' : 'password';
  }

  private propagateChange(value: string): void {}

  private propagateTouched(): void {}
}
