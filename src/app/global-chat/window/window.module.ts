import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WindowComponent } from './window.component';
import { HeaderComponent } from './header/header.component';
import { SharedModule } from 'src/app/_shared/shared.module';
import { MessageListComponent } from './message-list/message-list.component';
import { MessageComponent } from './message/message.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserListItemComponent } from './user-list-item/user-list-item.component';
import { CloseGlobalChatOnClickDirective } from '../_directives/close-global-chat-on-click.directive';
import { ChannelListComponent } from './channel-list/channel-list.component';

@NgModule({
  declarations: [
    WindowComponent,
    HeaderComponent,
    MessageListComponent,
    MessageComponent,
    UserListComponent,
    UserListItemComponent,
    ChannelListComponent,
    CloseGlobalChatOnClickDirective,
  ],
  imports: [CommonModule, SharedModule],
  exports: [WindowComponent],
})
export class WindowModule {}
