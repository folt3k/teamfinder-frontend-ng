import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopoverModule } from '@ng-poppy/lib';

import { ActionMenuComponent } from './action-menu.component';
import { IconButtonModule } from '../icon-button/icon-button.module';

@NgModule({
  declarations: [ActionMenuComponent],
  imports: [CommonModule, IconButtonModule, PopoverModule],
  exports: [ActionMenuComponent],
})
export class ActionMenuModule {}
