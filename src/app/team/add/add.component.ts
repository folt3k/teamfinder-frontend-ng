import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState } from '../../app.interface';
import { createTeam } from '../_reducer';
import { CreateTeamDto } from '../team.interface';
import { SeoService } from '../../_core/services/seo.service';

@Component({
  selector: 'tf-add-team',
  templateUrl: './add.component.html',
})
export class AddTeamComponent implements OnInit {
  constructor(private store: Store<AppState>, private seo: SeoService) {}

  ngOnInit(): void {
    this.seo.setSeo('add-team');
  }

  onSubmit(values: CreateTeamDto): void {
    this.store.dispatch(createTeam({ body: values }));
  }
}
