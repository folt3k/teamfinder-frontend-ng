import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MyProfilePreviewComponent } from './preview.component';
import { SharedModule } from '../../_shared/shared.module';
import { GamesModule } from '../../games/games.module';

@NgModule({
  declarations: [MyProfilePreviewComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: MyProfilePreviewComponent,
      },
    ]),
    SharedModule,
    GamesModule,
  ],
  exports: [RouterModule],
})
export class MyProfilePreviewModule {}
