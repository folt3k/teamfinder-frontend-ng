import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../_shared/shared.module';
import { AnnouncementDetailsContentComponent } from './announcement.component';
import { ProfileCardModule } from '../../../profile/card/card.module';

@NgModule({
  declarations: [AnnouncementDetailsContentComponent],
  imports: [CommonModule, SharedModule, ProfileCardModule],
  exports: [AnnouncementDetailsContentComponent],
})
export class AnnouncementDetailsContentModule {}
