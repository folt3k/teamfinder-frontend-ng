import { Actions, ofType } from '@ngrx/effects';
import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  OnDestroy,
  NgZone,
  ChangeDetectorRef,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { fromEvent, Subject } from 'rxjs';
import { takeUntil, first, filter, debounceTime } from 'rxjs/operators';

import { GlobalChatMessage } from '../../_models';
import { AppState } from 'src/app/app.interface';
import {
  fetchMoreMessages,
  fetchMoreMessagesSuccess,
  markAsRead,
  GlobalChatStateChannel,
} from '../../_reducer';
import { currentProfile } from 'src/app/auth/_reducer';
import { UserProfile } from '../../../_shared/models';

@Component({
  selector: 'tf-global-chat-message-list',
  templateUrl: './message-list.component.html',
  styleUrls: ['./message-list.component.scss'],
})
export class MessageListComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges {
  @ViewChild('listEl') listEl: ElementRef;

  @Input() activeChannel: GlobalChatStateChannel;

  messages: GlobalChatMessage[] = [];
  currentUser: UserProfile;

  private wasFirstOpen: boolean = false;
  private currentMessagesCount: number = 0;
  private destroy$ = new Subject();

  constructor(
    private store: Store<AppState>,
    private actions: Actions,
    private ngZone: NgZone,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.store
      .select(currentProfile)
      .pipe(takeUntil(this.destroy$))
      .subscribe((user) => (this.currentUser = user));
  }

  ngAfterViewInit(): void {
    this.listenOnFetchMoreMessagesAction();
    this.listenOnOpenWindowFirstTime();
    this.listenOnListScroll();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.activeChannel) {
      if (changes.activeChannel.previousValue?.id !== changes.activeChannel.currentValue?.id) {
        this.onActiveChannelChange();
      }
      if (!changes.activeChannel.previousValue) {
        this.currentMessagesCount = this.activeChannel.messages.results.length;
      }
      if (
        changes.activeChannel.previousValue?.messages.results.length !==
        changes.activeChannel.currentValue?.messages.results.length
      ) {
        this.onMessagesChange();
      }
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  get canDisplayNewMessageAlert(): boolean {
    return (
      !this.isScrolledToBottom() && this.activeChannel.unseenMessagesSinceLastDisplay > 0 && this.wasFirstOpen
    );
  }

  trackByFn(index: number, item: GlobalChatMessage): number {
    return item.id;
  }

  onNewMessagesClick(): void {
    this.scrollToBottom();
  }

  private listenOnFetchMoreMessagesAction(): void {
    this.actions.pipe(ofType(fetchMoreMessagesSuccess), takeUntil(this.destroy$)).subscribe((action) => {
      setTimeout(() => {
        this.onLoadMoreMessages();
      });
    });
  }

  private listenOnOpenWindowFirstTime(): void {
    this.store
      .select((state) => state.globalChat.isChatOpen)
      .pipe(
        filter((isOpen) => !!isOpen),
        first()
      )
      .subscribe(() => {
        setTimeout(() => {
          this.scrollToBottom();
          this.wasFirstOpen = true;
        });
      });
  }

  private onActiveChannelChange(): void {
    if (typeof this.activeChannel.scrollPosition === 'number' && this.activeChannel.wasInitialDataFetched) {
      setTimeout(() => {
        this.scrollToStoredPosition();
      });
    } else {
      this.scrollToBottom();
    }
    if (this.wasFirstOpen) {
      this.currentMessagesCount = this.activeChannel.messages.results.length;
    }
  }

  private onMessagesChange(): void {
    const data = this.activeChannel.messages.results;
    const newestMessage = data[data.length - 1];
    const scrollToBottomIfNewMessageSenderIsCurrentUser =
      newestMessage && newestMessage.sender.id === this.currentUser?.id;
    const scrollToBottomIfListIsScrolledToBottom = this.isScrolledToBottom();

    this.messages = [...data];
    this.cdr.detectChanges();

    if (scrollToBottomIfNewMessageSenderIsCurrentUser || scrollToBottomIfListIsScrolledToBottom) {
      this.scrollToBottom();
    }
  }

  private onLoadMoreMessages(): void {
    const offset = this.messages.length - this.currentMessagesCount;
    const items = [...this.listEl.nativeElement.childNodes].filter(
      (el: HTMLElement) => el.localName === 'tf-global-chat-message'
    );
    const item = items[offset] || items[items.length - 1];

    this.currentMessagesCount = this.messages.length;
    if (item) {
      this.listEl.nativeElement.scrollTop = item.offsetTop - 119;
    }
  }

  private listenOnListScroll(): void {
    fromEvent(this.listEl.nativeElement, 'scroll')
      .pipe(debounceTime(50), takeUntil(this.destroy$))
      .subscribe(() => {
        if (this.isScrolledToTop()) {
          this.fetchMoreMessages();
        }
        if (this.isScrolledToBottom()) {
          this.store.dispatch(markAsRead({ channelId: this.activeChannel.id }));
        }
      });
  }

  private fetchMoreMessages(): void {
    if (this.activeChannel.messages.results.length < this.activeChannel.messages.pagination?.totalResults) {
      this.store.dispatch(fetchMoreMessages({ channelId: this.activeChannel.id }));
    }
  }

  private scrollToBottom(): void {
    if (this.listEl) {
      this.listEl.nativeElement.scrollTop = this.listEl.nativeElement.scrollHeight;
    }
  }

  private scrollToStoredPosition(): void {
    if (this.listEl) {
      this.listEl.nativeElement.scrollTop = this.activeChannel.scrollPosition;
      this.cdr.detectChanges();
    }
  }

  private isScrolledToBottom(): boolean {
    if (!this.listEl) {
      return false;
    }

    const listScrollTop = Math.round(+this.listEl.nativeElement.scrollTop);
    const listOffset = this.listEl.nativeElement.scrollHeight - this.listEl.nativeElement.offsetHeight;
    const diff = listScrollTop - listOffset;
    return diff < 5 && diff >= -5;
  }

  private isScrolledToTop(): boolean {
    return this.listEl.nativeElement.scrollTop === 0;
  }
}
