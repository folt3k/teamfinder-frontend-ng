import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { triggerOnEnterKey } from 'src/app/_helpers';
import { noEmptyTextValidator } from '../../form-validation';

@Component({
  selector: 'tf-send-message',
  templateUrl: './send-message.component.html',
  styleUrls: ['./send-message.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SendMessageComponent implements OnInit, OnChanges {
  @ViewChild('textAreaEl') textAreaEl: ElementRef;

  @Input() setFocus: boolean = false;
  @Output() submitted = new EventEmitter<string>();
  @Output() focused = new EventEmitter<void>();

  form: FormGroup;
  private message: AbstractControl | null;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      message: ['', [Validators.required, noEmptyTextValidator()]],
    });
    this.message = this.form.get('message');
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.setFocus.currentValue) {
      this.setInputFocus();
    }
  }

  onSubmit(): void {
    if (this.form.valid) {
      this.submitted.next(this.message?.value);
      this.form.setValue({ message: '' });
    }
  }

  onKeyPress(event: KeyboardEvent): void {
    triggerOnEnterKey(event, () => {
      this.onSubmit();
    });
  }

  onFocus(): void {
    this.focused.emit();
  }

  onEmojiSelect(emoji: string): void {
    this.form.setValue({ message: `${this.message?.value}${emoji}` });
  }

  private setInputFocus(): void {
    this.textAreaEl.nativeElement.focus();
  }
}
