import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../_shared/shared.module';
import { TeamDetailsContentComponent } from './team.component';
import { ProfileCardModule } from '../../../profile/card/card.module';

@NgModule({
  declarations: [TeamDetailsContentComponent],
  imports: [CommonModule, SharedModule, ProfileCardModule],
  exports: [TeamDetailsContentComponent],
})
export class TeamDetailsContentModule {}
