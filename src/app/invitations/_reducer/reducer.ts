import { Action, createReducer, on } from '@ngrx/store';

import * as actions from './actions';
import { BaseReducerState, createBaseReducerState } from '../../_reducers';
import { Pagination } from '../../_shared/interfaces';
import { TeamInvitationStatus } from '../invitations.model';

export interface State extends BaseReducerState {
  invitations: { results: any[]; pagination: Pagination } & BaseReducerState;
  requests: { results: any[]; pagination: Pagination } & BaseReducerState;
}

export const initialState: State = createBaseReducerState({
  invitations: createBaseReducerState({ results: [], pagination: null }),
  requests: createBaseReducerState({ results: [], pagination: null }),
});

export const _reducer = createReducer(
  initialState,
  on(actions.fetchRequestsSuccess, (state, payload) => ({
    ...state,
    requests: {
      ...payload.data,
      loading: false,
      error: null,
    },
  })),
  on(actions.fetchInvitationsSuccess, (state, payload) => ({
    ...state,
    invitations: {
      ...payload.data,
      loading: false,
      error: null,
    },
  })),
  on(actions.manageRequestSuccess, (state, payload) => ({
    ...state,
    requests: {
      ...state.requests,
      results:
        payload.status === TeamInvitationStatus.CANCELED
          ? state.requests.results.filter((item) => item.id !== payload.id)
          : state.requests.results.map((item) =>
              item.id === payload.id ? { ...item, status: payload.status } : item
            ),
    },
  })),
  on(actions.manageInvitationSuccess, (state, payload) => ({
    ...state,
    invitations: {
      ...state.invitations,
      results:
        payload.status === TeamInvitationStatus.CANCELED
          ? state.invitations.results.filter((item) => item.id !== payload.id)
          : state.invitations.results.map((item) =>
              item.id === payload.id ? { ...item, status: payload.status } : item
            ),
    },
  }))
);

export function reducer(state: State, action: Action) {
  return _reducer(state, action);
}
