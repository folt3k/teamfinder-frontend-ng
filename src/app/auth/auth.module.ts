import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthGuard, NotLoggedInGuard } from './_guards';

@NgModule({
  declarations: [],
  imports: [CommonModule],
  providers: [AuthGuard, NotLoggedInGuard],
})
export class AuthModule {}
