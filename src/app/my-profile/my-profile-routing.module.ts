import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MY_PROFILE_STATIC_URLS } from '../_shared/common';
import { MyProfileComponent } from './my-profile.component';

const routes: Routes = [
  {
    path: '',
    component: MyProfileComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: MY_PROFILE_STATIC_URLS['preview-profile'].path,
      },
      {
        path: MY_PROFILE_STATIC_URLS['preview-profile'].path,
        loadChildren: () => import('./preview/preview.module').then((m) => m.MyProfilePreviewModule),
      },
      {
        path: MY_PROFILE_STATIC_URLS['edit-profile'].path,
        loadChildren: () => import('./edit/edit.module').then((m) => m.MyProfileEditModule),
      },
      {
        path: MY_PROFILE_STATIC_URLS['settings'].path,
        loadChildren: () => import('./settings/settings.module').then((m) => m.MyProfileSettingsModule),
      },
      {
        path: MY_PROFILE_STATIC_URLS['announcements'].path,
        loadChildren: () =>
          import('./announcements/announcements.module').then((m) => m.MyProfileAnnouncementsModule),
      },
      {
        path: MY_PROFILE_STATIC_URLS['teams'].path,
        loadChildren: () => import('./teams/teams.module').then((m) => m.MyProfileTeamsModule),
      },
      {
        path: MY_PROFILE_STATIC_URLS['my-invitations'].path,
        loadChildren: () =>
          import('./invitations/my-invitations/my-invitations.module').then(
            (m) => m.MyProfileMyInvitationsModule
          ),
      },
      {
        path: MY_PROFILE_STATIC_URLS['team-invitations'].path,
        loadChildren: () =>
          import('./invitations/team-invitations/team-invitations.module').then(
            (m) => m.MyProfileTeamInvitationsModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyProfileRoutingModule {}
