import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import { AppState } from '../../app.interface';
import { AlertService } from '../../_shared/components/alert/alert.service';
import * as actions from './actions';
import { TeamInvitationsService } from '../invitations.service';
import { AlertTheme } from '../../_shared/components/alert/alert.interface';

@Injectable()
export class TeamInvitationsEffects {
  sendInvitation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.sendInvitation),
      switchMap((action) => {
        return this.invitationsService.sendInvitation(action.body).pipe(
          map((data) => actions.sendInvitationSuccess(data)),
          catchError((err) => of(actions.sendInvitationFailed(err)))
        );
      })
    )
  );

  sendInvitationSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.sendInvitationSuccess),
        tap((data) => {
          this.alertService.show('Zaproszenie zostało pomyślnie wysłane');
        })
      ),
    { dispatch: false }
  );

  sendInvitationFailed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.sendInvitationFailed),
        tap((action) => {
          this.alertService.show(action.message, { theme: AlertTheme.FAILED });
        })
      ),
    { dispatch: false }
  );

  fetchInvitations$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.fetchInvitations),
      switchMap((action) => {
        return this.invitationsService.fetchTeamInvitations(action.params).pipe(
          map((data) => actions.fetchInvitationsSuccess({ data })),
          catchError((err) => of(actions.fetchRequestsFailed(err)))
        );
      })
    )
  );

  sendRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.sendRequest),
      switchMap((action) => {
        return this.invitationsService.sendRequest(action.body).pipe(
          map((data) => actions.sendRequestSuccess(data)),
          catchError((err) => of(actions.sendRequestFailed(err)))
        );
      })
    )
  );

  fetchRequests$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.fetchRequests),
      switchMap((action) => {
        return this.invitationsService.fetchTeamRequests(action.params).pipe(
          map((data) => actions.fetchRequestsSuccess({ data })),
          catchError((err) => of(actions.fetchRequestsFailed(err)))
        );
      })
    )
  );

  manageRequest$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.manageRequest),
      switchMap(({ id, action }) => {
        return this.invitationsService.manageRequest(id, action).pipe(
          map((data) => actions.manageRequestSuccess(data)),
          catchError((err) => of(actions.manageRequestFailed(err)))
        );
      })
    )
  );

  manageInvitation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.manageInvitation),
      switchMap(({ id, action }) => {
        return this.invitationsService.manageInvitation(id, action).pipe(
          map((data) => actions.manageInvitationSuccess(data)),
          catchError((err) => of(actions.manageInvitationFailed(err)))
        );
      })
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private router: Router,
    private route: ActivatedRoute,
    private alertService: AlertService,
    private invitationsService: TeamInvitationsService
  ) {}
}
