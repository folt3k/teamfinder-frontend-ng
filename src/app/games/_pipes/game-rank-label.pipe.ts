import { Pipe, PipeTransform } from '@angular/core';

import { GameRank } from '../../_shared/models';
import { BaseEntity, GameType } from '../../_shared/interfaces';

@Pipe({
  name: 'gameRankLabel',
})
export class GameRankLabelPipe implements PipeTransform {
  transform(entity: BaseEntity): string {
    if (!entity || !entity?.ranks) {
      return null;
    }

    if (!entity.ranks.length) {
      return '---';
    }

    switch (entity.type) {
      case GameType.CS:
        return entity.ranks[0].name;
      case GameType.LOL:
        return this.getLolPrimaryRankLabel(entity.ranks);
      default:
        return '---';
    }
  }

  private getLolPrimaryRankLabel(ranks: GameRank[]): string {
    const soloQRank = ranks.find((r) => r.queue === 'RANKED_SOLO_5x5');

    return !!soloQRank ? soloQRank.name : ranks[0].name;
  }
}
