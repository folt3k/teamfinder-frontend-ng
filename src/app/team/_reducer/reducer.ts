import { createReducer, on, Action } from '@ngrx/store';

import * as actions from './actions';
import { BaseReducerState, createBaseReducerState, clearError, getErrorState } from '../../_reducers';

export interface State extends BaseReducerState {
  invitations: { results: any[] } & BaseReducerState;
}

export const initialState: State = createBaseReducerState({
  invitations: createBaseReducerState({ results: [] }),
});

export const _reducer = createReducer(
  initialState,
  on(actions.createTeam, actions.updateTeam, (state) => ({ ...state, loading: true, error: clearError() })),
  on(actions.createTeamSuccess, actions.updateTeamSuccess, (state) => ({ ...state, loading: false })),
  on(actions.createTeamFailed, actions.updateTeamFailed, (state, payload) => ({
    ...state,
    error: getErrorState(payload),
    loading: false,
  }))
);

export function reducer(state: State, action: Action) {
  return _reducer(state, action);
}
