import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { MY_PROFILE_STATIC_URLS } from '../../_shared/common';
import { UserService } from '../../user/user.service';

@Component({
  selector: 'tf-unsubcribe-from-mail-notifications',
  templateUrl: './unsubcribe-from-mail-notifications.component.html',
  styleUrls: ['./unsubcribe-from-mail-notifications.component.scss'],
})
export class UnsubcribeFromMailNotificationsComponent implements OnInit {
  MY_PROFILE_STATIC_URLS = MY_PROFILE_STATIC_URLS;
  success: boolean = false;

  constructor(private usersService: UserService, private route: ActivatedRoute) {}

  ngOnInit(): void {}

  endMailSubscription(): void {
    this.usersService
      .unsubscribeFromMailNotifications({ token: this.route.snapshot.paramMap.get('token') })
      .subscribe(() => {
        this.success = true;
      });
  }
}
