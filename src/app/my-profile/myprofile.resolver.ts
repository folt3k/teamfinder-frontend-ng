import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { mapTo, switchMap, take } from 'rxjs/operators';

import { AppState } from '../app.interface';
import { currentProfile, meSuccess } from '../auth/_reducer';

@Injectable()
export class MyProfileResolver implements Resolve<boolean> {
  constructor(private store: Store<AppState>, private actions: Actions, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return this.store.select(currentProfile).pipe(
      take(1),
      switchMap((profile) => {
        // If current profile is fetched than OK..
        if (profile) {
          return of(true);
        }
        // If current user is fetching
        return this.actions.pipe(ofType(meSuccess), take(1), mapTo(true));
      })
    );
  }
}
