export const seoTranslations = {
  home: {
    title: 'Teamfinder | Szukaj graczy i teamu do LoL, CS:GO i Fortnite',
    description:
      'Największa w Polsce strona dla graczy i drużyn eSportowych. Szukaj graczy i teamów w League of Legends, CS:GO i Fortnite, szukam teamu, szukam gracza',
    meta: [
      {
        property: 'og:image',
        content: '/assets/images/app_thumb.jpg',
      },
    ],
  },
  homeGame: {
    lol: {
      title: 'Teamfinder | Szukaj graczy i teamu do League of Legends',
      description:
        'Największa w Polsce strona dla graczy i drużyn eSportowych. Szukaj graczy i teamów w League of Legends LoL, szukam graczy LoL, szukam teamu LoL',
      meta: [
        {
          property: 'og:image',
          content: '/assets/images/app_thumb.jpg',
        },
      ],
    },
    csgo: {
      title: 'Teamfinder | Szukaj graczy i teamu do CS:GO',
      description:
        'Największa w Polsce strona dla graczy i drużyn eSportowych. Szukaj graczy i teamów w Counter Strike: Global Offensive, szukam graczy CS:GO, szukam teamu CS:GO',
      meta: [
        {
          property: 'og:image',
          content: '/assets/images/app_thumb.jpg',
        },
      ],
    },
    fortnite: {
      title: 'Teamfinder | Szukaj graczy i teamu do Fortnite',
      description:
        'Największa w Polsce strona dla graczy i drużyn eSportowych. Szukaj graczy i teamów w Fortnite, szukam graczy Fortnite, szukam teamu Fortnite',
      meta: [
        {
          property: 'og:image',
          content: '/assets/images/app_thumb.jpg',
        },
      ],
    },
  },
  users: {
    title:
      'Znajdź graczy do rozgrywek w <% game %>. Szukaj graczy w <% game %>',
    description:
      'Znajdź graczy do wspólnego grania w <% game %>. Szukam graczy <% game %>',
  },
  teams: {
    title:
      'Znajdź team do grania w <% game %>. Szukaj teamu w <% game %>. Znajdź drużynę do <% game %>',
    description:
      'Znajdź team do wspólnego grania w <% game %>. Znajdź drużynę w <% game %> Szukaj teamu <% game %>. Rekrutacja do drużyn w <% game %>',
  },
  announcements: {
    title:
      'Ogłoszenia dla graczy i drużyn w <% game %>, szukam gracza, szukam teamu',
    description:
      'Ogłoszenia dla graczy i drużyn w <% game %>, szukam gracza <% game %>, szukam teamu <% game %>',
  },
  blog: {
    title:
      'Najnowsze wiadomości, newsy i artykuły ze świata gier MMO, League of Legends, CS:GO. Turnieje, newsy z League of Legends, CS:GO',
    description:
      'Informacje, artykuły, newsy ze świata League of Legends i CS:GO. Poznaj najnowsze wieści z LOL, MMO, CS:GO',
  },
  'my-profile': {
    title: 'Zarządzaj swoim profilem gracza',
    description: 'Zarządzaj swoim profilem gracza',
  },
  'add-team': {
    title:
      'Teamfinder | Szukaj graczy i teamu do grania w LoL, CS:GO i Fortnite',
    description:
      'Największa w Polsce strona dla graczy i drużyn eSportowych. Szukaj graczy i teamów w League of Legends, CS:GO i Fortnite, zakładaj drużyny na turnieje eSportowe. Ogłoszenia dla graczy i drużyn - szukam teamu, szukam graczy.',
    meta: [
      {
        property: 'og:image',
        content: '/assets/images/app_thumb.jpg',
      },
    ],
  },
};
