import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.interface';
import { GameType } from '../../_shared/interfaces';
import { getGameKeyByType } from '../games.helpers';
import { gamesTranslations, GameTranslations } from './games-translations';
import { selectCurrentGame } from '../_reducer';
import { map, take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class GamesTranslationsService {
  constructor(private store: Store<AppState>) {}

  get(gameType?: GameType): Observable<GameTranslations> {
    if (gameType) {
      const gameKey = getGameKeyByType(gameType);
      return of(gamesTranslations[gameKey]);
    }
    return this.store.select(selectCurrentGame).pipe(
      map((game) => gamesTranslations[game.key]),
      take(1)
    );
  }
}
