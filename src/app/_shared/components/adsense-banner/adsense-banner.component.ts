import { AfterViewInit, ChangeDetectionStrategy, Component, isDevMode, OnInit } from '@angular/core';

import { environment } from '../../../../environments/environment';

@Component({
  selector: 'tf-adsense-banner',
  templateUrl: './adsense-banner.component.html',
  styleUrls: ['./adsense-banner.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdsenseBannerComponent implements OnInit, AfterViewInit {
  isProdMode = !isDevMode();
  environment = environment;

  constructor() {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    setTimeout(() => {
      try {
        (window['adsbygoogle'] = window['adsbygoogle'] || []).push({});
      } catch (e) {
        console.error(e);
      }
    });
  }
}
