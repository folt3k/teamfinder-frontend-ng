import { featureKey } from './constants';
import { AppState } from '../../app.interface';
import { createSelector } from '@ngrx/store';
import { State } from './reducer';

const selectFeature = (state: AppState): State => state[featureKey];

export const selectTeamInvitations = createSelector(selectFeature, (state) => state.invitations);
export const selectTeamRequests = createSelector(selectFeature, (state) => state.requests);
