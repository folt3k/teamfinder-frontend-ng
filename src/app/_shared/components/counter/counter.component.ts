import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'tf-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CounterComponent implements OnInit {
  @Input() count: number = 0;
  @Input() theme: 'success' | 'danger' = 'success';
  @Input() size: 'sm' | 'md' | 'lg' = 'sm';

  constructor() {}

  get panelClasses(): { [key: string]: boolean } {
    return {
      [`counter--success`]: this.theme === 'success',
      ['counter--danger']: this.theme === 'danger',
      [`counter--${this.size}`]: true,
    };
  }

  ngOnInit(): void {}
}
