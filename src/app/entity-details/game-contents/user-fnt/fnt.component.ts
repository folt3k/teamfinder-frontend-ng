import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';

import { UserProfile } from '../../../_shared/models';
import { FntDetailsContentService } from './fnt.service';
import { FntGameAccountStats, fntGameControllerOptions } from '../../../games';
import { IconSwitcherOption } from '../../../_shared/form-controls/icon-switcher';

@Component({
  selector: 'tf-fnt-details-content',
  templateUrl: './fnt.component.html',
  styleUrls: ['./fnt.component.scss'],
})
export class FntDetailsContentComponent implements OnInit, OnChanges {
  @Input() data: UserProfile;

  stats: { tileStats: any; gameModeStats: any };
  gameControllerSwitcherOptions: IconSwitcherOption[] = fntGameControllerOptions;
  // TODO: Ma byc ladowana dynamicznie, w zaleznosci gdzie najwiecej gier..
  selectedGameController: string = 'keyboardmouse';

  constructor(private service: FntDetailsContentService) {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    this.loadStats();
  }

  onGameControllerSelect(value): void {
    this.selectedGameController = value;
    this.loadStats();
  }

  private loadStats(): void {
    if (this.data.game_metadata.accountStats) {
      this.stats = this.service.getStats(
        this.data.game_metadata.accountStats as FntGameAccountStats,
        this.selectedGameController
      );
    }
  }
}
