export interface LolGameChampion {
  championId: number;
  championPoints: number;
  championLevel: number;
}
