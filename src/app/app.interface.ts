import * as fromRouter from '@ngrx/router-store';

import * as fromGlobalChat from './global-chat/_reducer';
import * as fromUI from './_reducers/ui';
import * as fromAuth from './auth/_reducer';
import * as fromDicts from './_reducers/dictionaries';
import * as fromUser from './user/_reducer';
import * as fromNotifications from './notifications/_reducer';
import * as fromChat from './chat/_reducer';
import * as fromProfile from './profile/_reducer';
import * as fromGame from './games/_reducer';
import * as fromTeam from './team/_reducer';
import * as fromComment from './comments/_reducer';
import * as fromEntityDetails from './entity-details/_reducer';

export interface AppState {
  router: fromRouter.RouterReducerState<any>;
  [fromGame.featureKey]: fromGame.State;
  [fromDicts.featureKey]: fromDicts.State;
  [fromGlobalChat.featureKey]: fromGlobalChat.State;
  [fromChat.featureKey]: fromChat.State;
  [fromUI.featureKey]: fromUI.State;
  [fromAuth.featureKey]: fromAuth.State;
  [fromUser.featureKey]: fromUser.State;
  [fromProfile.featureKey]: fromProfile.State;
  [fromNotifications.featureKey]: fromNotifications.State;
  [fromTeam.featureKey]: fromTeam.State;
  [fromComment.featureKey]: fromComment.State;
  [fromEntityDetails.featureKey]: fromEntityDetails.State;
}
