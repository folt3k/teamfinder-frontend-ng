import { Component, Input, OnInit } from '@angular/core';

import { Announcement } from '../../../_shared/models';

@Component({
  selector: 'tf-announcement-details-content',
  templateUrl: './announcement.component.html',
  styleUrls: ['./announcement.component.scss'],
})
export class AnnouncementDetailsContentComponent implements OnInit {
  @Input() data: Announcement;

  constructor() {}

  ngOnInit(): void {}
}
