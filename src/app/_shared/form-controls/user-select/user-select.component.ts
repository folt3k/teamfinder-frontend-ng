import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Host,
  Input,
  OnInit,
  Optional,
  Output,
  SkipSelf,
} from '@angular/core';
import { ControlContainer, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Observable } from 'rxjs';
import { first, map, switchMap } from 'rxjs/operators';

import { UserProfile } from '../../models';
import { CustomFormControl } from '../custom-form-control';
import { UserService } from '../../../user/user.service';
import { ProfileService } from '../../../profile/profile.service';
import { Store } from '@ngrx/store';
import { AppState } from '../../../app.interface';
import { currentProfileId } from '../../../auth/_reducer';

@Component({
  selector: 'tf-user-select',
  templateUrl: './user-select.component.html',
  styleUrls: ['./user-select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => UserSelectComponent),
      multi: true,
    },
  ],
})
export class UserSelectComponent extends CustomFormControl implements OnInit {
  @Input() excludedUsers: number[] = [];
  @Output() changed: EventEmitter<UserProfile> = new EventEmitter<UserProfile>();

  value: UserProfile;

  constructor(
    cdr: ChangeDetectorRef,
    @Optional()
    @Host()
    @SkipSelf()
    controlContainer: ControlContainer,
    private profileService: ProfileService,
    private store: Store<AppState>
  ) {
    super(cdr, controlContainer);
  }

  ngOnInit(): void {}

  writeValue(value: UserProfile): void {
    this.value = value;
  }

  registerOnChange(fn: () => void): void {
    this.propagateChange = fn;
  }

  registerOnTouched(): void {}

  setDisabledState(isDisabled: boolean): void {}

  propagateChange = (value: UserProfile) => {};

  onSelectValue(user: UserProfile): void {
    this.propagateChange(user);
    this.value = user;
    this.changed.emit(user);
  }

  fetchUsers$(searchPhrase?: string): Observable<UserProfile[]> {
    return this.store.select(currentProfileId).pipe(
      first(),
      switchMap((profileId) =>
        this.profileService
          .fetchAll({ perPage: 8, q: searchPhrase, excluded: [profileId, ...this.excludedUsers] })
          .pipe(map((data) => data.results))
      )
    );
  }
}
