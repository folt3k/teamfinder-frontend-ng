import { TemplateRef } from '@angular/core';

export enum AlertTheme {
  DEFAULT = 'DEFAULT',
  SUCCESS = 'SUCCESS',
  FAILED = 'FAILED',
}

export interface AlertOptions {
  theme?: AlertTheme;
  duration?: number;
}

export interface AlertConfig {
  content: string | TemplateRef<any>;
  options: AlertOptions;
}

export interface DefinedAlerts {
  [key: string]: { show: () => void };
}
