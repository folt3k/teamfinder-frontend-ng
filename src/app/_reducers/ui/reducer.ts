import { createReducer, on } from '@ngrx/store';
import { omit } from 'lodash';

import { updateUI } from './actions';

export interface State {
  isWindowActive: boolean;
  isLoginPage: boolean;
  alignCenterMainContent: boolean;
  isPageContainerFull: boolean;
  isMainContainerFull: boolean;
  canDisplayGameSelect: boolean;
  redirectPath: string;
}

export const initialState: State = {
  isLoginPage: true,
  alignCenterMainContent: false,
  isPageContainerFull: false,
  canDisplayGameSelect: false,
  isMainContainerFull: false,
  redirectPath: null,
  isWindowActive: true,
};

export const reducer = createReducer(
  initialState,
  on(updateUI, (state, payload) => {
    const newState = { ...omit(payload, ['type']) };
    return { ...state, ...newState };
  })
);
