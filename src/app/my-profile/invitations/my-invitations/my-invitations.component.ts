import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { BaseListRO } from '../../../_shared/interfaces';
import { TeamInvitation } from '../../../invitations/invitations.model';
import { AppState } from '../../../app.interface';
import {
  fetchInvitations,
  fetchRequests,
  selectTeamInvitations,
  selectTeamRequests,
} from '../../../invitations/_reducer';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'tf-my-profile-my-invitations',
  templateUrl: './my-invitations.component.html',
  styleUrls: ['./my-invitations.component.scss'],
})
export class MyProfileMyInvitationsComponent implements OnInit {
  receivedInvitations$: Observable<BaseListRO<TeamInvitation>>;
  sentRequests$: Observable<BaseListRO<TeamInvitation>>;
  activeTab: 'sentRequests' | 'receivedInvitations';

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {}

  ngOnInit(): void {
    if (this.route.snapshot.fragment) {
      switch (this.route.snapshot.fragment) {
        case 'otrzymane':
          this.activeTab = 'receivedInvitations';
          break;
        case 'wyslane':
          this.activeTab = 'sentRequests';
          break;
      }
    } else {
      this.activeTab = 'sentRequests';
    }

    this.store.dispatch(fetchInvitations({ params: { scope: 'receivedInvitations' } }));
    this.store.dispatch(fetchRequests({ params: { scope: 'sentRequests' } }));

    this.receivedInvitations$ = this.store.select(selectTeamInvitations);
    this.sentRequests$ = this.store.select(selectTeamRequests);
  }
}
