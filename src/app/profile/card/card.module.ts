import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ProfileCardComponent } from './card.component';
import { OnlineStatusIndicatorModule, UserAvatarModule } from '../../_shared/components';
import { GameRoleIconGroupModule } from '../../games/_components';
import { PipesModule } from '../../_shared/pipes/pipes.module';
import { GamesModule } from '../../games/games.module';

@NgModule({
  declarations: [ProfileCardComponent],
  imports: [
    CommonModule,
    RouterModule,
    UserAvatarModule,
    OnlineStatusIndicatorModule,
    GameRoleIconGroupModule,
    PipesModule,
    GamesModule
  ],
  exports: [ProfileCardComponent],
})
export class ProfileCardModule {}
