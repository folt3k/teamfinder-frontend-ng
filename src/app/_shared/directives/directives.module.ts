import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ResizeTextareaDirective } from './resize-textarea.directive';
import { AuthorizationNeededDirective } from './authorization-needed.directive';
import { StopPropagationOnClickDirective } from './stop-propagation-on-click.directive';

const DIRECTIVES = [ResizeTextareaDirective, AuthorizationNeededDirective, StopPropagationOnClickDirective];

@NgModule({
  declarations: [...DIRECTIVES],
  imports: [CommonModule],
  exports: [...DIRECTIVES],
})
export class DirectivesModule {}
