import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Dictionaries } from '../../../_shared/models';
import { GameOption } from '../../../_shared/common';
import { GameType } from '../../../_shared/interfaces';

@Component({
  selector: 'tf-additional-step',
  templateUrl: './additional-step.component.html',
  styleUrls: ['./additional-step.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdditionalStepComponent implements OnInit {
  @Input() form: FormGroup;
  @Input() dictionaries: Partial<Dictionaries>;
  @Input() selectedGame: GameOption;

  constructor() {}

  get canDisplayGameRolesField(): boolean {
    return this.selectedGame.value !== GameType.FNT;
  }

  get canDisplayGameModesField(): boolean {
    return this.selectedGame.value !== GameType.FNT;
  }

  get canDisplayGameRankField(): boolean {
    return this.selectedGame.value === GameType.CS;
  }

  ngOnInit(): void {}

  avatarSelected(avatar: string): void {
    this.form.patchValue({ avatar });
  }
}
