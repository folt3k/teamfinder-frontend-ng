import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmblematGamePickerComponent } from './emblemat-game-picker.component';

@NgModule({
  declarations: [EmblematGamePickerComponent],
  imports: [
    CommonModule
  ],
  exports: [EmblematGamePickerComponent]
})
export class EmblematGamePickerModule { }
