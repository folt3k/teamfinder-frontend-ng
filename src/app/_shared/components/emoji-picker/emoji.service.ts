import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

interface ResponseEmojiItem {
  codes: string;
  char: string;
}

const EXCLUDES_EMOJIS = [
  '263A FE0F',
  '263A',
  '1F972',
  '1F978',
  '2639 FE0F',
  '2639',
  '2620 FE0F',
  '2620',
  '2764 FE0F',
  '2764',
  '1F90C',
  '1F90C 1F3FB',
  '1F90C 1F3FC',
  '1F90C 1F3FD',
  '1F90C 1F3FE',
  '1F90C 1F3FF',
  '1F977',
  '1F977 1F3FB',
  '1F977 1F3FC',
  '1F977 1F3FD',
  '1F977 1F3FE',
  '1F977 1F3FF',
];

@Injectable({
  providedIn: 'root',
})
export class EmojiService {
  emojis$ = new BehaviorSubject<string[][]>([]);

  constructor(private http: HttpClient) {}

  inti(): void {
    this.http
      .get<ResponseEmojiItem[]>('https://unpkg.com/emoji.json@13.0.0/emoji.json')
      .pipe(map((emojis) => emojis.filter((e) => !EXCLUDES_EMOJIS.includes(e.codes)).map((e) => e.char)))
      .subscribe((emojis) => {
        const mappedEmojis = [];
        while (emojis.length > 0) {
          const chunk = emojis.splice(0, 8);
          mappedEmojis.push(chunk);
        }
        this.emojis$.next(mappedEmojis);
      });
  }
}
