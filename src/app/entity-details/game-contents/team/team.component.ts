import { Component, Input, OnInit } from '@angular/core';

import { Team } from '../../../team';
import { UserProfile } from '../../../_shared/models';
import { Store } from '@ngrx/store';
import { AppState } from '../../../app.interface';
import { sendRequest } from '../../../invitations/_reducer';
import { isCurrentUserGameEqualsToEntityGame } from '../../../games';

@Component({
  selector: 'tf-team-details-content',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.scss'],
})
export class TeamDetailsContentComponent implements OnInit {
  @Input() data: Team;
  @Input() currentUser: UserProfile;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {}

  get canDisplayJoinBtn(): boolean {
    return (
      isCurrentUserGameEqualsToEntityGame(this.currentUser, this.data) &&
      this.data.owner.id !== this.currentUser?.id &&
      this.data.canCurrentUserSendTeamRequest &&
      !this.data.members.find((member) => member.profile.id === this.currentUser.id)
    );
  }

  get canDisplaySearchUsersBtn(): boolean {
    return this.data.owner.id === this.currentUser?.id;
  }

  joinToTeam(): void {
    this.store.dispatch(sendRequest({ body: { teamId: this.data.id } }));
  }
}
