import { Pipe, PipeTransform } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { AppState } from '../../app.interface';
import { selectCurrentGame } from '../../games/_reducer';
import { STATIC_URLS } from '../common';

type LinkWithGamePrefix = 'users' | 'announcements' | 'teams';

@Pipe({
  name: 'gamePrefixLink',
})
export class GamePrefixLinkPipe implements PipeTransform {
  transform(type: LinkWithGamePrefix): Observable<string> {
    return this.store.select(selectCurrentGame).pipe(
      take(1),
      map((game) => `${game.slashRouterPrefix}${STATIC_URLS[type].slashPath}`)
    );
  }

  constructor(private store: Store<AppState>) {}
}
