import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Params } from '@angular/router';
import { filter } from 'rxjs/operators';

import { AppState } from './app.interface';
import { selectUI } from './_reducers/ui';
import { selectRouteParams } from './_reducers/router';
import { GAMES } from './_shared/common';

@Component({
  selector: 'tf-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'teamfinder-chat-frontend';
  isLoginPage: boolean = true;
  alignCenterMainContent: boolean = false;
  isPageContainerFull: boolean = false;
  isMainContainerFull: boolean = false;
  showNotFoundPage: boolean = false;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.updateDisplaysOnUiChange();
    this.toggleNotFoundPageOnGamePrefixChange();
  }

  private updateDisplaysOnUiChange(): void {
    this.store
      .select(selectUI, [
        'alignCenterMainContent',
        'isPageContainerFull',
        'isMainContainerFull',
        'isLoginPage',
      ])
      .subscribe((results) => {
        this.isLoginPage = results.isLoginPage;
        this.alignCenterMainContent = results.alignCenterMainContent;
        this.isPageContainerFull = results.isPageContainerFull;
        this.isMainContainerFull = results.isMainContainerFull;
      });
  }

  private toggleNotFoundPageOnGamePrefixChange(): void {
    this.store
      .select(selectRouteParams)
      .pipe(filter(Boolean))
      .subscribe((params: Params) => {
        if (params.identifier && !params.gamePrefix) {
          this.showNotFoundPage = true;
        } else if (params.gamePrefix) {
          const isCorrectGamePrefix = !!GAMES.find((g) => g.routerPrefix === params.gamePrefix);
          this.showNotFoundPage = !isCorrectGamePrefix;
        } else {
          this.showNotFoundPage = false;
        }
      });
  }
}
