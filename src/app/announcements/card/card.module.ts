import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { AnnouncementCardComponent } from './card.component';
import { GameRoleIconGroupModule } from '../../games/_components';
import { PipesModule } from '../../_shared/pipes/pipes.module';
import { UserAvatarModule } from '../../_shared/components';

@NgModule({
  declarations: [AnnouncementCardComponent],
  imports: [CommonModule, RouterModule, GameRoleIconGroupModule, PipesModule, UserAvatarModule],
  exports: [AnnouncementCardComponent],
})
export class AnnouncementCardModule {}
