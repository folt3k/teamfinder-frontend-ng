import { GameType } from '../../_shared/interfaces';
import * as memoizee from 'memoizee';

export type TeamFormFieldsDisplayCondition = (fieldKey: string) => boolean;

export const fieldsDisplayCondition = memoizee(
  (type: GameType): TeamFormFieldsDisplayCondition => {
    let displayFields: string[] = [];

    switch (type) {
      case GameType.LOL:
        displayFields = ['roles', 'gameServer', 'rank', 'modes'];
        break;
      case GameType.CS:
        displayFields = ['roles', 'rank', 'modes'];
        break;
      case GameType.FNT:
        displayFields = [];
        break;
    }

    return (fieldKey: string) => displayFields.includes(fieldKey);
  }
);
