import { IconSwitcherOption } from '../_shared/form-controls/icon-switcher';

export const fntGameControllerOptions: IconSwitcherOption[] = [
  {
    label: 'Keyboard & Mouse',
    value: 'keyboardmouse',
    icon: 'keyboard',
  },
  {
    label: 'Gamepad',
    value: 'gamepad',
    icon: 'gamepad',
  },
  {
    label: 'Touch',
    value: 'touch',
    icon: 'tablet_mac',
  },
];
