import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { DialogService } from '../../_shared/poppy/dialog';
import { ADD_ANNOUNCEMENT_DIALOG_UNIQUE_ID } from '../announcements.constants';
import { AddAnnouncementComponent } from '../add/add.component';
import { currentProfile, isAuthorizationCompleted } from '../../auth/_reducer';
import { selectCurrentGame } from '../../games/_reducer';
import { AppState } from '../../app.interface';

@Component({
  selector: 'tf-all-announcements',
  templateUrl: './all.component.html',
  styleUrls: ['./all.component.scss'],
})
export class AllAnnouncementsComponent implements OnInit {
  canDisplayCreateSection$: Observable<boolean>;

  constructor(private dialogService: DialogService, private store: Store<AppState>) {}

  ngOnInit(): void {
    this.canDisplayCreateSection$ = combineLatest(
      this.store.select(isAuthorizationCompleted),
      this.store.select(currentProfile),
      this.store.select(selectCurrentGame)
    ).pipe(
      filter(([isAuthorizationCompleted]) => isAuthorizationCompleted),
      map(([_, profile, game]) => !profile || profile.type === game.value)
    );
  }

  showAddDialog(): void {
    this.dialogService.open(AddAnnouncementComponent, { uniqueId: ADD_ANNOUNCEMENT_DIALOG_UNIQUE_ID });
  }
}
