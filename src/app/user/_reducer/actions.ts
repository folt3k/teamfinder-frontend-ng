import { createAction, props } from '@ngrx/store';

import { types } from './constants';
import {
  ChangePasswordDto,
  CreateUserDto,
  UpdateMailNotificationSettingsDto,
  UpdateUserDto,
} from '../user.interface';
import { UserProfile } from './../../_shared/models/user-profile.model';
import { ErrorRO } from '../../_shared/interfaces';
import { User, UserMailNotificationSettings } from '../../_shared/models';

export const updateUserNetworkStatus = createAction(
  types.UpdateUserNetworkStatus,
  props<{ profile: Partial<UserProfile>; isOnline: boolean }>()
);

export const register = createAction(types.Register, props<{ body: CreateUserDto }>());
export const registerSuccess = createAction(types.RegisterSuccess, props<any>());
export const registerFailed = createAction(types.RegisterFailed, props<ErrorRO>());

export const updateUser = createAction(types.Update, props<{ body: UpdateUserDto }>());
export const updateUserSuccess = createAction(types.UpdateSuccess, props<{ data: User }>());
export const updateUserFailed = createAction(types.UpdateFailed, props<ErrorRO>());

export const changeUserPassword = createAction(types.ChangePassword, props<{ body: ChangePasswordDto }>());
export const changeUserPasswordSuccess = createAction(types.ChangePasswordSuccess);
export const changeUserPasswordFailed = createAction(types.ChangePasswordFailed, props<ErrorRO>());

export const updateUserMailNotificationSettings = createAction(
  types.UpdateUserMailNotificationSettings,
  props<{ body: UpdateMailNotificationSettingsDto }>()
);
export const updateUserMailNotificationSettingsSuccess = createAction(
  types.UpdateUserMailNotificationSettingsSuccess,
  props<{ data: UserMailNotificationSettings }>()
);
export const updateUserMailNotificationSettingsFailed = createAction(
  types.UpdateUserMailNotificationSettingsFailed,
  props<ErrorRO>()
);
