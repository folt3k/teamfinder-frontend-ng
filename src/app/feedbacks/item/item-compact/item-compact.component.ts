import { ChangeDetectionStrategy, Component } from '@angular/core';
import { FeedbackItemComponent } from '../item.component';

@Component({
  selector: 'tf-feedback-compact-item',
  templateUrl: './item-compact.component.html',
  styleUrls: ['./item-compact.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeedbackCompactItemComponent extends FeedbackItemComponent {}
