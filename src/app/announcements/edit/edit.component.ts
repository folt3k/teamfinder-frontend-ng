import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { UpdateAnnouncementDto } from '../announcements.interface';
import { updateAnnouncement } from '../_reducer';
import { AppState } from '../../app.interface';

@Component({
  selector: 'tf-edit-announcement',
  templateUrl: './edit.component.html',
})
export class EditAnnouncementComponent implements OnInit {
  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {}

  onSubmit(values: UpdateAnnouncementDto): void {
    this.store.dispatch(updateAnnouncement({ body: values }));
  }
}
