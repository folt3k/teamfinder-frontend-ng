import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { Dictionaries } from '../models';

@Injectable({
  providedIn: 'root',
})
export class DictionariesService {
  constructor(private http: HttpClient) {}

  fetchStaticDicts(): Observable<Partial<Dictionaries>> {
    return this.http.get<Partial<Dictionaries>>('/dictionaries/static');
  }

  fetchDynamicDicts(): Observable<Partial<Dictionaries>> {
    return this.http.get<Partial<Dictionaries>>('/dictionaries/dynamic');
  }
}
