import { Component, Input, OnChanges } from '@angular/core';

import { UserProfile } from '../../../_shared/models';
import { CsGameAccountStats } from '../../../games';

@Component({
  selector: 'tf-csgo-details-content',
  templateUrl: './csgo.component.html',
  styleUrls: ['./csgo.component.scss'],
})
export class CsDetailsContentComponent implements OnChanges {
  @Input() data: UserProfile;

  stats: CsGameAccountStats;
  t;
  constructor() {}

  ngOnChanges(): void {
    this.stats = this.data.game_metadata.accountStats as CsGameAccountStats;
  }
}
