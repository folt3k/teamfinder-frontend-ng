import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FetchPrivateChatRoomsRO } from '../chat.inerface';

@Injectable({
  providedIn: 'root',
})
export class PrivateChatService {
  constructor(private http: HttpClient) {}

  fetchRooms(): Observable<FetchPrivateChatRoomsRO> {
    return this.http.get<FetchPrivateChatRoomsRO>('/chat/rooms');
  }
}
