import { createAction, props } from '@ngrx/store';

import { types } from './constants';
import { ErrorRO } from '../../_shared/interfaces';
import { CreateAnnouncementDto, UpdateAnnouncementDto } from '../announcements.interface';
import { Announcement, AnnouncementMember } from '../../_shared/models';

export const createAnnouncement = createAction(types.Create, props<{ body: CreateAnnouncementDto }>());
export const createAnnouncementSuccess = createAction(types.CreateSuccess, props<{ data: Announcement }>());
export const createAnnouncementFailed = createAction(types.CreateFailed, props<ErrorRO>());

export const updateAnnouncement = createAction(types.Update, props<{ body: UpdateAnnouncementDto }>());
export const updateAnnouncementSuccess = createAction(types.UpdateSuccess, props<{ data: Announcement }>());
export const updateAnnouncementFailed = createAction(types.UpdateFailed, props<ErrorRO>());

export const removeAnnouncement = createAction(
  types.Remove,
  props<{ id: number; redirectFromCurrentUrl?: boolean }>()
);
export const removeAnnouncementSuccess = createAction(
  types.RemoveSuccess,
  props<{ redirectFromCurrentUrl?: boolean }>()
);
export const removeAnnouncementFailed = createAction(types.RemoveFailed, props<ErrorRO>());

export const addAnnouncementMember = createAction(types.AddMember, props<{ id: number }>());
export const addAnnouncementMemberSuccess = createAction(
  types.AddMemberSuccess,
  props<{ data: AnnouncementMember }>()
);
export const addAnnouncementMemberFailed = createAction(types.AddMemberFailed, props<ErrorRO>());
