export interface KeyLabelPair {
  key: string;
  label: string;
}
