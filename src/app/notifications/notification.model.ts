import { BaseEntity } from '../_shared/interfaces';
import { Announcement, UserProfile } from '../_shared/models';
import { Team } from '../team';

export enum NotificationType {
  USER_INVITED_TO_TEAM = 'user_invited_to_team',
  USER_ACCEPT_INVITATION = 'user_accept_invitation',
  USER_REQUESTED_TO_TEAM = 'user_requested_to_team',
  USER_JOINED_TO_TEAM = 'user_joined_to_team',
  USER_RECEIVED_MESSAGE = 'user_received_message',
  USER_COMMENTED_TEAM = 'user_commented_team',
  USER_COMMENTED_GAME_ANNOUNCEMENT = 'user_commented_game_announcement',
  USER_RESPONSE_TO_TEAM_COMMENT = 'user_response_to_team_comment',
  USER_RESPONSE_TO_GAME_ANNOUNCEMENT_COMMENT = 'user_response_to_game_announcement_comment',
  USER_JOINED_TO_GAME_ANNOUNCEMENT = 'user_joined_to_game_announcement',
  USER_JOINED_TO_APP = 'user_joined_to_app',
}

export interface Notification extends BaseEntity {
  type: NotificationType;
  wasDisplayed: boolean;
  wasRead: boolean;
  sender?: UserProfile;
  receiver?: UserProfile;
  team?: Team;
  gameAnnouncement?: Announcement;
}
