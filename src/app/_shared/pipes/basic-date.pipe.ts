import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'basicDate',
})
export class BasicDatePipe implements PipeTransform {
  transform(value: Date): string {
    if (!value) {
      return '---';
    }

    const transformDate = new Date(value).getTime();
    const currentDate = new Date().getTime();

    const diff = currentDate - transformDate;

    if (diff / 1000 < 60) {
      return `przed chwilą`;
    }
    if (diff / 1000 / 60 < 60) {
      return `${Math.round(diff / 1000 / 60)} minut temu`;
    }
    if (diff / 1000 / 60 >= 60) {
      if (diff / 1000 / 60 / 60 > 23) {
        return `${Math.round(diff / 1000 / 60 / 60 / 24)} dni temu`;
      }
      return `${Math.round(diff / 1000 / 60 / 60)} godzin temu`;
    }

    return '';
  }
}
