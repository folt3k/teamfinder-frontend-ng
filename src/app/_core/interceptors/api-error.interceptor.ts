import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { catchError, first } from 'rxjs/operators';

import { logout, logoutSuccess } from '../../auth/_reducer';
import { STATIC_URLS } from '../../_shared/common';
import { AppState } from '../../app.interface';
import { AlertService } from '../../_shared/components/alert/alert.service';
import { AlertTheme } from '../../_shared/components/alert/alert.interface';

@Injectable()
export class ApiErrorInterceptor implements HttpInterceptor {
  constructor(
    private store: Store<AppState>,
    private router: Router,
    private actions: Actions,
    private alertService: AlertService
  ) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      catchError((err) => {
        // Logout user and redirect to login page when is user is unauthorized
        switch (err.status) {
          case 401:
            this.store.dispatch(logout());
            this.actions.pipe(ofType(logoutSuccess), first()).subscribe(() => {
              this.router.navigate([STATIC_URLS['login'].slashPath]);
            });
            break;
          case 403:
            if (err.error?.type === 'USER_BANNED_EXCEPTION') {
              this.alertService.definedAlerts.userBanned.show();
            }
            break;
          case 500:
            this.alertService.show('Wystąpił nieznany błąd 😓', { theme: AlertTheme.FAILED });
            break;
        }

        throw err.error;
      })
    );
  }
}
