import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyProfileInvitationsListComponent } from './list.component';
import { TeamInvitationsModule } from '../../../invitations/invitations.module';
import { SharedModule } from '../../../_shared/shared.module';

@NgModule({
  declarations: [MyProfileInvitationsListComponent],
  imports: [CommonModule, TeamInvitationsModule, SharedModule],
  exports: [MyProfileInvitationsListComponent],
})
export class MyProfileInvitationListModule {}
