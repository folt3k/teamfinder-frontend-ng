import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { STATIC_URLS } from 'src/app/_shared/common';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'tf-registration-base-step',
  templateUrl: './base-step.component.html',
  styleUrls: ['./base-step.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BaseStepComponent implements OnInit {
  @Input() form: FormGroup;

  STATIC_URLS = STATIC_URLS;

  constructor() {}

  ngOnInit(): void {}
}
