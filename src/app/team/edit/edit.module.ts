import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditTeamComponent } from './edit.component';
import { EditTeamRoutingModule } from './edit-routing.module';
import { TeamModule } from '../team.module';
import { SharedModule } from '../../_shared/shared.module';
import { EditTeamResolver } from './edit.resolver';
import { EditTeamMembersComponent } from './members/members.component';

@NgModule({
  declarations: [EditTeamComponent, EditTeamMembersComponent],
  imports: [CommonModule, EditTeamRoutingModule, SharedModule, TeamModule],
  exports: [EditTeamComponent],
  providers: [EditTeamResolver],
})
export class EditTeamModule {}
