export const featureKey = 'entity-details';

export const types = {
  FetchDetails: '[Entity details] Fetch entity details',
  FetchDetailsSuccess: '[Entity details] Fetch entity details success',
  FetchDetailsFailed: '[Entity details] Fetch entity details failed',
};
