import { Component, OnInit, ChangeDetectionStrategy, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'tf-global-chat-launcher',
  templateUrl: './launcher.component.html',
  styleUrls: ['./launcher.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LauncherComponent implements OnInit {
  @Input() activeUsersCount: number = 0;
  @Input() unseenMessagesCount: number = 0;

  constructor() {}

  @HostBinding('class.active') get active(): boolean {
    return this.unseenMessagesCount > 0;
  }

  ngOnInit(): void {}
}
