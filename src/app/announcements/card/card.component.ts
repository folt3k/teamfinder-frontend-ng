import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

import { GameCheckers, getGameCheckers } from '../../games';
import { Announcement } from '../../_shared/models';

@Component({
  selector: 'tf-announcement-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AnnouncementCardComponent implements OnInit {
  @Input() data: Announcement;

  gameCheckers: GameCheckers;

  constructor() {}

  ngOnInit(): void {
    this.gameCheckers = getGameCheckers(this.data.type);
  }

  get gameRoles(): string[] {
    return (this.data.roles || []).map((role) => role.name);
  }

  get gameModes(): string {
    return (this.data.modes || []).map((mode) => mode.name).join(', ');
  }
}
