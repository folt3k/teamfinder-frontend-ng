export const gamesTranslations: { lol: GameTranslations; cs: GameTranslations; fnt: GameTranslations } = {
  lol: {
    registerGameAccountLabel: 'Twoja nazwa przywoływacza w League of Legends',
    userInfoGameAccountNameLabel: 'Nazwa przywoływacza',
  },
  cs: {
    registerGameAccountLabel: 'Twój unikalny identyfikator w platformie Steam (Steam ID)',
    userInfoGameAccountNameLabel: 'CS:GO Nickname',
  },
  fnt: {
    registerGameAccountLabel: 'Twoja nazwa gracza w Fortnite',
    userInfoGameAccountNameLabel: 'Nazwa gracza',
  },
};

export interface GameTranslations {
  registerGameAccountLabel: string;
  userInfoGameAccountNameLabel: string;
}
