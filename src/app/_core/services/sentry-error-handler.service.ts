import { ErrorHandler, Injectable } from '@angular/core';
import * as Sentry from '@sentry/angular';

@Injectable({ providedIn: 'root' })
export class SentryErrorHandler implements ErrorHandler {
  constructor() {}

  handleError(error): void {
    const chunkFailedMessage = /Loading chunk [\d]+ failed/;
    if (chunkFailedMessage.test(error.message)) {
      window.location.reload();
    } else {
      Sentry.captureException(error.originalError || error);
    }
  }
}
