import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import {
  CheckNameAvailabilityDto,
  CheckNameAvailabilityRO,
  CreateTeamDto,
  CreateTeamRO,
  RemoveTeamMemberDto,
} from './team.interface';
import { Team, UserMemberTeam } from './team.model';
import { BaseListRO, ListQueryParams } from '../_shared/interfaces';
import { DEFAULT_PER_PAGE } from '../_shared/common';
import { User } from '../_shared/models';

@Injectable({
  providedIn: 'root',
})
export class TeamService {
  constructor(private http: HttpClient) {}

  fetchAll(params: ListQueryParams = {}): Observable<BaseListRO<Team>> {
    return this.http.get<BaseListRO<Team>>('/teams', {
      params: {
        ...params,
        perPage: params.perPage || DEFAULT_PER_PAGE,
      } as any,
    });
  }

  fetchLatest(): Observable<BaseListRO<Team>> {
    return this.http.get<BaseListRO<Team>>('/teams/latest');
  }

  fetchOne(slug: string): Observable<Team> {
    return this.http.get<Team>(`/teams/${slug}`);
  }

  create(body: CreateTeamDto): Observable<CreateTeamRO> {
    return this.http.post<CreateTeamRO>('/teams', body);
  }

  update(id: number, body: CreateTeamDto): Observable<Team> {
    return this.http.put<Team>(`/teams/${id}`, body);
  }

  remove(id: number): Observable<void> {
    return this.http.delete<void>(`/teams/${id}`);
  }

  checkNameAvailability(body: CheckNameAvailabilityDto): Observable<CheckNameAvailabilityRO> {
    return this.http.post<CheckNameAvailabilityRO>('/teams/check-name-availability', body);
  }

  fetchUserOwnedTeams(userId: number, params: ListQueryParams = {}): Observable<BaseListRO<Team>> {
    return this.http.get<BaseListRO<Team>>(`/profiles/${userId}/owned-teams`, {
      params: {
        ...params,
        perPage: params.perPage || DEFAULT_PER_PAGE,
      } as any,
    });
  }

  fetchUserMemberTeams(userId: number, params: ListQueryParams = {}): Observable<BaseListRO<UserMemberTeam>> {
    return this.http.get<BaseListRO<UserMemberTeam>>(`/profiles/${userId}/member-teams`, {
      params: {
        ...params,
        perPage: params.perPage || DEFAULT_PER_PAGE,
      } as any,
    });
  }

  removeTeamMember({ profileId, teamId }: RemoveTeamMemberDto): Observable<void> {
    return this.http.delete<void>(`/teams/${teamId}/members/${profileId}`);
  }
}
