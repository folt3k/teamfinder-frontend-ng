import { Component, OnInit } from '@angular/core';

import { EntityType } from '../../entity-details';

@Component({
  selector: 'tf-announcement-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
})
export class AnnouncementDetailsComponent implements OnInit {
  ENTITY_TYPE = EntityType;

  constructor() {}

  ngOnInit(): void {}
}
