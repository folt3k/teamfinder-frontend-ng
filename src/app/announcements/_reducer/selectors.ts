import { createSelector } from '@ngrx/store';

import { AppState } from '../../app.interface';
import { featureKey } from './constants';

const selectFeature = (state: AppState) => state[featureKey];

export const error =  createSelector(selectFeature, (state) => state.error);

export const loading = createSelector(selectFeature, (state) => state.loading);

export const createLoading = createSelector(selectFeature, (state) => state.createLoading);

