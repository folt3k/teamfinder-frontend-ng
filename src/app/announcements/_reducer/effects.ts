import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { Router } from '@angular/router';

import { AppState } from '../../app.interface';
import * as actions from '../_reducer/actions';
import { DYNAMIC_URLS, STATIC_URLS } from '../../_shared/common';
import { getGameRouterPrefixByType } from '../../games';
import { AnnouncementsService } from '../announcements.service';
import { DialogService } from '../../_shared/poppy/dialog';
import {
  ADD_ANNOUNCEMENT_DIALOG_UNIQUE_ID,
  EDIT_ANNOUNCEMENT_DIALOG_UNIQUE_ID,
} from '../announcements.constants';
import { AlertService } from '../../_shared/components/alert/alert.service';
import { selectCurrentGame } from '../../games/_reducer';
import { selectDetails } from '../../entity-details/_reducer/selectors';

@Injectable()
export class AnnouncementsEffects {
  create$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.createAnnouncement),
      switchMap((action) =>
        this.announcementService.create(action.body).pipe(
          map((data) => actions.createAnnouncementSuccess({ data })),
          catchError((err) => of(actions.createAnnouncementFailed(err)))
        )
      )
    )
  );

  createSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.createAnnouncementSuccess),
        map((response) => response.data),
        tap((data) => {
          const { id, type } = data;
          this.dialogService.remove(ADD_ANNOUNCEMENT_DIALOG_UNIQUE_ID);
          this.router.navigate([
            DYNAMIC_URLS['GAME-ANNOUNCEMENT']({
              gamePrefix: getGameRouterPrefixByType(type),
              identifier: id.toString(),
            }).slashPath,
          ]);
        })
      ),
    { dispatch: false }
  );

  update$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.updateAnnouncement),
      withLatestFrom(this.store.select(selectDetails)),
      switchMap(([action, details]) =>
        this.announcementService.update(details.id, action.body).pipe(
          map((data) => actions.updateAnnouncementSuccess({ data })),
          catchError((err) => of(actions.updateAnnouncementFailed(err)))
        )
      )
    )
  );

  updateSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.updateAnnouncementSuccess),
        tap(() => {
          this.dialogService.remove(EDIT_ANNOUNCEMENT_DIALOG_UNIQUE_ID);
        })
      ),
    { dispatch: false }
  );

  remove$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.removeAnnouncement),
      switchMap(({ id, redirectFromCurrentUrl }) =>
        this.announcementService.remove(id).pipe(
          map(() => actions.removeAnnouncementSuccess({ redirectFromCurrentUrl })),
          catchError((err) => of(actions.removeAnnouncementFailed(err)))
        )
      )
    )
  );

  removeSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.removeAnnouncementSuccess),
        withLatestFrom(this.store.select(selectCurrentGame)),
        tap(([{ redirectFromCurrentUrl }, game]) => {
          this.alertService.show('Twoje ogłoszenie zostało usunięte');

          if (redirectFromCurrentUrl) {
            this.router.navigate([STATIC_URLS['announcements'].pathWithGamePrefix(game)]);
          }
        })
      ),
    { dispatch: false }
  );

  addAnnouncementMember$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.addAnnouncementMember),
      switchMap((action) =>
        this.announcementService.addMember(action.id).pipe(
          map((data) => actions.addAnnouncementMemberSuccess({ data })),
          catchError((err) => of(actions.addAnnouncementMemberFailed(err)))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private router: Router,
    private announcementService: AnnouncementsService,
    private dialogService: DialogService,
    private alertService: AlertService
  ) {}
}
