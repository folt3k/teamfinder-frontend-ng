import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  NgZone,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef,
  ViewEncapsulation,
} from '@angular/core';
import { fromEvent, Subject } from 'rxjs';
import { filter, take } from 'rxjs/operators';

import { DIALOG_CONFIG, DialogConfig } from './dialog.token';
import { ActiveDialog } from './dialog.interface';
import { fadeInAnimation } from './dialog.animations';
import { IDialogRef } from './dialog-ref';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'poppy-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [fadeInAnimation],
})
export class DialogComponent implements IDialogRef, OnInit, AfterViewInit, OnDestroy {
  @ViewChild('container', { read: ViewContainerRef }) container: ViewContainerRef;

  @Output() afterClose = new EventEmitter<any>();
  @Output() afterShow = new EventEmitter();

  private activeDialog: ActiveDialog;
  private destroy$ = new Subject();
  animationState: 'open' | 'close' = 'close';

  constructor(
    @Inject(DIALOG_CONFIG) private config: DialogConfig,
    private ngZone: NgZone,
    private element: ElementRef,
    private cdr: ChangeDetectorRef,
    private hostEl: ElementRef
  ) {}

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.animationState = 'open';
      this.cdr.detectChanges();
      this.listenOnClickOutside();
    }, 0);
    this.setActiveDialog();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    this.activeDialog.contentRef.destroy();
  }

  close(payload?: any): void {
    this.config.service.remove(this.activeDialog.dialogRef, { payload });
  }

  private setActiveDialog(): void {
    this.afterShow.pipe(take(1)).subscribe(() => {
      this.activeDialog = this.config.service.getActiveDialog(this.config.dialogId);
    });
  }

  private listenOnClickOutside(): void {
    this.ngZone.runOutsideAngular(() => {
      const layerEl = this.hostEl.nativeElement.closest('poppy-layer');

      if (layerEl) {
        fromEvent<MouseEvent>(layerEl, 'click')
          .pipe(filter((event) => event.target === layerEl))
          .subscribe(() => {
            this.close();
          });
      }
    });
  }
}
