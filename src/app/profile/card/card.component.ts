import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

import { UserProfile } from '../../_shared/models';
import { GameType } from '../../_shared/interfaces';

@Component({
  selector: 'tf-profile-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileCardComponent implements OnInit {
  @Input() profile: UserProfile;
  @Input() theme: 'light' | 'dark' = 'dark';

  constructor() {}

  get gameRoles(): string[] {
    return (this.profile.roles || []).map((role) => role.name);
  }

  get panelClasses(): { [key: string]: boolean } {
    return {
      [`profile-card--${this.theme}`]: true,
    };
  }

  get canDisplayRank(): boolean {
    return this.profile && (this.profile.type === GameType.LOL || this.profile.type === GameType.CS);
  }

  get canDisplayRoles(): boolean {
    return this.profile && (this.profile.type === GameType.LOL || this.profile.type === GameType.CS);
  }

  get canDisplayLevel(): boolean {
    return this.profile?.type === GameType.FNT;
  }

  ngOnInit(): void {}
}
