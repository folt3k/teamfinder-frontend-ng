import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Store } from '@ngrx/store';

import { GameOption, GAMES } from '../../../_shared/common';
import { AppState } from '../../../app.interface';
import { changeCurrentGame, selectCurrentGame } from '../../_reducer';

@Component({
  selector: 'tf-game-select',
  templateUrl: './game-select.component.html',
  styleUrls: ['./game-select.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class GameSelectComponent implements OnInit {
  options: GameOption[] = GAMES;
  currentGame: GameOption;

  constructor(private store: Store<AppState>) {}

  get panelClasses(): { [key: string]: boolean } {
    return { 'game-select--active': !!this.currentGame, 'game-select--inactive': !this.currentGame };
  }

  ngOnInit(): void {
    this.store.select(selectCurrentGame).subscribe((game) => {
      this.currentGame = game;
    });
  }

  onSelect(game: GameOption): void {
    this.store.dispatch(changeCurrentGame({ gameId: game.value }));
  }
}
