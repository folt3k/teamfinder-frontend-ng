import { AbstractControl, ValidatorFn } from '@angular/forms';

export function noSpecialCharactersValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    return !control.value || control.value.match(/^[a-zA-Z0-9\s]+$/) ? null : { noSpecialCharacters: true };
  };
}
