import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { BaseListRO, ListQueryParams } from '../_shared/interfaces';
import { DEFAULT_PER_PAGE } from '../_shared/common';

import { Announcement, AnnouncementMember } from '../_shared/models';
import { CreateAnnouncementDto, UpdateAnnouncementDto } from './announcements.interface';

@Injectable({
  providedIn: 'root',
})
export class AnnouncementsService {
  constructor(private http: HttpClient) {}

  fetchAll(params: ListQueryParams = {}): Observable<BaseListRO<Announcement>> {
    return this.http.get<BaseListRO<Announcement>>('/game-announcements', {
      params: {
        ...params,
        perPage: params.perPage || DEFAULT_PER_PAGE,
      } as any,
    });
  }

  fetchOne(id: string): Observable<Announcement> {
    return this.http.get<Announcement>(`/game-announcements/${id}`);
  }

  fetchUserOwned(userId: number, params: ListQueryParams = {}): Observable<BaseListRO<Announcement>> {
    return this.http.get<BaseListRO<Announcement>>(`/profiles/${userId}/game-announcements`, {
      params: {
        ...params,
        perPage: params.perPage || DEFAULT_PER_PAGE,
      } as any,
    });
  }

  create(body: CreateAnnouncementDto): Observable<Announcement> {
    return this.http.post<Announcement>('/game-announcements', body);
  }

  update(id: number, body: UpdateAnnouncementDto): Observable<Announcement> {
    return this.http.put<Announcement>(`/game-announcements/${id}`, body);
  }

  remove(id: number): Observable<Announcement> {
    return this.http.delete<Announcement>(`/game-announcements/${id}`);
  }

  addMember(id: number): Observable<AnnouncementMember> {
    return this.http.post<AnnouncementMember>(`/game-announcements/${id}/members`, null);
  }
}
