import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { fromEvent } from 'rxjs';

import { updateUI } from '../../_reducers/ui';
import { SoundService } from './sound.service';

@Injectable({
  providedIn: 'root',
})
export class WindowFocusDetectorService {
  constructor(private store: Store, private soundService: SoundService) {}

  init(): void {
    this.listenOnFocusEvent();
    this.listenOnBlurEvent();
  }

  private listenOnFocusEvent(): void {
    fromEvent(window, 'focus').subscribe(() => {
      this.store.dispatch(updateUI({ isWindowActive: true }));
      this.soundService.enablePlay();
    });
  }

  private listenOnBlurEvent(): void {
    fromEvent(window, 'blur').subscribe(() => {
      this.store.dispatch(updateUI({ isWindowActive: false }));
    });
  }
}
