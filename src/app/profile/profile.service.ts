import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { UserProfile, UserProfileMetadata } from '../_shared/models';
import { ProfilesListParams, UpdateProfileDto } from './profile.interface';
import { BaseListRO } from '../_shared/interfaces';
import { DEFAULT_PER_PAGE } from '../_shared/common';

@Injectable({
  providedIn: 'root',
})
export class ProfileService {
  constructor(private http: HttpClient) {}

  fetchAll(params: ProfilesListParams = {}): Observable<BaseListRO<UserProfile>> {
    return this.http.get<BaseListRO<UserProfile>>('/profiles', {
      params: {
        ...params,
        perPage: params.perPage || DEFAULT_PER_PAGE,
        excluded: params.excluded ? params.excluded.join(',') : [],
      } as any,
    });
  }

  fetchLatest(): Observable<BaseListRO<UserProfile>> {
    return this.http.get<BaseListRO<UserProfile>>('/profiles/latest');
  }

  fetchOne(slug: string): Observable<UserProfile> {
    return this.http.get<UserProfile>(`/profiles/${slug}`);
  }

  update(id: number, body: UpdateProfileDto): Observable<void> {
    return this.http.put<void>(`/profiles/${id}`, body);
  }

  updateGameMetadata(id: number): Observable<UserProfileMetadata> {
    return this.http.put<UserProfileMetadata>(`/profiles/${id}/game-metadata`, null);
  }
}
