import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

export interface ActionMenuOption {
  label: string;
  callback: () => void;
}

@Component({
  selector: 'tf-action-menu',
  templateUrl: './action-menu.component.html',
  styleUrls: ['./action-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActionMenuComponent implements OnInit {
  @Input() actions: ActionMenuOption[] = [];

  constructor() {}

  ngOnInit(): void {}
}
