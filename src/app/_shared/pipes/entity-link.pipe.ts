import { Pipe, PipeTransform } from '@angular/core';

import { BaseEntity, EntityType } from '../interfaces';
import { DYNAMIC_URLS } from '../common';
import { getGameRouterPrefixByType } from '../../games';

@Pipe({
  name: 'entityLink',
})
export class EntityLinkPipe implements PipeTransform {
  transform(entity: BaseEntity): string {
    if (!entity || (entity && !entity.entityType)) {
      return null;
    }

    const identifier: string = entity.entityType === EntityType.ANNOUNCEMENT ? entity.id : entity.slug;

    return DYNAMIC_URLS[entity.entityType.toUpperCase()]({
      identifier,
      gamePrefix: getGameRouterPrefixByType(entity.type),
    }).slashPath;
  }
}
