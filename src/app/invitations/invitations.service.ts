import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { TeamInvitation } from './invitations.model';
import { BaseListRO } from '../_shared/interfaces';
import {
  ListTeamInvitationsQueryParams,
  ListTeamRequestsQueryParams,
  ManageTeamInvitationAction,
  SendTeamInvitationDto,
  SendTeamRequestDto,
} from './invitations.interface';

@Injectable({
  providedIn: 'root',
})
export class TeamInvitationsService {
  constructor(private http: HttpClient) {}

  sendInvitation(body: SendTeamInvitationDto): Observable<TeamInvitation> {
    return this.http.post<TeamInvitation>('/team-invitations', body);
  }

  sendRequest(body: SendTeamRequestDto): Observable<TeamInvitation> {
    return this.http.post<TeamInvitation>('/team-requests', body);
  }

  fetchTeamInvitations(params: ListTeamInvitationsQueryParams): Observable<BaseListRO<TeamInvitation>> {
    return this.http.get<BaseListRO<TeamInvitation>>(`/team-invitations`, { params: params as any });
  }

  fetchTeamRequests(params: ListTeamRequestsQueryParams): Observable<BaseListRO<TeamInvitation>> {
    return this.http.get<BaseListRO<TeamInvitation>>(`/team-requests`, { params: params as any });
  }

  manageRequest(id: number, action: ManageTeamInvitationAction): Observable<TeamInvitation> {
    return this.http.patch<TeamInvitation>(`/team-requests/${id}/${action}`, null);
  }

  manageInvitation(id: number, action: ManageTeamInvitationAction): Observable<TeamInvitation> {
    return this.http.patch<TeamInvitation>(`/team-invitations/${id}/${action}`, null);
  }
}
