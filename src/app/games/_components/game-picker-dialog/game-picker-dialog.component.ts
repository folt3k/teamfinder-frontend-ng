import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { DIALOG_DATA } from '../../../_shared/poppy/dialog/dialog.token';
import { DialogRef } from '../../../_shared/poppy/dialog';
import { GameOption, STATIC_URLS } from '../../../_shared/common';
import { changeCurrentGame } from '../../_reducer';

@Component({
  selector: 'tf-game-picker-dialog',
  templateUrl: './game-picker-dialog.component.html',
  styleUrls: ['./game-picker-dialog.component.scss'],
})
export class GamePickerDialogComponent implements OnInit {
  constructor(
    private dialogRef: DialogRef,
    @Inject(DIALOG_DATA) private dialogData,
    private router: Router,
    private store: Store
  ) {}

  ngOnInit(): void {}

  onGameSelect(game: GameOption): void {
    this.store.dispatch(changeCurrentGame({ gameId: game.value }));
    this.dialogRef.close();
    this.router.navigate([`${game.slashRouterPrefix}/${this.dialogData.link}`]);
  }

  onLogin(): void {
    this.dialogRef.close();
    this.router.navigate([STATIC_URLS['login'].slashPath]);
  }
}
