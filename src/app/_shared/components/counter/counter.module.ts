import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CounterComponent } from './counter.component';
import { CounterPipe } from './counter.pipe';

@NgModule({
  declarations: [CounterComponent, CounterPipe],
  imports: [CommonModule],
  exports: [CounterComponent],
})
export class CounterModule {}
