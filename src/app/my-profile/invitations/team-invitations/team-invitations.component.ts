import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { AppState } from '../../../app.interface';
import {
  fetchInvitations,
  fetchRequests,
  selectTeamInvitations,
  selectTeamRequests,
} from '../../../invitations/_reducer';
import { BaseListRO } from '../../../_shared/interfaces';
import { TeamInvitation } from '../../../invitations/invitations.model';

@Component({
  selector: '\tf-my-profile-team-invitations',
  templateUrl: './team-invitations.component.html',
  styleUrls: ['./team-invitations.component.scss'],
})
export class MyProfileTeamInvitationsComponent implements OnInit {
  sentInvitations$: Observable<BaseListRO<TeamInvitation>>;
  receivedRequests$: Observable<BaseListRO<TeamInvitation>>;
  activeTab: 'sentInvitations' | 'receivedRequests';

  constructor(private store: Store<AppState>, private route: ActivatedRoute) {}

  ngOnInit(): void {
    if (this.route.snapshot.fragment) {
      switch (this.route.snapshot.fragment) {
        case 'otrzymane':
          this.activeTab = 'receivedRequests';
          break;
        case 'wyslane':
          this.activeTab = 'sentInvitations';
          break;
      }
    } else {
      this.activeTab = 'sentInvitations';
    }

    this.store.dispatch(fetchInvitations({ params: { scope: 'sentInvitations' } }));
    this.store.dispatch(fetchRequests({ params: { scope: 'receivedRequests' } }));

    this.sentInvitations$ = this.store.select(selectTeamInvitations);
    this.receivedRequests$ = this.store.select(selectTeamRequests);
  }
}
