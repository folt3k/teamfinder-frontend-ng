import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DYNAMIC_URLS, STATIC_URLS } from '../_shared/common';

const routes: Routes = [
  {
    path: STATIC_URLS['users'].path,
    loadChildren: () => import('./all/all.module').then((m) => m.AllProfilesModule),
  },
  {
    path: DYNAMIC_URLS.USER().symbolPath,
    loadChildren: () => import('./details/details.module').then((m) => m.ProfileDetailsModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileRoutingModule {}
