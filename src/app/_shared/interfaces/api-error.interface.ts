export interface ErrorRO {
  statusCode: number;
  message: string;
}
