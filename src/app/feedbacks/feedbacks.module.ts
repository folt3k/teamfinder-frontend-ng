import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FeedbackCompactItemComponent } from './item/item-compact/item-compact.component';
import { SharedModule } from '../_shared/shared.module';
import { FeedbackCardItemComponent } from './item/item-card/item-card.component';
import { TeamModule } from '../team/team.module';
import { AnnouncementsModule } from '../announcements/announcements.module';
import { FeedbackItemPlaceholderComponent } from './item/item-placeholder/item-placeholder.component';

@NgModule({
  declarations: [FeedbackCompactItemComponent, FeedbackCardItemComponent, FeedbackItemPlaceholderComponent],
  imports: [CommonModule, RouterModule, SharedModule, TeamModule, AnnouncementsModule],
  exports: [FeedbackCompactItemComponent, FeedbackCardItemComponent, FeedbackItemPlaceholderComponent],
})
export class FeedbacksModule {}
