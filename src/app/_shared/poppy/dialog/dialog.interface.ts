import { ComponentRef } from '@angular/core';

import { DialogComponent } from './dialog.component';

export interface ActiveDialog {
  dialogId: number;
  dialogRef: ComponentRef<DialogComponent>;
  contentRef: ComponentRef<any>;
  uniqueId?: DialogUniqueId;
}

export interface DialogOptions {
  data?: any;
  uniqueId?: DialogUniqueId;
}

export type DialogUniqueId = string;
