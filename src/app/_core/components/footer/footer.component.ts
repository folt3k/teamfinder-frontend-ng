import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { MENU_LINKS, MenuLink, STATIC_URLS } from '../../../_shared/common';
import { selectCurrentGame } from '../../../games/_reducer';
import { AppState } from '../../../app.interface';

@Component({
  selector: 'tf-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent implements OnInit {
  links: MenuLink[] = [];
  STATIC_URLS = STATIC_URLS;

  constructor(private store: Store<AppState>, private cdr: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.listenOnCurrentGameChange();
  }

  private listenOnCurrentGameChange(): void {
    this.store.select(selectCurrentGame).subscribe((game) => {
      this.links = MENU_LINKS.map((link) => ({
        ...link,
        gamePrefix: game?.slashRouterPrefix,
      }));
      this.cdr.detectChanges();
    });
  }
}
