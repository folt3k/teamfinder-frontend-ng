export const featureKey = 'game';

export const types = {
  ChangeCurrentGame: '[Game] Change current game',

  FetchLolGamesHistory: '[Game] Fetch LoL games history',
  FetchLolGamesHistorySuccess: '[Game] Fetch LoL games history success',
  FetchLolGamesHistoryFailed: '[Game] Fetch LoL games history failed',

  ResetGamesHistory: '[Game] Reset games history',
};
