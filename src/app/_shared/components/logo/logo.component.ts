import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'tf-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LogoComponent {
  constructor() {}
}
