import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import {
  BasicDatePipe,
  DecodePipe,
  ImageUrlPipe,
  UserAgePipe,
  PercentPipe,
  EntityLinkPipe,
  GamePrefixLinkPipe,
  TruncatePipe,
  InteractiveTextPipe,
  MinutesDurationPipe,
  MapObjectPipe,
} from './';

const PIPES = [
  BasicDatePipe,
  DecodePipe,
  ImageUrlPipe,
  UserAgePipe,
  PercentPipe,
  EntityLinkPipe,
  GamePrefixLinkPipe,
  TruncatePipe,
  InteractiveTextPipe,
  MinutesDurationPipe,
  MapObjectPipe,
];

@NgModule({
  declarations: [...PIPES],
  imports: [CommonModule],
  exports: [...PIPES],
})
export class PipesModule {}
