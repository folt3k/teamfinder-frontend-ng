import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'tf-fnt-game-mode-stats',
  templateUrl: './fnt-game-mode-stats.component.html',
  styleUrls: ['./fnt-game-mode-stats.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FntGameModeStatsComponent implements OnInit {
  @Input() data: { mode: string; label: string; stats: { matches: number; stats: any[] } };

  constructor() {}

  ngOnInit(): void {}
}
