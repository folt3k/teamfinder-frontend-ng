import { BaseEntity, BaseListRO } from '../../_shared/interfaces';
import { UserProfile } from '../../_shared/models';
import { ChatMessage } from './chat-message.model';
import { BaseReducerState } from '../../_reducers';
import { ChatType, PrivateChatSenderType } from '../chat.inerface';
import { Team } from '../../team';

export interface ChatRoom extends BaseEntity {
  chatType: ChatType;
}

export interface PrivateChatRoomRO extends ChatRoom {
  host: UserProfile;
  member: UserProfile;
  hostUnreadCount: number;
  memberUnreadCount: number;
  messages: BaseListRO<ChatMessage> & BaseReducerState;
}

export interface PrivateChatRoom extends PrivateChatRoomRO {
  isCurrentUserAuthorOfLastMessage: boolean;
  memberProfile: UserProfile;
  currentProfile: UserProfile;
  active: boolean;
  maximized: boolean;
  wasInitialMessagesFetched: boolean;
  currentProfileSenderType: PrivateChatSenderType;
  getCurrentProfileUnreadCount: () => number;
  wasNewConversationCreated?: boolean;
  lastMessageDate: Date;
}

export interface TeamChatRoomRO extends ChatRoom {
  team: Team;
  messages: BaseListRO<ChatMessage> & BaseReducerState;
  unreadMessages: number;
}

export interface TeamChatRoom extends TeamChatRoomRO {
  isCurrentUserAuthorOfLastMessage: boolean;
  currentProfile: UserProfile;
  active: boolean;
  maximized: boolean;
  wasInitialMessagesFetched: boolean;
  lastMessageDate: Date;
  // currentProfileSenderType: ChatSenderType;
}

const serializePrivateChatRoom = (
  entity: PrivateChatRoomRO,
  { currentProfile }: { currentProfile: UserProfile }
): PrivateChatRoom => ({
  ...entity,
  isCurrentUserAuthorOfLastMessage: entity.messages.results[0].senderId === currentProfile.id,
  memberProfile: entity.host.id === currentProfile.id ? entity.member : entity.host,
  currentProfile: entity.host.id === currentProfile.id ? entity.host : entity.member,
  active: false,
  maximized: false,
  wasInitialMessagesFetched: false,
  currentProfileSenderType: entity.host.id === currentProfile.id ? 'host' : 'member',
  messages: {
    ...entity.messages,
    loading: false,
    error: null,
  },
  getCurrentProfileUnreadCount(): number {
    return this[`${this.currentProfileSenderType}UnreadCount`];
  },
  chatType: ChatType.PRIVATE,
  lastMessageDate: entity.messages.results[entity.messages.results.length - 1].updatedAt,
});

export const serializeTeamChatRoom = (
  entity: TeamChatRoomRO,
  { currentProfile }: { currentProfile: UserProfile }
): TeamChatRoom => ({
  ...entity,
  isCurrentUserAuthorOfLastMessage: entity.messages.results[0]?.senderId === currentProfile.id,
  currentProfile,
  active: false,
  maximized: false,
  wasInitialMessagesFetched: false,
  messages: {
    ...entity.messages,
    loading: false,
    error: null,
  },
  chatType: ChatType.TEAM,
  lastMessageDate: entity.messages.results[entity.messages.results.length - 1]?.updatedAt || entity.updatedAt,
});

export const serializePrivateChatRooms = (
  entities: PrivateChatRoomRO[],
  options: { currentProfile: UserProfile }
) => entities.map((entity) => serializePrivateChatRoom(entity, options));

export const serializeTeamChatRooms = (
  entities: TeamChatRoomRO[],
  options: { currentProfile: UserProfile }
) => entities.map((entity) => serializeTeamChatRoom(entity, options));
