import { createReducer, on } from '@ngrx/store';
import { omit } from 'lodash';

import * as actions from './actions';
import { Dictionaries } from 'src/app/_shared/models';

export interface State extends Dictionaries {
  loading: boolean;
}

export const initialState: State = {
  rankCategories: {
    lol: [],
  },
  roles: {
    lol: [],
    cs: [],
    fnt: [],
  },
  ranks: {
    lol: [],
    cs: [],
    fnt: [],
  },
  modes: {
    lol: [],
    cs: [],
    fnt: [],
  },
  servers: {
    lol: [],
    cs: [],
    fnt: [],
  },
  champions: [],
  birthYears: [],
  loading: true,
  gameApiVersion: null,
};

export const reducer = createReducer(
  initialState,
  on(actions.fetchDictionariesSuccess, (state, payload) => {
    return { ...state, ...omit(payload, ['type']), loading: false };
  })
);
