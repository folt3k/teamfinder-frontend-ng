import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'tf-registration-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss'],
})
export class ReviewComponent implements OnInit {
  @Input() success: boolean = false;
  @Input() registeredEmail: string;

  constructor() {}

  ngOnInit(): void {}
}
