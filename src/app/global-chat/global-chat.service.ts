import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { GlobalChatInitRO } from './global-chat.interface';

@Injectable({
  providedIn: 'root',
})
export class GlobalChatService {
  constructor(private http: HttpClient) {}

  init(): Observable<GlobalChatInitRO> {
    return this.http.get<GlobalChatInitRO>('/global-chat');
  }
}
