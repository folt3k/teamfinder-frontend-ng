import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EntitiesScreenComponent } from './entities-screen.component';
import { FiltersModule } from './filters/filters.module';
import { SharedModule } from '../_shared/shared.module';
import { ListComponent } from './list/list.component';
import { ProfileModule } from '../profile/profile.module';
import { EntitiesScreenService } from './entities-screen.service';
import { TeamModule } from '../team/team.module';
import { AnnouncementsModule } from '../announcements/announcements.module';

@NgModule({
  declarations: [EntitiesScreenComponent, ListComponent],
  imports: [CommonModule, SharedModule, FiltersModule, ProfileModule, TeamModule, AnnouncementsModule],
  exports: [EntitiesScreenComponent],
  providers: [],
})
export class EntitiesScreenModule {}
