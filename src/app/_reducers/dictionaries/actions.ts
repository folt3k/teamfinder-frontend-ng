import { createAction, props } from '@ngrx/store';

import { types } from './constants';
import { Dictionaries } from '../../_shared/models';

export const fetchDictionaries = createAction(types.FetchDictionaries);
export const fetchDictionariesSuccess = createAction(
  types.FetchDictionariesSuccess,
  props<Partial<Dictionaries>>()
);
