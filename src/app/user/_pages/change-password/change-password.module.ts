import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ChangePasswordComponent } from './change-password.component';
import { SharedModule } from '../../../_shared/shared.module';

@NgModule({
  declarations: [ChangePasswordComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: ChangePasswordComponent,
      },
    ]),
    SharedModule,
  ],
})
export class ChangePasswordModule {}
