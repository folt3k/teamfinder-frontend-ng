import { Directive, HostListener } from '@angular/core';
import { Store } from '@ngrx/store';
import { closeChat } from '../_reducer';

@Directive({
  selector: '[tfCloseGlobalChatOnClick]',
})
export class CloseGlobalChatOnClickDirective {
  @HostListener('click')
  onClick(): void {
    this.store.dispatch(closeChat());
  }

  constructor(private store: Store) {}
}
