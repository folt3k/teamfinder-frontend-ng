import { UserProfile } from '../../_shared/models';
import { GlobalChatChannel } from '../global-chat.interface';

export interface GlobalChatMessage {
  id: number;
  content: string;
  sender: UserProfile;
  channel: GlobalChatChannel;
  createdAt: Date;
}
