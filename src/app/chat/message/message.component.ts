import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { ChatMessage } from '../_models/chat-message.model';
import { UserProfile } from '../../_shared/models';
import { PrivateChatRoom, TeamChatRoom } from '../_models/chat-room.model';

@Component({
  selector: 'tf-chat-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessageComponent implements OnInit {
  @Input() message: ChatMessage;
  @Input() sender: UserProfile;
  @Input() room: TeamChatRoom | PrivateChatRoom;

  constructor() {}

  get panelClasses(): { [key: string]: boolean } {
    return {
      'chat-message--inverse': this.isCurrentUserMessageSender,
    };
  }

  get isCurrentUserMessageSender(): boolean {
    return this.message.senderId === this.room.currentProfile.id;
  }

  ngOnInit(): void {}
}
