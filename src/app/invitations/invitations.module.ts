import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';

import * as fromInvitations from './_reducer';
import { StoreModule } from '@ngrx/store';
import { InvitationItemComponent } from './item/item.component';
import { PipesModule } from '../_shared/pipes/pipes.module';
import { RouterModule } from '@angular/router';
import { InviteMemberDialogComponent } from './invite-member-dialog/invite-member-dialog.component';
import { SharedModule } from '../_shared/shared.module';

@NgModule({
  declarations: [InvitationItemComponent, InviteMemberDialogComponent],
  imports: [
    CommonModule,
    EffectsModule.forFeature([fromInvitations.TeamInvitationsEffects]),
    StoreModule.forFeature(fromInvitations.featureKey, fromInvitations.reducer),
    PipesModule,
    RouterModule,
    SharedModule,
  ],
  exports: [InvitationItemComponent],
})
export class TeamInvitationsModule {}
