import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, filter, map, switchMap } from 'rxjs/operators';

import { AppState } from '../../app.interface';
import * as actions from './actions';
import { TeamService } from '../../team/team.service';
import { EntityType } from '../entity-details.interface';
import { BaseEntity, GameType } from '../../_shared/interfaces';
import { ProfileService } from '../../profile/profile.service';
import { AnnouncementsService } from '../../announcements/announcements.service';
import * as fromGames from '../../games/_reducer';

@Injectable()
export class EntityDetailsEffects {
  fetchDetails$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.fetchEntityDetails),
      switchMap((action) => {
        return this.getFetchEntityObs(action.entityType, action.identifier).pipe(
          map((data) => actions.fetchEntityDetailsSuccess({ data })),
          catchError((err) => of(actions.fetchEntityDetailsFailed(err)))
        );
      })
    )
  );

  fetchDetailsSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.fetchEntityDetailsSuccess),
        filter((action) => action.data.entityType === EntityType.USER),
        map((action) => {
          switch (action.data.type) {
            case GameType.LOL:
              this.store.dispatch(fromGames.fetchLolGamesHistory());
              break;
          }
        })
      ),
    { dispatch: false }
  );

  private getFetchEntityObs(type: EntityType, identifier: string): Observable<BaseEntity> {
    switch (type) {
      case EntityType.TEAM:
        return this.teamService.fetchOne(identifier);
      case EntityType.USER:
        return this.profileService.fetchOne(identifier);
      case EntityType.ANNOUNCEMENT:
        return this.announcementService.fetchOne(identifier);
      default:
        return of(null);
    }
  }

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private teamService: TeamService,
    private profileService: ProfileService,
    private announcementService: AnnouncementsService
  ) {}
}
