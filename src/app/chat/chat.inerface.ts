import { PrivateChatRoomRO, TeamChatRoomRO } from './_models/chat-room.model';
import { ChatMessage } from './_models/chat-message.model';

export type FetchPrivateChatRoomsRO = PrivateChatRoomRO[];
export type FetchTeamChatRoomsRO = { results: TeamChatRoomRO[]; unreadMessages: number };

export interface ReceivePrivateChatMessageRO {
  roomId: number;
  chatType: ChatType;
  message: ChatMessage;
  senderType?: PrivateChatSenderType;
  type?: PrivateChatSenderType;
}

export interface ReceiveTeamChatMessageRO {
  roomId: number;
  senderType?: PrivateChatSenderType;
  message: ChatMessage;
  type?: PrivateChatSenderType;
}

export type SendChatMessageRO = ChatMessage & { room: number; chatType: ChatType; roomId?: number };

export interface CurrentUserJoinToTeamRO {
  chatRoom: TeamChatRoomRO;
}

export type PrivateChatSenderType = 'host' | 'member';

export enum ChatType {
  PRIVATE = 'private',
  TEAM = 'team',
}
