import { STATIC_URLS } from './path';

export interface MenuLink {
  key: string;
  label: string;
  path: string;
  needsGamePrefix: boolean;
  gamePrefix?: string;
  canDisplayInHeader?: boolean;
}

export const MENU_LINKS: MenuLink[] = [
  {
    key: 'users',
    label: 'Znajdź gracza',
    path: STATIC_URLS['users'].slashPath,
    canDisplayInHeader: true,
    needsGamePrefix: true,
  },
  {
    key: 'teams',
    label: 'Znajdź team',
    path: STATIC_URLS['teams'].slashPath,
    canDisplayInHeader: true,
    needsGamePrefix: true,
  },
  {
    key: 'add-team',
    label: 'Dodaj drużynę',
    path: STATIC_URLS['add-team'].slashPath,
    canDisplayInHeader: true,
    needsGamePrefix: false,
  },
  {
    key: 'announcements',
    label: 'Wspólne granie',
    path: STATIC_URLS['announcements'].slashPath,
    canDisplayInHeader: true,
    needsGamePrefix: true,
  },
  {
    key: 'my-profil',
    label: 'Mój profil',
    path: STATIC_URLS['my-profile'].slashPath,
    canDisplayInHeader: false,
    needsGamePrefix: false,
  },
  {
    key: 'contact',
    label: 'Kontakt',
    path: STATIC_URLS['contact'].slashPath,
    canDisplayInHeader: false,
    needsGamePrefix: false,
  },
];
