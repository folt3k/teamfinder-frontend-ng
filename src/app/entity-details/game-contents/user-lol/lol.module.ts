import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LolDetailsContentComponent } from './lol.component';
import { LolDetailsContentService } from './lol.service';
import { LolGamesHistoryModule } from '../../../games/_components/lol-games-history/lol-games-history.module';

@NgModule({
  declarations: [LolDetailsContentComponent],
  imports: [CommonModule, LolGamesHistoryModule],
  exports: [LolDetailsContentComponent],
  providers: [LolDetailsContentService],
})
export class LolDetailsContentModule {}
