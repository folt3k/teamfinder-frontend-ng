import {
  Component,
  OnInit,
  Inject,
  TemplateRef,
  ElementRef,
  Output,
  EventEmitter,
  AfterViewInit,
  ChangeDetectionStrategy,
  OnDestroy,
  HostBinding,
} from '@angular/core';
import { timer, fromEvent, Subject } from 'rxjs';
import { takeUntil, repeatWhen } from 'rxjs/operators';

import { ALERT_CONFIG } from './alert.token';
import { AlertOptions, AlertConfig, AlertTheme } from './alert.interface';
import { fadeInOut, FADE_IN_DURATION, FADE_OUT_DURATION } from './alert.animations';

@Component({
  selector: 'tf-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [fadeInOut],
})
export class AlertComponent implements OnInit, OnDestroy, AfterViewInit {
  @Output() closed = new EventEmitter();

  content: string | TemplateRef<any>;
  options: AlertOptions = {};
  animationState: 'open' | 'close' = 'close';
  private destroy = new Subject();

  constructor(@Inject(ALERT_CONFIG) private config: AlertConfig, public host: ElementRef) {}

  @HostBinding('@fadeInOut') get fadeInOut(): string {
    return this.animationState;
  }

  ngOnInit(): void {
    this.content = this.config.content;
    this.options = this.config.options;
  }

  ngAfterViewInit(): void {
    this.initDurationCloseTimer();
    setTimeout(() => {
      this.animationState = 'open';
    }, FADE_IN_DURATION);
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }

  get panelClasses(): { [key: string]: boolean } {
    return {
      [`tf-alert--success`]: this.options.theme === AlertTheme.SUCCESS,
      ['tf-alert--failed']: this.options.theme === AlertTheme.FAILED,
    };
  }

  onClose(): void {
    this.close();
  }

  isContentString(): boolean {
    return typeof this.content === 'string';
  }

  private initDurationCloseTimer(): void {
    const mouseenter$ = fromEvent(this.host.nativeElement, 'mouseenter').pipe(takeUntil(this.destroy));
    const mouseleave$ = fromEvent(this.host.nativeElement, 'mouseleave').pipe(takeUntil(this.destroy));

    timer(this.options.duration)
      .pipe(
        takeUntil(this.destroy),
        takeUntil(mouseenter$),
        repeatWhen(() => mouseleave$)
      )
      .subscribe(() => {
        this.close();
      });
  }

  private close(): void {
    this.animationState = 'close';
    setTimeout(() => {
      this.closed.emit();
    }, FADE_OUT_DURATION);
  }
}
