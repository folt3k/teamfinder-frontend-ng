import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { LolGamesHistoryItem } from '../../games.interface';
import { AppState } from '../../../app.interface';
import { fetchLolGamesHistory, resetGamesHistory, selectGameHistory } from '../../_reducer';

@Component({
  selector: 'tf-lol-games-history',
  templateUrl: './lol-games-history.component.html',
  styleUrls: ['./lol-games-history.component.scss'],
})
export class LolGamesHistoryComponent implements OnInit, OnDestroy {
  results: LolGamesHistoryItem[] = [];
  loading: boolean = false;

  private destroy$ = new Subject();

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store
      .select(selectGameHistory)
      .pipe(takeUntil(this.destroy$))
      .subscribe((data) => {
        this.results = data.results;
        this.loading = data.isLoading;
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
    this.store.dispatch(resetGamesHistory());
  }

  loadMore(): void {
    this.store.dispatch(fetchLolGamesHistory());
  }
}
