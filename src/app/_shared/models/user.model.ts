export interface User {
  id: number;
  email: string;
  createdAt: Date;
  updatedAt: Date;
  age: number;
  disabled: boolean;
  active: boolean;
  banned: boolean;
  lastDisplayGlobalChatMessages: Date;
  mailNotificationSettings: UserMailNotificationSettings;
}

export interface UserMailNotificationSettings {
  enableAppNotifications: boolean;
  enableGameAnnouncementNotifications: boolean;
  enablePrivateMessageNotifications: boolean;
  enableTeamCommentNotifications: boolean;
  enableTeamRequestNotifications: boolean;
  [key: string]: boolean;
}
