import { AppState } from './../../app.interface';

export const error = (state: AppState) => state.auth.error;

export const loading = (state: AppState) => state.auth.loading;
