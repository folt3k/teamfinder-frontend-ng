import { Component, OnInit } from '@angular/core';

import { STATIC_URLS } from '../../_shared/common';

@Component({
  selector: 'tf-home-jumbotron',
  templateUrl: './jumbotron.component.html',
  styleUrls: ['./jumbotron.component.scss'],
})
export class JumbotronComponent implements OnInit {
  STATIC_URLS = STATIC_URLS;

  constructor() {}

  ngOnInit(): void {}
}
