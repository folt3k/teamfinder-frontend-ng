import { ChangeDetectionStrategy, Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { Store } from '@ngrx/store';
import { Notification, NotificationType } from '../notification.model';
import { markAsRead } from '../_reducer';
import { MY_PROFILE_STATIC_URLS, STATIC_URLS } from '../../_shared/common';
import { EntityLinkPipe } from '../../_shared/pipes';

@Component({
  selector: 'tf-notification-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ItemComponent implements OnInit {
  @Input() item: Notification;

  NOTIFICATION_TYPE = NotificationType;
  constructor(private store: Store, private router: Router) {}

  get panelClasses(): { [key: string]: boolean } {
    return {
      'notification-item--unread': !this.item.wasRead,
    };
  }

  ngOnInit(): void {}

  onClick(): void {
    if (!this.item.wasRead) {
      this.store.dispatch(markAsRead({ id: this.item.id }));
    }
    this.navigate();
  }

  private navigate(): void {
    const entityLink = new EntityLinkPipe().transform;
    const { team, gameAnnouncement } = this.item;

    switch (this.item.type) {
      case NotificationType.USER_INVITED_TO_TEAM:
        this.router.navigate([MY_PROFILE_STATIC_URLS['my-invitations'].slashPath], { fragment: 'otrzymane' });
        break;
      case NotificationType.USER_JOINED_TO_TEAM:
        this.router.navigate([entityLink(team)]);
        break;
      case NotificationType.USER_REQUESTED_TO_TEAM:
        this.router.navigate([MY_PROFILE_STATIC_URLS['team-invitations'].slashPath], {
          fragment: 'otrzymane',
        });
        break;
      case NotificationType.USER_ACCEPT_INVITATION:
        this.router.navigate([entityLink(team)]);
        break;
      case NotificationType.USER_COMMENTED_TEAM:
        this.router.navigate([entityLink(team)]);
        break;
      case NotificationType.USER_COMMENTED_GAME_ANNOUNCEMENT:
        this.router.navigate([entityLink(gameAnnouncement)]);
        break;
      case NotificationType.USER_RESPONSE_TO_TEAM_COMMENT:
        this.router.navigate([entityLink(team)]);
        break;
      case NotificationType.USER_RESPONSE_TO_GAME_ANNOUNCEMENT_COMMENT:
        this.router.navigate([entityLink(gameAnnouncement)]);
        break;
      case NotificationType.USER_JOINED_TO_GAME_ANNOUNCEMENT:
        this.router.navigate([entityLink(gameAnnouncement)]);
        break;
      case NotificationType.USER_JOINED_TO_APP:
        this.router.navigate([STATIC_URLS['add-team'].slashPath]);
        break;
    }
  }
}
