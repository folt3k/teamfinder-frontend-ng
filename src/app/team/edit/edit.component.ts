import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AppState } from '../../app.interface';
import { CreateTeamDto } from '../team.interface';
import { updateTeam } from '../_reducer';
import { selectDetails } from '../../entity-details/_reducer';

@Component({
  selector: 'tf-edit-team',
  templateUrl: './edit.component.html',
})
export class EditTeamComponent implements OnInit {
  teamName$: Observable<string>;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.teamName$ = this.store.select(selectDetails).pipe(map((details) => details.name));
  }

  onSubmit(values: CreateTeamDto): void {
    this.store.dispatch(updateTeam({ body: values }));
  }
}
