import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import * as Sentry from '@sentry/angular';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { hmrBootstrap } from './hmr';

if (environment.production) {
  enableProdMode();

  Sentry.init({
    dsn: 'https://3e1cd42c20da4c7f83cf2a42d7cf6372@o297387.ingest.sentry.io/5256960',

    // Set tracesSampleRate to 1.0 to capture 100%
    // of transactions for performance monitoring.
    // We recommend adjusting this value in production
    tracesSampleRate: 1.0,
  });
}

document.addEventListener('DOMContentLoaded', () => {
  const bootstrap = () => platformBrowserDynamic().bootstrapModule(AppModule);

  if (!environment.production && module['hot']) {
    hmrBootstrap(module, bootstrap);
  } else {
    bootstrap().catch((err) => console.log(err));
  }
});
