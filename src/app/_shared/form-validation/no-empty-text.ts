import { AbstractControl, ValidatorFn } from '@angular/forms';

export function noEmptyTextValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    return !control.value || control.value.trim() === '' ? { noEmptyText: true } : null;
  };
}
