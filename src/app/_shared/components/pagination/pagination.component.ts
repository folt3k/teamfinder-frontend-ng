import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
  EventEmitter,
} from '@angular/core';

import { Pagination } from '../../interfaces';
import { PaginationService } from './pagination.service';

@Component({
  selector: 'tf-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [PaginationService],
})
export class PaginationComponent implements OnChanges {
  @Input() data: Pagination;
  @Output() pageChanged = new EventEmitter<number>();

  items: Array<'more' | 'less' | number> = [];
  totalPages = 0;

  constructor(private service: PaginationService) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.data.currentValue) {
      const { currentPage, totalResults, resultsPerPage } = this.data;
      this.totalPages = Math.ceil(totalResults / resultsPerPage);
      this.items = this.service.paginate(currentPage, this.totalPages);
    }
  }

  get isRightArrowDisabled(): boolean {
    const { currentPage, totalResults, resultsPerPage } = this.data;
    return currentPage === Math.ceil(totalResults / resultsPerPage);
  }

  get isLeftArrowDisabled(): boolean {
    return this.data.currentPage === 1;
  }

  pagin(page: number): void {
    this.pageChanged.emit(page);
  }
}
