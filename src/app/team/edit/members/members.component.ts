import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map, take, withLatestFrom } from 'rxjs/operators';
import { selectDetails } from '../../../entity-details/_reducer';
import { AppState } from '../../../app.interface';
import { sendInvitation } from '../../../invitations/_reducer';
import { TeamMember } from '../../team.model';
import { DialogService } from '../../../_shared/poppy/dialog';
import { ConfirmationComponent } from '../../../_shared/components/confirmation/confirmation.component';
import { removeTeamMember } from '../../_reducer';

@Component({
  selector: 'tf-edit-team-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss'],
})
export class EditTeamMembersComponent implements OnInit {
  selectedUser = new FormControl(null, [Validators.required]);
  members$: Observable<TeamMember[]>;

  constructor(private store: Store<AppState>, private dialogService: DialogService) {}

  ngOnInit(): void {
    this.members$ = this.store.select(selectDetails).pipe(map((details) => details.members));
  }

  onSubmit(): void {
    this.store
      .select(selectDetails)
      .pipe(take(1))
      .subscribe((details) => {
        this.store.dispatch(
          sendInvitation({ body: { profileId: this.selectedUser.value.id, teamId: details.id } })
        );
      });
    this.selectedUser.reset();
  }

  remove(member: TeamMember): void {
    const dialogRef = this.dialogService.open(ConfirmationComponent, {
      data: { question: 'Czy na pewno chcesz usunąć tego gracza ze swojej drużyny?' },
    });

    dialogRef.afterClose
      .pipe(
        take(1),
        withLatestFrom(this.store.select(selectDetails)),
        filter(([response]) => !!response)
      )
      .subscribe(([_, details]) => {
        this.store.dispatch(removeTeamMember({ profileId: member.profile.id, teamId: details.id }));
      });
  }
}
