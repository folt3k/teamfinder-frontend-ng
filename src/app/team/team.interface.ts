import { Team } from './team.model';
import { TeamChatRoomRO } from '../chat/_models/chat-room.model';

export interface CreateTeamDto {
  name: string;
  avatar: string;
  desc: string;
  modes: number[];
  roles: number[];
  rank: number;
  gameServer: string;
}

export interface CreateTeamRO {
  team: Team;
  chatRoom: TeamChatRoomRO;
}

export interface CheckNameAvailabilityDto {
  name: string;
}

export interface CheckNameAvailabilityRO {
  isAvailable: string;
}

export interface RemoveTeamMemberDto {
  teamId: number;
  profileId: number;
  redirectFromCurrentPage?: boolean;
}
