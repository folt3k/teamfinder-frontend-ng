import { BaseEntity } from '../../_shared/interfaces';
import { UserProfile } from '../../_shared/models';

export interface ChatMessage extends BaseEntity {
  content: string;
  sender?: UserProfile;
  senderId?: number;
}
