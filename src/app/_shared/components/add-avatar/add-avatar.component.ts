import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { filter, map } from 'rxjs/operators';

import { DialogService } from '../../poppy';
import { AddAvatarDialogComponent } from './dialog/dialog.component';
import { ImageUrlPipe } from '../../pipes';

@Component({
  selector: 'tf-add-avatar',
  templateUrl: './add-avatar.component.html',
  styleUrls: ['./add-avatar.component.scss'],
})
export class AddAvatarComponent {
  @Output() selected = new EventEmitter<string>();
  @Input()
  set thumbImage(value: string) {
    if (value) {
      this._thumbImage = new ImageUrlPipe().transform(value);
    }
  }
  get thumbImage(): string {
    return this._thumbImage;
  }

  private _thumbImage: string = new ImageUrlPipe().transform('/images/avatars/noavatar.png');

  constructor(private dialogService: DialogService) {}

  showDialog(): void {
    const dialogRef = this.dialogService.open(AddAvatarDialogComponent);

    dialogRef.afterClose
      .pipe(
        filter((payload) => payload?.image),
        map((payload) => payload.image)
      )
      .subscribe((image) => {
        this.selected.emit(image);
        this._thumbImage = image;
      });
  }
}
