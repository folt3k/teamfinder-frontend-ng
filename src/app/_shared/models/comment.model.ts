import { EntityType } from '../interfaces';
import { UserProfile } from './user-profile.model';

export interface Comment {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  content: string;
  entityType: EntityType;
  entityId: number;
  author: UserProfile;
  subcommentsCount: number;
  currentUserLikes: number;
  likesCount: number;
  subcomments: Comment[];
  showSubcomments?: boolean;
}
