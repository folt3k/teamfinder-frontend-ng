import { Injectable } from '@angular/core';
import { AsyncValidator, AbstractControl, ValidationErrors } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable, of, timer } from 'rxjs';
import { map, catchError, first, switchMap, withLatestFrom } from 'rxjs/operators';

import { UserService } from '../../user/user.service';
import { TeamService } from '../../team/team.service';
import { AppState } from '../../app.interface';
import { selectCurrentGame } from '../../games/_reducer';
import { GameType } from '../interfaces';

@Injectable()
export class UsernameAvailableAsyncValidator implements AsyncValidator {
  constructor(private usersService: UserService) {}

  validate(control: AbstractControl): Observable<ValidationErrors | null> {
    return timer(1000).pipe(
      switchMap(() => {
        return this.usersService
          .checkRegistrationDataAvailability({
            type: 'username',
            value: control.value,
            gameType: '1',
          })
          .pipe(
            map((data) => (!data.isUsernameAvailable ? { usernameAvailable: true } : null)),
            first(),
            catchError(() => of(null))
          );
      })
    );
  }
}

@Injectable()
export class EmailAvailableAsyncValidator implements AsyncValidator {
  constructor(private usersService: UserService) {}

  validate(control: AbstractControl): Observable<ValidationErrors | null> {
    return timer(1000).pipe(
      switchMap(() => {
        return this.usersService
          .checkRegistrationDataAvailability({
            type: 'email',
            value: control.value,
            gameType: '1',
          })
          .pipe(
            map((data) => (!data.isEmailAvailable ? { emailAvailable: true } : null)),
            first(),
            catchError(() => of(null))
          );
      })
    );
  }
}

@Injectable()
export class GameAccountAvailableAsyncValidator implements AsyncValidator {
  constructor(private usersService: UserService, private store: Store<AppState>) {}

  validate(control: AbstractControl): Observable<ValidationErrors | null> {
    return timer(1000).pipe(
      withLatestFrom(this.store.select(selectCurrentGame)),
      switchMap(([_, game]) => {
        return this.usersService
          .checkRegistrationDataAvailability({
            type: 'gameAccountName',
            value: control.value,
            ...(game.value === GameType.LOL
              ? { gameAccountServer: control.parent.controls['gameAccountServer'].value?.value }
              : null),
            gameType: game.value.toString(),
          })
          .pipe(
            map((data) => (!data.isGameAccountExists ? { gameAccountExists: true } : null)),
            first(),
            catchError(() => of(null))
          );
      })
    );
  }
}

@Injectable()
export class TeamNameAvailableAsyncValidator implements AsyncValidator {
  constructor(private teamService: TeamService) {}

  validate(control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> {
    return timer(1000).pipe(
      switchMap(() => {
        return this.teamService
          .checkNameAvailability({
            name: control.value,
          })
          .pipe(
            map((data) => (!data.isAvailable ? { nameAvailable: true } : null)),
            first(),
            catchError(() => of(null))
          );
      })
    );
  }
}
