import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { AppState } from '../../../app.interface';
import {
  GlobalChatStateChannel,
  GlobalChatStateGroup,
  selectActiveChannel,
  selectChannel,
  selectChannels,
  selectGroups,
  toggleGroup,
} from '../../_reducer';
import { UserProfile } from '../../../_shared/models';
import { currentProfile } from '../../../auth/_reducer';
import { memoize } from '../../../_helpers';

@Component({
  selector: 'tf-global-chat-channel-list',
  templateUrl: './channel-list.component.html',
  styleUrls: ['./channel-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ChannelListComponent implements OnInit {
  channels$: Observable<GlobalChatStateChannel[]>;
  groups$: Observable<GlobalChatStateGroup[]>;
  activeChannelId: number;
  currentUser: UserProfile;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.channels$ = this.store.select(selectChannels);
    this.groups$ = this.store.select(selectGroups);
    this.store.select(currentProfile).subscribe((user) => {
      this.currentUser = user;
    });
    this.store
      .select(selectActiveChannel)
      .pipe(
        filter((ch) => !!ch),
        map((ch) => ch.id)
      )
      .subscribe((id) => {
        this.activeChannelId = id;
      });
  }

  trackByChannelId(index: number, item: GlobalChatStateChannel): number {
    return item.id;
  }

  trackByGroupId(index: number, item: GlobalChatStateGroup): number {
    return item.id;
  }

  toggleGroup(groupId: number): void {
    this.store.dispatch(toggleGroup({ groupId }));
  }

  selectChannel(channelId: number): void {
    this.store.dispatch(selectChannel({ channelId }));
  }

  @memoize()
  hasGroupNewMessages(group: GlobalChatStateGroup): boolean {
    return !!group.channels.find((ch) => ch.unseenMessagesSinceLastDisplay);
  }
}
