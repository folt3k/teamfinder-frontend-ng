import { ListQueryParams } from '../_shared/interfaces';

export type TeamInvitationsScope = 'sentInvitations' | 'receivedInvitations';
export type TeamRequestsScope = 'sentRequests' | 'receivedRequests';

export interface SendTeamInvitationDto {
  profileId: number;
  teamId: number;
}

export interface SendTeamRequestDto {
  teamId: number;
}

export interface ListTeamInvitationsQueryParams extends ListQueryParams {
  scope: TeamInvitationsScope;
}

export interface ListTeamRequestsQueryParams extends ListQueryParams {
  scope: TeamRequestsScope;
}

export type ManageTeamInvitationAction = 'accept' | 'reject' | 'cancel';
