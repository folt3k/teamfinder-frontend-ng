import {
  Injectable,
  ComponentRef,
  ComponentFactoryResolver,
  ApplicationRef,
  Injector,
  TemplateRef,
  EmbeddedViewRef,
} from '@angular/core';

import { AlertComponent } from './alert.component';
import { AlertOptions, AlertTheme, DefinedAlerts } from './alert.interface';
import { ALERT_CONFIG } from './alert.token';
import { first } from 'rxjs/operators';
import { FADE_OUT_DURATION } from './alert.animations';

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  private activeAlerts: Array<ComponentRef<AlertComponent>> = [];
  definedAlerts: Partial<DefinedAlerts>;

  constructor(
    private readonly componentFactoryResolver: ComponentFactoryResolver,
    private readonly appRef: ApplicationRef
  ) {
    this.setDefinedAlerts();
  }

  show(content: string | TemplateRef<any>, _options: AlertOptions = {}): void {
    const options: AlertOptions = { ...this.getDefaultOptions(), ..._options };
    const injector = Injector.create([
      {
        provide: ALERT_CONFIG,
        useValue: {
          content,
          options,
        },
      },
    ]);

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(AlertComponent);
    const componentRef = componentFactory.create(injector);

    componentRef.instance.closed.pipe(first()).subscribe(() => {
      this.remove(componentRef);
    });

    this.activeAlerts = [componentRef, ...this.activeAlerts];
    this.appRef.attachView(componentRef.hostView);

    const domElem = (componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    document.body.appendChild(domElem);

    this.updateAlertsPosition();
  }

  private remove(componentRef: ComponentRef<AlertComponent>): void {
    this.activeAlerts = this.activeAlerts.filter((ref) => ref !== componentRef);
    this.appRef.detachView(componentRef.hostView);
    componentRef.changeDetectorRef.detectChanges();
    componentRef.destroy();
    setTimeout(() => {
      this.updateAlertsPosition();
    }, FADE_OUT_DURATION);
  }

  private getDefaultOptions(): AlertOptions {
    return {
      theme: AlertTheme.SUCCESS,
      duration: 5000,
    };
  }

  private updateAlertsPosition(): void {
    this.activeAlerts.forEach((alert, index) => {
      alert.location.nativeElement.style.top =
        (index === 0 ? 30 : 15) * (index + 1) +
        index * 85 +
        (index > 0 ? 30 - 15 * (index + 1) + 15 * index : 0) +
        'px';
    });
  }

  private setDefinedAlerts(): void {
    const alerts = {
      userBanned: {
        message: 'Otrzymałeś bana i nie możesz wykonać tej akcji',
        options: {
          theme: AlertTheme.FAILED,
        },
      },
    };

    this.definedAlerts = {};

    Object.keys(alerts).forEach((key) => {
      const { message, options } = alerts[key];
      this.definedAlerts[key] = {
        show: () => this.show(message, options),
      };
    });
  }
}
