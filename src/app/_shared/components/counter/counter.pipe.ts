import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'counter',
})
export class CounterPipe implements PipeTransform {
  transform(value: number): string {
    if (value >= 99) {
      return '+99';
    }

    return value.toString();
  }
}
