import { Component, OnInit, ViewChild, AfterViewInit, Inject, PLATFORM_ID } from '@angular/core';
import { Store } from '@ngrx/store';
import { PopoverDirective } from '@ng-poppy/lib';
import { merge, Observable } from 'rxjs';

import { MenuLink, MENU_LINKS, STATIC_URLS } from './../../../_shared/common';
import { AppState } from 'src/app/app.interface';
import { isLoggedIn, currentUser, logout, isAuthorizationCompleted } from 'src/app/auth/_reducer';
import { CurrentUser } from 'src/app/_shared/models';
import { selectCurrentGame } from '../../../games/_reducer';
import { selectUI } from '../../../_reducers/ui';
import * as fromNotifications from '../../../notifications/_reducer';
import { filter, first, mapTo, tap } from 'rxjs/operators';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'tf-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, AfterViewInit {
  @ViewChild('responsiveMenuPopover', { read: PopoverDirective }) responsiveMenuPopover: PopoverDirective;

  links: MenuLink[] = [];
  STATIC_URLS = STATIC_URLS;
  isLoggedIn$: Observable<boolean>;
  isAuthorizationCompleted$: Observable<boolean>;
  isBrowser: boolean = false;
  undisplayedNotificationsCount$: Observable<number>;
  canDisplayGameSelect$: Observable<boolean>;
  unreadMessagesCount: number = 0;
  currentUser: CurrentUser;
  responsiveMenuOpen$: Observable<boolean>;

  constructor(private store: Store<AppState>, @Inject(PLATFORM_ID) private platformId: object) {}

  ngOnInit(): void {
    this.isLoggedIn$ = this.store.select(isLoggedIn);
    this.isAuthorizationCompleted$ = this.store.select(isAuthorizationCompleted);
    this.undisplayedNotificationsCount$ = this.store.select(fromNotifications.selectUndisplayedCount);
    this.canDisplayGameSelect$ = this.store.select(selectUI, 'canDisplayGameSelect');
    this.isBrowser = isPlatformBrowser(this.platformId);
    this.listenOnUnreadMessagesCountChange();
    this.listenOnCurrentUserChange();
    this.listenOnCurrentGameChange();
  }

  ngAfterViewInit(): void {
    if (this.responsiveMenuPopover) {
      this.listenOnResponsiveMenuOpen();
    }
  }

  onLogout(): void {
    this.store.dispatch(logout());
  }

  onDisplayNotifications(): void {
    this.store
      .select(fromNotifications.selectUndisplayedCount)
      .pipe(
        first(),
        filter((count) => count > 0)
      )
      .subscribe(() => {
        this.store.dispatch(fromNotifications.markAllAsDisplayed());
      });
  }

  private listenOnUnreadMessagesCountChange(): void {
    this.store
      .select((state) => state.chat.unreadMessagesCount)
      .subscribe((count) => {
        this.unreadMessagesCount = count;
      });
  }

  private listenOnCurrentUserChange(): void {
    this.store.select(currentUser).subscribe((data) => {
      this.currentUser = data;
    });
  }

  private listenOnCurrentGameChange(): void {
    this.store.select(selectCurrentGame).subscribe((game) => {
      this.links = MENU_LINKS.filter((link) => link.canDisplayInHeader).map((link) => ({
        ...link,
        gamePrefix: game?.slashRouterPrefix,
      }));
    });
  }

  private listenOnResponsiveMenuOpen(): void {
    this.responsiveMenuOpen$ = merge(
      this.responsiveMenuPopover.afterShow.pipe(mapTo(true)),
      this.responsiveMenuPopover.afterClose.pipe(mapTo(false))
    );
  }
}
