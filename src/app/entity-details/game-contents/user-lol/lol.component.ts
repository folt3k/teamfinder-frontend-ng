import { Component, Input, isDevMode, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { UserProfile } from '../../../_shared/models';
import { DisplayLolGameChampion, LolDetailsContentService } from './lol.service';
import { map, startWith, switchMap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { selectDetails } from '../../_reducer';

@Component({
  selector: 'tf-lol-details-content',
  templateUrl: './lol.component.html',
  styleUrls: ['./lol.component.scss'],
})
export class LolDetailsContentComponent implements OnInit {
  @Input() data: UserProfile;

  isDevMode: boolean = isDevMode();
  favouriteChampions$: Observable<DisplayLolGameChampion[]>;

  constructor(private service: LolDetailsContentService, private store: Store) {}

  ngOnInit(): void {
    this.favouriteChampions$ = this.store.select(selectDetails).pipe(
      map((details: UserProfile) => details.game_metadata.favouriteChampions),
      switchMap((champions) => this.service.getDisplayFavouriteChampions(champions)),
      startWith([])
    );
  }
}
