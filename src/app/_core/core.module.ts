import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FooterModule, HeaderModule } from './components';

const MODULES = [HeaderModule, FooterModule];

@NgModule({
  declarations: [],
  imports: [CommonModule, ...MODULES],
  exports: [...MODULES],
})
export class CoreModule {}
