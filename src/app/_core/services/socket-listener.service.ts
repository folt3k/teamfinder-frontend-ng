import { Injectable } from '@angular/core';

import { GlobalChatSocketService } from './../../global-chat/global-chat-ws.service';
import { UserSocketService } from 'src/app/user/user-ws.service';
import { PrivateChatSocketService } from '../../chat/_services/private-chat-ws.service';
import { SocketService } from './socket.service';
import { NotificationsSocketService } from '../../notifications/notifications-ws.service';
import { TeamChatSocketService } from '../../chat/_services/team-chat-ws.service';

export interface WithListeners {
  // tslint:disable-next-line:no-any
  listeners(): { [key: string]: (data: any) => void };
}

@Injectable({
  providedIn: 'root',
})
export class SocketListenerService {
  services: WithListeners[] = [];

  constructor(
    private globalChatSocketService: GlobalChatSocketService,
    private privateChatSocketService: PrivateChatSocketService,
    private teamChatSocketService: TeamChatSocketService,
    private userSocketService: UserSocketService,
    private notificationSocketService: NotificationsSocketService,
    private socket: SocketService
  ) {
    this.services = [
      this.userSocketService,
      this.globalChatSocketService,
      this.privateChatSocketService,
      this.notificationSocketService,
      this.teamChatSocketService,
    ];
  }

  init(): void {
    this.services.forEach((service) => {
      const listeners = service.listeners();

      Object.keys(listeners).forEach((event) => {
        this.socket.listen(event, listeners[event]);
      });
    });
  }
}
