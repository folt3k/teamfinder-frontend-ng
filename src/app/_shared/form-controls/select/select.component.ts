import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Host,
  Input,
  OnInit,
  Optional,
  Output,
  SkipSelf,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';

import { Observable } from 'rxjs';
import { ControlContainer, FormControlDirective, NG_VALIDATORS, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CustomFormControl } from '../custom-form-control';

@Component({
  selector: 'tf-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => SelectComponent),
      multi: true,
    },
  ],
})
export class SelectComponent extends CustomFormControl implements OnInit {
  @ViewChild(FormControlDirective, { static: true }) formControlDirective: FormControlDirective;

  @Input() label: string;
  @Input() options: any[] = [];
  @Input() async: (searchPhrase?: string) => Observable<any>;
  @Input() bindLabel: string = 'label';
  @Input() bindValue: string;
  @Input() multiselect: boolean = false;
  @Input() clearable: boolean = true;
  @Input() searchable: boolean = true;
  @Input() placeholder: string = 'Wybierz..';

  @Output() changed: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    cdr: ChangeDetectorRef,
    @Optional()
    @Host()
    @SkipSelf()
    controlContainer: ControlContainer
  ) {
    super(cdr, controlContainer);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  registerOnTouched(fn: any): void {
    this.formControlDirective.valueAccessor.registerOnTouched(fn);
  }

  registerOnChange(fn: any): void {
    this.formControlDirective.valueAccessor.registerOnChange(fn);
  }

  writeValue(obj: any): void {
    this.formControlDirective.valueAccessor.writeValue(obj);
  }

  setDisabledState(isDisabled: boolean): void {
    this.formControlDirective.valueAccessor.setDisabledState(isDisabled);
  }
}
