import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { AppState } from '../../app.interface';
import * as actions from './actions';
import { AlertService } from '../../_shared/components/alert/alert.service';
import { CommentsService } from '../comments.service';
import { selectDetails } from '../../entity-details/_reducer';
import { selectComments } from './selectors';

@Injectable()
export class CommentsEffects {
  create$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.createComment),
      withLatestFrom(this.store.select(selectDetails)),
      switchMap(([action, details]) =>
        this.commentsService
          .create({ content: action.content, entityId: details.id, entityType: details.entityType })
          .pipe(
            map((data) => actions.createCommentSuccess({ data })),
            catchError((err) => of(actions.createCommentFailed(err)))
          )
      )
    )
  );

  createSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.createCommentSuccess),
        map((response) => response.data),
        tap((data) => {})
      ),
    { dispatch: false }
  );

  update$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.updateComment),
      switchMap((action) =>
        this.commentsService.update(action).pipe(
          map((data) => actions.updateCommentSuccess({ data })),
          catchError((err) => of(actions.updateCommentFailed(err)))
        )
      )
    )
  );

  fetchAll$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.fetchAllComments),
      withLatestFrom(this.store.select(selectDetails), this.store.select(selectComments)),
      switchMap(([action, details, comments]) => {
        const page = comments.pagination ? comments.pagination.currentPage + 1 : 1;
        return this.commentsService.fetchAll({ id: details.id, type: details.entityType }, { page }).pipe(
          map((data) => actions.fetchAllCommentsSuccess(data)),
          catchError((err) => of(actions.fetchAllCommentsFailed(err)))
        );
      })
    )
  );

  createSubcomment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.createSubcomment),
      switchMap((action) =>
        this.commentsService.createSubcomment(action).pipe(
          map(() => actions.createSubcommentSuccess({ commentId: action.commentId })),
          catchError((err) => of(actions.createSubcommentFailed(err)))
        )
      )
    )
  );

  createSubcommentSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.createSubcommentSuccess),
      map((action) => actions.fetchSubcomments({ commentId: action.commentId }))
    )
  );

  updateSubcomment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.updateSubcomment),
      switchMap((action) =>
        this.commentsService.updateSubcomment(action).pipe(
          map((data) => actions.updateSubcommentSuccess({ subcommentId: data.id })),
          catchError((err) => of(actions.updateSubcommentFailed(err)))
        )
      )
    )
  );

  fetchSubcomments$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.fetchSubcomments),
      switchMap((action) => {
        return this.commentsService.fetchSubcomments(action.commentId).pipe(
          map((data) => actions.fetchSubcommentsSuccess({ data, commentId: action.commentId })),
          catchError((err) => of(actions.fetchSubcommentsFailed(err)))
        );
      })
    )
  );

  likeComment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.likeComment),
      switchMap((action) =>
        this.commentsService.likeComment(action.commentId).pipe(
          map((data) => actions.likeCommentSuccess({ data })),
          catchError((err) => of(actions.likeCommentFailed(err)))
        )
      )
    )
  );

  removeComment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.removeComment),
      switchMap(({ id }) =>
        this.commentsService.removeComment(id).pipe(
          map(() => actions.removeCommentSuccess({ id })),
          catchError((err) => of(actions.removeCommentFailed(err)))
        )
      )
    )
  );

  removeSubcomment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.removeSubcomment),
      switchMap(({ commentId, subcommentId }) =>
        this.commentsService.removeSubcomment(commentId, subcommentId).pipe(
          map(() => actions.removeSubcommentSuccess({ commentId, subcommentId })),
          catchError((err) => of(actions.removeSubcommentFailed(err)))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private router: Router,
    private route: ActivatedRoute,
    private commentsService: CommentsService,
    private alertService: AlertService
  ) {}
}
