import { createSelector } from '@ngrx/store';

import { AppState } from '../../app.interface';
import { featureKey } from './constants';
import { State } from './reducer';

const selectFeature = (state: AppState) => state[featureKey];

export const error = createSelector(selectFeature, (state: State) => state.error);

export const loading = createSelector(selectFeature, (state: State) => state.loading);

export const selectDetails = createSelector(selectFeature, (state: State) => state?.data);
