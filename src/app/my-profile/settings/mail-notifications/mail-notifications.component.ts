import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';

import { User } from '../../../_shared/models';
import { AppState } from '../../../app.interface';
import { mailNotificationSettings } from './mail-notification-settings';
import { memoize } from '../../../_helpers';
import { updateUserMailNotificationSettings } from '../../../user/_reducer';

@Component({
  selector: 'tf-my-profile-settings-mail-notifications',
  templateUrl: './mail-notifications.component.html',
  styleUrls: ['./mail-notifications.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MyProfileSettingsMailNotificationsComponent implements OnInit {
  @Input() currentUser: User;

  form: FormGroup;

  constructor(private fb: FormBuilder, private store: Store<AppState>) {}

  ngOnInit(): void {
    this.initForm();
  }

  onSubmit(): void {
    this.store.dispatch(updateUserMailNotificationSettings({ body: this.form.value }));
  }

  @memoize()
  getOptionLabel(key: string): string {
    return mailNotificationSettings.find((option) => option.key === key).label;
  }

  private initForm(): void {
    this.form = this.fb.group(this.currentUser.mailNotificationSettings || {});
  }
}
