import { createAction, props } from '@ngrx/store';

import { types } from './constants';
import { PrivateChatRoom, TeamChatRoom } from '../_models/chat-room.model';
import { BaseListRO, ErrorRO } from '../../_shared/interfaces';
import { ChatMessage } from '../_models/chat-message.model';
import {
  ChatType,
  FetchTeamChatRoomsRO,
  ReceivePrivateChatMessageRO,
  SendChatMessageRO,
} from '../chat.inerface';
import { UserProfile } from '../../_shared/models';
import { TeamMember } from '../../team';

export const fetchPrivateRooms = createAction(types.FetchPrivateRooms);
export const fetchPrivateRoomsSuccess = createAction(
  types.FetchPrivateRoomsSuccess,
  props<{ results: PrivateChatRoom[] }>()
);
export const fetchPrivateRoomsFailed = createAction(types.FetchPrivateRoomsFailed, props<ErrorRO>());

export const fetchTeamRooms = createAction(types.FetchTeamRooms);
export const fetchTeamRoomsSuccess = createAction(types.FetchTeamRoomsSuccess, props<FetchTeamChatRoomsRO>());
export const fetchTeamRoomsFailed = createAction(types.FetchTeamRoomsFailed, props<ErrorRO>());

export const fetchMoreMessages = createAction(
  types.FetchMoreMessages,
  props<{ roomId: number; chatType: ChatType }>()
);
export const fetchMoreMessagesSuccess = createAction(
  types.FetchMoreMessagesSuccess,
  props<BaseListRO<ChatMessage> & { roomId: number; chatType: ChatType }>()
);

export const sendRoomMessage = createAction(
  types.SendRoomMessage,
  props<{ roomId: number; chatType: ChatType; content: string }>()
);
export const sendRoomMessageSuccess = createAction(types.SendRoomMessageSuccess, props<SendChatMessageRO>());

export const receiveMessage = createAction(types.ReceiveMessage, props<ReceivePrivateChatMessageRO>());

export const markRoomAsRead = createAction(
  types.MarkRoomAsRead,
  props<{ roomId: number; chatType: ChatType }>()
);

export const receiveNewConversation = createAction(types.ReceiveNewConversation, props<{ roomId: number }>());
export const startNewConversation = createAction(
  types.StartNewConversation,
  props<{ receiver?: UserProfile }>()
);
export const createNewConversation = createAction(types.CreateNewConversation, props<{ content: string }>());
export const createNewConversationSuccess = createAction(types.CreateNewConversationSuccess);
export const openConversation = createAction(types.OpenConversation, props<{ receiver: UserProfile }>());

export const receiveNewTeamRoom = createAction(types.ReceiveNewTeamRoom, props<{ data: TeamChatRoom }>());

export const receiveNewTeamMember = createAction(
  types.ReceiveNewTeamMember,
  props<{ member: TeamMember; teamId: number }>()
);

export const addRoomWindow = createAction(
  types.AddRoomWindow,
  props<{ roomId: number; chatType: ChatType }>()
);
export const closeRoomWindow = createAction(
  types.CloseRoomWindow,
  props<{ roomId: number; chatType: ChatType }>()
);
export const maximizeRoomWindow = createAction(
  types.MaximizeRoomWindow,
  props<{ roomId: number; chatType: ChatType }>()
);
export const minimizeRoomWindow = createAction(
  types.MinimizeRoomWindow,
  props<{ roomId: number; chatType: ChatType }>()
);
