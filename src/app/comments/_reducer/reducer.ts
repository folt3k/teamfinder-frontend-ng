import { createReducer, on, Action } from '@ngrx/store';

import * as actions from './actions';
import * as fromEntityDetails from '../../entity-details/_reducer';
import { BaseReducerState, createBaseReducerState, clearError, getErrorState } from '../../_reducers';
import { Comment } from '../../_shared/models';
import { Pagination } from '../../_shared/interfaces';

export interface State extends BaseReducerState {
  results: Comment[];
  pagination: Pagination;
  createLoading: boolean;
}

export const initialState: State = createBaseReducerState({
  results: [],
  pagination: null,
  createLoading: false,
});

export const _reducer = createReducer(
  initialState,
  on(actions.createComment, (state) => ({ ...state, createLoading: true, error: clearError() })),
  on(actions.createCommentSuccess, (state, payload) => ({
    ...state,
    createLoading: false,
    results: [payload.data, ...state.results],
    pagination: {
      ...state.pagination,
      totalResults: state.pagination.totalResults + 1,
    },
  })),
  on(actions.createCommentFailed, (state, payload) => ({
    ...state,
    error: getErrorState(payload),
    createLoading: false,
  })),
  on(actions.updateComment, (state, payload) => ({
    ...state,
    results: state.results.map((comment) =>
      comment.id === payload.entityId ? { ...comment, content: payload.content } : comment
    ),
  })),
  on(actions.fetchAllComments, (state) => ({ ...state, loading: true })),
  on(actions.fetchAllCommentsSuccess, (state, payload) => ({
    ...state,
    loading: false,
    results: [...state.results, ...payload.results],
    pagination: payload.pagination,
  })),
  on(fromEntityDetails.fetchEntityDetails, (state) => ({
    ...state,
    results: [],
    pagination: null,
  })),
  on(actions.fetchSubcommentsSuccess, (state, payload) => ({
    ...state,
    loading: false,
    results: state.results.map((comment) =>
      comment.id === payload.commentId
        ? {
            ...comment,
            subcomments: payload.data,
            showSubcomments: true,
            subcommentsCount: payload.data.length,
          }
        : comment
    ),
  })),
  on(actions.closeSubcomments, (state, payload) => ({
    ...state,
    results: state.results.map((comment) =>
      comment.id === payload.commentId ? { ...comment, showSubcomments: false } : comment
    ),
  })),
  on(actions.updateSubcomment, (state, payload) => ({
    ...state,
    results: state.results.map((comment) =>
      comment.id === payload.commentId
        ? {
            ...comment,
            subcomments: comment.subcomments.map((sc) =>
              sc.id === payload.subcommentId ? { ...sc, content: payload.content } : sc
            ),
          }
        : comment
    ),
  })),
  on(actions.likeCommentSuccess, (state, payload) => ({
    ...state,
    results: state.results.map((comment) =>
      comment.id === payload.data.id
        ? {
            ...comment,
            likesCount: payload.data.likesCount,
            currentUserLikes: payload.data.currentUserLikes,
          }
        : comment
    ),
  })),
  on(actions.removeCommentSuccess, (state, payload) => ({
    ...state,
    results: state.results.filter((comment) => comment.id !== payload.id),
    pagination: {
      ...state.pagination,
      totalResults: state.pagination.totalResults - 1,
    },
  })),
  on(actions.removeSubcommentSuccess, (state, payload) => ({
    ...state,
    results: state.results.map((c) =>
      c.id === payload.commentId
        ? {
            ...c,
            subcommentsCount: c.subcommentsCount - 1,
            showSubcomments: c.subcommentsCount > 1,
            subcomments: c.subcomments.filter((sc) => sc.id !== payload.subcommentId),
          }
        : c
    ),
  }))
);

export function reducer(state: State, action: Action) {
  return _reducer(state, action);
}
