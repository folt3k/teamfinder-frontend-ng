import { createAction, props } from '@ngrx/store';

import { types } from './constants';
import { ErrorRO, GameType } from '../../_shared/interfaces';
import { LolGamesHistoryRO } from '../games.interface';

export const changeCurrentGame = createAction(types.ChangeCurrentGame, props<{ gameId: GameType }>());

export const fetchLolGamesHistory = createAction(types.FetchLolGamesHistory);
export const fetchLolGamesHistorySuccess = createAction(
  types.FetchLolGamesHistorySuccess,
  props<{ data: LolGamesHistoryRO }>()
);
export const fetchLolGamesHistoryFailed = createAction(types.FetchLolGamesHistoryFailed, props<ErrorRO>());

export const resetGamesHistory = createAction(types.ResetGamesHistory);
