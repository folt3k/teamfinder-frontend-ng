import { AppState } from '../../app.interface';
import { featureKey } from './constants';
import { createSelector } from '@ngrx/store';

const selectFeature = (state: AppState) => state[featureKey];

export const error =  createSelector(selectFeature, (state) => state.error);

export const loading = createSelector(selectFeature, (state) => state.loading);
