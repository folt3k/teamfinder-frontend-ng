import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

import { MenuLink, STATIC_URLS } from '../../../../_shared/common';

@Component({
  selector: 'tf-responsive-menu',
  templateUrl: './responsive-menu.component.html',
  styleUrls: ['./responsive-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResponsiveMenuComponent implements OnInit {
  @Input() links: MenuLink[] = [];
  @Input() isLoggedIn: boolean;
  STATIC_URLS = STATIC_URLS;

  constructor() {}

  ngOnInit(): void {}
}
