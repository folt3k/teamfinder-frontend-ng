import { ChangeDetectorRef, Component } from '@angular/core';

import { DialogRef } from '../../../poppy/dialog';

@Component({
  selector: 'tf-add-avatar-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
})
export class AddAvatarDialogComponent {
  constructor(public dialogRef: DialogRef) {}

  onSave(image: string): void {
    this.dialogRef.close({ image });
  }
}
