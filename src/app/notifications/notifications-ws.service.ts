import { Injectable } from '@angular/core';

import { receiveNewNotification } from './_reducer/actions';
import { SocketService } from './../_core/services/socket.service';
import { Store } from '@ngrx/store';
import { AppState } from '../app.interface';
import { WithListeners } from '../_core/services';
import { Notification } from './notification.model';

export enum NotificationsSocketEvents {
  NewNotification = 'USER_GET_NEW_NOTIFICATION',
}

@Injectable({
  providedIn: 'root',
})
export class NotificationsSocketService implements WithListeners {
  constructor(private socket: SocketService, private store: Store<AppState>) {}

  // tslint:disable-next-line:no-any
  listeners(): { [p: string]: (data: any) => void } {
    return {
      [NotificationsSocketEvents.NewNotification]: ({ data }: { data: Notification }) => {
        this.store.dispatch(receiveNewNotification({ data }));
      },
    };
  }
}
