import { createReducer, on, Action } from '@ngrx/store';

import * as actions from './actions';
import { BaseReducerState, createBaseReducerState, clearError, getErrorState } from '../../_reducers';

export interface State extends BaseReducerState {
  createLoading: boolean;
}

export const initialState: State = createBaseReducerState({
  createLoading: false,
});

export const _reducer = createReducer(
  initialState,
  on(actions.createAnnouncement, actions.updateAnnouncement, (state) => ({
    ...state,
    createLoading: true,
    error: clearError(),
  })),
  on(actions.createAnnouncementSuccess, actions.updateAnnouncementSuccess, (state, payload) => ({
    ...state,
    createLoading: false,
  })),
  on(actions.createAnnouncementFailed, actions.updateAnnouncementFailed, (state, payload) => ({
    ...state,
    createLoading: false,
    error: getErrorState(payload),
  }))
);

export function reducer(state: State, action: Action) {
  return _reducer(state, action);
}
