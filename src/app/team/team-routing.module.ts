import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DYNAMIC_URLS, STATIC_URLS } from '../_shared/common';

const routes: Routes = [
  {
    path: STATIC_URLS['teams'].path,
    loadChildren: () => import('./all/all.module').then((m) => m.AllTeamsModule),
  },
  {
    path: DYNAMIC_URLS.TEAM().symbolPath,
    loadChildren: () => import('./details/details.module').then((m) => m.TeamDetailsModule),
  },
  {
    path: DYNAMIC_URLS.TEAM_EDIT().symbolPath,
    loadChildren: () => import('./edit/edit.module').then((m) => m.EditTeamModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TeamRoutingModule {}
