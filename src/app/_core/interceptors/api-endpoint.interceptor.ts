import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
  HttpParams,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { keyBy, mapValues, isArray, identity, pickBy } from 'lodash';
import { environment } from 'src/environments/environment';

const EXTERNAL_APIS = ['https://unpkg.com/emoji.json@13.0.0/emoji.json'];

@Injectable()
export class ApiEndpointInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const url = EXTERNAL_APIS.includes(request.url) ? request.url : `${environment.apiURL}${request.url}`;
    const hasParams = !!request.params.keys().length;

    request = request.clone({
      url,
      // Map all query params for every request
      params: hasParams && this.getParams(request),
    });

    return next.handle(request).pipe(
      map((event: HttpEvent<{ data?: unknown }>) => {
        // Omit 'data' field wrapper in all positive responses
        if (event instanceof HttpResponse && event.body?.data) {
          // tslint:disable-next-line:no-any
          (event as any).body = event.body.data;
        }

        return event;
      })
    );
  }

  private getParams(request: HttpRequest<unknown>): HttpParams {
    const params = request.params;
    const mappedParams = params.keys().map((key) => ({
      key,
      value: key.includes('[]') ? params.getAll(key) : params.get(key),
    }));
    const paramsObject = mapValues(keyBy(mappedParams, 'key'), 'value');
    const filteredInputParams = pickBy(paramsObject, identity);
    let outputParams: HttpParams = new HttpParams();

    Object.keys(filteredInputParams).forEach((key) => {
      const param = filteredInputParams[key];

      if (isArray(param)) {
        const arraySuffix = !key.includes('[]') ? [] : '';
        param.forEach((p) => {
          outputParams = outputParams.append(`${key.toString()}${arraySuffix}`, p);
        });
      } else {
        outputParams = outputParams.append(key.toString(), param as string);
      }
    });

    return outputParams;
  }
}
