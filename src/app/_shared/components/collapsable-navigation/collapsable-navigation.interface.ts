interface CollapsableNavigationItem {
  label: string;
  key: string;
  path?: string;
  selected?: boolean;
}

export interface CollapsableNavigationGroup extends CollapsableNavigationItem {
  expanded?: boolean;
  children?: CollapsableNavigationItem[];
}

export type CollapsableNavigationSchema = CollapsableNavigationGroup[];
