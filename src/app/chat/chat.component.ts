import { AfterViewInit, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { debounceTime, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { AppState } from '../app.interface';
import { closeRoomWindow, maximizeRoomWindow, minimizeRoomWindow, selectRoomWindows } from './_reducer';
import { PrivateChatRoom } from './_models/chat-room.model';
import { isMobile } from '../_helpers';

@Component({
  selector: 'tf-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit, AfterViewInit {
  roomWindows$: Observable<PrivateChatRoom[]>;
  isMobile: boolean = false;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.roomWindows$ = this.store.select(selectRoomWindows).pipe(
      debounceTime(0),
      tap((rooms) => {
        this.onFetchNewRooms(rooms);
      })
    );
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.isMobile = isMobile();
    });
  }

  onClose(room: PrivateChatRoom): void {
    this.store.dispatch(closeRoomWindow({ roomId: room.id, chatType: room.chatType }));
  }

  onMaximize(room: PrivateChatRoom): void {
    this.store.dispatch(maximizeRoomWindow({ roomId: room.id, chatType: room.chatType }));
  }

  onMinimize(room: PrivateChatRoom): void {
    this.store.dispatch(minimizeRoomWindow({ roomId: room.id, chatType: room.chatType }));
  }

  trackByFn(index: number, item: PrivateChatRoom): number {
    return item?.id;
  }

  private onFetchNewRooms(rooms: PrivateChatRoom[]): void {
    if (this.isMobile) {
      if (rooms.length) {
        document.body.classList.add('block-scroll');
      } else {
        document.body.classList.remove('block-scroll');
      }
    }
  }
}
