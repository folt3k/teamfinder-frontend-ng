import { Pipe, PipeTransform } from '@angular/core';

import { DecodePipe } from './decode.pipe';

@Pipe({
  name: 'interactiveText',
})
export class InteractiveTextPipe implements PipeTransform {
  value: string = '';

  transform(value: string): string {
    if (!value) {
      return '';
    }

    this.value = value;

    // transformations
    this.decode();
    this.transformUrlsToHTMLLinks();

    return this.value;
  }

  private decode(): void {
    this.value = new DecodePipe().transform(this.value);
  }

  private transformUrlsToHTMLLinks(): void {
    const pattern = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    this.value = this.value.replace(pattern, '<a href="$1" class="underlined" target="_blank">$1</a>');
  }
}
