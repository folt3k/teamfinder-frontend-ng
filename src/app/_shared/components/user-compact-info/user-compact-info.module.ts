import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopoverModule } from '@ng-poppy/lib';

import { UserCompactInfoComponent } from './user-compact-info.component';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [UserCompactInfoComponent],
  imports: [CommonModule, PopoverModule, PipesModule],
  exports: [UserCompactInfoComponent],
})
export class UserCompactInfoModule {}
