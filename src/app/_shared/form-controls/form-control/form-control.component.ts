import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { merge, Subject } from 'rxjs';
import { debounceTime, takeUntil } from 'rxjs/operators';
import { isEmpty } from 'lodash';

@Component({
  selector: 'tf-form-control',
  templateUrl: './form-control.component.html',
  styleUrls: ['./form-control.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FormControlComponent implements OnInit, OnDestroy {
  @Input() label: string;
  @Input() required: boolean = false;
  @Input() control: AbstractControl;

  private destroy$ = new Subject();

  constructor(private cdr: ChangeDetectorRef) {}

  ngOnInit(): void {
    if (this.control) {
      merge(this.control.valueChanges, this.control.statusChanges)
        .pipe(debounceTime(0), takeUntil(this.destroy$))
        .subscribe(() => {
          this.cdr.detectChanges();
        });
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  canShowError(key: string): boolean {
    return (
      this.control.errors[key] &&
      (this.control.touched ||
        (!this.control.touched && !!this.control.value && !isEmpty(this.control.value)))
    );
  }
}
