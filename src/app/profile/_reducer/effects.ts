import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError, first, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { AppState } from '../../app.interface';

import * as actions from './actions';
import { ProfileService } from '../profile.service';
import { currentProfile, me, meSuccess } from '../../auth/_reducer';
import { MY_PROFILE_STATIC_URLS } from '../../_shared/common';
import { AlertService } from '../../_shared/components/alert/alert.service';
import { selectDetails } from '../../entity-details/_reducer';

@Injectable()
export class ProfileEffects {
  update$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.updateProfile),
      withLatestFrom(this.store.select(currentProfile)),
      switchMap(([action, profile]) => {
        return this.profileService.update(profile.id, action.body).pipe(
          map(() => actions.updateProfileSuccess()),
          catchError((err) => of(actions.updateProfileFailed(err)))
        );
      })
    )
  );

  updateSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.updateProfileSuccess),
      tap(() => {
        this.store.dispatch(me());
      }),
      switchMap(() =>
        this.actions$.pipe(
          ofType(meSuccess),
          first(),
          tap(() => {
            this.router.navigate([MY_PROFILE_STATIC_URLS['preview-profile'].slashPath]);
            this.alertService.show('Twój profil został zaktualizowany pomyślnie');
          })
        )
      )
    )
  );

  updateGameMetadata$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.updateProfileGameMetadata),
      withLatestFrom(this.store.select(selectDetails)),
      switchMap(([_, profile]) => {
        return this.profileService.updateGameMetadata(profile.id).pipe(
          map((data) => actions.updateProfileGameMetadataSuccess({ data })),
          catchError((err) => of(actions.updateProfileGameMetadataFailed(err)))
        );
      })
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private router: Router,
    private profileService: ProfileService,
    private alertService: AlertService
  ) {}
}
