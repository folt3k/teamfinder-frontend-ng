import { GameType } from '../interfaces';

export interface GameOption {
  value: GameType;
  key: string;
  routerPrefix: string;
  slashRouterPrefix: string;
  displayName: string;
  logo: string;
  logoGameSelector: string;
  label: string;
  shortLabel?: string;
}

export const GAMES: GameOption[] = [
  {
    value: GameType.LOL,
    key: 'lol',
    label: 'LoL',
    routerPrefix: 'lol',
    slashRouterPrefix: '/lol',
    displayName: 'League of Legends',
    logo: '/assets/images/game-brands/lol_game_selector.png',
    logoGameSelector: '/assets/images/game-brands/lol_game_selector.png',
  },
  {
    value: GameType.CS,
    key: 'cs',
    label: 'CS:GO',
    routerPrefix: 'csgo',
    slashRouterPrefix: '/csgo',
    shortLabel: 'Counter Strike: Global Offensive',
    displayName: 'CS:GO',
    logo: '/assets/images/game-brands/counter-strike-global-offensive-2.svg',
    logoGameSelector: '/assets/images/game-brands/cs_game_selector.jpg',
  },
  {
    value: GameType.FNT,
    key: 'fnt',
    label: 'Fortnite',
    routerPrefix: 'fortnite',
    slashRouterPrefix: '/fortnite',
    displayName: 'Fortnite',
    logo: '/assets/images/game-brands/fortnite.png',
    logoGameSelector: '/assets/images/game-brands/fortnite.png',
  },
];
