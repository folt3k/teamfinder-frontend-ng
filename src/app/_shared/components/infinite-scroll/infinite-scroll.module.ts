import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InfiniteScrollComponent } from './infinite-scroll.component';
import { LoaderModule } from '../loader/loader.module';

@NgModule({
  declarations: [InfiniteScrollComponent],
  imports: [CommonModule, LoaderModule],
  exports: [InfiniteScrollComponent],
})
export class InfiniteScrollModule {}
