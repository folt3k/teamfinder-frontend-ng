import { Component, Inject, OnDestroy, OnInit, PLATFORM_ID } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { BaseListRO } from '../_shared/interfaces';
import { Feedback } from '../_shared/models';
import {
  FeedbacksCommentsState,
  fetchFeedbacks,
  fetchLatestCommentFeedbacks,
  resetFeedbacks,
  selectFeedbacks,
  selectLatestCommentFeedbacks,
} from './_reducer';
import { isPlatformBrowser } from '@angular/common';
import { FEEDBACKS_PER_PAGE, LATEST_COMMENT_FEEDBACKS_PER_PAGE } from '../feedbacks/feedbacks.service';

@Component({
  selector: 'tf-cockpit',
  templateUrl: './cockpit.component.html',
  styleUrls: ['./cockpit.component.scss'],
})
export class CockpitComponent implements OnInit, OnDestroy {
  feedbacks$: Observable<BaseListRO<Feedback>>;
  comments$: Observable<FeedbacksCommentsState>;
  feedbacksPlaceholders = new Array(FEEDBACKS_PER_PAGE).fill(0);
  commentsPlaceholders = new Array(3).fill(0);

  constructor(private store: Store, @Inject(PLATFORM_ID) private platformId: object) {}

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.init();
    }
  }

  ngOnDestroy(): void {
    this.store.dispatch(resetFeedbacks());
  }

  init(): void {
    this.feedbacks$ = this.store.select(selectFeedbacks);
    this.comments$ = this.store.select(selectLatestCommentFeedbacks);

    this.loadFeedbacks();
    this.loadComments();
  }

  loadMoreFeedbacks(): void {
    this.loadFeedbacks();
  }

  private loadFeedbacks(): void {
    this.store.dispatch(fetchFeedbacks());
  }

  private loadComments(): void {
    this.store.dispatch(fetchLatestCommentFeedbacks());
  }
}
