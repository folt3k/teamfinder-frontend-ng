import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AllAnnouncementsComponent } from './all.component';

const routes: Routes = [
  {
    path: '',
    component: AllAnnouncementsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AllAnnouncementsRoutingModule {}
