import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TableComponent } from './table.component';
import { PaginationModule } from '../pagination/pagination.module';
import { TableCellDirective } from './table-cell.directive';
import { ButtonModule } from '../button/button.module';

@NgModule({
  declarations: [TableComponent, TableCellDirective],
  imports: [CommonModule, PaginationModule, ButtonModule],
  exports: [TableComponent, TableCellDirective],
})
export class TableModule {}
