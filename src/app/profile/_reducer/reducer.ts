import { createReducer, on, Action } from '@ngrx/store';

import * as actions from './actions';
import { BaseReducerState, createBaseReducerState } from '../../_reducers';

export interface State extends BaseReducerState {
  updatingGameMetadata: boolean;
}

export const initialState: State = createBaseReducerState({
  updatingGameMetadata: false,
});

export const _reducer = createReducer(
  initialState,
  on(actions.updateProfileGameMetadata, (state) => ({
    ...state,
    updatingGameMetadata: true,
  })),
  on(actions.updateProfileGameMetadataSuccess, actions.updateProfileGameMetadataFailed, (state) => ({
    ...state,
    updatingGameMetadata: false,
  }))
);

export function reducer(state: State, action: Action) {
  return _reducer(state, action);
}
