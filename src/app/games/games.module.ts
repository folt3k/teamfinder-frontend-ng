import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { SharedModule } from '../_shared/shared.module';
import {
  GameSelectModule,
  EmblematGamePickerModule,
  GameRoleIconGroupModule,
  CsWeaponCardComponent,
  UpdateProfileGameMetadataComponent,
} from './_components';
import * as fromGame from './_reducer';
import { FntGameModeStatsComponent } from './_components/fnt-game-mode-stats/fnt-game-mode-stats.component';
import { GamePickerDialogComponent } from './_components/game-picker-dialog/game-picker-dialog.component';
import { NeedsSelectGameDirective } from './_directives/needs-select-game.directive';
import { GameRankNamePipe, GameRankLabelPipe } from './_pipes';

const MODULES = [GameSelectModule, EmblematGamePickerModule, GameRoleIconGroupModule];
const PIPES = [GameRankNamePipe, GameRankLabelPipe];

@NgModule({
  declarations: [
    CsWeaponCardComponent,
    FntGameModeStatsComponent,
    GamePickerDialogComponent,
    NeedsSelectGameDirective,
    UpdateProfileGameMetadataComponent,
    ...PIPES,
    GameRankLabelPipe,
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromGame.featureKey, fromGame.reducer),
    EffectsModule.forFeature([fromGame.GameEffects]),
    SharedModule,
    ...MODULES,
  ],
  exports: [
    ...MODULES,
    CsWeaponCardComponent,
    FntGameModeStatsComponent,
    NeedsSelectGameDirective,
    UpdateProfileGameMetadataComponent,
    ...PIPES,
  ],
})
export class GamesModule {}
