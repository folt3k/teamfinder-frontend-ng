import { Directive, Input, OnInit } from '@angular/core';

import { Announcement, Feedback, UserProfile } from '../../_shared/models';
import { Team } from '../../team';
import * as helpers from '../feedbacks.helpers';

@Directive()
export class FeedbackItemComponent implements OnInit {
  @Input() data: Feedback;

  sender: UserProfile | Team;
  receiver: UserProfile | Team | Announcement;

  constructor() {}

  ngOnInit(): void {
    this.sender = this.data.senderUser || this.data.senderTeam;
    this.receiver = this.data.receiverUser || this.data.receiverTeam || this.data.receiverGameAnnouncement;
  }

  get senderTypeLabel(): string {
    return helpers.getSenderTypeLabel(this.sender.entityType);
  }

  get receiverTypeLabel(): string {
    return helpers.getReceiverTypeLabel(this.receiver.entityType);
  }

  get actionText(): string {
    return helpers.getActionText(this.data.action);
  }
}
