import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddTeamComponent } from './add.component';
import { AddTeamRoutingModule } from './add-routing.module';
import { SharedModule } from '../../_shared/shared.module';
import { TeamModule } from '../team.module';

@NgModule({
  declarations: [AddTeamComponent],
  imports: [CommonModule, AddTeamRoutingModule, SharedModule, TeamModule],
})
export class AddTeamModule {}
