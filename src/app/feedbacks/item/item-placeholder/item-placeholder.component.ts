import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'tf-feedback-item-placeholder',
  templateUrl: './item-placeholder.component.html',
  styleUrls: ['./item-placeholder.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeedbackItemPlaceholderComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
