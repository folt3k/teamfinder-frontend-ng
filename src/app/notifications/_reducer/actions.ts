import { createAction, props } from '@ngrx/store';

import { types } from './constants';
import { ErrorRO, ListQueryParams } from '../../_shared/interfaces';
import { NotificationsListAllRO } from '../notifications.interface';
import { Notification } from '../notification.model';

export const fetchAll = createAction(types.FetchAll, props<ListQueryParams>());
export const fetchAllSuccess = createAction(types.FetchAllSuccess, props<NotificationsListAllRO>());
export const fetchAllFailed = createAction(types.FetchAllFailed, props<ErrorRO>());

export const markAsRead = createAction(types.MarkAsRead, props<{ id: number }>());
export const markAsReadSuccess = createAction(types.MarkAsReadSuccess);
export const markAsReadFailed = createAction(types.MarkAsReadFailed, props<ErrorRO>());

export const markAllAsRead = createAction(types.MarkAllAsRead);
export const markAllAsReadSuccess = createAction(types.MarkAllAsReadSuccess);
export const markAllAsReadFailed = createAction(types.MarkAllAsReadFailed, props<ErrorRO>());

export const markAllAsDisplayed = createAction(types.MarkAllAsDisplayed);
export const markAllAsDisplayedSuccess = createAction(types.MarkAllAsDisplayedSuccess);
export const markAllAsDisplayedFailed = createAction(types.MarkAllAsDisplayedFailed, props<ErrorRO>());

export const receiveNewNotification = createAction(
  types.ReceiveNewNotification,
  props<{ data: Notification }>()
);
