declare interface FileEvent extends Event {
  target: EventTarget & Partial<HTMLInputElement>;
}
