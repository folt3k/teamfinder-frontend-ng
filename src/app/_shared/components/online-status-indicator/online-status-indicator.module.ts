import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnlineStatusIndicatorComponent } from './online-status-indicator.component';

@NgModule({
  declarations: [OnlineStatusIndicatorComponent],
  imports: [CommonModule],
  exports: [OnlineStatusIndicatorComponent],
})
export class OnlineStatusIndicatorModule {}
