import { Injectable } from '@angular/core';

@Injectable()
export class PaginationService {
  constructor() {}

  paginate(current: number, count: number): Array<'more' | 'less' | number> {
    let paginArray = [];
    paginArray.push(1);
    if (current > 4) {
      paginArray.push('less');
      let i;
      if (count - current === 1) {
        i = -2;
      } else if (count - current === 0) {
        i = -3;
      } else {
        i = -1;
      }
      const limit = i + 3;
      for (i; i < limit; i++) {
        if (current + i > 1 && current + i <= count) {
          paginArray.push(current + i);
        }
      }
      if (count - current + 2 > 3) {
        paginArray = paginArray.concat(['more', count]);
      } else {
        for (let i = 1; i < count - current - 2; i++) {
          if (current + i <= count) {
            paginArray.push(current + i);
          }
        }
        if (paginArray[paginArray.length - 1] !== count) {
          paginArray.push(count);
        }
      }
    } else {
      for (let i = 2; i < 6; i++) {
        if (i <= count) {
          paginArray.push(i);
        }
      }
      if (count - paginArray[paginArray.length - 1] > 1) {
        paginArray = paginArray.concat(['more', count]);
      }
      if (count - paginArray[paginArray.length - 1] === 1) {
        paginArray = paginArray.concat([count]);
      }
    }
    return paginArray;
  }
}
