import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { take } from 'rxjs/operators';

import { BaseSelectOption, GameType } from '../../_shared/interfaces';
import { UserProfile } from '../../_shared/models';
import { AppState } from '../../app.interface';
import { currentProfile } from '../../auth/_reducer';
import { selectDicts } from '../../_reducers/dictionaries';
import { getGameKeyByType } from '../../games';
import { updateProfile } from '../../profile/_reducer';

@Component({
  selector: 'tf-my-profile-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class MyProfileEditComponent implements OnInit {
  form: FormGroup;
  currentProfile: UserProfile;
  options: { modes: BaseSelectOption[] } = { modes: [] };
  submitting: boolean = false;

  constructor(private fb: FormBuilder, private store: Store<AppState>) {}

  ngOnInit(): void {
    combineLatest(this.store.select(currentProfile), this.store.select(selectDicts, ['modes']))
      .pipe(take(1))
      .subscribe(([profile, dicts]) => {
        this.currentProfile = profile;
        this.options = {
          modes: dicts.modes[getGameKeyByType(profile.type)],
        };
        this.initForm();
      });
  }

  get canDisplayGameRolesField(): boolean {
    return this.currentProfile.type !== GameType.FNT;
  }

  get canDisplayGameModesField(): boolean {
    return this.currentProfile.type !== GameType.FNT;
  }

  onSubmit(): void {
    this.store.dispatch(updateProfile({ body: this.form.value }));
    this.submitting = true;
  }

  avatarSelected(avatar: string): void {
    this.form.patchValue({ avatar });
  }

  private initForm(): void {
    this.form = this.fb.group({
      modes: [this.currentProfile.modes.map((mode) => mode.id)],
      roles: [this.currentProfile.roles.map((role) => role.id)],
      desc: [this.currentProfile.desc],
      avatar: [null],
    });
  }
}
