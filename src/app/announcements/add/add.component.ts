import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState } from '../../app.interface';
import { createAnnouncement } from '../_reducer';
import { CreateAnnouncementDto } from '../announcements.interface';

@Component({
  selector: 'tf-add-announcement',
  templateUrl: './add.component.html',
})
export class AddAnnouncementComponent implements OnInit {
  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {}

  onSubmit(values: CreateAnnouncementDto): void {
    this.store.dispatch(createAnnouncement({ body: values }));
  }
}
