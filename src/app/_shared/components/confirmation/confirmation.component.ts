import { ChangeDetectionStrategy, Component, Inject, Input, OnInit } from '@angular/core';

import { DialogRef } from '../../poppy/dialog';
import { DIALOG_DATA } from '../../poppy/dialog/dialog.token';

@Component({
  selector: 'tf-confirmation-dialog',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfirmationComponent implements OnInit {
  @Input() question: string = 'Czy na pewno chcesz wykonać te operacje?';

  constructor(public dialogRef: DialogRef, @Inject(DIALOG_DATA) private dialogData) {}

  ngOnInit(): void {
    if (this.dialogRef) {
      this.question = this.dialogData.question;
    }
  }

  confirm(): void {
    this.dialogRef?.close(true);
  }

  decline(): void {
    this.dialogRef?.close(false);
  }
}
