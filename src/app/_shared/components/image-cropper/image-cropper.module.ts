import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ImageCropperComponent } from './image-cropper.component';
import { ButtonModule } from '../button/button.module';

@NgModule({
  declarations: [ImageCropperComponent],
  imports: [CommonModule, ButtonModule],
  exports: [ImageCropperComponent],
})
export class ImageCropperModule {}
