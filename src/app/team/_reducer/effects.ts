import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { ActivatedRoute, Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError, first, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { AppState } from '../../app.interface';
import * as actions from './actions';
import { TeamService } from '../team.service';
import { DYNAMIC_URLS, STATIC_URLS } from '../../_shared/common';
import { getGameByType, getGameRouterPrefixByType } from '../../games';
import { selectDetails } from '../../entity-details/_reducer';
import { AlertService } from '../../_shared/components/alert/alert.service';
import { currentProfile } from '../../auth/_reducer';
import { serializeTeamChatRoom } from '../../chat/_models/chat-room.model';

@Injectable()
export class TeamEffects {
  create$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.createTeam),
      withLatestFrom(this.store.select(currentProfile)),
      switchMap(([action, profile]) =>
        this.teamService.create(action.body).pipe(
          map((data) =>
            actions.createTeamSuccess({
              team: data.team,
              chatRoom: serializeTeamChatRoom(data.chatRoom, { currentProfile: profile }),
            })
          ),
          catchError((err) => of(actions.createTeamFailed(err)))
        )
      )
    )
  );

  createSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.createTeamSuccess),
        map((response) => response.team),
        tap((data) => {
          const { type, slug } = data;
          this.router.navigate([
            DYNAMIC_URLS.TEAM({ gamePrefix: getGameRouterPrefixByType(type), identifier: slug }).slashPath,
          ]);
        })
      ),
    { dispatch: false }
  );

  update$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.updateTeam),
      withLatestFrom(this.store.select(selectDetails)),
      switchMap(([action, details]) =>
        this.teamService.update(details.id, action.body).pipe(
          map((data) => actions.updateTeamSuccess({ data })),
          catchError((err) => of(actions.updateTeamFailed(err)))
        )
      )
    )
  );

  updateSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.updateTeamSuccess),
        tap(() => {
          this.alertService.show('Zmiany zostały pomyślnie zapisane');
        })
      ),
    { dispatch: false }
  );

  remove$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.removeTeam),
      switchMap(({ id, redirectFromCurrentUrl }) =>
        this.teamService.remove(id).pipe(
          map(() => actions.removeTeamSuccess({ id, redirectFromCurrentUrl })),
          catchError((err) => of(actions.removeTeamFailed(err)))
        )
      )
    )
  );

  removeSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(actions.removeTeamSuccess),
        tap(({ redirectFromCurrentUrl }) => {
          this.alertService.show('Twoja drużyna została usunięta');

          if (redirectFromCurrentUrl) {
            this.router.navigate(['/']);
          }
        })
      ),
    { dispatch: false }
  );

  fetchDetails$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.fetchTeamDetails),
      switchMap((action) =>
        this.teamService.fetchOne(action.slug).pipe(
          map((data) => actions.fetchTeamDetailsSuccess({ data })),
          catchError((err) => of(actions.fetchTeamDetailsFailed(err)))
        )
      )
    )
  );

  removeMember$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.removeTeamMember),
      switchMap((action) =>
        this.teamService.removeTeamMember(action).pipe(
          map(
            () => actions.removeTeamMemberSuccess({ memberId: action.profileId }),
            catchError((err) => of(actions.removeTeamMemberFailed(err)))
          )
        )
      )
    )
  );

  leaveTeam$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.leaveTeam),
      tap((action) => {
        this.store.dispatch(actions.removeTeamMember(action));
      }),
      withLatestFrom(this.store.select(selectDetails)),
      switchMap(([action, details]) =>
        this.actions$.pipe(
          ofType(actions.removeTeamMemberSuccess),
          first(),
          tap(() => {
            if (action.redirectFromCurrentPage) {
              this.router.navigate([STATIC_URLS['teams'].pathWithGamePrefix(getGameByType(details.type))]);
            }
            this.alertService.show('Zostałeś usunięty z drużyny pomyślnie');
          })
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private router: Router,
    private route: ActivatedRoute,
    private teamService: TeamService,
    private alertService: AlertService
  ) {}
}
