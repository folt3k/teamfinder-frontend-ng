export interface Pagination {
  currentPage: number;
  resultsPerPage: number;
  totalResults: number;
}
