import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileDetailsComponent } from './details.component';
import { ProfileDetailsRoutingModule } from './details-routing.module';
import { EntityDetailsModule } from '../../entity-details/entity-details.module';

@NgModule({
  declarations: [ProfileDetailsComponent],
  imports: [
    CommonModule,
    ProfileDetailsRoutingModule,
    EntityDetailsModule,
  ]
})
export class ProfileDetailsModule { }
