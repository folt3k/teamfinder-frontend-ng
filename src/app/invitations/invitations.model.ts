import { UserProfile } from '../_shared/models';
import { Team } from '../team';

export interface TeamInvitation {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  status: TeamInvitationStatus;
  profile: UserProfile;
  team?: Team;
}

export enum TeamInvitationStatus {
  ACCEPTED = 'accepted',
  REJECTED = 'rejected',
  PENDING = 'pending',
  CANCELED = 'canceled',
}
