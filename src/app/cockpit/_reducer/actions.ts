import { createAction, props } from '@ngrx/store';

import { types } from './constants';
import { BaseListRO, ErrorRO } from '../../_shared/interfaces';
import { Feedback } from '../../_shared/models';

export const fetchFeedbacks = createAction(types.FetchFeedbacks);
export const fetchFeedbacksSuccess = createAction(types.FetchFeedbacksSuccess, props<BaseListRO<Feedback>>());
export const fetchFeedbacksFailed = createAction(types.FetchFeedbacksFailed, props<ErrorRO>());

export const fetchLatestCommentFeedbacks = createAction(types.FetchLatestCommentFeedbacks);
export const fetchLatestCommentFeedbacksSuccess = createAction(
  types.FetchLatestCommentFeedbacksSuccess,
  props<BaseListRO<Feedback>>()
);
export const fetchLatestCommentFeedbacksFailed = createAction(
  types.FetchLatestCommentFeedbacksFailed,
  props<ErrorRO>()
);

export const resetFeedbacks = createAction(types.ResetFeedbacks);
