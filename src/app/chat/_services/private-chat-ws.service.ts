import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import {
  createNewConversationSuccess,
  fetchMoreMessagesSuccess,
  receiveMessage,
  receiveNewConversation,
  sendRoomMessageSuccess,
} from '../_reducer/actions';
import { SocketService } from '../../_core/services/socket.service';
import { BaseListRO } from '../../_shared/interfaces';
import { AppState } from '../../app.interface';
import { ChatMessage } from '../_models/chat-message.model';
import { ChatType, ReceivePrivateChatMessageRO, SendChatMessageRO } from '../chat.inerface';
import { WithListeners } from '../../_core/services';

export enum PrivateChatSocketEvents {
  FetchMoreMessages = 'FETCH_MORE_MESSAGES',
  FetchMoreMessagesSuccess = 'FETCH_MORE_MESSAGES_SUCCESS',
  SendRoomMessage = 'SEND_NEW_CHAT_ROOM_MESSAGE',
  SendRoomMessageSuccess = 'SEND_NEW_CHAT_ROOM_MESSAGE_SUCCESS',
  ReceiveMessage = 'EMIT_CHAT_MESSAGE',
  ReceiveNewConversation = 'EMIT_NEW_ROOM',
  CreateNewConversation = 'SEND_NEW_MESSAGE',
  CreateNewConversationSuccess = 'SEND_NEW_MESSAGE_SUCCESS',
  MarkRoomAsRead = 'MARK_ROOM_AS_READ',
}

@Injectable({
  providedIn: 'root',
})
export class PrivateChatSocketService implements WithListeners {
  constructor(private socket: SocketService, private store: Store<AppState>) {}

  fetchMoreMessages(body: { roomId: number; page: number }): void {
    this.socket.emit(PrivateChatSocketEvents.FetchMoreMessages, body);
  }

  sendRoomMessage(body: { roomId: number; senderId: number; content: string }): void {
    this.socket.emit(PrivateChatSocketEvents.SendRoomMessage, body);
  }

  markRoomAsRead(body: { roomId: number; memberId: number }): void {
    this.socket.emit(PrivateChatSocketEvents.MarkRoomAsRead, body);
  }

  createNewConversation(body: { senderId: number; receiverId: number; content: string }): void {
    this.socket.emit(PrivateChatSocketEvents.CreateNewConversation, body);
  }

  // tslint:disable-next-line:no-any
  listeners(): { [key: string]: (data: any) => void } {
    return {
      [PrivateChatSocketEvents.FetchMoreMessagesSuccess]: (
        data: BaseListRO<ChatMessage> & { roomId: number }
      ) => {
        this.store.dispatch(fetchMoreMessagesSuccess({ ...data, chatType: ChatType.PRIVATE }));
      },
      [PrivateChatSocketEvents.SendRoomMessageSuccess]: (data: SendChatMessageRO) => {
        this.store.dispatch(
          sendRoomMessageSuccess({ ...data, roomId: data.room, chatType: ChatType.PRIVATE })
        );
      },
      [PrivateChatSocketEvents.ReceiveMessage]: (data: ReceivePrivateChatMessageRO) => {
        this.store.dispatch(
          receiveMessage({
            roomId: data.roomId,
            message: data.message,
            senderType: data.type,
            chatType: ChatType.PRIVATE,
          })
        );
      },
      [PrivateChatSocketEvents.ReceiveNewConversation]: (data: { roomId: number }) => {
        this.store.dispatch(receiveNewConversation(data));
      },
      [PrivateChatSocketEvents.CreateNewConversationSuccess]: () => {
        this.store.dispatch(createNewConversationSuccess());
      },
    };
  }
}
