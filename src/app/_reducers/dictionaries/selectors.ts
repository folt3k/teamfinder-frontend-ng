import { AppState } from '../../../app/app.interface';
import { Dictionaries } from 'src/app/_shared/models';
import { pick } from 'lodash';

export const selectDicts = (state: AppState, dicts: string[]): Partial<Dictionaries> => {
  return pick(state.dicts, dicts);
};
