import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BaseSelectOption } from '../../interfaces';

export interface IconSwitcherOption extends BaseSelectOption {
  icon: string;
}

@Component({
  selector: 'tf-icon-switcher',
  templateUrl: './icon-switcher.component.html',
  styleUrls: ['./icon-switcher.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconSwitcherComponent implements OnInit {
  @Input() options: IconSwitcherOption[] = [];
  @Input() value: string | number;
  @Output() selected = new EventEmitter<string | number>();

  constructor() {}

  ngOnInit(): void {}

  onSelect(opt: IconSwitcherOption): void {
    this.value = opt.value;
    this.selected.emit(this.value);
  }
}
