import * as memoize from 'memoizee';

import { BaseEntity, GameType } from '../_shared/interfaces';
import { GameOption, GAMES } from '../_shared/common';
import { UserProfile } from '../_shared/models';

export interface GameCheckers {
  isLol: boolean;
  isCs: boolean;
  isFnt: boolean;
}

export const getGameTypeByRouterPrefix = memoize(
  (routerPrefix: string): GameType => GAMES.find((game) => game.routerPrefix === routerPrefix).value
);

export const getGameRouterPrefixByType = memoize(
  (type: GameType): string => GAMES.find((game) => game.value === type).routerPrefix
);

export const getGameKeyByType = memoize(
  (type: GameType): string => GAMES.find((game) => game.value === type).key
);

export const getGameByType = memoize(
  (type: GameType): GameOption => GAMES.find((game) => game.value === type)
);

export const getGameCheckers = memoize(
  (gameType: GameType): GameCheckers => ({
    isLol: gameType === GameType.LOL,
    isCs: gameType === GameType.CS,
    isFnt: gameType === GameType.FNT,
  })
);

export const isCurrentUserGameEqualsToEntityGame = (user: UserProfile, entity: BaseEntity): boolean =>
  user && entity && user.type === entity.type;
