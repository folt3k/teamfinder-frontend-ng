import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import { AppState } from '../../../app.interface';
import { updateComment, updateSubcomment } from '../../_reducer';
import { Comment } from '../../../_shared/models';

@Component({
  selector: 'tf-edit-comment',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditCommentComponent implements OnInit {
  @Output() canceled = new EventEmitter();
  @Input() comment: Comment;
  @Input() parentComment: Comment;
  @Input() isSubcomment: boolean = false;

  form: FormGroup;

  constructor(private fb: FormBuilder, protected store: Store<AppState>) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      content: [this.comment.content, [Validators.required]],
    });
  }

  onSubmit(): void {
    const content = this.form.controls.content.value;
    const commentId = this.comment.id;

    if (this.isSubcomment) {
      this.store.dispatch(
        updateSubcomment({ content, subcommentId: commentId, commentId: this.parentComment.id })
      );
    } else {
      this.store.dispatch(updateComment({ content, entityId: commentId }));
    }
    this.cancel();
  }

  cancel(): void {
    this.canceled.emit();
  }
}
