import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { BehaviorSubject, fromEvent } from 'rxjs';
import { withLatestFrom } from 'rxjs/operators';

import { Pagination } from '../../interfaces';

@Component({
  selector: 'tf-infinite-scroll',
  templateUrl: './infinite-scroll.component.html',
  styleUrls: ['./infinite-scroll.component.scss'],
})
export class InfiniteScrollComponent implements OnChanges, AfterViewInit {
  @Input() pagination: Pagination;
  @Input() trigger: 'element' | 'body' = 'element';
  @Input() size: 'sm' | 'lg' = 'sm';
  @Output() loaded = new EventEmitter<number>();

  loading: boolean = true;

  constructor(private hostEl: ElementRef) {}

  ngAfterViewInit(): void {
    this.listenOnScroll();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.pagination && changes.pagination.currentValue) {
      this.loading = false;
    }
  }

  canDisplayLoaderContainer(): boolean {
    return !this.pagination || this.isMoreResults();
  }

  private listenOnScroll(): void {
    const element = this.trigger === 'body' ? document : this.hostEl.nativeElement;
    fromEvent(element, 'scroll').subscribe(() => {
      if (this.pagination && this.canLoad()) {
        this.startLoading();
      }
    });
  }

  private canLoad(): boolean {
    const host = this.hostEl.nativeElement;
    const listScrollTop = this.trigger === 'element' ? Math.round(+host.scrollTop) : window.scrollY;
    const listOffset =
      this.trigger === 'element'
        ? host.scrollHeight - host.offsetHeight
        : document.body.offsetHeight - window.innerHeight;
    const START_LOADING_RATIO = 0.2;

    return listOffset - listScrollTop <= listOffset * START_LOADING_RATIO;
  }

  private startLoading(): void {
    if (this.isMoreResults() && !this.loading) {
      this.loaded.emit(this.pagination.currentPage + 1);
      this.loading = true;
    }
  }

  private isMoreResults(): boolean {
    const { totalResults, currentPage, resultsPerPage } = this.pagination;
    return totalResults > currentPage * resultsPerPage;
  }
}
