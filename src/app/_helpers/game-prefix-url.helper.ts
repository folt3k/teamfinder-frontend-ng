import * as memoizee from 'memoizee';

import { GAMES } from '../_shared/common';

export const isHomeURLWithGamePrefix = memoizee((url: string): boolean => {
  return !!GAMES.find((g) => g.slashRouterPrefix === url || g.routerPrefix === url);
});

export const isURLWithGamePrefix = memoizee((url: string): boolean => {
  return !!GAMES.find((g) => url?.includes(g.slashRouterPrefix));
});
