import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { GlobalChatComponent } from './global-chat.component';
import { LauncherComponent } from './launcher/launcher.component';
import { SharedModule } from '../_shared/shared.module';
import { WindowModule } from './window/window.module';
import * as fromGlobalChat from './_reducer';
import { GlobalChatEffects } from './_reducer';

@NgModule({
  declarations: [GlobalChatComponent, LauncherComponent],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(fromGlobalChat.featureKey, fromGlobalChat.reducer),
    EffectsModule.forFeature([GlobalChatEffects]),
    WindowModule,
  ],
  exports: [GlobalChatComponent],
})
export class GlobalChatModule {}
