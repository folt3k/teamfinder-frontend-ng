import { trigger, state, style, transition, animate } from '@angular/animations';

export const FADE_IN_DURATION = 150;
export const FADE_OUT_DURATION = 100;

export const fadeInOut = trigger('fadeInOut', [
  state(
    'open',
    style({
      transform: 'translateY(0px)',
      opacity: 1,
    })
  ),
  state(
    'close',
    style({
      transform: 'translateY(-5px)',
      opacity: 0,
    })
  ),
  transition('close => open', [animate(FADE_IN_DURATION)]),
  transition('open => close', [animate(FADE_OUT_DURATION)]),
]);
