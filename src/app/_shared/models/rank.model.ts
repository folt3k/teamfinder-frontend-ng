export interface GameRank {
  id: number;
  level: string;
  name: string;
  queue: string;
  tier: string;
  badge: {
    lg: string;
    md: string;
    sm: string;
  };
}
