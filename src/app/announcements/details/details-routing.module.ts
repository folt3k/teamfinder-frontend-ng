import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AnnouncementDetailsComponent } from './details.component';

const routes: Routes = [
  {
    path: '',
    component: AnnouncementDetailsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnnouncementDetailsRoutingModule {}
