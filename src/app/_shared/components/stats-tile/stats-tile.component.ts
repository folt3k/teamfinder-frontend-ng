import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'tf-stats-tile',
  templateUrl: './stats-tile.component.html',
  styleUrls: ['./stats-tile.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StatsTileComponent implements OnInit {
  @Input() label: string;
  @Input() value: string | number;

  constructor() {}

  ngOnInit(): void {}
}
