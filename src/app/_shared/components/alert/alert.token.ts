import { InjectionToken, TemplateRef } from '@angular/core';
import { AlertConfig } from './alert.interface';

export const ALERT_CONFIG = new InjectionToken<AlertConfig>('alert-options');
