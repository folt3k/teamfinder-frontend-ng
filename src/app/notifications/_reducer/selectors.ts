import { AppState } from './../../app.interface';
import { featureKey } from './constants';
import { State } from './reducer';
import { createSelector } from '@ngrx/store';

const selectFeature = (state: AppState): State => state[featureKey];

export const selectResults = createSelector(selectFeature, (state) => state.results);
export const selectPagination = createSelector(selectFeature, (state) => state.pagination);

export const selectUndisplayedCount = createSelector(selectFeature, (state) => state.undisplayedCount);
