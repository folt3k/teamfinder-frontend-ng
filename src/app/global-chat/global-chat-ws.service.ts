import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import { SocketService } from './../_core/services/socket.service';
import { AppState } from '../app.interface';
import { fetchMoreMessagesSuccess, receiveMessage } from './_reducer/actions';
import { BaseListRO } from '../_shared/interfaces';
import { WithListeners } from '../_core/services';
import { GlobalChatMessage } from './_models/global-chat-message.model';
import { currentProfile, currentProfileId } from '../auth/_reducer';
import { first } from 'rxjs/operators';

export enum GlobalChatSocketEvents {
  LoadMoreMessages = 'EMIT_LOAD_MORE_GLOBAL_CHAT_MESSAGES',
  LoadMoreMessagesSuccess = 'BROADCAST_LOAD_MORE_GLOBAL_CHAT_MESSAGES',
  SendMessage = 'EMIT_NEW_GLOBAL_CHAT_MESSAGE',
  ReceiveMessage = 'BROADCAST_NEW_GLOBAL_CHAT_MESSAGE',
  MarkAsRead = 'EMIT_MARK_AS_READ',
}

@Injectable({
  providedIn: 'root',
})
export class GlobalChatSocketService implements WithListeners {
  constructor(private socket: SocketService, private store: Store<AppState>) {}

  fetchMoreMessages(body: { page: number; channelId: number }): void {
    this.socket.emit(GlobalChatSocketEvents.LoadMoreMessages, body);
  }

  sendMessage(body: { senderId: number; content: string; channelId: number }): void {
    this.socket.emit(GlobalChatSocketEvents.SendMessage, {
      ...body,
      content: encodeURIComponent(body.content),
    });
  }

  markAsRead(body: { userId: number, channelId: number }): void {
    this.socket.emit(GlobalChatSocketEvents.MarkAsRead, body);
  }

  // tslint:disable-next-line:no-any
  listeners(): { [p: string]: (data: any) => void } {
    return {
      [GlobalChatSocketEvents.LoadMoreMessagesSuccess]: (
        data: BaseListRO<GlobalChatMessage> & { channelId: number }
      ) => {
        this.store.dispatch(fetchMoreMessagesSuccess(data));
      },
      [GlobalChatSocketEvents.ReceiveMessage]: (data: GlobalChatMessage) => {
        this.store
          .select(currentProfile)
          .pipe(first())
          .subscribe((profile) => {
            this.store.dispatch(receiveMessage({ message: data, currentProfile: profile }));
          });
      },
    };
  }
}
