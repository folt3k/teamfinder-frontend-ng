import { createReducer, on, Action } from '@ngrx/store';

import * as actions from './actions';
import { BaseReducerState, createBaseReducerState, clearError, getErrorState } from '../../_reducers';

export interface State extends BaseReducerState {}

export const initialState: State = createBaseReducerState({});

export const _reducer = createReducer(
  initialState,
  on(actions.register, actions.updateUser, actions.changeUserPassword, (state) => ({
    ...state,
    loading: true,
  })),
  on(
    actions.registerSuccess,
    actions.updateUserSuccess,
    actions.changeUserPasswordSuccess,
    (state, payload) => ({
      ...state,
      loading: false,
      error: clearError(),
    })
  ),
  on(
    actions.registerFailed,
    actions.updateUserFailed,
    actions.changeUserPasswordFailed,
    (state, payload) => ({
      ...state,
      loading: false,
      error: getErrorState(payload),
    })
  )
);

export function reducer(state: State, action: Action) {
  return _reducer(state, action);
}
