import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter, take } from 'rxjs/operators';

import { Comment, UserProfile } from '../../_shared/models';
import { DialogService } from '../../_shared/poppy/dialog';
import { ActionMenuOption } from '../../_shared/components/action-menu';
import { ConfirmationComponent } from '../../_shared/components/confirmation/confirmation.component';
import { AppState } from '../../app.interface';
import {
  canPerformAuthorizedAction,
  closeSubcomments,
  fetchSubcomments,
  likeComment,
  removeComment,
  removeSubcomment,
} from '../_reducer';

@Component({
  selector: 'tf-comments-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CommentsItemComponent implements OnInit {
  @Input() data: Comment;
  @Input() parentComment: Comment;
  @Input() currentUser: UserProfile;
  @Input() isSubcomment: boolean = false;

  edit: boolean = false;
  addSubcomment: boolean = false;
  actions: ActionMenuOption[] = [];
  canPerformAuthorizeAction$: Observable<boolean>;

  constructor(private dialogService: DialogService, private store: Store<AppState>) {}

  ngOnInit(): void {
    this.canPerformAuthorizeAction$ = this.store.select(canPerformAuthorizedAction);
    this.actions = this.prepareActions();
  }

  get canDisplayActionMenu(): boolean {
    return this.currentUser?.id === this.data.author.id;
  }

  get showSubcommentsLinkText(): string {
    return !this.data.showSubcomments
      ? `Wyświetl ${this.data.subcommentsCount} odpowiedzi`
      : 'Ukryj odpowiedzi';
  }

  onCancelEdit(): void {
    this.edit = false;
  }

  onAddSubcomment(): void {
    this.addSubcomment = true;
  }

  onCancelAddingSubcomment(): void {
    this.addSubcomment = false;
  }

  onLike(): void {
    this.store.dispatch(likeComment({ commentId: this.data.id }));
  }

  toggleListSubcomments(): void {
    if (this.data.showSubcomments) {
      this.store.dispatch(closeSubcomments({ commentId: this.data.id }));
    } else {
      this.store.dispatch(fetchSubcomments({ commentId: this.data.id }));
    }
  }

  private prepareActions(): ActionMenuOption[] {
    return [
      {
        label: 'Edytuj',
        callback: () => {
          this.edit = true;
        },
      },
      {
        label: 'Usuń',
        callback: () => {
          this.onRemove();
        },
      },
    ];
  }

  private onRemove(): void {
    const dialogRef = this.dialogService.open(ConfirmationComponent, {
      data: { question: 'Czy na pewno chcesz usunąć komentarz?' },
    });

    dialogRef.afterClose
      .pipe(
        take(1),
        filter((payload) => !!payload)
      )
      .subscribe(() => {
        if (this.isSubcomment) {
          this.store.dispatch(
            removeSubcomment({ commentId: this.parentComment.id, subcommentId: this.data.id })
          );
        } else {
          this.store.dispatch(removeComment({ id: this.data.id }));
        }
      });
  }
}
