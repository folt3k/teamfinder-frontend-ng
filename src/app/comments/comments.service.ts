import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import {
  CreateCommentDto,
  CreateSubcommentDto,
  UpdateCommentDto,
  UpdateSubcommentDto,
} from './comments.interface';
import { Comment } from '../_shared/models';
import {
  BaseListRO,
  EntityType,
  getEndpointPrefixByEntityType,
  ListQueryParams,
} from '../_shared/interfaces';

@Injectable({
  providedIn: 'root',
})
export class CommentsService {
  constructor(private http: HttpClient) {}

  fetchAll(
    entityData: { id: number; type: EntityType },
    params: ListQueryParams = {}
  ): Observable<BaseListRO<Comment>> {
    return this.http.get<BaseListRO<Comment>>(
      `/${getEndpointPrefixByEntityType(entityData.type)}/${entityData.id}/comments`,
      {
        params: params as any,
      }
    );
  }

  create(body: CreateCommentDto): Observable<Comment> {
    return this.http.post<Comment>('/comments', body);
  }

  update(data: UpdateCommentDto): Observable<Comment> {
    return this.http.put<Comment>(`/comments/${data.entityId}`, data);
  }

  createSubcomment(data: CreateSubcommentDto): Observable<void> {
    return this.http.post<void>(`/comments/${data.commentId}/subcomments`, data);
  }

  updateSubcomment(data: UpdateSubcommentDto): Observable<Comment> {
    return this.http.put<Comment>(`/comments/${data.commentId}/subcomments/${data.subcommentId}`, data);
  }

  fetchSubcomments(commentId: number): Observable<Comment[]> {
    return this.http.get<Comment[]>(`/comments/${commentId}/subcomments`);
  }

  likeComment(commentId: number): Observable<Comment> {
    return this.http.patch<Comment>(`/comments/${commentId}/like`, null);
  }

  removeSubcomment(commentId: number, subcommentId: number): Observable<void> {
    return this.http.delete<void>(`/comments/${commentId}/subcomments/${subcommentId}`);
  }

  removeComment(commentId: number): Observable<void> {
    return this.http.delete<void>(`/comments/${commentId}`);
  }
}
