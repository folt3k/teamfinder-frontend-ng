import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BaseStepComponent } from './base-step.component';

describe('BaseStepComponent', () => {
  let component: BaseStepComponent;
  let fixture: ComponentFixture<BaseStepComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseStepComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseStepComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
