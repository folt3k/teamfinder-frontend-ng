import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { UnsubcribeFromMailNotificationsComponent } from './unsubcribe-from-mail-notifications.component';
import { ButtonModule } from '../../_shared/components';

@NgModule({
  declarations: [UnsubcribeFromMailNotificationsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: UnsubcribeFromMailNotificationsComponent,
      },
    ]),
    ButtonModule,
  ],
})
export class UnsubcribeFromMailNotificationsModule {}
