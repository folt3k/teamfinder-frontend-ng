import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
} from '@angular/core';

import { GlobalChatMessage } from '../../_models/';

@Component({
  selector: 'tf-global-chat-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MessageComponent implements OnInit {
  @Input() message: GlobalChatMessage;

  constructor() {}

  ngOnInit(): void {}
}
