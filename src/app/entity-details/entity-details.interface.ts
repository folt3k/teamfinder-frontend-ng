import { ButtonColor } from '../_shared/components/button/button.interface';

export enum EntityType {
  USER = 'user',
  TEAM = 'team',
  ANNOUNCEMENT = 'game-announcement',
}

export interface EntityDetailsHelpers {
  isTeam: boolean;
  isUser: boolean;
  isGameAnnouncement: boolean;
}

export interface EntityDetailsDisplayConditions {
  serverRegionInfoSection?: boolean;
  userGameAccountNameInfoSection?: boolean;
  userDescInfoSection?: boolean;
  gameRolesInfoSection?: boolean;
  gameModesInfoSection?: boolean;
  gameRankInfoSection?: boolean;
  userGameAccountLevelInfoSection?: boolean;
  userLoLGameContentSection?: boolean;
  userCsGameContentSection?: boolean;
  userFntGameContentSection?: boolean;
  teamOwnerInfoSection?: boolean;
  announcementAuthorInfoSection?: boolean;
}

export interface EntityDetailsAction {
  label: string;
  callback: () => void;
  icon?: string;
  btnType?: ButtonColor;
  disabled?: boolean;
}
