import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import {
  fetchMoreMessagesSuccess,
  receiveMessage,
  receiveNewTeamMember,
  receiveNewTeamRoom,
  sendRoomMessageSuccess,
} from '../_reducer/actions';
import { SocketService } from '../../_core/services/socket.service';
import { BaseListRO } from '../../_shared/interfaces';
import { AppState } from '../../app.interface';
import { ChatMessage } from '../_models/chat-message.model';
import {
  ChatType,
  CurrentUserJoinToTeamRO,
  ReceiveTeamChatMessageRO,
  SendChatMessageRO,
} from '../chat.inerface';
import { WithListeners } from '../../_core/services';
import { currentProfile } from '../../auth/_reducer';
import { take } from 'rxjs/operators';
import { serializeTeamChatRoom } from '../_models/chat-room.model';
import { TeamMember } from '../../team';

export enum TeamChatSocketEvents {
  FetchMoreMessages = 'FETCH_MORE_TEAM_MESSAGES',
  FetchMoreMessagesSuccess = 'FETCH_MORE_TEAM_MESSAGES_SUCCESS',
  SendRoomMessage = 'SEND_NEW_TEAM_CHAT_ROOM_MESSAGE',
  SendRoomMessageSuccess = 'SEND_NEW_TEAM_CHAT_ROOM_MESSAGE_SUCCESS',
  ReceiveMessage = 'EMIT_TEAM_CHAT_MESSAGE',
  MarkRoomAsRead = 'MARK_TEAM_ROOM_AS_READ',
  CurrentUserJoinedToTeam = 'USER_ADDED_TO_TEAM',
  ReceiveNewTeamMember = 'EMIT_NEW_TEAM_MEMBER',
}

@Injectable({
  providedIn: 'root',
})
export class TeamChatSocketService implements WithListeners {
  constructor(private socket: SocketService, private store: Store<AppState>) {}

  fetchMoreMessages(body: { roomId: number; page: number }): void {
    this.socket.emit(TeamChatSocketEvents.FetchMoreMessages, body);
  }

  sendRoomMessage(body: { roomId: number; senderId: number; content: string }): void {
    this.socket.emit(TeamChatSocketEvents.SendRoomMessage, body);
  }

  markRoomAsRead(body: { roomId: number; memberId: number }): void {
    this.socket.emit(TeamChatSocketEvents.MarkRoomAsRead, body);
  }

  // tslint:disable-next-line:no-any
  listeners(): { [key: string]: (data: any) => void } {
    return {
      [TeamChatSocketEvents.FetchMoreMessagesSuccess]: (
        data: BaseListRO<ChatMessage> & { roomId: number }
      ) => {
        this.store.dispatch(fetchMoreMessagesSuccess({ ...data, chatType: ChatType.TEAM }));
      },
      [TeamChatSocketEvents.SendRoomMessageSuccess]: (data: SendChatMessageRO) => {
        this.store.dispatch(sendRoomMessageSuccess({ ...data, roomId: data.room, chatType: ChatType.TEAM }));
      },
      [TeamChatSocketEvents.ReceiveMessage]: (data: ReceiveTeamChatMessageRO) => {
        this.store.dispatch(
          receiveMessage({
            roomId: data.roomId,
            message: data.message,
            chatType: ChatType.TEAM,
          })
        );
      },
      [TeamChatSocketEvents.CurrentUserJoinedToTeam]: (data: CurrentUserJoinToTeamRO) => {
        this.store
          .select(currentProfile)
          .pipe(take(1))
          .subscribe((profile) => {
            this.store.dispatch(
              receiveNewTeamRoom({ data: serializeTeamChatRoom(data.chatRoom, { currentProfile: profile }) })
            );
          });
      },
      [TeamChatSocketEvents.ReceiveNewTeamMember]: (data: { member: TeamMember; teamId: number }) => {
        this.store.dispatch(receiveNewTeamMember(data));
      },
    };
  }
}
