import { isArray, isEmpty, isObject } from 'lodash';

type QueryParams = { [key: string]: string | string[] };

export const mapValuesToParams = (params: QueryParams): QueryParams => {
  const mappedParams: {} = {};

  Object.keys(params)
    .filter(
      (key) => !!params[key] && (isObject(params[key]) || isArray(params[key]) ? !isEmpty(params[key]) : true)
    )
    .forEach((key) => {
      const param = params[key];

      if (isArray(param)) {
        mappedParams[`${key.toString()}[]`] = param;
      } else {
        mappedParams[key] = param;
      }
    });

  return mappedParams;
};

export const mapParamsToValues = (params: QueryParams): QueryParams => {
  const mappedParams: {} = {};

  Object.keys(params).map((key) => {
    const param = params[key];
    const isArrayParam = key.includes('[]');
    if (isArrayParam) {
      const paramKey = key.replace('[]', '');
      mappedParams[paramKey] = isArray(param) ? param : [param];
    } else {
      mappedParams[key] = param;
    }
  });
  return mappedParams;
};
