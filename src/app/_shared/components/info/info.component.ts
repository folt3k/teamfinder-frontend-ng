import { ChangeDetectionStrategy, Component, HostBinding, Input, OnInit } from '@angular/core';

export type InfoStatus = 'success' | 'normal';

@Component({
  selector: 'tf-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoComponent implements OnInit {
  @Input() status: InfoStatus = 'success';

  @HostBinding(`class.success`) get success(): boolean {
    return this.status === 'success';
  }

  @HostBinding(`class.normal`) get normal(): boolean {
    return this.status === 'normal';
  }

  constructor() {}

  ngOnInit(): void {}
}
