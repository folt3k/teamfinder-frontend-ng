import { GameRole } from './role.models';

export interface Dictionaries {
  rankCategories: {
    lol: any[];
  };
  roles: {
    lol: GameRole[];
    cs: GameRole[];
    fnt: GameRole[];
  };
  ranks: {
    lol: any[];
    cs: any[];
    fnt: any[];
  };
  modes: {
    lol: any[];
    cs: any[];
    fnt: any[];
  };
  servers: {
    lol: any[];
    cs: any[];
    fnt: any[];
  };
  champions: any[];
  birthYears: any[];
  gameApiVersion: {
    n: {
      champion: string;
    };
  };
}
