import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AccountActivationBody, AccountActivationRO, LoginBody, LoginRO, UserMeRO } from './auth.interface';
import { LocalStorageService } from '../_core/services/local-storage.service';
import { AppState } from '../app.interface';
import { finalAuthorization, init } from './_reducer/actions';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(
    private http: HttpClient,
    private store: Store<AppState>,
    private localStorage: LocalStorageService
  ) {}

  login(body: LoginBody): Observable<LoginRO> {
    return this.http.post<LoginRO>('/users/login', body);
  }

  logout(): Observable<void> {
    return this.http.post<void>('/users/logout', {});
  }

  me(): Observable<UserMeRO> {
    return this.http.get<UserMeRO>('/users/me');
  }

  activateAccount(body: AccountActivationBody): Observable<AccountActivationRO> {
    return this.http.post<AccountActivationRO>('/users/activate-account', body);
  }

  init(): void {
    const token = this.localStorage.get('token');

    if (token) {
      this.store.dispatch(init({ token }));
    } else {
      this.store.dispatch(finalAuthorization());
    }
  }

  setTokenInLocalStorage(token: string): void {
    this.localStorage.set('token', token);
  }
}
