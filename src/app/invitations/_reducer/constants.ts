export const featureKey = 'team-invitations';

export const types = {
  SendInvitation: '[Invitations] Send team invitation',
  SendInvitationSuccess: '[Invitations] Send team invitation success',
  SendInvitationFailed: '[Invitations] Send team invitation failed',

  FetchInvitations: '[Invitations] Fetch invitations',
  FetchInvitationsSuccess: '[Invitations] Fetch invitations success',
  FetchInvitationsFailed: '[Invitations] Fetch invitations failed',

  FetchRequests: '[Invitations] Fetch requests',
  FetchRequestsSuccess: '[Invitations] Fetch requests success',
  FetchRequestsFailed: '[Invitations] Fetch requests failed',

  SendRequest: '[Invitations] Send team request',
  SendRequestSuccess: '[Invitations] Send team request success',
  SendRequestFailed: '[Invitations] Send team request failed',

  ManageRequest: '[Invitations] Manage team request',
  ManageRequestSuccess: '[Invitations] Manage team request success',
  ManageRequestFailed: '[Invitations] Manage team request success',

  ManageInvitation: '[Invitations] Manage team invitation',
  ManageInvitationSuccess: '[Invitations] Manage team invitation success',
  ManageInvitationFailed: '[Invitations] Manage team invitation success',
};
