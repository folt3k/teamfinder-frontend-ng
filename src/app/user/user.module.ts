import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { UserEffects } from './_reducer/effects';

import * as fromUser from './_reducer';
import { RemindPasswordDialogComponent } from './remind-password-dialog/remind-password-dialog.component';
import { SharedModule } from '../_shared/shared.module';

@NgModule({
  declarations: [RemindPasswordDialogComponent],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromUser.featureKey, fromUser.reducer),
    EffectsModule.forFeature([UserEffects]),
    SharedModule,
  ],
  exports: [RemindPasswordDialogComponent],
})
export class UserModule {}
