import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';

import { ProfileService } from '../profile/profile.service';
import { BaseEntity, BaseListRO, GameType } from '../_shared/interfaces';
import { AppState } from '../app.interface';
import { getGameTypeByRouterPrefix } from '../games';
import { STATIC_URLS } from '../_shared/common';
import { TeamService } from '../team/team.service';
import { AnnouncementsService } from '../announcements/announcements.service';
import { SeoService } from '../_core/services/seo.service';

export enum EntitiesScreens {
  USERS = 'users',
  TEAMS = 'teams',
  GAME_ANNOUNCEMENTS = 'announcements',
}

@Injectable({ providedIn: 'root' })
export class EntitiesScreenService {
  private gameType: GameType;
  private screenType: EntitiesScreens;

  data$ = new BehaviorSubject<BaseListRO<BaseEntity>>({ results: [], pagination: null });

  constructor(
    private profileService: ProfileService,
    private teamService: TeamService,
    private announcementsService: AnnouncementsService,
    private store: Store<AppState>,
    private seo: SeoService
  ) {}

  getScreenType(): EntitiesScreens {
    return this.screenType;
  }

  getGameType(): GameType {
    return this.gameType;
  }

  init(): void {
    this.store
      .select((state) => state.router)
      .pipe(take(1))
      .subscribe((router) => {
        const gamePrefix = router.state.root.firstChild.url[0]?.path;
        const screenPrefix = router.state.root.firstChild.firstChild?.url[0]?.path;

        if (this.getScreen(screenPrefix) !== this.screenType) {
          this.data$.next({ results: [], pagination: null });
        }

        if (gamePrefix) {
          this.gameType = getGameTypeByRouterPrefix(gamePrefix);
        }

        this.screenType = this.getScreen(screenPrefix);

        if (this.screenType && this.gameType) {
          this.seo.setSeo(this.screenType, this.gameType);
        }
      });
  }

  fetchAllEntities(params: { [key: string]: string | string[] } = {}): void {
    switch (this.screenType) {
      case EntitiesScreens.USERS:
        this.profileService.fetchAll(params).subscribe((data) => this.data$.next(data));
        break;
      case EntitiesScreens.TEAMS:
        this.teamService.fetchAll(params).subscribe((data) => this.data$.next(data));
        break;
      case EntitiesScreens.GAME_ANNOUNCEMENTS:
        this.announcementsService.fetchAll(params).subscribe((data) => this.data$.next(data));
        break;
    }
  }

  private getScreen(routerSegment: string): EntitiesScreens {
    switch (routerSegment) {
      case STATIC_URLS['users'].path:
        return EntitiesScreens.USERS;
      case STATIC_URLS['teams'].path:
        return EntitiesScreens.TEAMS;
      case STATIC_URLS['announcements'].path:
        return EntitiesScreens.GAME_ANNOUNCEMENTS;
      default:
        return null;
    }
  }
}
