import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { HeaderComponent } from './header.component';
import { SharedModule } from '../../../_shared/shared.module';
import { UserMenuComponent } from './user-menu/user-menu.component';
import { NotificationsModule } from '../../../notifications/notifications.module';
import { ChatModule } from '../../../chat/chat.module';
import { GamesModule } from '../../../games/games.module';
import { ResponsiveMenuComponent } from './responsive-menu/responsive-menu.component';

@NgModule({
  declarations: [HeaderComponent, UserMenuComponent, ResponsiveMenuComponent],
  imports: [CommonModule, SharedModule, RouterModule, NotificationsModule, ChatModule, GamesModule],
  exports: [HeaderComponent],
})
export class HeaderModule {}
