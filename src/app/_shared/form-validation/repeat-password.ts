import { FormGroup } from '@angular/forms';

export const repeatPasswordValidator = (passwordFieldKeys: [string, string]) => (group: FormGroup) => {
  const pass = group.get(passwordFieldKeys[0]).value;
  const repeatedPass = group.get(passwordFieldKeys[1]).value;

  return pass === repeatedPass ? null : { repeatPassword: true };
};
