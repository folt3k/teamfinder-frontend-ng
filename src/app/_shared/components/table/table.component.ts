import {
  Component,
  Input,
  OnInit,
  Output,
  ViewEncapsulation,
  EventEmitter,
  TemplateRef,
  QueryList,
  ContentChildren,
} from '@angular/core';

import { Pagination } from '../../interfaces';
import { TableCellDirective } from './table-cell.directive';
import { TableAction, TableActions } from './table.interface';
import { TeamInvitation } from '../../../invitations/invitations.model';

@Component({
  selector: 'tf-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class TableComponent<T> implements OnInit {
  @ContentChildren(TableCellDirective, { read: TemplateRef }) templates: QueryList<TemplateRef<HTMLElement>>;

  @Input() columns: string[];
  @Input() data: T[];
  @Input() pagination: Pagination;
  @Input() actions: (T) => TableActions<T>;

  @Output() pageChanged = new EventEmitter<number>();

  constructor() {}

  ngOnInit(): void {}

  trackBy(index, item: T & { id: number }): number {
    return item.id;
  }

  actionTrackBy(index, item: TableAction<TeamInvitation>): string {
    return item.label;
  }

  onPageChange(page: number): void {
    this.pageChanged.emit(page);
  }
}
