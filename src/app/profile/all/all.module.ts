import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AllProfilesRoutingModule } from './all-routing.module';
import { AllProfilesComponent } from './all.component';
import { EntitiesScreenModule } from '../../entities-screen/entities-screen.module';

@NgModule({
  declarations: [AllProfilesComponent],
  imports: [CommonModule, AllProfilesRoutingModule, EntitiesScreenModule],
})
export class AllProfilesModule {}
