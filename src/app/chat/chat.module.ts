import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '../_shared/shared.module';
import { StoreModule } from '@ngrx/store';
import * as fromChat from './_reducer';
import { RoomsCompactListComponent } from './rooms-compact-list/rooms-compact-list.component';
import { RoomsCompactListItemComponent } from './rooms-compact-list/item/item.component';
import { ChatComponent } from './chat.component';
import { RoomWindowComponent } from './room-window/room-window.component';
import { MessageComponent } from './message/message.component';
import { RoomWindowLauncherComponent } from './room-window-launcher/room-window-launcher.component';

@NgModule({
  declarations: [
    RoomsCompactListComponent,
    RoomsCompactListItemComponent,
    ChatComponent,
    RoomWindowComponent,
    MessageComponent,
    RoomWindowLauncherComponent,
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromChat.featureKey, fromChat.reducer),
    EffectsModule.forFeature([fromChat.ChatEffects]),
    SharedModule,
  ],
  exports: [ChatComponent, RoomsCompactListComponent],
})
export class ChatModule {}
