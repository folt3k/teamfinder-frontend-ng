export const featureKey = 'comment';

export const types = {
  Create: '[Comment] Create comment',
  CreateSuccess: '[Comment] Create comment success',
  CreateFailed: '[Comment] Create comment failed',

  Update: '[Comment] Update comment',
  UpdateSuccess: '[Comment] Update comment success',
  UpdateFailed: '[Comment] Update comment failed',

  CreateSubcomment: '[Comment] Create subcomment',
  CreateSubcommentSuccess: '[Comment] Create  subcomment success',
  CreateSubcommentFailed: '[Comment] Create subcomment failed',

  UpdateSubcomment: '[Comment] Update subcomment',
  UpdateSubcommentSuccess: '[Comment] Update  subcomment success',
  UpdateSubcommentFailed: '[Comment] Update subcomment failed',

  FetchAll: '[Comment] Fetch all comments',
  FetchAllSuccess: '[Comment] Fetch all comments success',
  FetchAllFailed: '[Comment] Fetch all comments failed',

  FetchSubcomments: '[Comment] Fetch  subcomments',
  FetchSubcommentsSuccess: '[Comment] Fetch  subcomments success',
  FetchSubcommentsFailed: '[Comment] Fetch  subcomments failed',

  Like: '[Comment] Like comment',
  LikeSuccess: '[Comment] Like comment success',
  LikeFailed: '[Comment] Like comment failed',

  CloseSubcomments: '[Comment] Close subcomments',

  Remove: '[Comment] Remove comment',
  RemoveSuccess: '[Comment] Remove comment success',
  RemoveFailed: '[Comment] Remove comment failed',

  RemoveSubcomment: '[Comment] Remove subcomment',
  RemoveSubcommentSuccess: '[Comment] Remove subcomment success',
  RemoveSubcommentFailed: '[Comment] Remove subcomment failed',
};
