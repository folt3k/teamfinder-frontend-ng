import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState } from '../../app.interface';
import { fetchAllComments, loading, selectCommentResults, canLoadMoreComments } from '../_reducer';
import { Observable } from 'rxjs';
import { Comment, UserProfile } from '../../_shared/models';
import { currentProfile } from '../../auth/_reducer';

@Component({
  selector: 'tf-comments-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class CommentsListComponent implements OnInit {
  comments$: Observable<Comment[]>;
  currentUser$: Observable<UserProfile>;
  loading$: Observable<boolean>;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.store.dispatch(fetchAllComments());
    this.comments$ = this.store.select(selectCommentResults);
    this.currentUser$ = this.store.select(currentProfile);
    this.loading$ = this.store.select(loading);
  }

  get canDisplayLoadMoreBtn$(): Observable<boolean> {
    return this.store.select(canLoadMoreComments);
  }

  loadMore(): void {
    this.store.dispatch(fetchAllComments());
  }
}
