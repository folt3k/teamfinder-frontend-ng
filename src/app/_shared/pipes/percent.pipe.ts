import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'percent',
})
export class PercentPipe implements PipeTransform {
  transform(value: number | string): string {
    // if (typeof value === 'number') {
    //   return value / 100 + '%';
    // }
    return value + '%';
  }
}
