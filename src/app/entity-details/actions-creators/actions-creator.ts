import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { isPlatformServer } from '@angular/common';
import { combineLatest, Observable } from 'rxjs';

import { AppState } from '../../app.interface';
import { DialogService } from '../../_shared/poppy/dialog';
import { currentProfile, isAuthorizationCompleted } from '../../auth/_reducer';
import { UserProfile } from '../../_shared/models';
import { BaseEntity } from '../../_shared/interfaces';
import { selectDetails } from '../_reducer';
import { EntityDetailsAction } from '../entity-details.interface';
import { isCurrentUserGameEqualsToEntityGame } from '../../games';

export interface IEntityDetailsActionsCreator {
  getActions$(): Observable<EntityDetailsAction[]>;
}

@Injectable({
  providedIn: 'root',
})
export class EntityDetailsActionsCreator {
  constructor(
    protected store: Store<AppState>,
    protected dialogService: DialogService,
    protected router: Router,
    @Inject(PLATFORM_ID) private platformId: object
  ) {}

  get data$(): Observable<[UserProfile, BaseEntity, boolean]> {
    return combineLatest(
      this.store.select(currentProfile),
      this.store.select(selectDetails),
      this.store.select(isAuthorizationCompleted)
    );
  }

  isCurrentUserGameEqualsToEntityGame(user: UserProfile, entity: BaseEntity): boolean {
    return isCurrentUserGameEqualsToEntityGame(user, entity);
  }

  isPlatformServer(): boolean {
    return isPlatformServer(this.platformId);
  }
}
