import { createAction, props } from '@ngrx/store';

import { types } from './constants';
import { BaseListRO, ErrorRO } from '../../_shared/interfaces';
import { Comment } from '../../_shared/models';

export const createComment = createAction(types.Create, props<{ content: string }>());
export const createCommentSuccess = createAction(types.CreateSuccess, props<{ data: Comment }>());
export const createCommentFailed = createAction(types.CreateFailed, props<ErrorRO>());

export const updateComment = createAction(types.Update, props<{ content: string; entityId: number }>());
export const updateCommentSuccess = createAction(types.UpdateSuccess, props<{ data: Comment }>());
export const updateCommentFailed = createAction(types.UpdateFailed, props<ErrorRO>());

export const createSubcomment = createAction(
  types.CreateSubcomment,
  props<{ content: string; commentId: number }>()
);
export const createSubcommentSuccess = createAction(
  types.CreateSubcommentSuccess,
  props<{ commentId: number }>()
);
export const createSubcommentFailed = createAction(types.CreateSubcommentFailed, props<ErrorRO>());

export const updateSubcomment = createAction(
  types.UpdateSubcomment,
  props<{ content: string; commentId: number; subcommentId: number }>()
);
export const updateSubcommentSuccess = createAction(
  types.UpdateSubcommentSuccess,
  props<{ subcommentId: number }>()
);
export const updateSubcommentFailed = createAction(types.UpdateSubcommentFailed, props<ErrorRO>());

export const fetchAllComments = createAction(types.FetchAll);
export const fetchAllCommentsSuccess = createAction(types.FetchAllSuccess, props<BaseListRO<Comment>>());
export const fetchAllCommentsFailed = createAction(types.FetchAllFailed, props<ErrorRO>());

export const fetchSubcomments = createAction(types.FetchSubcomments, props<{ commentId: number }>());
export const fetchSubcommentsSuccess = createAction(
  types.FetchSubcommentsSuccess,
  props<{ data: Comment[]; commentId: number }>()
);
export const fetchSubcommentsFailed = createAction(types.FetchSubcommentsFailed, props<ErrorRO>());

export const likeComment = createAction(types.Like, props<{ commentId: number }>());
export const likeCommentSuccess = createAction(types.LikeSuccess, props<{ data: Comment }>());
export const likeCommentFailed = createAction(types.LikeFailed, props<ErrorRO>());

export const closeSubcomments = createAction(types.CloseSubcomments, props<{ commentId: number }>());

export const removeComment = createAction(types.Remove, props<{ id: number }>());
export const removeCommentSuccess = createAction(types.RemoveSuccess, props<{ id: number }>());
export const removeCommentFailed = createAction(types.RemoveFailed, props<ErrorRO>());

export const removeSubcomment = createAction(
  types.RemoveSubcomment,
  props<{ commentId: number; subcommentId: number }>()
);
export const removeSubcommentSuccess = createAction(
  types.RemoveSubcommentSuccess,
  props<{ commentId: number; subcommentId: number }>()
);
export const removeSubcommentFailed = createAction(types.RemoveSubcommentFailed, props<ErrorRO>());
