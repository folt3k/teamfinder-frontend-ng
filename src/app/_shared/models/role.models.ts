export interface GameRole {
  id: number;
  name: string;
  value: string;
  label: string;
  iconUrl: string;
}
