import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { combineLatest, Observable, of } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';

import { AppState } from '../../app.interface';
import { fetchEntityDetails, fetchEntityDetailsSuccess, selectDetails } from '../../entity-details/_reducer';
import { currentProfile, meSuccess } from '../../auth/_reducer';
import { UserProfile } from '../../_shared/models';
import { EntityType } from '../../entity-details';

@Injectable()
export class EditTeamResolver implements Resolve<boolean> {
  constructor(private store: Store<AppState>, private actions: Actions, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    return combineLatest(this.store.select(currentProfile), this.store.select(selectDetails)).pipe(
      take(1),
      switchMap(([profile, details]) => {
        // If user is logged in and team page is opened
        if (profile && details?.owner) {
          if (profile.id === details.owner.id) {
            return of(true);
          }
          this.router.navigate(['/']);
          return of(false);
        }

        // On page load directly

        // If current user exists
        if (profile) {
          return this.fetchTeamsDetails(route, profile);
        }
        // If current user is fetching
        return this.actions.pipe(
          ofType(meSuccess),
          take(1),
          switchMap((action) => this.fetchTeamsDetails(route, action.currentProfile))
        );
      })
    );
  }

  private fetchTeamsDetails(route: ActivatedRouteSnapshot, profile: UserProfile): Observable<boolean> {
    this.store.dispatch(
      fetchEntityDetails({ identifier: route.paramMap.get('identifier'), entityType: EntityType.TEAM })
    );

    return this.actions.pipe(
      ofType(fetchEntityDetailsSuccess),
      take(1),
      map(({ data }) => {
        if (data.owner.id === profile.id) {
          return true;
        }
        this.router.navigate(['/']);
        return false;
      })
    );
  }
}
