import { Notification } from './notification.model';
import { BaseListRO } from '../_shared/interfaces';

export interface NotificationsListAllRO extends BaseListRO<Notification> {}
