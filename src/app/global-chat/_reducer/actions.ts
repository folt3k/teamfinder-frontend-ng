import { createAction, props } from '@ngrx/store';

import { types } from './constants';
import { GlobalChatInitRO } from './../global-chat.interface';
import { BaseListRO, ErrorRO } from 'src/app/_shared/interfaces';
import { GlobalChatMessage } from '../_models';
import { UserProfile } from 'src/app/_shared/models';

export const init = createAction(types.Init);
export const initSuccess = createAction(
  types.InitSuccess,
  props<GlobalChatInitRO & { currentUser: UserProfile }>()
);
export const initFailed = createAction(types.InitFailed, props<ErrorRO>());

export const fetchMoreMessages = createAction(types.FetchMoreMessages, props<{ channelId: number }>());
export const fetchMoreMessagesSuccess = createAction(
  types.FetchMoreMessagesSuccess,
  props<BaseListRO<GlobalChatMessage> & { channelId: number }>()
);

export const sendMessage = createAction(types.SendMessage, props<{ content: string }>());

export const markAsRead = createAction(types.MarkAsRead, props<{ channelId: number }>());

export const receiveMessage = createAction(
  types.ReceiveMessage,
  props<{ message: GlobalChatMessage; currentProfile: UserProfile }>()
);

export const openChat = createAction(types.OpenChat);
export const closeChat = createAction(types.CloseChat);
export const maximizeChat = createAction(types.MaximizeChat);
export const minimizeChat = createAction(types.MinimizeChat);

export const updateActiveUsers = createAction(
  types.UpdateActiveUsers,
  props<{ profile: Partial<UserProfile>; isOnline: boolean }>()
);

export const toggleGroup = createAction(types.ToggleGroup, props<{ groupId: number }>());
export const selectChannel = createAction(types.SelectChannel, props<{ channelId: number }>());
