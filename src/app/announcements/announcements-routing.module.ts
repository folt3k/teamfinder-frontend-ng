import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DYNAMIC_URLS, STATIC_URLS } from '../_shared/common';

const routes: Routes = [
  {
    path: STATIC_URLS['announcements'].path,
    loadChildren: () => import('./all/all.module').then((m) => m.AllAnnouncementsModule),
  },
  {
    path: DYNAMIC_URLS['GAME-ANNOUNCEMENT']().symbolPath,
    loadChildren: () => import('./details/details.module').then((m) => m.AnnouncementDetailsModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AnnouncementsRoutingModule {}
