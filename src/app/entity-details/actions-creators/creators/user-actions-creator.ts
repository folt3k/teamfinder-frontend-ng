import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { EntityDetailsAction } from '../../entity-details.interface';
import { EntityDetailsActionsCreator, IEntityDetailsActionsCreator } from '../actions-creator';
import { openConversation } from '../../../chat/_reducer';
import { InviteMemberDialogComponent } from '../../../invitations/invite-member-dialog/invite-member-dialog.component';
import { UserProfile } from '../../../_shared/models';

@Injectable({
  providedIn: 'root',
})
export class EntityDetailsUserActionsCreator extends EntityDetailsActionsCreator
  implements IEntityDetailsActionsCreator {
  getActions$(): Observable<EntityDetailsAction[]> {
    return this.data$.pipe(
      map(([currentUser, _data]) => {
        const actions: EntityDetailsAction[] = [];
        const data = _data as UserProfile;

        if (this.isPlatformServer()) {
          return actions;
        }

        if (currentUser && currentUser?.id !== data.id) {
          actions.push({
            label: 'Wyślij wiadomość',
            callback: () => {
              this.store.dispatch(openConversation({ receiver: data }));
            },
            icon: 'mail_outline',
          });
          if (this.isCurrentUserGameEqualsToEntityGame(currentUser, data)) {
            actions.push({
              label: 'Zaproś gracza do drużyny',
              callback: () => {
                this.dialogService.open(InviteMemberDialogComponent);
              },
              icon: 'person_add',
            });
          }
        }

        return actions;
      })
    );
  }
}
