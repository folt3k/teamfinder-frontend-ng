import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';

import { CurrentUser } from 'src/app/_shared/models';
import { MY_PROFILE_STATIC_URLS, STATIC_URLS } from '../../../../_shared/common';

@Component({
  selector: 'tf-header-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserMenuComponent implements OnInit {
  @Input() currentUser: CurrentUser;
  @Output() logout = new EventEmitter();

  STATIC_URLS = STATIC_URLS;
  MY_PROFILE_STATIC_URLS = MY_PROFILE_STATIC_URLS;

  constructor() {}

  ngOnInit(): void {}

  onLogout(): void {
    this.logout.emit();
  }
}
