import { createSelector } from '@ngrx/store';
import { isArray, pick } from 'lodash';

import { AppState } from '../../app.interface';
import { State } from './reducer';
import { featureKey as notificationFeatureKey } from '../../notifications/_reducer';
import { featureKey as chatFeatureKey } from '../../chat/_reducer';
import { featureKey } from './constants';

const selectFeature = (state: AppState) => state[featureKey];

export const selectUI = createSelector(selectFeature, (state, keys?: string | string[]):
  | State
  | Partial<State>
  | any => {
  if (keys) {
    if (isArray(keys)) {
      return pick(state, keys);
    }
    return state[keys] as any;
  }

  return state as State;
});

export const selectNotificationCountersSum = createSelector(
  (state: AppState) => state[notificationFeatureKey].undisplayedCount,
  (state: AppState) => state[chatFeatureKey].unreadMessagesCount,
  (nCount, mCount) => nCount + mCount
);
