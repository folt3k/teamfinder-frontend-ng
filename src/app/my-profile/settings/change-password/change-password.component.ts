import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

import { repeatPasswordValidator } from '../../../_shared/form-validation';
import { AppState } from '../../../app.interface';
import { changeUserPassword, loading } from '../../../user/_reducer';
import * as fromUser from '../../../user/_reducer';

@Component({
  selector: 'tf-my-profile-settings-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class MyProfileSettingsChangePasswordComponent implements OnInit {
  form: FormGroup;
  submitting$: Observable<boolean>;
  USER_REDUCER_KEY = fromUser.featureKey;
  show = false;

  constructor(private fb: FormBuilder, private store: Store<AppState>, private actions: Actions) {}

  ngOnInit(): void {
    this.initForm();
    this.submitting$ = this.store.select(loading);
  }

  onSubmit(): void {
    this.store.dispatch(changeUserPassword({ body: this.form.value }));

    this.actions.pipe(ofType(fromUser.changeUserPasswordSuccess), take(1)).subscribe(() => {
      this.show = false;
      this.form.reset();
    });
  }

  private initForm(): void {
    this.form = this.fb.group(
      {
        currentPassword: ['', [Validators.required]],
        newPassword: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(50)]],
        repeatedPassword: ['', [Validators.required]],
      },
      { validator: repeatPasswordValidator(['newPassword', 'repeatedPassword']) }
    );
  }
}
