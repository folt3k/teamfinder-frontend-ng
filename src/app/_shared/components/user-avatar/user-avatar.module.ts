import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopoverModule } from '@ng-poppy/lib';

import { UserAvatarComponent } from './user-avatar.component';
import { OnlineStatusIndicatorModule } from '../online-status-indicator/online-status-indicator.module';
import { PipesModule } from '../../pipes/pipes.module';
import { UserCompactInfoModule } from '../user-compact-info/user-compact-info.module';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [UserAvatarComponent],
  imports: [
    CommonModule,
    PopoverModule,
    OnlineStatusIndicatorModule,
    PipesModule,
    UserCompactInfoModule,
    DirectivesModule,
  ],
  exports: [UserAvatarComponent],
})
export class UserAvatarModule {}
