import { AppState } from './../../app.interface';
import { CurrentUser } from 'src/app/_shared/models';

export const error = (state: AppState) => state.auth.error;

export const loading = (state: AppState) => state.auth.loading;

export const isLoggedIn = (state: AppState) => !!state.auth.token;

export const currentProfile = (state: AppState) => state.auth.profile;

export const currentUser = (state: AppState): CurrentUser => ({
  profile: state.auth.profile,
  user: state.auth.user,
});

export const currentProfileId = (state: AppState) => state.auth.profile && state.auth.profile.id;

export const isAuthorizationCompleted = (state: AppState) => state.auth.authorizationCompleted;
