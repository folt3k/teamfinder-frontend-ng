import { Directive, HostListener, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { first, withLatestFrom } from 'rxjs/operators';

import { updateUI } from '../../_reducers/ui';
import { STATIC_URLS } from '../common';
import { AppState } from '../../app.interface';
import { isLoggedIn, meSuccess } from '../../auth/_reducer';
import { Actions, ofType } from '@ngrx/effects';

@Directive({
  selector: '[tfAuthorizationNeeded]',
  exportAs: 'tfAuthorizationNeeded',
})
export class AuthorizationNeededDirective {
  @Output() afterAuthorization = new EventEmitter();

  constructor(private store: Store<AppState>, private router: Router, private actions: Actions) {}

  @HostListener('click')
  onClick(): void {
    this.store
      .select(isLoggedIn)
      .pipe(withLatestFrom(this.store.select((state) => state.router)), first())
      .subscribe(([logged, router]) => {
        if (!logged) {
          const redirectPath: string = router.state.url;
          this.store.dispatch(updateUI({ redirectPath }));
          this.router.navigate([STATIC_URLS['login'].slashPath]);

          if (this.afterAuthorization) {
            this.actions.pipe(ofType(meSuccess), first()).subscribe(() => {
              this.afterAuthorization.emit();
            });
          }
        } else {
          if (this.afterAuthorization) {
            this.afterAuthorization.emit();
          }
        }
      });
  }
}
