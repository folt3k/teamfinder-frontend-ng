import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'minutesDuration',
})
export class MinutesDurationPipe implements PipeTransform {
  transform(value: number): string {
    const mins = Math.ceil(value / 60);
    return `${mins} minut`;
  }
}
