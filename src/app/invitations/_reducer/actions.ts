import { createAction, props } from '@ngrx/store';

import { types } from './constants';
import {
  ListTeamInvitationsQueryParams,
  ListTeamRequestsQueryParams,
  ManageTeamInvitationAction,
  SendTeamInvitationDto,
  SendTeamRequestDto,
} from '../invitations.interface';
import { TeamInvitation } from '../invitations.model';
import { BaseListRO, ErrorRO } from '../../_shared/interfaces';

export const sendInvitation = createAction(types.SendInvitation, props<{ body: SendTeamInvitationDto }>());
export const sendInvitationSuccess = createAction(types.SendInvitationSuccess, props<TeamInvitation>());
export const sendInvitationFailed = createAction(types.SendInvitationFailed, props<ErrorRO>());

export const fetchInvitations = createAction(
  types.FetchInvitations,
  props<{ params: ListTeamInvitationsQueryParams }>()
);
export const fetchInvitationsSuccess = createAction(
  types.FetchInvitationsSuccess,
  props<{ data: BaseListRO<TeamInvitation> }>()
);
export const fetchInvitationsFailed = createAction(types.FetchInvitationsFailed, props<ErrorRO>());

export const sendRequest = createAction(types.SendRequest, props<{ body: SendTeamRequestDto }>());
export const sendRequestSuccess = createAction(types.SendRequestSuccess, props<TeamInvitation>());
export const sendRequestFailed = createAction(types.SendRequestFailed, props<ErrorRO>());

export const fetchRequests = createAction(
  types.FetchRequests,
  props<{ params: ListTeamRequestsQueryParams }>()
);
export const fetchRequestsSuccess = createAction(
  types.FetchRequestsSuccess,
  props<{ data: BaseListRO<TeamInvitation> }>()
);
export const fetchRequestsFailed = createAction(types.FetchRequestsFailed, props<ErrorRO>());

export const manageRequest = createAction(
  types.ManageRequest,
  props<{ id: number; action: ManageTeamInvitationAction }>()
);
export const manageRequestSuccess = createAction(types.ManageRequestSuccess, props<TeamInvitation>());
export const manageRequestFailed = createAction(types.ManageRequestFailed, props<ErrorRO>());

export const manageInvitation = createAction(
  types.ManageInvitation,
  props<{ id: number; action: ManageTeamInvitationAction }>()
);
export const manageInvitationSuccess = createAction(types.ManageInvitationSuccess, props<TeamInvitation>());
export const manageInvitationFailed = createAction(types.ManageInvitationFailed, props<ErrorRO>());
