export const featureKey = 'auth';

export const types = {
  Init: '[Auth] Init',

  Login: '[Auth] Login',
  LoginSuccess: '[Auth] Login success',
  LoginFailed: '[Auth] Login failed',

  Logout: '[Auth] Logout',
  LogoutSuccess: '[Auth] Logout success',

  Me: '[Auth] Me',
  MeSuccess: '[Auth] Me success',
  MeFailed: '[Auth] Me failed',

  AccountActivation: '[Auth] Account activation',
  AccountActivationSuccess: '[Auth] Account activation success',
  AccountActivationFailed: '[Auth] Account activation failed',

  ClearError: '[Auth] Clear error',

  FinalAuthorization: '[Auth] Final authorization',
};
