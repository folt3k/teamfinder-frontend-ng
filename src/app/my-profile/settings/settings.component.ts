import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, filter, map, switchMap } from 'rxjs/operators';

import { User } from '../../_shared/models';
import { AppState } from '../../app.interface';
import { currentUser } from '../../auth/_reducer';
import { DialogService } from '../../_shared/poppy/dialog';
import { ConfirmationComponent } from '../../_shared/components/confirmation/confirmation.component';
import { UserService } from '../../user/user.service';

@Component({
  selector: 'tf-my-profile-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class MyProfileSettingsComponent implements OnInit {
  currentUser$: Observable<User>;

  constructor(
    private store: Store<AppState>,
    private dialogService: DialogService,
    private usersService: UserService
  ) {}

  ngOnInit(): void {
    this.currentUser$ = this.store.select(currentUser).pipe(map((user) => user.user));
  }

  removeAccount(): void {
    const dialogRef = this.dialogService.open(ConfirmationComponent, {
      data: {
        question: 'Czy na pewno chcesz usunąć swoje konto? Jego przywrócenie nie będzie już później możliwe.',
      },
    });

    dialogRef.afterClose
      .pipe(filter(Boolean))
      .pipe(
        switchMap(() => this.usersService.remove()),
        catchError(() => of(false))
      )
      .subscribe(() => {
        window.location.reload();
      });
  }
}
