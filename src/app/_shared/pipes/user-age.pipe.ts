import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'userAge',
})
export class UserAgePipe implements PipeTransform {
  transform(value: number): number {
    return new Date().getFullYear() - value;
  }
}
