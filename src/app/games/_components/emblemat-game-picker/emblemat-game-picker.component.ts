import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { GAMES, GameOption } from '../../../_shared/common';

@Component({
  selector: 'tf-emblemat-game-picker',
  templateUrl: './emblemat-game-picker.component.html',
  styleUrls: ['./emblemat-game-picker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmblematGamePickerComponent implements OnInit {
  @Output() selected = new EventEmitter<GameOption>();

  GAMES: GameOption[] = GAMES;

  constructor() {}

  ngOnInit(): void {}

  onItemClick(item: GameOption): void {
    this.selected.emit(item);
  }
}
