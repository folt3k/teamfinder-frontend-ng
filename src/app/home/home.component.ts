import { Component, Inject, OnInit, PLATFORM_ID } from '@angular/core';
import { Store } from '@ngrx/store';
import { isPlatformBrowser } from '@angular/common';
import { Observable } from 'rxjs';

import { LocalStorageService } from '../_core/services';
import { AppState } from '../app.interface';

@Component({
  selector: 'tf-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  isAuthToken$: Observable<boolean>;
  isBrowser: boolean = false;

  constructor(
    private localStorage: LocalStorageService,
    @Inject(PLATFORM_ID) private platformId: object,
    private store: Store<AppState>
  ) {}

  ngOnInit(): void {
    this.isAuthToken$ = this.store.select((store) => !!store.auth.token);
    this.isBrowser = isPlatformBrowser(this.platformId);
  }
}
