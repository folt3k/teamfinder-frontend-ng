import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

import { UserProfile } from '../../models';

@Component({
  selector: 'tf-user-compact-info',
  templateUrl: './user-compact-info.component.html',
  styleUrls: ['./user-compact-info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserCompactInfoComponent implements OnInit {
  @Input() user: UserProfile;

  constructor() {}

  ngOnInit(): void {}
}
