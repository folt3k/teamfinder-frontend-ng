import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  Host,
  OnInit,
  Optional,
  Output,
  SkipSelf,
  ViewEncapsulation,
} from '@angular/core';
import { ControlContainer, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { CustomFormControl } from '../custom-form-control';
import { triggerOnEnterKey } from '../../../_helpers';

@Component({
  selector: 'tf-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SearchInputComponent),
      multi: true,
    },
  ],
})
export class SearchInputComponent extends CustomFormControl implements OnInit, ControlValueAccessor {
  @Output() searched = new EventEmitter<string>();

  value: string = '';

  constructor(
    cdr: ChangeDetectorRef,
    @Optional()
    @Host()
    @SkipSelf()
    controlContainer: ControlContainer
  ) {
    super(cdr, controlContainer);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  writeValue(value: string): void {
    this.value = value || '';
    this.cdr.detectChanges();
  }

  registerOnChange(fn: () => {}): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => {}): void {
    this.propagateTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
    this.cdr.detectChanges();
  }

  onChange(value: string): void {
    this.value = value;
    this.cdr.detectChanges();
  }

  onBlur(): void {}

  onFocus(): void {
    this.propagateTouched();
    this.cdr.detectChanges();
  }

  onKeyPress(event: KeyboardEvent): void {
    triggerOnEnterKey(event, () => {
      this.onSearch();
    });
  }

  onSearch(): void {
    this.searched.emit(this.value);
    this.propagateChange(this.value);
  }

  onClear(): void {
    this.onChange('');
    this.propagateChange(this.value);
  }

  private propagateChange(value: string): void {}

  private propagateTouched(): void {}
}
