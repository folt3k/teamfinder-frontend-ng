import { BaseEntity, GameType } from '../interfaces';
import { GameRole } from './role.models';
import { GameRank } from './rank.model';
import { LolGameChampion } from './game-models';
import { GameAccountStats } from '../../games';
import { Team } from '../../team';
import { User } from './user.model';
import { TeamInvitation } from '../../invitations/invitations.model';

export interface UserProfile extends BaseEntity {
  username: string;
  logoUrl: string;
  isOnline: boolean;
  lastOnlineAt: Date;
  type: GameType;
  roles: GameRole[];
  ranks: GameRank[];
  game_metadata: UserProfileMetadata;
  ownedTeams?: Team[];
  teamRequests?: TeamInvitation[];
  user?: User;
}

export interface UserProfileMetadata {
  id: string;
  accountId: string;
  accountName: string;
  lastGameStatsUpdated: Date;
  favouriteChampions: LolGameChampion[];
  accountServer?: { id: string; name: string };
  accountStats?: GameAccountStats;
}
