import { EventEmitter } from '@angular/core';

export class DialogRef implements IDialogRef {
  afterShow: EventEmitter<void>;
  afterClose: EventEmitter<any>;
  close: (payload?: any) => void;
}

export interface IDialogRef {
  afterShow: EventEmitter<void>;
  afterClose: EventEmitter<any>;
  close: (payload?: any) => void;
}
