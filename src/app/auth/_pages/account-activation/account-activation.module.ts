import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountActivationComponent } from './account-activation.component';
import { AccountActivationRoutingModule } from './account-activation-routing.module';
import { LoaderModule } from '../../../_shared/components';

@NgModule({
  declarations: [AccountActivationComponent],
  imports: [CommonModule, AccountActivationRoutingModule, LoaderModule],
})
export class AccountActivationModule {}
