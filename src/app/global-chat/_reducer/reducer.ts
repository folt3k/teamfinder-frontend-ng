import { createReducer, on, Action } from '@ngrx/store';

import { GlobalChatMessage } from './../_models';
import { Pagination } from '../../_shared/interfaces';
import { UserProfile } from '../../_shared/models';
import * as actions from './actions';
import { BaseReducerState, createBaseReducerState, clearError, getErrorState } from 'src/app/_reducers';
import { meSuccess } from 'src/app/auth/_reducer';
import { updateUserNetworkStatus } from '../../user/_reducer/actions';
import { GlobalChatChannel, GlobalChatGroup } from '../global-chat.interface';

export interface GlobalChatStateChannel extends GlobalChatChannel {
  messages: {
    results: GlobalChatMessage[];
    pagination: Pagination;
  };
  active?: boolean;
  wasInitialDataFetched?: boolean;
  loading?: boolean;
  unseenMessagesSinceLastDisplay?: number;
  scrollPosition?: number;
}

export interface GlobalChatStateGroup extends GlobalChatGroup {
  channels: GlobalChatStateChannel[];
  expanded: boolean;
}

export interface State extends BaseReducerState {
  groups: GlobalChatStateGroup[];
  channels: GlobalChatStateChannel[];
  activeUsers: UserProfile[];
  isChatOpen: boolean;
  isChatMaximized: boolean;
  isChatInitialized: boolean;
}

export const initialState: State = createBaseReducerState<State>({
  activeUsers: [],
  groups: [],
  channels: [],
  isChatOpen: false,
  isChatMaximized: false,
  isChatInitialized: false,
});

const _reducer = createReducer(
  initialState,

  on(actions.init, (state) => ({
    ...state,
    error: clearError(),
    loading: true,
  })),
  on(actions.initSuccess, (state, { groups, channels, initChannel, activeUsers, currentUser }) => ({
    ...state,
    groups: groups.map((group) => ({
      ...group,
      expanded: currentUser
        ? currentUser?.type === group.channels[0].type
        : !!group.channels.find((ch) => initChannel.id === ch.id),
      channels: group.channels.map((ch) =>
        initChannel.id === ch.id
          ? {
              ...ch,
              messages: { results: initChannel.results, pagination: initChannel.pagination },
              active: true,
              wasInitialDataFetched: true,
            }
          : { ...ch, messages: { results: [], pagination: null } }
      ),
    })),
    channels: channels.map((ch) =>
      initChannel.id === ch.id
        ? {
            ...ch,
            messages: { results: initChannel.results, pagination: initChannel.pagination },
            active: true,
            wasInitialDataFetched: true,
          }
        : { ...ch, messages: { results: [], pagination: null } }
    ),
    loading: false,
    isChatInitialized: true,
    activeUsers,
  })),
  on(actions.initFailed, (state, payload) => ({
    ...state,
    error: getErrorState(payload),
    loading: false,
  })),
  on(actions.toggleGroup, (state, { groupId }) => ({
    ...state,
    groups: state.groups.map((g) => (g.id === groupId ? { ...g, expanded: !g.expanded } : g)),
  })),
  on(actions.selectChannel, (state, { channelId }) => ({
    ...state,
    groups: state.groups.map((group) => ({
      ...group,
      expanded: !!group.channels.find((ch) => ch.id === channelId) || group.expanded,
      channels: group.channels.map((ch: GlobalChatStateChannel) => ({
        ...ch,
        active: ch.id === channelId,
        ...(ch.active ? { scrollPosition: getCurrentChannelScrollPosition() } : null),
      })),
    })),
    channels: state.channels.map((ch: GlobalChatStateChannel) => ({
      ...ch,
      active: ch.id === channelId,
      ...(ch.active ? { scrollPosition: getCurrentChannelScrollPosition() } : null),
    })),
  })),
  on(actions.fetchMoreMessages, (state, { channelId }) => ({
    ...state,
    groups: state.groups.map((group) => ({
      ...group,
      channels: group.channels.map((ch) =>
        ch.id === channelId
          ? {
              ...ch,
              loading: true,
            }
          : ch
      ),
    })),
    channels: state.channels.map((ch) =>
      ch.id === channelId
        ? {
            ...ch,
            loading: true,
          }
        : ch
    ),
  })),
  on(actions.fetchMoreMessagesSuccess, (state, { results, pagination, channelId }) => ({
    ...state,
    groups: state.groups.map((group) => ({
      ...group,
      channels: group.channels.map((ch) =>
        ch.id === channelId
          ? {
              ...ch,
              messages: { results: [...results, ...ch.messages.results], pagination },
              wasInitialDataFetched: true,
              loading: false,
            }
          : ch
      ),
    })),
    channels: state.channels.map((ch) =>
      ch.id === channelId
        ? {
            ...ch,
            messages: { results: [...results, ...ch.messages.results], pagination },
            wasInitialDataFetched: true,
            loading: false,
          }
        : ch
    ),
  })),
  on(actions.receiveMessage, (state, { message, currentProfile }) => ({
    ...state,
    groups: state.groups.map((group) => ({
      ...group,
      channels: group.channels.map((ch: GlobalChatStateChannel) =>
        message.channel.id === ch.id
          ? {
              ...ch,
              messages: {
                ...ch.messages,
                results: [...ch.messages.results, message],
              },
              unseenMessagesSinceLastDisplay:
                !currentProfile || currentProfile?.id !== message.sender.id
                  ? ch.unseenMessagesSinceLastDisplay + 1
                  : ch.unseenMessagesSinceLastDisplay,
            }
          : ch
      ),
    })),
    channels: state.channels.map((ch: GlobalChatStateChannel) =>
      message.channel.id === ch.id
        ? {
            ...ch,
            messages: { ...ch.messages, results: [...ch.messages.results, message] },
            unseenMessagesSinceLastDisplay:
              !currentProfile || currentProfile?.id !== message.sender.id
                ? ch.unseenMessagesSinceLastDisplay + 1
                : ch.unseenMessagesSinceLastDisplay,
          }
        : ch
    ),
  })),
  on(actions.openChat, (state) => ({
    ...state,
    isChatOpen: true,
  })),
  on(actions.closeChat, (state) => ({
    ...state,
    isChatOpen: false,
  })),
  on(actions.maximizeChat, (state) => ({
    ...state,
    isChatMaximized: true,
  })),
  on(actions.minimizeChat, (state) => ({
    ...state,
    isChatMaximized: false,
  })),
  on(actions.updateActiveUsers, (state, payload) => {
    return {
      ...state,
      activeUsers: preapreActiveUsers(state, payload),
    };
  }),
  on(actions.markAsRead, (state, { channelId }) => ({
    ...state,
    groups: state.groups.map((group) => ({
      ...group,
      channels: group.channels.map((ch) =>
        ch.id === channelId
          ? {
              ...ch,
              unseenMessagesSinceLastDisplay: 0,
            }
          : ch
      ),
    })),
    channels: state.channels.map((ch) =>
      ch.id === channelId
        ? {
            ...ch,
            unseenMessagesSinceLastDisplay: 0,
          }
        : ch
    ),
  })),
  // OTHER ACTIONS
  on(meSuccess, (state, payload) => ({
    ...state,
    unseenMessages: payload.metadata.counters.unreadGlobalChatMessages,
  })),
  on(updateUserNetworkStatus, (state, payload) => ({
    ...state,
    // results: state.results.map((m) =>
    //   m.sender.id === payload.profile.id ? { ...m, sender: { ...m.sender, isOnline: payload.isOnline } } : m
    // ),
  }))
);

export function reducer(state: State, action: Action) {
  return _reducer(state, action);
}

function preapreActiveUsers(
  state: State,
  payload: { profile: Partial<UserProfile>; isOnline: boolean }
): UserProfile[] {
  const currentUsers = state.activeUsers;
  const { profile, isOnline } = payload;
  const userExists = currentUsers.find((user) => user.id === profile.id);

  if (isOnline && !userExists) {
    return [...currentUsers, { ...(profile as UserProfile), isOnline: true }];
  }

  return currentUsers.filter((user) => user.id !== profile.id);
}

function getCurrentChannelScrollPosition(): number {
  const listEl = document.querySelector('.global-chat-messages-list__wrapper');

  if (listEl) {
    return listEl.scrollTop;
  }

  return 0;
}
