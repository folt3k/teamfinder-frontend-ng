import { createAction, props } from '@ngrx/store';

import { types } from './constants';
import { ErrorRO } from '../../_shared/interfaces';
import { Team } from '../team.model';
import { CreateTeamDto, CreateTeamRO, RemoveTeamMemberDto } from '../team.interface';

export const createTeam = createAction(types.Create, props<{ body: CreateTeamDto }>());
export const createTeamSuccess = createAction(types.CreateSuccess, props<CreateTeamRO>());
export const createTeamFailed = createAction(types.CreateFailed, props<ErrorRO>());

export const updateTeam = createAction(types.Update, props<{ body: CreateTeamDto }>());
export const updateTeamSuccess = createAction(types.UpdateSuccess, props<{ data: Team }>());
export const updateTeamFailed = createAction(types.UpdateFailed, props<ErrorRO>());

export const removeTeam = createAction(
  types.Remove,
  props<{ id: number; redirectFromCurrentUrl?: boolean }>()
);
export const removeTeamSuccess = createAction(
  types.RemoveSuccess,
  props<{ id: number; redirectFromCurrentUrl?: boolean }>()
);
export const removeTeamFailed = createAction(types.RemoveFailed, props<ErrorRO>());

export const fetchTeamDetails = createAction(types.FetchDetails, props<{ slug: string }>());
export const fetchTeamDetailsSuccess = createAction(types.FetchDetailsSuccess, props<{ data: Team }>());
export const fetchTeamDetailsFailed = createAction(types.FetchDetailsFailed, props<ErrorRO>());

export const removeTeamMember = createAction(types.RemoveMember, props<RemoveTeamMemberDto>());
export const removeTeamMemberSuccess = createAction(types.RemoveMemberSuccess, props<{ memberId: number }>());
export const removeTeamMemberFailed = createAction(types.RemoveMemberFailed, props<ErrorRO>());

export const leaveTeam = createAction(types.LeaveTeam, props<RemoveTeamMemberDto>());
