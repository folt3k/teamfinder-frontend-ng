import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

import { EntityDetailsDisplayConditions, EntityDetailsHelpers } from '../entity-details.interface';
import { BaseEntity } from '../../_shared/interfaces';
import { GameRank, UserProfile } from '../../_shared/models';
import { EntityDetailsService } from '../entity-details.service';
import { GamesTranslationsService } from '../../games/_services/games-translations.service';
import { GameTranslations } from '../../games/_services/games-translations';
import { Store } from '@ngrx/store';
import { updateProfileGameMetadata } from '../../profile/_reducer';

@Component({
  selector: 'tf-entity-details-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EntityDetailsInfoComponent implements OnInit {
  @Input() data: BaseEntity;

  displayConditions: EntityDetailsDisplayConditions = {};
  translations: GameTranslations;
  helpers: EntityDetailsHelpers;

  constructor(
    private store: Store,
    private service: EntityDetailsService,
    private gamesTranslations: GamesTranslationsService
  ) {}

  get profile(): UserProfile {
    return this.data as UserProfile;
  }

  get roles(): string[] {
    return this.data.roles.map((role) => role.name);
  }

  get modes(): string {
    return this.data.modes.map((mode) => mode.name).join(', ');
  }

  get ranks(): GameRank[] {
    return this.data.ranks || (this.data.rank ? [this.data.rank] : []);
  }

  ngOnInit(): void {
    this.displayConditions = this.service.displayConditions();
    this.helpers = this.service.helpers();
    this.gamesTranslations.get().subscribe((results) => {
      this.translations = results;
    });
  }

  updateProfileGameMetadata(): void {
    this.store.dispatch(updateProfileGameMetadata());
  }
}
