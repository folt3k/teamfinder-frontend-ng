import { GameOption } from './games';

export interface StaticURL {
  path: string;
  slashPath: string;
  pathWithGamePrefix?: (game: GameOption) => string;
}

export const staticUrlWithGamePrefix = (data: StaticURL): StaticURL => ({
  ...data,
  pathWithGamePrefix(game): string {
    return game.slashRouterPrefix + this.slashPath;
  },
});

export const STATIC_URLS: {
  [key: string]: StaticURL;
} = {
  ['teams']: staticUrlWithGamePrefix({
    path: 'teams',
    slashPath: '/teams',
  }),
  ['users']: {
    path: 'users',
    slashPath: '/users',
  },
  ['announcements']: staticUrlWithGamePrefix({
    path: 'wspolne-granie',
    slashPath: '/wspolne-granie',
  }),
  ['add-team']: {
    path: 'dodaj-druzyne',
    slashPath: '/dodaj-druzyne',
  },
  ['my-profile']: {
    path: 'moj-profil',
    slashPath: '/moj-profil',
  },
  ['contact']: {
    path: 'kontakt',
    slashPath: '/kontakt',
  },
  ['policy']: {
    path: 'regulamin-i-polityka-prywatnosci',
    slashPath: '/regulamin-i-polityka-prywatnosci',
  },
  ['login']: {
    path: 'logowanie',
    slashPath: '/logowanie',
  },
  ['sign-up']: {
    path: 'zarejestruj-sie',
    slashPath: '/zarejestruj-sie',
  },
};

export const MY_PROFILE_STATIC_URLS: {
  [key: string]: { path: string; slashPath: string };
} = {
  ['preview-profile']: {
    path: 'podglad',
    slashPath: '/moj-profil/podglad',
  },
  ['edit-profile']: {
    path: 'edycja',
    slashPath: '/moj-profil/edycja',
  },
  ['settings']: {
    path: 'ustawienia-konta',
    slashPath: '/moj-profil/ustawienia-konta',
  },
  ['notifications']: {
    path: 'notyfikacje',
    slashPath: '/moj-profil/notyfikacje',
  },
  ['my-invitations']: {
    path: 'moje-zaproszenia',
    slashPath: '/moj-profil/moje-zaproszenia',
  },
  ['team-invitations']: {
    path: 'zaproszenia-druzyny',
    slashPath: '/moj-profil/zaproszenia-druzyny',
  },
  ['teams']: {
    path: 'moje-druzyny',
    slashPath: '/moj-profil/moje-druzyny',
  },
  ['announcements']: {
    path: 'ogloszenia',
    slashPath: '/moj-profil/ogloszenia',
  },
};

interface DynamicUrl {
  path: string;
  slashPath: string;
  symbolPath: string;
  match?: (mathUrl: string) => boolean;
}

export const DYNAMIC_URLS = {
  ACCOUNT_ACTIVATION: (options: { token?: string } = {}) => ({
    path: `aktywacja-konta/${options.token}`,
    slashPath: `/aktywacja-konta/${options.token}`,
    symbolPath: `aktywacja-konta/:token`,
    match: (matchUrl: string) => !!matchUrl.match(/aktywacja-konta\/*/),
  }),
  TEAM: (options: { identifier?: string; gamePrefix?: string } = {}) => ({
    path: `${options.gamePrefix}/team/${options.identifier}`,
    slashPath: `/${options.gamePrefix}/team/${options.identifier}`,
    symbolPath: `team/:identifier`,
    match: (matchUrl: string) => !!matchUrl.match(/team\//),
  }),
  TEAM_EDIT: (options: { identifier?: string; gamePrefix?: string } = {}) => ({
    path: `${options.gamePrefix}/team/${options.identifier}/edycja`,
    slashPath: `/${options.gamePrefix}/team/${options.identifier}/edycja`,
    symbolPath: `team/:identifier/edycja`,
  }),
  USER: (options: { identifier?: string; gamePrefix?: string } = {}) => ({
    path: `${options.gamePrefix}/user/${options.identifier}`,
    slashPath: `/${options.gamePrefix}/user/${options.identifier}`,
    symbolPath: `user/:identifier`,
    match: (matchUrl: string) => !!matchUrl.match(/user\//),
  }),
  ['GAME-ANNOUNCEMENT']: (options: { identifier?: string; gamePrefix?: string } = {}) => ({
    path: `${options.gamePrefix}/wspolne-granie/${options.identifier}`,
    slashPath: `/${options.gamePrefix}/wspolne-granie/${options.identifier}`,
    symbolPath: `wspolne-granie/:identifier`,
    match: (matchUrl: string) => !!matchUrl.match(/wspolne-granie\//),
  }),
  UNSUBSCRIBE_FROM_MAIL_NOTIFICATIONS: (options: { token?: string } = {}) => ({
    path: `anuluj-powiadomienia-email/${options.token}`,
    slashPath: `/anuluj-powiadomienia-email/${options.token}`,
    symbolPath: `anuluj-powiadomienia-email/:token`,
    match: (matchUrl: string) => !!matchUrl.match(/anuluj-powiadomienia-email\/*/),
  }),
  CHANGE_PASSWORD: (options: { token?: string } = {}) => ({
    path: `zmiana-hasla/${options.token}`,
    slashPath: `/zmiana-hasla/${options.token}`,
    symbolPath: `zmiana-hasla/:token`,
    match: (matchUrl: string) => !!matchUrl.match(/zmiana-hasla\/*/),
  }),
};
