import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { ROUTER_NAVIGATED, ROUTER_NAVIGATION, RouterReducerState } from '@ngrx/router-store';

import { AppState } from '../../app.interface';
import { first, map, pairwise, tap, withLatestFrom } from 'rxjs/operators';
import { DYNAMIC_URLS, GAMES, STATIC_URLS } from '../../_shared/common';
import { changeCurrentGame } from '../../games/_reducer';
import { isHomeURLWithGamePrefix } from '../../_helpers';
import { updateUI } from '../ui';
import { SeoService } from '../../_core/services/seo.service';
import { getGameTypeByRouterPrefix } from '../../games';
import { LocalStorageService } from '../../_core/services';

@Injectable()
export class RouterEffects {
  changeRoute$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(ROUTER_NAVIGATED),
        withLatestFrom(this.store.select((state) => state.router)),
        map(([action, state]) => state),
        pairwise(),
        tap(([prev, curr]) => {
          this.onRouteChange(curr, prev);
        })
      ),
    { dispatch: false }
  );

  onChangeRouterFirstTime$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(ROUTER_NAVIGATED),
        withLatestFrom(this.store.select((state) => state.router)),
        map(([action, state]) => state),
        first(),
        tap((curr) => {
          this.onRouteChange(curr, null, { changeFirstTime: true });
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private router: Router,
    private seo: SeoService,
    private localStorage: LocalStorageService
  ) {}

  private onRouteChange(
    currState: RouterReducerState<any>,
    prevState?: RouterReducerState<any>,
    options: { changeFirstTime?: boolean } = {}
  ): void {
    const url = currState.state.url.split('?')[0];
    const currentGamePrefix = currState.state.root.firstChild?.firstChild?.params?.gamePrefix;
    const prevGamePrefix = prevState?.state.root.firstChild?.firstChild?.params?.gamePrefix;

    if (currentGamePrefix) {
      if (
        // options.changeFirstTime &&
        isHomeURLWithGamePrefix(currentGamePrefix) &&
        currentGamePrefix !== prevGamePrefix
      ) {
        const gameId = GAMES.find((g) => !!currentGamePrefix.match(g.routerPrefix)).value;
        this.store.dispatch(changeCurrentGame({ gameId }));
      }
    }

    if (url === '/') {
      this.seo.setSeo('home');
    } else if (isHomeURLWithGamePrefix(url)) {
      this.seo.setSeo(`homeGame.${currentGamePrefix}`, getGameTypeByRouterPrefix(currentGamePrefix));
    }

    this.store.dispatch(
      updateUI({
        isLoginPage: url === STATIC_URLS['login'].slashPath,
        alignCenterMainContent: DYNAMIC_URLS.ACCOUNT_ACTIVATION().match(url),
        canDisplayGameSelect:
          (url === '/' || isHomeURLWithGamePrefix(url) ? !this.localStorage.get('token') : true) &&
          url !== STATIC_URLS['add-team'].slashPath &&
          url !== STATIC_URLS['sign-up'].slashPath &&
          !DYNAMIC_URLS.TEAM().match(url) &&
          !DYNAMIC_URLS.USER().match(url) &&
          !DYNAMIC_URLS['GAME-ANNOUNCEMENT']().match(url),
        isMainContainerFull:
          url === STATIC_URLS['login'].slashPath ||
          (url === '/' && !this.localStorage.get('token')) ||
          (isHomeURLWithGamePrefix(url) && !this.localStorage.get('token')),
        isPageContainerFull: url === '/' || isHomeURLWithGamePrefix(url),
      })
    );
  }
}
