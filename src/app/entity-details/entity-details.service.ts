import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import {
  EntityDetailsAction,
  EntityDetailsDisplayConditions,
  EntityDetailsHelpers,
  EntityType,
} from './entity-details.interface';
import { AppState } from '../app.interface';
import { fetchEntityDetails, fetchEntityDetailsSuccess } from './_reducer';
import { GameCheckers, getGameByType, getGameCheckers, getGameTypeByRouterPrefix } from '../games';
import { BaseEntity, GameType } from '../_shared/interfaces';
import { DialogService } from '../_shared/poppy/dialog';
import {
  EntityDetailsAnnouncementActionsCreator,
  EntityDetailsTeamActionsCreator,
  EntityDetailsUserActionsCreator,
} from './actions-creators';
import { SeoService } from '../_core/services/seo.service';

@Injectable({
  providedIn: 'root',
})
export class EntityDetailsService {
  private type: EntityType;
  private identifier: string;
  private currentGame: GameType;

  constructor(
    private store: Store<AppState>,
    private actions: Actions,
    private dialogService: DialogService,
    private router: Router,
    private seo: SeoService,
    private announcementActionsCreator: EntityDetailsAnnouncementActionsCreator,
    private teamActionsCreator: EntityDetailsTeamActionsCreator,
    private userActionsCreator: EntityDetailsUserActionsCreator
  ) {}

  init(type: EntityType, identifier: string, gameRouterPrefix: string): void {
    this.type = type;
    this.identifier = identifier;
    this.currentGame = getGameTypeByRouterPrefix(gameRouterPrefix);
  }

  fetchDetails(): void {
    this.store.dispatch(fetchEntityDetails({ identifier: this.identifier, entityType: this.type }));

    this.actions.pipe(ofType(fetchEntityDetailsSuccess), first()).subscribe(({ data }) => {
      this.setSeo(data);
    });
  }

  helpers(): EntityDetailsHelpers {
    return {
      isTeam: this.type === EntityType.TEAM,
      isUser: this.type === EntityType.USER,
      isGameAnnouncement: this.type === EntityType.ANNOUNCEMENT,
    };
  }

  displayConditions(): EntityDetailsDisplayConditions {
    const checkers: GameCheckers = getGameCheckers(this.currentGame);

    switch (this.type) {
      case EntityType.USER:
        return {
          serverRegionInfoSection: checkers.isLol,
          userGameAccountNameInfoSection: true,
          userDescInfoSection: true,
          userLoLGameContentSection: checkers.isLol,
          userCsGameContentSection: checkers.isCs,
          userFntGameContentSection: checkers.isFnt,
          userGameAccountLevelInfoSection: checkers.isFnt,
          gameRankInfoSection: checkers.isCs || checkers.isLol,
          gameRolesInfoSection: checkers.isCs || checkers.isLol,
          gameModesInfoSection: checkers.isCs || checkers.isLol,
        };
      case EntityType.TEAM:
        return {
          teamOwnerInfoSection: true,
          serverRegionInfoSection: checkers.isLol,
          gameRankInfoSection: checkers.isCs || checkers.isLol,
          gameRolesInfoSection: checkers.isCs || checkers.isLol,
          gameModesInfoSection: checkers.isCs || checkers.isLol,
        };
      case EntityType.ANNOUNCEMENT:
        return {
          announcementAuthorInfoSection: true,
          gameRankInfoSection: checkers.isCs || checkers.isLol,
          gameRolesInfoSection: checkers.isCs || checkers.isLol,
          gameModesInfoSection: checkers.isCs || checkers.isLol,
        };
    }
  }

  prepareActions(): Observable<EntityDetailsAction[]> {
    switch (this.type) {
      case EntityType.ANNOUNCEMENT:
        return this.announcementActionsCreator.getActions$();
      case EntityType.TEAM:
        return this.teamActionsCreator.getActions$();
      case EntityType.USER:
        return this.userActionsCreator.getActions$();
    }
  }

  private setSeo(entity: BaseEntity): void {
    switch (this.type) {
      case EntityType.USER:
        this.seo.setTitle(
          `${entity.username} - profil gracza ${getGameByType(this.currentGame).displayName}`
        );
        this.seo.setMeta({
          description: `${entity.username} - profil gracza ${getGameByType(this.currentGame).displayName}`,
        });
        break;
      case EntityType.TEAM:
        this.seo.setTitle(`${entity.name} - profil drużyny ${getGameByType(this.currentGame).displayName}`);
        this.seo.setMeta({
          description: `${entity.name} - profil drużyny ${getGameByType(this.currentGame).displayName}`,
        });
        break;
      case EntityType.ANNOUNCEMENT:
        this.seo.setTitle(`Ogłoszenie - szukam graczy w ${getGameByType(this.currentGame).displayName}`);
        this.seo.setMeta({
          description: `Ogłoszenie - szukam graczy w ${getGameByType(this.currentGame).displayName}`,
        });
        break;
    }
  }
}
