import { ChangeDetectionStrategy, Component, Input, Output, EventEmitter } from '@angular/core';

import { CollapsableNavigationSchema, CollapsableNavigationGroup } from './collapsable-navigation.interface';

@Component({
  selector: 'tf-collapsable-navigation',
  templateUrl: './collapsable-navigation.component.html',
  styleUrls: ['./collapsable-navigation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CollapsableNavigationComponent {
  @Input() set schema(value: CollapsableNavigationSchema) {
    this.displaySchema = [...value];
  }
  @Output() changed = new EventEmitter<CollapsableNavigationSchema>();

  displaySchema: CollapsableNavigationSchema = [];

  constructor() {}

  toggleGroup(clickedGroup: CollapsableNavigationGroup): void {
    this.displaySchema = this.displaySchema.map((group) => {
      if (group.key === clickedGroup.key) {
        return { ...group, expanded: !group.expanded, selected: !group.selected };
      }
      if (!clickedGroup.children) {
        return {
          ...group,
          children: group.children?.map((child) => ({ ...child, selected: false })),
          selected: false,
        };
      }

      return group;
    });
    this.changed.emit(this.displaySchema);
  }

  childClick(groupKey: string, childKey: string): void {
    this.displaySchema = this.displaySchema.map((group) => {
      return {
        ...group,
        selected: groupKey === group.key,
        children: group.children?.map((child) => {
          return {
            ...child,
            selected: child.key === childKey,
          };
        }),
      };
    });
    this.changed.emit(this.displaySchema);
  }

  canToggleGroup(group: CollapsableNavigationGroup): boolean {
    return group.children ? !group.children.find((child) => child.selected) : !group.selected;
  }
}
