import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MyProfileEditComponent } from './edit.component';
import { SharedModule } from '../../_shared/shared.module';

@NgModule({
  declarations: [MyProfileEditComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: MyProfileEditComponent,
      },
    ]),
    SharedModule,
  ],
  exports: [RouterModule],
})
export class MyProfileEditModule {}
