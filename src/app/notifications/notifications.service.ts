import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { NotificationsListAllRO } from './notifications.interface';
import { ListQueryParams } from '../_shared/interfaces';

@Injectable({
  providedIn: 'root',
})
export class NotificationsService {
  constructor(private http: HttpClient) {}

  fetchAll(params: ListQueryParams = {}): Observable<NotificationsListAllRO> {
    return this.http.get<NotificationsListAllRO>('/notifications', {
      params: {
        ...params,
        perPage: params.perPage || 10,
      } as any,
    });
  }

  markAllAsDisplayed(): Observable<void> {
    return this.http.patch<void>('/notifications/mark-all-as-displayed', null);
  }

  markAllAsRead(): Observable<void> {
    return this.http.patch<void>('/notifications/mark-all-as-read', null);
  }

  markAsRead(id: number): Observable<void> {
    return this.http.patch<void>(`/notifications/${id}`, null);
  }
}
