import { Injectable, PLATFORM_ID, Inject } from '@angular/core';
import * as io from 'socket.io-client';
import { isPlatformBrowser } from '@angular/common';

import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SocketService {
  private instance: SocketIOClient.Socket;

  constructor(@Inject(PLATFORM_ID) private platformId: any) {
    if (isPlatformBrowser(this.platformId)) {
      this.instance = io(environment.apiSocketPort);
    }
  }

  // tslint:disable-next-line: no-any
  emit(event: string, data: any): void {
    if (this.instance) {
      this.instance.emit(event, data);
    }
  }

  // tslint:disable-next-line: no-any
  listen(event: string, callback: (data?: any) => void): void {
    if (this.instance) {
      this.instance.on(event, callback);
    }
  }
}
