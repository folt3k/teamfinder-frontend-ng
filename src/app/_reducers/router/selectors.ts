import { createFeatureSelector } from '@ngrx/store';
import * as fromRouter from '@ngrx/router-store';
import { AppState } from '../../app.interface';

const selectRouter = createFeatureSelector<AppState, fromRouter.RouterReducerState<any>>('router');
const routerSelectors = fromRouter.getSelectors(selectRouter);

export const selectRouteParam = routerSelectors.selectRouteParam;
export const selectRouteParams = routerSelectors.selectRouteParams;
