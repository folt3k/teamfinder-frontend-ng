import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
} from '@angular/core';

@Component({
  selector: 'tf-icon-btn',
  templateUrl: './icon-button.component.html',
  styleUrls: ['./icon-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconButtonComponent implements AfterViewInit {
  @Input() color: 'primary' | 'secondary' = 'primary';

  name: string;

  constructor(private hostElement: ElementRef, private cdr: ChangeDetectorRef) {}

  get panelClasses(): { [key: string]: boolean } {
    return {
      [`icon-btn--${this.color}`]: true,
    };
  }

  ngAfterViewInit(): void {
    this.setName();
  }

  private setName(): void {
    const content = this.hostElement.nativeElement?.childNodes[0];

    if (!content) {
      throw new Error('Provide icon name!');
    }

    this.name = content.nodeValue;
    content.remove();
    this.cdr.detectChanges();
  }
}
