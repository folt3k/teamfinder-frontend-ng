export default {
  search: 'Szukaj',
  clear: 'Wyczyść', // Accessible label on "clear" button
  notfound: 'Nie znaleziono emotek',
  skintext: 'Choose your default skin tone',
  categories: {
    search: 'Wyniki wyszukiwania',
    recent: 'Najczęściej używane',
    smileys: 'Uśmiechy & Emocje',
    people: 'Ludzie & Ciało',
    nature: 'Zwierzęta & Natura',
    foods: 'Jedzienie & Picie',
    activity: 'Aktywność',
    places: 'Podróże',
    objects: 'Objekty',
    symbols: 'Symbole',
    flags: 'Flagi',
    custom: 'Niestandardowe',
  },
  categorieslabel: 'Kategorie', // Accessible title for the list of categories
  skintones: {
    1: 'Default Skin Tone',
    2: 'Light Skin Tone',
    3: 'Medium-Light Skin Tone',
    4: 'Medium Skin Tone',
    5: 'Medium-Dark Skin Tone',
    6: 'Dark Skin Tone',
  },
};
