export type ButtonColor = 'primary' | 'secondary' | 'danger';
export type ButtonSize = 'large' | 'medium' | 'small';
export type ButtonTheme = 'normal' | 'outline';
