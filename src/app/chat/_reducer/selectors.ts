import { createSelector } from '@ngrx/store';

import { featureKey } from './constants';
import { AppState } from '../../app.interface';
import { ChatType } from '../chat.inerface';
import { ChatRoom, TeamChatRoom } from '../_models/chat-room.model';

export const selectFeature = (state: AppState) => state[featureKey];

export const selectRoom = createSelector(
  selectFeature,
  (state, props: { roomId: number; chatType: ChatType }) =>
    state.rooms.results.find((room) => room.id === props.roomId && room.chatType === props.chatType)
);

export const selectRooms = createSelector(selectFeature, (state) =>
  state.rooms.results.filter((room) => room.id)
);

export const selectPrivateRooms = createSelector(selectRooms, (rooms) =>
  rooms.filter((room) => room.chatType === ChatType.PRIVATE)
);

export const selectNewConversation = createSelector(selectFeature, (state) => {
  return state.rooms.results.find((room) => !room.id);
});

export const selectNewConversationReceiver = createSelector(
  selectNewConversation,
  (conversation) => conversation?.memberProfile
);

export const selectRoomWindows = createSelector(selectFeature, (state) => {
  return state.roomWindowIds.map((id) =>
    state.rooms.results.find((room) => room.id === id.value && room.chatType === id.type)
  );
});

export const selectRoomMessages = createSelector(selectRoom, (room) => room?.messages?.results || []);

export const selectRoomMessagesCount = createSelector(selectRoom, (room) => {
  return room.messages?.results?.length || 0;
});

export const messagesLoading = createSelector(selectRoom, (room) => {
  return room?.messages?.loading || false;
});

export const selectRoomsMemberIds = createSelector(selectFeature, (state) =>
  state.rooms.results
    .filter((room) => room.id && room.chatType === ChatType.PRIVATE)
    .map((room) => room.memberProfile.id)
);
