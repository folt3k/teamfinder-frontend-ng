import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchInputComponent } from './search-input.component';
import { ButtonModule } from '../../components';
import { InputModule } from '../input/input.module';
import { FormControlModule } from '../form-control/form-control.module';

@NgModule({
  declarations: [SearchInputComponent],
  imports: [CommonModule, ButtonModule, InputModule, FormControlModule],
  exports: [SearchInputComponent],
})
export class SearchInputModule {}
