import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { SelectModule as PoppySelectModule } from '@ng-poppy/lib';

import { SelectComponent } from './select.component';
import { FormControlModule } from '../form-control/form-control.module';

@NgModule({
  declarations: [SelectComponent],
  imports: [CommonModule, ReactiveFormsModule, PoppySelectModule, FormControlModule],
  exports: [SelectComponent],
})
export class SelectModule {}
