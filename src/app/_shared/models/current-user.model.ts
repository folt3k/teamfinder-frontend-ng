import { User } from './user.model';
import { UserProfile } from './user-profile.model';

export interface CurrentUser {
  profile: UserProfile;
  user: User;
}
