import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { filter, first } from 'rxjs/operators';

import { AppState } from '../../app.interface';
import { selectUI } from '../../_reducers/ui';

export enum SoundType {
  NEW_MESSAGE = 'NEW_MESSAGE',
}

@Injectable({
  providedIn: 'root',
})
export class SoundService {
  private canPlay: boolean = true;

  constructor(private store: Store<AppState>) {}

  play(type: SoundType): void {
    this.store
      // Play sound only if browser window is inactive
      .select(selectUI, 'isWindowActive')
      .pipe(
        first(),
        filter((a) => this.canPlay && !a)
      )
      .subscribe(() => {
        this._play(type);
        this.canPlay = false;
      });
  }

  enablePlay(): void {
    this.canPlay = true;
  }

  private _play(type: SoundType): void {
    const sound = new Audio(`/assets/sounds/${this.getSoundFileName(type)}.mp3`);

    if (type === SoundType.NEW_MESSAGE) {
      try {
        sound.volume = 1;
        sound.play().catch((e) => {});
      } catch (e) {}
    }
  }

  private getSoundFileName(type: SoundType): string {
    switch (type) {
      case SoundType.NEW_MESSAGE:
        return 'new-message';
      default:
        return 'new-message';
    }
  }
}
