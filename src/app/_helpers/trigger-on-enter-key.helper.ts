export const triggerOnEnterKey = (event: KeyboardEvent, callback: () => void | any) => {
  if (event.which === 13 && !event.shiftKey) {
    if (callback) {
      callback();
    }
    event.preventDefault();
  }
};
