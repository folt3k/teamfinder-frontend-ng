import { AppState } from './../../app.interface';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { switchMap, map, catchError, tap, withLatestFrom, delay } from 'rxjs/operators';

import {
  updateUserNetworkStatus,
  register,
  registerSuccess,
  registerFailed,
  updateUser,
  updateUserSuccess,
  updateUserFailed,
  changeUserPassword,
  changeUserPasswordSuccess,
  changeUserPasswordFailed,
  updateUserMailNotificationSettings,
  updateUserMailNotificationSettingsSuccess,
  updateUserMailNotificationSettingsFailed,
} from './actions';
import { currentProfileId } from '../../auth/_reducer';
import { updateActiveUsers } from '../../global-chat/_reducer';
import { UserService } from '../user.service';
import { AlertService } from '../../_shared/components/alert/alert.service';

@Injectable()
export class UserEffects {
  updateUserNetworkStatus$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(updateUserNetworkStatus),
        withLatestFrom(this.store.select(currentProfileId)),
        tap(([action, profileId]) => {
          if (action.profile.id !== profileId) {
            this.store.dispatch(updateActiveUsers(action));
          }
        })
      ),
    { dispatch: false }
  );

  register$ = createEffect(() =>
    this.actions$.pipe(
      ofType(register),
      switchMap((action) =>
        this.userService.register(action.body).pipe(
          map((response) => registerSuccess(response)),
          catchError((err) => of(registerFailed(err)))
        )
      )
    )
  );

  update$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateUser),
      switchMap((action) =>
        this.userService.update(action.body).pipe(
          map((data) => updateUserSuccess({ data })),
          catchError((err) => of(updateUserFailed(err)))
        )
      )
    )
  );

  updateSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(updateUserSuccess),
        tap(() => {
          this.alertService.show('Dane twojego konta zostały zaktualizowane');
        })
      ),
    { dispatch: false }
  );

  changePassword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(changeUserPassword),
      switchMap((action) =>
        this.userService.changePassword(action.body).pipe(
          map(() => changeUserPasswordSuccess()),
          catchError((err) => of(changeUserPasswordFailed(err)))
        )
      )
    )
  );

  changePasswordSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(changeUserPasswordSuccess),
        tap(() => {
          this.alertService.show('Twoje hasło zostało zmienione poprawnie');
        })
      ),
    { dispatch: false }
  );

  updateMailNotificationSettings$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateUserMailNotificationSettings),
      switchMap((action) =>
        this.userService.updateMailNotificationSetting(action.body).pipe(
          map((data) => updateUserMailNotificationSettingsSuccess({ data })),
          catchError((err) => of(updateUserMailNotificationSettingsFailed(err)))
        )
      )
    )
  );

  updateMailNotificationSettingsSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(updateUserMailNotificationSettingsSuccess),
        tap(() => {
          this.alertService.show('Ustawienia konta zostały pomyślnie zmienione');
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private router: Router,
    private userService: UserService,
    private alertService: AlertService
  ) {}
}
