import { Component, ChangeDetectionStrategy, Output, EventEmitter, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { EmojiService } from './emoji.service';

@Component({
  selector: 'tf-emoji-picker',
  templateUrl: './emoji-picker.component.html',
  styleUrls: ['./emoji-picker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmojiPickerComponent implements OnInit {
  @Output() emojiSelected = new EventEmitter<string>();

  emojis$: Observable<string[][]>;

  constructor(private emojiService: EmojiService) {}

  ngOnInit(): void {
    this.emojis$ = this.emojiService.emojis$;
  }

  onSelectEmoji(emoji: string): void {
    this.emojiSelected.emit(emoji);
  }
}
