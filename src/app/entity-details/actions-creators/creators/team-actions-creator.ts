import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, map, take } from 'rxjs/operators';

import { EntityDetailsAction } from '../../entity-details.interface';
import { DYNAMIC_URLS } from '../../../_shared/common';
import { getGameRouterPrefixByType } from '../../../games';
import { ConfirmationComponent } from '../../../_shared/components/confirmation/confirmation.component';
import { leaveTeam, removeTeam } from '../../../team/_reducer';
import { sendRequest } from '../../../invitations/_reducer';
import { EntityDetailsActionsCreator, IEntityDetailsActionsCreator } from '../actions-creator';
import { addRoomWindow, selectRooms } from '../../../chat/_reducer';
import { ChatType } from '../../../chat/chat.inerface';
import { Team } from '../../../team';

@Injectable({
  providedIn: 'root',
})
export class EntityDetailsTeamActionsCreator extends EntityDetailsActionsCreator
  implements IEntityDetailsActionsCreator {
  getActions$(): Observable<EntityDetailsAction[]> {
    return this.data$.pipe(
      map(([currentUser, data, isAuthorizationCompleted]) => {
        const actions: EntityDetailsAction[] = [];

        if (this.isPlatformServer()) {
          return actions;
        }

        if (currentUser?.id === data?.owner.id) {
          actions.push({
            label: 'Czat drużynowy',
            callback: () => {
              this.openTeamChatWindow(data as Team);
            },
            icon: 'mail_outline',
          });
          actions.push({
            label: 'Edytuj',
            callback: () => {
              this.router.navigate([
                DYNAMIC_URLS.TEAM_EDIT({
                  identifier: data.slug,
                  gamePrefix: getGameRouterPrefixByType(data.type),
                }).slashPath,
              ]);
            },
            icon: 'edit',
          });
          actions.push({
            label: 'Usuń',
            callback: () => {
              const dialogRef = this.dialogService.open(ConfirmationComponent, {
                data: {
                  question: 'Czy na pewno chcesz usunąć tę druzynę? Jej przywrócenie nie będzie możliwe.',
                },
              });

              dialogRef.afterClose
                .pipe(
                  take(1),
                  filter((payload) => !!payload)
                )
                .subscribe(() => {
                  this.store.dispatch(removeTeam({ id: data.id, redirectFromCurrentUrl: true }));
                });
            },
            icon: 'delete',
            btnType: 'danger',
          });
        } else if (currentUser) {
          const isCurrentUserTeamMember = !!data.members.find((m) => m.profile.id === currentUser.id);

          if (isCurrentUserTeamMember) {
            actions.push({
              label: 'Czat drużynowy',
              callback: () => {
                this.openTeamChatWindow(data as Team);
              },
              icon: 'mail_outline',
            });
            actions.push({
              label: 'Opuść drużynę',
              callback: () => {
                this.store.dispatch(
                  leaveTeam({ profileId: currentUser.id, teamId: data.id, redirectFromCurrentPage: true })
                );
              },
            });
          } else if (isAuthorizationCompleted) {
            const canCurrentUserSendTeamRequest = !currentUser.teamRequests.find(
              (r) => r.team.id === data.id
            );
            if (canCurrentUserSendTeamRequest) {
              if (this.isCurrentUserGameEqualsToEntityGame(currentUser, data)) {
                actions.push({
                  label: 'Dołącz do drużyny',
                  callback: () => {
                    this.store.dispatch(sendRequest({ body: { teamId: data.id } }));
                  },
                  icon: 'add',
                });
              }
            } else {
              actions.push({
                label: 'Wysłałeś podanie',
                callback: () => {},
                icon: 'check',
                disabled: true,
              });
            }
          }
        } else if (isAuthorizationCompleted) {
          actions.push({
            label: 'Dołącz do drużyny',
            callback: () => {
              this.store.dispatch(sendRequest({ body: { teamId: data.id } }));
            },
            icon: 'add',
          });
        }

        return actions;
      })
    );
  }

  private openTeamChatWindow(team: Team): void {
    this.store
      .select(selectRooms)
      .pipe(take(1))
      .subscribe((rooms) => {
        const room = rooms.find((r) => r.team?.id === team.id);
        this.store.dispatch(addRoomWindow({ roomId: room.id, chatType: ChatType.TEAM }));
      });
  }
}
