import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';

import { DictionariesService } from './_shared/services';
import { Dictionaries } from './_shared/models';
import { AppState } from './app.interface';
import { fetchDictionariesSuccess } from './_reducers/dictionaries';

@Injectable({
  providedIn: 'root',
})
export class AppInitService {
  constructor(private dictionariesService: DictionariesService, private store: Store<AppState>) {}

  init(): Promise<Partial<Dictionaries>> {
    this.dictionariesService.fetchDynamicDicts().subscribe((results) => {
      this.store.dispatch(fetchDictionariesSuccess(results));
    });
    return this.dictionariesService
      .fetchStaticDicts()
      .pipe(
        tap((results) => {
          this.store.dispatch(fetchDictionariesSuccess(results));
        })
      )
      .toPromise();
  }
}
