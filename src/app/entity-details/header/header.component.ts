import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

import { EntityDetailsAction, EntityDetailsHelpers, EntityType } from '../entity-details.interface';
import { EntityDetailsService } from '../entity-details.service';
import { BaseEntity } from '../../_shared/interfaces';
import { UserProfile } from '../../_shared/models';

@Component({
  selector: 'tf-entity-details-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EntityDetailsHeaderComponent implements OnInit {
  @Input() data: BaseEntity;
  @Input() actions: EntityDetailsAction[] = [];

  ENTITY_TYPE = EntityType;
  helpers: EntityDetailsHelpers;

  get user(): UserProfile {
    return this.data as UserProfile;
  }

  constructor(private service: EntityDetailsService) {}

  ngOnInit(): void {
    this.helpers = this.service.helpers();
  }
}
