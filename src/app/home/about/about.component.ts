import { Component, OnInit } from '@angular/core';

import { STATIC_URLS } from '../../_shared/common';

@Component({
  selector: 'tf-home-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit {
  STATIC_URLS = STATIC_URLS;

  constructor() {}

  ngOnInit(): void {}
}
