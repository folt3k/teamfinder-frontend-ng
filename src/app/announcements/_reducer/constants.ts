export const featureKey = 'announcement';

export const types = {
  Create: '[Announcement] Create announcement',
  CreateSuccess: '[Announcement] Create announcement success',
  CreateFailed: '[Announcement] Create announcement failed',

  Update: '[Announcement] Update announcement',
  UpdateSuccess: '[Announcement] Update announcement success',
  UpdateFailed: '[Announcement] Update announcement failed',

  Remove: '[Announcement] Remove announcement',
  RemoveSuccess: '[Announcement] Remove announcement success',
  RemoveFailed: '[Announcement] Remove announcement failed',

  AddMember: '[Announcement] Add announcement member ',
  AddMemberSuccess: '[Announcement] Add announcement  member success',
  AddMemberFailed: '[Announcement] Add announcement  member failed',
};
