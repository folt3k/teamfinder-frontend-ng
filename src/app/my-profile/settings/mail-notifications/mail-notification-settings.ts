import { KeyLabelPair } from '../../../_shared/interfaces';

export const mailNotificationSettings: KeyLabelPair[] = [
  {
    key: 'enableAppNotifications',
    label: 'Powiadomienia dotyczące aplikacji (np. informacje o nowościach w Teamfinder)',
  },
  { key: 'enableGameAnnouncementNotifications', label: 'Powiadomienia o nowych prywatnych wiadomościach' },
  { key: 'enablePrivateMessageNotifications', label: 'Powiadomienia dotyczące zaproszeń do drużyn' },
  {
    key: 'enableTeamCommentNotifications',
    label: 'Powiadomienia o nowych komentarzach na stronie drużyny i Twoich ogłoszeń',
  },
  {
    key: 'enableTeamRequestNotifications',
    label: 'Powiadomienia o przyłączeniu się innych graczy do twoich ogłoszeń "Wspólne granie"',
  },
];
