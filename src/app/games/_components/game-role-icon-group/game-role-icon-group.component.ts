import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { AppState } from '../../../app.interface';
import { selectDicts } from '../../../_reducers/dictionaries';
import { GameRole } from '../../../_shared/models';
import { GameType } from '../../../_shared/interfaces';
import { getGameKeyByType } from '../../games.helpers';

@Component({
  selector: 'tf-game-role-icon-group',
  templateUrl: './game-role-icon-group.component.html',
  styleUrls: ['./game-role-icon-group.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GameRoleIconGroupComponent implements OnInit {
  @Input() roles: string[] = ['top', 'mid', 'jungle', 'bot', 'support'];
  @Input() gameType: GameType;

  dictRoles: GameRole[] = [];

  constructor(private store: Store<AppState>) {}

  get displayRoles(): GameRole[] {
    return this.dictRoles.filter((role) => this.roles.includes(role.value));
  }

  ngOnInit(): void {
    this.store.select(selectDicts, ['roles']).subscribe((results) => {
      this.dictRoles = results.roles[getGameKeyByType(this.gameType)];
    });
  }
}
