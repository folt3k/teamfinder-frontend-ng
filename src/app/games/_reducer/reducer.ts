import { createReducer, on, Action } from '@ngrx/store';

import * as actions from './actions';
import { GameOption, GAMES } from '../../_shared/common';
import { LolGamesHistoryItem } from '../games.interface';

export interface State {
  selected: GameOption;
  gamesHistory: {
    results: LolGamesHistoryItem[];
    page: number;
    isLoading: boolean;
  };
}

export const initialState: State = {
  selected: null,
  gamesHistory: {
    results: [],
    page: 0,
    isLoading: false,
  },
};

export const _reducer = createReducer(
  initialState,
  on(actions.changeCurrentGame, (state, { gameId }) => ({
    ...state,
    selected: GAMES.find((game) => game.value === gameId),
  })),
  on(actions.fetchLolGamesHistory, (state) => ({
    ...state,
    gamesHistory: {
      ...state.gamesHistory,
      isLoading: true,
    },
  })),
  on(actions.fetchLolGamesHistorySuccess, (state, payload) => ({
    ...state,
    gamesHistory: {
      results: [...state.gamesHistory.results, ...payload.data.results],
      page: payload.data.page,
      isLoading: false,
    },
  })),
  on(actions.resetGamesHistory, (state) => ({
    ...state,
    gamesHistory: {
      results: [],
      page: 0,
      isLoading: false,
    },
  }))
);

export function reducer(state: State, action: Action) {
  return _reducer(state, action);
}
