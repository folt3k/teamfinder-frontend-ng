import { BaseSelectOption } from '../interfaces';

export const ageYears: BaseSelectOption[] = Array.from(Array(99), (_, i) => ({
  value: i + 1,
  label: (i + 1).toString(),
}));
