import { ListQueryParams } from '../_shared/interfaces';

export interface ProfilesListParams extends ListQueryParams {
  excluded?: Array<number>;
}

export interface UpdateProfileDto {
  modes: number[];
  roles: number[];
  desc: string;
  avatar: string;
}
