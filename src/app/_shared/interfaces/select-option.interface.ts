export interface BaseSelectOption {
  value: any;
  label: string;
}
