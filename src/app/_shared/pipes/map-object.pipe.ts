import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mapObject',
})
export class MapObjectPipe implements PipeTransform {
  transform<T>(value: T): Array<{ key: string; value: any }> {
    return Object.keys(value).map((key) => ({ key, value: value[key] }));
  }
}
