import { BaseEntity, GameType } from '../_shared/interfaces';
import { GameMode, GameRank, GameRole, UserProfile } from '../_shared/models';

export interface Team extends BaseEntity {
  name: string;
  type: GameType;
  slug: string;
  logoUrl: string;
  rank: GameRank;
  modes: GameMode[];
  roles: GameRole[];
  gameServer?: string;
  gameServerMeta?: { id: string; name: string };
  disabled: boolean;
  desc: string;
  remarks: string;
  owner: UserProfile;
  members?: TeamMember[];
}

export interface TeamMember {
  id: number;
  dateOfJoin: Date;
  profile: UserProfile;
}

export interface UserMemberTeam {
  dateOfJoin: Date;
  id: number;
  team: Team;
}
