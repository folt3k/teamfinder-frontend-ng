import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LolGamesHistoryComponent } from './lol-games-history.component';
import { LolGamesHistoryItemComponent } from './item/item.component';
import { SharedModule } from '../../../_shared/shared.module';

@NgModule({
  declarations: [LolGamesHistoryComponent, LolGamesHistoryItemComponent],
  imports: [CommonModule, SharedModule],
  exports: [LolGamesHistoryComponent],
})
export class LolGamesHistoryModule {}
