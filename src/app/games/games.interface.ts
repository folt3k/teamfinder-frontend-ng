export interface CsGameAccountStats {
  headshotsRatio: string;
  kdRatio: string;
  winRatio: string;
  accuracyRatio: string;
  totalHoursPlayed: number;
  bestWeapons: CsGameAccountStatsBestWeapon[];
}

export interface CsGameAccountStatsBestWeapon {
  name: string;
  kills: number;
  accuracy: number;
}

export interface FntGameAccountStats {
  account: {
    level: number;
  };
  per_input: FntGameDeviceStats;
  global_stats: FntGameModeStats;
}

export interface FntGameDeviceStats {
  keyboardmouse: FntGameModeStats;
  gamepad: FntGameModeStats;
  touch: FntGameModeStats;
}

export interface FntGameModeStats {
  duo: FntGameModeStat;
  solo: FntGameModeStat;
  squad: FntGameModeStat;
}

export interface FntGameModeStat {
  kd: number;
  kills: number;
  score: number;
  winrate: number;
  placetop1: number;
  placetop3: number;
  placetop5: number;
  placetop6: number;
  placetop10: number;
  placetop12: number;
  placetop25: number;
  matchesplayed: number;
  minutesplayed: number;
  playersoutlived: number;
}

export type GameAccountStats = CsGameAccountStats & FntGameAccountStats;

export interface LolGamesHistoryItem {
  gameCreation: Date;
  gameDuration: number;
  otherPlayers: Array<{ champion: { name: string; imageUrl: string }; playerName: string }>;
  queue: number;
  summonerPlayer: {
    champion: { name: string; imageUrl: string };
    imageUrl: string;
    name: string;
    goldEarned: number;
    items: string[];
    kda: { k: number; d: number; a: number };
    minionsKilled: number;
    spells: string[];
    win: boolean;
  };
}

export interface LolGamesHistoryRO {
  page: number;
  results: LolGamesHistoryItem[];
}
