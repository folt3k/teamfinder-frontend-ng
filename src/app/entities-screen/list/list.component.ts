import { Component, Input, OnInit } from '@angular/core';

import { BaseEntity } from '../../_shared/interfaces';
import { EntitiesScreens, EntitiesScreenService } from '../entities-screen.service';

@Component({
  selector: 'tf-entities-screen-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  @Input() records: any[] = [];

  ENTITIES_SCREENS = EntitiesScreens;

  constructor(private entitiesScreenService: EntitiesScreenService) {}

  get screenType(): EntitiesScreens {
    return this.entitiesScreenService.getScreenType();
  }

  ngOnInit(): void {}

  trackByFn(index: number, item: BaseEntity): number {
    return item.id;
  }
}
