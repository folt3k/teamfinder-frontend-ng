import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { PopoverModule } from '@ng-poppy/lib';

import {
  OnlineStatusIndicatorModule,
  UserAvatarModule,
  LoaderModule,
  EmojiPickerModule,
  ButtonModule,
  LogoComponent,
  ErrorMessageModule,
  CounterModule,
  StepperModule,
  IconButtonModule,
  PaginationModule,
  AddAvatarModule,
  HeadlineModule,
  StatsTileComponent,
  ConfirmationModule,
  TabsModule,
  ActionMenuModule,
  CollapsableNavigationModule,
  InfoModule,
  InfiniteScrollModule,
  TableModule,
  AdsenseBannerComponent,
} from './components';
import {
  InputModule,
  CheckboxModule,
  GameRolePickerModule,
  UserSelectModule,
  SearchInputModule,
  TilesSelectModule,
  RangeSelectModule,
  IconSwitcherModule,
  SelectModule,
} from './form-controls';
import { SendMessageModule } from './forms';
import { DirectivesModule } from './directives/directives.module';
import { PipesModule } from './pipes/pipes.module';
import { PoppyModule } from './poppy/poppy.module';

const MODULES = [
  DirectivesModule,
  PipesModule,
  PoppyModule,
  PopoverModule,
  OnlineStatusIndicatorModule,
  UserAvatarModule,
  LoaderModule,
  SendMessageModule,
  EmojiPickerModule,
  ButtonModule,
  InputModule,
  SearchInputModule,
  ErrorMessageModule,
  CounterModule,
  StepperModule,
  CheckboxModule,
  GameRolePickerModule,
  UserSelectModule,
  IconButtonModule,
  SelectModule,
  TilesSelectModule,
  RangeSelectModule,
  PaginationModule,
  AddAvatarModule,
  HeadlineModule,
  IconSwitcherModule,
  ConfirmationModule,
  TabsModule,
  ActionMenuModule,
  CollapsableNavigationModule,
  InfoModule,
  InfiniteScrollModule,
  TableModule,
];

const COMPONENTS = [LogoComponent, StatsTileComponent, AdsenseBannerComponent];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [CommonModule, RouterModule, ReactiveFormsModule, ...MODULES],
  exports: [RouterModule, ReactiveFormsModule, ...MODULES, ...COMPONENTS],
})
export class SharedModule {}
