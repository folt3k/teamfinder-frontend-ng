import { EntityType } from '../_shared/interfaces';
import { FeedbackAction } from '../_shared/models';

export const getSenderTypeLabel = (senderEntityType: EntityType): string => {
  switch (senderEntityType) {
    case EntityType.USER:
      return 'Gracz';
    case EntityType.TEAM:
      return 'Drużyna';
    default:
      return '';
  }
};

export const getReceiverTypeLabel = (receiverEntityType: EntityType): string => {
  switch (receiverEntityType) {
    case EntityType.USER:
      return 'Gracz';
    case EntityType.TEAM:
      return 'Drużyna';
    case EntityType.ANNOUNCEMENT:
      return 'Ogłoszenie';
    default:
      return '';
  }
};

export const getActionText = (action: FeedbackAction): string => {
  switch (action) {
    case FeedbackAction.INVITED:
      return 'zaprosiła gracza';
    case FeedbackAction.TEAM_REQUESTED:
      return 'zgłosił chęć dołączenia do';
    case FeedbackAction.JOINED:
      return 'dołączył do drużyny';
    case FeedbackAction.TEAM_CREATED:
      return 'uwtorzył drużynę';
    case FeedbackAction.USER_CREATED:
      return 'zarejestrował się w aplikacji';
    case FeedbackAction.TEAM_COMMENTED:
      return 'dodał komentarz na stronie drużyny';
    case FeedbackAction.GAME_ANNOUNCEMENT_COMMENTED:
      return 'skomentował ogłoszenie';
    case FeedbackAction.GAME_ANNOUNCEMENT_CREATED:
      return 'dodał nowe ogłoszenie';
  }
};
