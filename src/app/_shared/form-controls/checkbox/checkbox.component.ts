import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewEncapsulation,
  forwardRef,
  Input,
  ChangeDetectorRef,
  TemplateRef,
} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, ControlContainer } from '@angular/forms';
import { CustomFormControl } from '../custom-form-control';

@Component({
  selector: 'tf-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxComponent),
      multi: true,
    },
  ],
})
export class CheckboxComponent extends CustomFormControl implements OnInit, ControlValueAccessor {
  @Input() label: string | TemplateRef<any> = null;
  @Input() disabled: boolean = false;
  @Input() checked: boolean = false;

  constructor(cdr: ChangeDetectorRef, controlContainer: ControlContainer) {
    super(cdr, controlContainer);
  }

  ngOnInit(): void {
    super.ngOnInit();
  }

  get isLabelTemplate(): boolean {
    return this.label instanceof TemplateRef;
  }

  get labelTemp(): TemplateRef<any> {
    return this.label as TemplateRef<any>;
  }

  writeValue(value: any): void {
    this.checked = !!value;
    this.cdr.detectChanges();
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.propagateTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
    this.cdr.detectChanges();
  }

  onClick(event: MouseEvent): void {
    if (event) {
      event.stopPropagation();
    }

    this.checked = !this.checked;
    this.cdr.detectChanges();
    this.propagateChange(this.checked);
  }

  onBlur(): void {
    this.propagateTouched();
    this.cdr.detectChanges();
  }

  private propagateChange(value: any) {}
  private propagateTouched() {}
}
