import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { FetchTeamChatRoomsRO } from '../chat.inerface';

@Injectable({
  providedIn: 'root',
})
export class TeamChatService {
  constructor(private http: HttpClient) {}

  fetchRooms(): Observable<FetchTeamChatRoomsRO> {
    return this.http.get<FetchTeamChatRoomsRO>('/team-chat/rooms');
  }
}
