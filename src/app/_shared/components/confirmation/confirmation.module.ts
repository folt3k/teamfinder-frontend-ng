import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfirmationComponent } from './confirmation.component';
import { ButtonModule } from '../button/button.module';

@NgModule({
  declarations: [ConfirmationComponent],
  imports: [CommonModule, ButtonModule],
  exports: [ConfirmationComponent],
})
export class ConfirmationModule {}
