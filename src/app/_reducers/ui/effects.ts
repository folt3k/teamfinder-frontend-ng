import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Title } from '@angular/platform-browser';
import { tap, withLatestFrom } from 'rxjs/operators';

import { AppState } from '../../app.interface';
import { meSuccess } from '../../auth/_reducer/actions';
import { markRoomAsRead, receiveMessage, receiveNewConversation } from '../../chat/_reducer/actions';
import { markAllAsDisplayed, receiveNewNotification } from '../../notifications/_reducer/actions';
import { selectNotificationCountersSum } from './selectors';

@Injectable()
export class UiEffects {
  updateNotificationSumCounterInPageTitle$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          meSuccess,
          receiveMessage,
          receiveNewConversation,
          receiveNewNotification,
          markRoomAsRead,
          markAllAsDisplayed
        ),
        withLatestFrom(this.store.select(selectNotificationCountersSum)),
        tap(([_, counterSum]) => {
          const currentDocumentTitle = document.title.replace(/\(([^)]+)\)/, '');
          const title = !!counterSum ? `(${counterSum}) ${currentDocumentTitle}` : currentDocumentTitle;

          this.title.setTitle(title);
        })
      ),
    { dispatch: false }
  );

  constructor(private actions$: Actions, private store: Store<AppState>, private title: Title) {}
}
