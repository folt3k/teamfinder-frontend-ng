import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { repeatPasswordValidator } from '../../../_shared/form-validation';
import { UserService } from '../../user.service';
import { STATIC_URLS } from '../../../_shared/common';

@Component({
  selector: 'tf-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
  form: FormGroup;
  submitting: boolean = false;
  success: boolean = false;
  STATIC_URLS = STATIC_URLS;

  constructor(private fb: FormBuilder, private usersService: UserService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.initForm();
  }

  onSubmit(): void {
    this.submitting = true;
    this.usersService
      .recoverPassword({
        ...this.form.value,
        token: this.route.snapshot.paramMap.get('token'),
      })
      .subscribe(() => {
        this.success = true;
        this.submitting = false;
      });
  }

  private initForm(): void {
    this.form = this.fb.group(
      {
        password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(50)]],
        passwordRepeat: ['', [Validators.required]],
      },
      { validator: repeatPasswordValidator(['password', 'passwordRepeat']) }
    );
  }
}
