import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { combineLatest, Observable, Subject } from 'rxjs';
import { distinctUntilChanged, filter, map, take, takeUntil, withLatestFrom } from 'rxjs/operators';

import { TileSelectOption } from '../../_shared/form-controls/tiles-select/tiles-select.component';
import { BaseSelectOption } from '../../_shared/interfaces';
import { selectDicts } from '../../_reducers/dictionaries';
import { changeCurrentGame, selectCurrentGame } from '../../games/_reducer';
import { AppState } from '../../app.interface';
import { TeamFormFieldsDisplayCondition, fieldsDisplayCondition } from './fields-display-condition';
import { loading } from '../_reducer';
import { noSpecialCharactersValidator, TeamNameAvailableAsyncValidator } from '../../_shared/form-validation';
import { currentProfile } from '../../auth/_reducer';
import { CreateTeamDto } from '../team.interface';
import { selectDetails } from '../../entity-details/_reducer';

interface FormFieldOptions {
  roles: TileSelectOption[];
  modes: TileSelectOption[];
  servers: BaseSelectOption[];
  ranks: BaseSelectOption[];
}

@Component({
  selector: 'tf-team-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss'],
  providers: [TeamNameAvailableAsyncValidator],
})
export class TeamFormComponent implements OnInit, OnDestroy {
  @Output() submitted = new EventEmitter<CreateTeamDto>();
  @Input() edit: boolean = false;

  form: FormGroup;
  options: FormFieldOptions = { roles: [], modes: [], ranks: [], servers: [] };
  fieldsDisplayCondition: TeamFormFieldsDisplayCondition;
  avatar$: Observable<string>;
  submitting$: Observable<boolean>;
  private destroy$ = new Subject();

  constructor(
    private fb: FormBuilder,
    private store: Store<AppState>,
    private teamNameAsyncValidator: TeamNameAvailableAsyncValidator
  ) {
    this.submitting$ = this.store.select(loading);
    this.avatar$ = this.store.select(selectDetails).pipe(map((details) => details.logoUrl));
  }

  ngOnInit(): void {
    this.init();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onSubmit(): void {
    this.submitted.emit(this.form.value);
  }

  onAvatarChange(avatar: string): void {
    this.form.patchValue({ avatar });
  }

  private init(): void {
    this.store
      .select(currentProfile)
      .pipe(
        filter((profile) => !!profile),
        take(1)
      )
      .subscribe((profile) => {
        this.store.dispatch(changeCurrentGame({ gameId: profile.type }));
        this.initOptions();
      });
  }

  private initOptions(): void {
    combineLatest(
      this.store.select(selectDicts, ['roles', 'modes', 'servers', 'ranks']).pipe(take(1)),
      this.store.select(selectCurrentGame)
    )
      .pipe(
        takeUntil(this.destroy$),
        filter(([dicts, currentGame]) => !!currentGame)
      )
      .subscribe(([dicts, currentGame]) => {
        const gameKey = currentGame.key;

        this.fieldsDisplayCondition = fieldsDisplayCondition(currentGame.value);
        this.options = {
          roles: dicts.roles[gameKey],
          modes: dicts.modes[gameKey],
          servers: dicts.servers[gameKey],
          ranks: dicts.ranks[gameKey],
        };

        this.initForm();
        this.setValidators();
      });
  }

  private initForm(): void {
    this.form = this.fb.group({
      name: ['', [Validators.required, noSpecialCharactersValidator()]],
      avatar: [null],
      desc: [''],
      modes: [[]],
      roles: [[]],
      rank: [],
      gameServer: [],
    });

    if (this.edit) {
      this.setInitialValues();
    }
  }

  private setInitialValues(): void {
    this.store
      .select(selectDetails)
      .pipe(take(1))
      .subscribe((details) => {
        this.form.patchValue({
          name: details.name,
          desc: details.desc,
          rank: details.rank?.id,
          modes: details.modes.map((mode) => mode.id),
          roles: details.roles.map((role) => role.id),
          gameServer: details.gameServer,
        });
      });
  }

  private setValidators(): void {
    if (this.fieldsDisplayCondition('gameServer')) {
      this.form.controls.gameServer.setValidators([Validators.required]);
    }
    if (this.fieldsDisplayCondition('rank')) {
      this.form.controls.rank.setValidators([Validators.required]);
    }
    if (this.fieldsDisplayCondition('modes')) {
      this.form.controls.modes.setValidators([Validators.required]);
    }
    this.setTeamNameAsyncValidator();
  }

  private setTeamNameAsyncValidator(): void {
    if (this.edit) {
      this.form.controls.name.valueChanges
        .pipe(
          distinctUntilChanged(),
          withLatestFrom(this.store.select(selectDetails)),
          takeUntil(this.destroy$)
        )
        .subscribe(([value, details]) => {
          if (value === details.name) {
            this.form.controls.name.clearAsyncValidators();
          } else {
            // @ts-ignore
            this.form.controls.name.setAsyncValidators([this.teamNameAsyncValidator]);
          }
          this.form.controls.name.updateValueAndValidity();
        });
    } else {
      // @ts-ignore
      this.form.controls.name.setAsyncValidators([this.teamNameAsyncValidator]);
    }
  }
}
