import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AppState } from '../../app.interface';
import { Notification } from '../notification.model';
import { fetchAll, markAllAsRead, selectPagination, selectResults } from '../_reducer';
import { Pagination } from '../../_shared/interfaces';

@Component({
  selector: 'tf-notifications-compact-list',
  templateUrl: './compact-list.component.html',
  styleUrls: ['./compact-list.component.scss'],
})
export class CompactListComponent implements OnInit {
  items$: Observable<Notification[]>;
  pagination$: Observable<Pagination>;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.items$ = this.store.select(selectResults);
    this.pagination$ = this.store.select(selectPagination);
  }

  markAllAsRead(): void {
    this.store.dispatch(markAllAsRead());
  }

  loadMore(page: number): void {
    this.store.dispatch(fetchAll({ page }));
  }
}
