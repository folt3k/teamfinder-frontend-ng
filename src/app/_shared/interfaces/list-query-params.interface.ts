export interface ListQueryParams {
  perPage?: number;
  page?: number;
  order?: 'desc' | 'asc';
  q?: string;
}
