import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AllProfilesComponent } from './all.component';

const routes: Routes = [
  {
    path: '',
    component: AllProfilesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AllProfilesRoutingModule {}
