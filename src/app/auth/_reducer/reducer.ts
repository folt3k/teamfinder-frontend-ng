import { createReducer, on, Action } from '@ngrx/store';

import * as actions from './actions';
import * as userActions from '../../user/_reducer/actions';
import * as teamActions from '../../team/_reducer/actions';
import * as fromInvitations from '../../invitations/_reducer';
import { BaseReducerState, createBaseReducerState, clearError, getErrorState } from '../../_reducers';
import { UserProfile, User } from 'src/app/_shared/models';

export interface State extends BaseReducerState {
  token: string;
  profile: UserProfile;
  otherProfiles: UserProfile[];
  user: User;
  authorizationCompleted: boolean;
}

export const initialState: State = createBaseReducerState({
  token: null,
  profile: null,
  otherProfiles: [],
  user: null,
  authorizationCompleted: false,
});

export const _reducer = createReducer(
  initialState,
  on(actions.init, (state, { token }) => ({
    ...state,
    token,
  })),
  on(actions.login, (state) => ({
    ...state,
    loading: true,
  })),
  on(actions.loginSuccess, (state, { token }) => ({
    ...state,
    token,
    loading: false,
    error: clearError(),
  })),
  on(actions.loginFailed, (state, payload) => {
    return {
      ...state,
      error: getErrorState(payload),
      loading: false,
    };
  }),
  on(actions.logoutSuccess, (state) => ({
    ...initialState,
    authorizationCompleted: state.authorizationCompleted,
  })),
  on(actions.me, (state) => ({
    ...state,
    loading: true,
  })),
  on(actions.meSuccess, (state, payload) => ({
    ...state,
    profile: payload.currentProfile,
    otherProfiles: payload.otherProfiles,
    user: payload.user,
    loading: false,
    authorizationCompleted: true,
  })),
  on(actions.meFailed, (state, payload) => {
    return {
      ...state,
      error: getErrorState(payload),
      loading: false,
      authorizationCompleted: true,
    };
  }),
  on(actions.clearError, (state) => ({
    ...state,
    error: clearError(),
  })),
  on(actions.activateAccount, (state) => ({
    ...state,
    loading: true,
  })),
  on(actions.activateAccountSuccess, (state, payload) => ({
    ...state,
    token: payload.token,
  })),
  on(actions.activateAccountFailed, (state) => ({
    ...state,
    loading: false,
  })),
  on(userActions.updateUserSuccess, (state, payload) => ({
    ...state,
    user: payload.data,
  })),
  on(teamActions.removeTeamSuccess, (state, { id }) => ({
    ...state,
    profile: {
      ...state.profile,
      ownedTeams: state.profile.ownedTeams.filter((t) => t.id !== id),
    },
  })),
  on(actions.finalAuthorization, (state) => ({
    ...state,
    authorizationCompleted: true,
  })),
  on(fromInvitations.sendRequestSuccess, (state, payload) => ({
    ...state,
    profile: {
      ...state.profile,
      teamRequests: [...state.profile.teamRequests, payload],
    },
  }))
);

export function reducer(state: State, action: Action) {
  return _reducer(state, action);
}
