import { Observable } from 'rxjs';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { UserProfile } from 'src/app/_shared/models';
import { select, Store } from '@ngrx/store';
import { AppState } from 'src/app/app.interface';

@Component({
  selector: 'tf-global-chat-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserListComponent implements OnInit {
  users$: Observable<UserProfile[]>;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.users$ = this.store.pipe(select((state) => state.globalChat.activeUsers));
  }

  trackByFn(index: number, item: UserProfile): number {
    return item.id;
  }
}
