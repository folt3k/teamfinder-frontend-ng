import { ControlContainer, NG_VALUE_ACCESSOR, ControlValueAccessor, NG_VALIDATORS } from '@angular/forms';
import {
  Component,
  ChangeDetectorRef,
  forwardRef,
  ChangeDetectionStrategy,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';

import { BaseSelectOption } from '../../interfaces';
import { CustomFormControl } from '../custom-form-control';

export interface TileSelectOption extends BaseSelectOption {
  iconUrl?: string;
}

interface DisplayedOption extends BaseSelectOption {
  label: string;
  value: number | string;
  selected: boolean;
  iconUrl?: string;
}

@Component({
  selector: 'tf-tile-select',
  templateUrl: './tiles-select.component.html',
  styleUrls: ['./tiles-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TilesSelectComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => TilesSelectComponent),
      multi: true,
    },
  ],
})
export class TilesSelectComponent extends CustomFormControl implements OnChanges, ControlValueAccessor {
  @Input() options: TileSelectOption[] = [];

  displayedOptions: DisplayedOption[] = [];
  currentValue: (string | number)[] = [];

  constructor(cdr: ChangeDetectorRef, controlContainer: ControlContainer) {
    super(cdr, controlContainer);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.options.currentValue) {
      this.setOptions();
    }
  }

  writeValue(value: (string | number)[]): void {
    this.displayedOptions = this.displayedOptions.map((opt) => ({
      ...opt,
      selected: value.includes(opt.value),
    }));
    this.currentValue = value;
    this.cdr.detectChanges();
  }

  registerOnChange(fn: () => {}): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => {}): void {
    this.propagateTouched = fn;
  }

  onItemClick(item: DisplayedOption): void {
    this.displayedOptions = this.displayedOptions.map((option) => {
      if (option.value === item.value) {
        return {
          ...option,
          selected: !option.selected,
        };
      }

      return option;
    });
    this.propagateChange(this.populateValue());
  }

  private populateValue(): (number | string)[] {
    return this.displayedOptions.filter((opt) => opt.selected).map((opt) => opt.value);
  }

  private propagateChange(value: (number | string)[]): void {}

  private propagateTouched(): void {}

  private setOptions(): void {
    this.displayedOptions = [...this.options].map((opt) => ({
      ...opt,
      selected: this.currentValue.includes(opt.value),
    }));
  }
}
