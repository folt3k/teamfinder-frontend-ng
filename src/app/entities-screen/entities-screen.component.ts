import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, merge, Subject } from 'rxjs';
import {
  debounceTime,
  filter,
  map,
  mapTo,
  pairwise,
  startWith,
  takeUntil,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { isEmpty, isEqual } from 'lodash';

import * as sorterOptions from '../_shared/common/sorters';
import { DEFAULT_PER_PAGE } from '../_shared/common/sorters';
import { mapParamsToValues, mapValuesToParams } from '../_helpers';
import { EntitiesScreens, EntitiesScreenService } from './entities-screen.service';
import { BaseEntity, GameType, Pagination } from '../_shared/interfaces';
import { Actions, ofType } from '@ngrx/effects';
import { updateUserNetworkStatus } from '../user/_reducer';
import { Announcement, UserProfile } from '../_shared/models';
import { SeoService } from '../_core/services/seo.service';
import { getGameTypeByRouterPrefix } from '../games';

@Component({
  selector: 'tf-entities-screen',
  templateUrl: './entities-screen.component.html',
  styleUrls: ['./entities-screen.component.scss'],
})
export class EntitiesScreenComponent implements OnInit, OnDestroy {
  filtersForm: FormGroup;
  sorterOptions: { perPageOptions: any[]; orderOptions: any[] };
  pagination: Pagination;
  records: BaseEntity[] = [];
  showFiltersOnLowResolution = false;
  private wasGameChanged$ = new BehaviorSubject<boolean>(false);
  private destroy$ = new Subject();

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private entitiesService: EntitiesScreenService,
    private actions: Actions,
    private seo: SeoService
  ) {
    this.sorterOptions = { ...sorterOptions };
  }

  get totalResults(): number {
    return this.pagination && this.pagination.totalResults;
  }

  get foundResultsEntityText(): string {
    switch (this.entitiesService.getScreenType()) {
      case EntitiesScreens.USERS:
        return 'graczy';
      case EntitiesScreens.TEAMS:
        return 'drużyn';
      case EntitiesScreens.GAME_ANNOUNCEMENTS:
        return 'ogłoszeń';
    }
  }

  get canDisplayFilters(): boolean {
    if (this.entitiesService.getScreenType() === EntitiesScreens.GAME_ANNOUNCEMENTS) {
      return this.entitiesService.getGameType() !== GameType.FNT;
    }

    return true;
  }

  ngOnInit(): void {
    this.entitiesService.init();
    this.initForm();
    this.listenOnFormValuesChange();
    this.listenOnParamsChange();
    this.updateOnlineStatusOnUserStatusChange();
    this.entitiesService.data$.pipe(takeUntil(this.destroy$)).subscribe((data) => {
      this.records = data.results;
      this.pagination = data.pagination;
    });

    setTimeout(() => {
      if (isEmpty(this.route.snapshot.queryParams)) {
        this.loadData();
      }
    });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onReset(): void {
    this.filtersForm.setValue(this.getInitValues());
  }

  onPageChanged(page: number): void {
    this.updateQueryParams({ page });
  }

  toggleFiltersOnLowResolutionVisibility(): void {
    this.showFiltersOnLowResolution = !this.showFiltersOnLowResolution;
  }

  private initForm(): void {
    this.filtersForm = this.fb.group({
      q: [null],
      gameServer: [null],
      rank: [null],
      mode: [[]],
      role: [[]],
      age_from: null,
      age_to: null,
      perPage: DEFAULT_PER_PAGE,
      order: 'desc',
    });
  }

  private loadData(params: any = {}): void {
    this.entitiesService.fetchAllEntities(params);
  }

  private updateOnlineStatusOnUserStatusChange(): void {
    this.actions.pipe(ofType(updateUserNetworkStatus), takeUntil(this.destroy$)).subscribe((data) => {
      const screenType = this.entitiesService.getScreenType();

      switch (screenType) {
        case EntitiesScreens.USERS:
          this.records = (this.records as UserProfile[]).map((r) =>
            r.id === data.profile.id ? { ...r, isOnline: data.isOnline } : r
          );
          break;
        case EntitiesScreens.GAME_ANNOUNCEMENTS:
          this.records = (this.records as Announcement[]).map((r) =>
            r.author.id === data.profile.id ? { ...r, author: { ...r.author, isOnline: data.isOnline } } : r
          );
          break;
      }
    });
  }

  private listenOnFormValuesChange(): void {
    this.filtersForm.valueChanges
      .pipe(
        startWith({}),
        pairwise(),
        filter(([prev, curr]) => this.hasValuesChanged(prev, curr)),
        withLatestFrom(
          this.route.params.pipe(
            startWith(this.route.snapshot.params),
            pairwise(),
            map(([prev, curr]) => [prev.gamePrefix, curr.gamePrefix])
          ),
          this.wasGameChanged$.asObservable()
        ),
        takeUntil(this.destroy$)
      )
      .subscribe(([values, gamePrefixes, wasGameChanged]) => {
        const [prevGamePrefix, currGamePrefix] = gamePrefixes;
        if (wasGameChanged ? prevGamePrefix === currGamePrefix : true) {
          this.updateQueryParams();
        } else {
          this.wasGameChanged$.next(false);
        }
      });
  }

  private listenOnParamsChange(): void {
    const queryParams$ = this.route.queryParams.pipe(
      startWith({}),
      pairwise(),
      filter((values) => this.hasValuesChanged(...values)),
      map(([prev, curr]) => curr)
    );

    const params$ = this.route.params.pipe(
      startWith({ gamePrefix: this.route.snapshot.params.gamePrefix }),
      pairwise(),
      filter((values) => this.hasValuesChanged(...values)),
      tap(([_, currValues]) => {
        this.wasGameChanged$.next(true);

        if (currValues.gamePrefix) {
          this.entitiesService.data$.next({ results: [], pagination: null });
          this.entitiesService.init();
          this.seo.setSeo(
            this.entitiesService.getScreenType(),
            getGameTypeByRouterPrefix(currValues.gamePrefix)
          );
        }
      }),
      mapTo(null)
    );

    merge(queryParams$, params$)
      .pipe(debounceTime(0), takeUntil(this.destroy$))
      .subscribe((values) => {
        const params = values || this.route.snapshot.queryParams;
        const newValues = { ...mapParamsToValues(params) };
        this.filtersForm.setValue(this.getInitValues(), { emitEvent: false });
        this.filtersForm.patchValue(newValues, { emitEvent: false });
        this.loadData(params);
      });
  }

  private updateQueryParams(additionalParams: { page?: number } = {}): void {
    this.router.navigate([], {
      relativeTo: this.route,
      queryParams: {
        ...mapValuesToParams(this.filtersForm.value),
        page: additionalParams.page || 1,
      },
    });
  }

  // tslint:disable-next-line:no-any
  private hasValuesChanged(prev: any, curr: any): boolean {
    return !isEqual(prev, curr);
  }

  private getInitValues(): any {
    return {
      q: null,
      mode: [],
      role: [],
      rank: null,
      gameServer: null,
      age_from: null,
      age_to: null,
      perPage: DEFAULT_PER_PAGE,
      order: 'desc',
    };
  }
}
