import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { GameRank, UserProfile } from '../../_shared/models';
import { AppState } from '../../app.interface';
import { currentProfile } from '../../auth/_reducer';
import { FntGameAccountStats, GameCheckers, getGameCheckers } from '../../games';
import { GameTranslations } from '../../games/_services/games-translations';
import { GamesTranslationsService } from '../../games/_services/games-translations.service';

@Component({
  selector: 'tf-my-profile-preview',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss'],
})
export class MyProfilePreviewComponent implements OnInit, OnDestroy {
  currentProfile: UserProfile;
  gameCheckers: GameCheckers;
  gameTranslations: GameTranslations;

  private destroy$ = new Subject();

  constructor(private store: Store<AppState>, private gameTranslationsService: GamesTranslationsService) {}

  ngOnInit(): void {
    this.store
      .select(currentProfile)
      .pipe(takeUntil(this.destroy$))
      .subscribe((profile) => {
        this.currentProfile = profile;
        this.gameCheckers = getGameCheckers(profile.type);
        this.gameTranslationsService.get(profile.type).subscribe((results) => {
          this.gameTranslations = results;
        });
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  get roles(): string[] {
    return this.currentProfile.roles.map((role) => role.name);
  }

  get modes(): string {
    return this.currentProfile.modes.map((mode) => mode.name).join(', ');
  }

  get ranks(): GameRank[] {
    return this.currentProfile.ranks || (this.currentProfile.rank ? [this.currentProfile.rank] : []);
  }

  get fntAccountLevel(): number {
    return (this.currentProfile.game_metadata?.accountStats as FntGameAccountStats)?.account?.level;
  }
}
