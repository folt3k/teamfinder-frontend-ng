import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../../_shared/shared.module';
import { GameSelectComponent } from './game-select.component';

@NgModule({
  declarations: [GameSelectComponent],
  imports: [CommonModule, SharedModule],
  exports: [GameSelectComponent],
})
export class GameSelectModule {}
