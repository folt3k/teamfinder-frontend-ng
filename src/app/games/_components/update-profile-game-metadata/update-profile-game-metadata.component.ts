import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { UserProfile } from '../../../_shared/models';
import { Store } from '@ngrx/store';
import { updateProfileGameMetadata, updatingGameMetadata } from '../../../profile/_reducer';
import { Observable } from 'rxjs';

@Component({
  selector: 'tf-update-profile-game-metadata',
  templateUrl: './update-profile-game-metadata.component.html',
  styleUrls: ['./update-profile-game-metadata.component.scss'],
})
export class UpdateProfileGameMetadataComponent implements OnInit {
  @Input() data: UserProfile;

  loading$: Observable<boolean>;

  constructor(private store: Store) {}

  get canUpdateGameStats(): boolean {
    const lastUpdate = this.data?.game_metadata?.lastGameStatsUpdated;
    const ONE_DAY = 1000 * 3600 * 24;

    if (!lastUpdate) {
      return true;
    }

    const diff = new Date().getTime() - new Date(lastUpdate).getTime();
    return diff >= ONE_DAY;
  }

  ngOnInit(): void {
    this.loading$ = this.store.select(updatingGameMetadata);
  }

  updateGameStats(): void {
    this.store.dispatch(updateProfileGameMetadata());
  }
}
