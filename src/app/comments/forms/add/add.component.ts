import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AppState } from '../../../app.interface';
import { createComment, createSubcomment } from '../../_reducer';
import { Comment } from '../../../_shared/models';
import { isLoggedIn } from '../../../auth/_reducer';
import { AuthorizationNeededDirective } from '../../../_shared/directives';
import { noEmptyTextValidator } from '../../../_shared/form-validation';

@Component({
  selector: 'tf-add-comment',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss'],
})
export class AddCommentComponent implements OnInit {
  @ViewChild('loginToAddComment') loginToAddCommentLink: AuthorizationNeededDirective;

  @Input() isSubcomment: boolean = false;
  @Input() parentComment: Comment;
  @Output() canceled = new EventEmitter();

  form: FormGroup;
  isLoggedIn$: Observable<boolean>;

  constructor(private fb: FormBuilder, protected store: Store<AppState>) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      content: [null, [Validators.required, noEmptyTextValidator()]],
    });
    this.isLoggedIn$ = this.store.select(isLoggedIn);
  }

  onSubmit(): void {
    const content = this.form.controls.content.value;
    if (this.isSubcomment) {
      this.store.dispatch(createSubcomment({ content, commentId: this.parentComment.id }));
      this.cancel();
    } else {
      this.store.dispatch(createComment({ content }));
      this.form.reset();
    }
  }

  cancel(): void {
    this.canceled.emit();
  }
}
