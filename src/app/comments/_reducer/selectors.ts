import { AppState } from '../../app.interface';
import { createSelector } from '@ngrx/store';
import { isCurrentUserGameEqualsToEntityGame } from '../../games';
import { featureKey as detailsFeatureKey } from '../../entity-details/_reducer';
import { featureKey as authFeatureKey } from '../../auth/_reducer';
import { featureKey } from './constants';

const selectFeature = (state: AppState) => state[featureKey];

export const error = createSelector(selectFeature, (state) => state.error);

export const loading = createSelector(selectFeature, (state) => state.loading);

export const selectComments = createSelector(selectFeature, (state) => state);

export const selectCommentResults = createSelector(selectComments, (state) => state.results);

export const canLoadMoreComments = createSelector(
  selectFeature,
  (state) => state.pagination?.totalResults > state.results.length
);

export const canPerformAuthorizedAction = createSelector(
  (state: AppState) => state[detailsFeatureKey].data,
  (state: AppState) => state[authFeatureKey].profile,
  (entity, user) => !user || isCurrentUserGameEqualsToEntityGame(user, entity)
);
