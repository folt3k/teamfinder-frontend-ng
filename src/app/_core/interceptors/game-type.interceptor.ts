import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { switchMap, take } from 'rxjs/operators';

import { AppState } from '../../app.interface';
import { selectCurrentGame } from '../../games/_reducer';

@Injectable()
export class GameTypeInterceptor implements HttpInterceptor {
  constructor(private store: Store<AppState>) {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.store.select(selectCurrentGame).pipe(
      take(1),
      switchMap((currentGame) => {
        request = request.clone({
          setHeaders: {
            ['X-Teamfinder-GameType']: currentGame?.value.toString() || '1',
          },
        });

        return next.handle(request);
      })
    );
  }
}
