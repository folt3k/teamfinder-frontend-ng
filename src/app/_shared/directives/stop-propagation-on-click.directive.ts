import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[tfStopPropagationOnClick]',
})
export class StopPropagationOnClickDirective {
  @HostListener('click', ['$event'])
  onClick(event: Event): void {
    event.stopPropagation();
  }

  constructor() {}
}
