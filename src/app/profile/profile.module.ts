import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import * as fromProfile from '../profile/_reducer';
import { ProfileRoutingModule } from './profile-routing.module';
import { ProfileCardModule } from './card/card.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProfileRoutingModule,
    ProfileCardModule,
    StoreModule.forFeature(fromProfile.featureKey, fromProfile.reducer),
    EffectsModule.forFeature([fromProfile.ProfileEffects]),
  ],
  exports: [ProfileCardModule],
})
export class ProfileModule {}
