import { ChangeDetectionStrategy, Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { TeamInvitation, TeamInvitationStatus } from '../invitations.model';
import { TeamInvitationsScope, TeamRequestsScope } from '../invitations.interface';

@Component({
  selector: 'tf-invitation-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InvitationItemComponent implements OnInit {
  @Input() data: TeamInvitation;
  @Input() scope: TeamInvitationsScope | TeamRequestsScope;
  @Output() accepted = new EventEmitter<TeamInvitation>();
  @Output() rejected = new EventEmitter<TeamInvitation>();

  STATUSES = TeamInvitationStatus;

  constructor() {}

  ngOnInit(): void {}

  get profileColumnLabel(): string {
    switch (this.scope) {
      case 'receivedInvitations':
      case 'receivedRequests':
        return 'Od:';
      case 'sentRequests':
      case 'sentInvitations':
        return 'Do:';
    }
  }

  get canDisplayUserColumn(): boolean {
    return this.scope !== 'receivedInvitations' && this.scope !== 'sentRequests';
  }

  get canDisplayActions(): boolean {
    return (
      this.data.status === this.STATUSES.PENDING &&
      (this.scope === 'receivedRequests' || this.scope === 'receivedInvitations')
    );
  }

  onAccept(): void {
    this.accepted.emit(this.data);
  }

  onReject(): void {
    this.rejected.emit(this.data);
  }
}
