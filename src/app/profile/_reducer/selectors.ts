import { AppState } from '../../app.interface';
import { featureKey } from './constants';

export const updatingGameMetadata = (state: AppState) => state[featureKey].updatingGameMetadata;
