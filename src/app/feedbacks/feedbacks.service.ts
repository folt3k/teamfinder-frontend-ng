import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Feedback } from '../_shared/models';
import { BaseListRO, GameType, ListQueryParams } from '../_shared/interfaces';

export const FEEDBACKS_PER_PAGE = 10;
export const LATEST_COMMENT_FEEDBACKS_PER_PAGE = 6;

@Injectable({
  providedIn: 'root',
})
export class FeedbacksService {
  constructor(private http: HttpClient) {}

  fetchAll(params: ListQueryParams & { game?: GameType } = {}): Observable<BaseListRO<Feedback>> {
    return this.http.get<BaseListRO<Feedback>>('/feedbacks', {
      params: {
        perPage: FEEDBACKS_PER_PAGE,
        ...(params as any),
      },
    });
  }

  fetchLatestCommentFeeds(): Observable<BaseListRO<Feedback>> {
    return this.http.get<BaseListRO<Feedback>>('/feedbacks', {
      params: {
        perPage: LATEST_COMMENT_FEEDBACKS_PER_PAGE,
        entityType: 'comment',
      } as any,
    });
  }
}
