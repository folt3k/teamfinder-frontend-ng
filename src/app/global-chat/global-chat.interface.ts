import { UserProfile } from '../_shared/models';
import { BaseListRO, GameType } from '../_shared/interfaces';
import { GlobalChatMessage } from './_models';

export interface GlobalChatGroup {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  channels: GlobalChatChannel[];
  name: string;
}

export interface GlobalChatChannel {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  name: string;
  type: GameType;
}

export interface GlobalChatInitRO {
  groups: GlobalChatGroup[];
  channels: GlobalChatChannel[];
  initChannel: BaseListRO<GlobalChatMessage> & { id: number };
  activeUsers: UserProfile[];
}
