import * as memoize from 'memoizee';

import { environment } from '../../environments/environment';

export const imageUrl = memoize((url: string) => {
  return environment.apiStaticDir + url;
});
