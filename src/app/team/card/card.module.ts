import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { TeamCardComponent } from './card.component';
import { GameRoleIconGroupModule } from '../../games/_components';
import { PipesModule } from '../../_shared/pipes/pipes.module';

@NgModule({
  declarations: [TeamCardComponent],
  imports: [CommonModule, RouterModule, GameRoleIconGroupModule, PipesModule],
  exports: [TeamCardComponent],
})
export class TeamCardModule {}
