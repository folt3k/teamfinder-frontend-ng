import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegistrationRoutingModule } from './registration-routing.module';
import { RegistrationComponent } from './registration.component';
import { SharedModule } from '../_shared/shared.module';
import { BaseStepComponent } from './steps/base/base-step.component';
import { GameAccountStepComponent } from './steps/game-account/game-account-step.component';
import { AdditionalStepComponent } from './steps/additional/additional-step.component';
import { ReviewComponent } from './review/review.component';
import { GamesModule } from '../games/games.module';

@NgModule({
  declarations: [
    RegistrationComponent,
    BaseStepComponent,
    GameAccountStepComponent,
    AdditionalStepComponent,
    ReviewComponent,
  ],
  imports: [CommonModule, RegistrationRoutingModule, SharedModule, GamesModule],
})
export class RegistrationModule {}
