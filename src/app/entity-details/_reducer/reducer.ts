import { createReducer, on, Action } from '@ngrx/store';

import * as actions from './actions';
import * as fromInvitations from '../../invitations/_reducer';
import * as fromAnnouncements from '../../announcements/_reducer';
import * as fromTeams from '../../team/_reducer/';
import * as fromProfiles from '../../profile/_reducer';
import { BaseReducerState, createBaseReducerState, clearError, getErrorState } from '../../_reducers';
import { BaseEntity } from '../../_shared/interfaces';
import { updateAnnouncementSuccess } from '../../announcements/_reducer/';
import { updateUserNetworkStatus } from '../../user/_reducer';

export interface State extends BaseReducerState {
  data: BaseEntity;
}

export const initialState: State = createBaseReducerState({});

export const _reducer = createReducer(
  initialState,
  on(actions.fetchEntityDetails, (state) => ({ ...state, loading: true, error: clearError() })),
  on(actions.fetchEntityDetailsSuccess, (state, payload) => ({
    ...state,
    loading: false,
    data: payload.data,
  })),
  on(actions.fetchEntityDetailsFailed, (state, payload) => ({
    ...state,
    error: getErrorState(payload),
    loading: false,
  })),
  on(updateAnnouncementSuccess, (state, payload) => ({
    ...state,
    data: {
      ...state.data,
      ...payload.data,
    },
  })),
  on(fromAnnouncements.addAnnouncementMemberSuccess, (state, payload) => ({
    ...state,
    data: {
      ...state.data,
      members: [...(state.data.members || []), payload.data],
    },
  })),
  on(fromTeams.removeTeamMemberSuccess, (state, payload) => ({
    ...state,
    data: {
      ...state.data,
      members: (state.data.members || []).filter((m) => m.profile.id !== payload.memberId),
    },
  })),
  on(fromProfiles.updateProfileGameMetadataSuccess, (state, payload) => ({
    ...state,
    data: {
      ...state.data,
      game_metadata: payload.data,
    },
  })),
  on(updateUserNetworkStatus, (state, payload) => ({
    ...state,
    data: {
      ...state.data,
      ...(state.data && state.data.id === payload.profile.id && state.data?.isOnline !== undefined
        ? { isOnline: payload.isOnline }
        : null),
      ...(state.data?.owner?.id === payload.profile.id
        ? { owner: { ...state.data.owner, isOnline: payload.isOnline } }
        : null),
      ...(state.data?.members
        ? {
            members: state.data.members.map((m) => {
              const prefix = m.profile ? 'profile' : 'member';
              return m[prefix].id === payload.profile.id
                ? { ...m, [prefix]: { ...m[prefix], isOnline: payload.isOnline } }
                : m;
            }),
          }
        : null),
    },
  }))
);

export function reducer(state: State, action: Action) {
  return _reducer(state, action);
}
