import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SelectModule } from '@ng-poppy/lib';

import { UserSelectComponent } from './user-select.component';
import { UserAvatarModule } from '../../components';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [UserSelectComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SelectModule,
    PipesModule,
    UserAvatarModule,
  ],
  exports: [UserSelectComponent],
})
export class UserSelectModule {}
