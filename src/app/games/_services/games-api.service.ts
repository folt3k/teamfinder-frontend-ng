import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ListQueryParams } from '../../_shared/interfaces';
import { LolGamesHistoryRO } from '../games.interface';

@Injectable({
  providedIn: 'root',
})
export class GamesApiService {
  constructor(private http: HttpClient) {}

  fetchLolGamesHistory(
    summonerId: string,
    params: ListQueryParams & { summonerServer: string }
  ): Observable<LolGamesHistoryRO> {
    return this.http.get<LolGamesHistoryRO>(`/lol/${summonerId}/game-history`, {
      params: {
        ...params,
        perPage: params.perPage || 10,
      } as any,
    });
  }
}
