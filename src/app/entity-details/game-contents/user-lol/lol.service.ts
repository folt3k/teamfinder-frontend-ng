import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { filter, switchMap, take } from 'rxjs/operators';

import { AppState } from '../../../app.interface';
import { LolGameChampion } from '../../../_shared/models';
import { selectDicts } from '../../../_reducers/dictionaries';

export interface DisplayLolGameChampion extends LolGameChampion {
  name: string;
  imageUrl: string;
}

@Injectable()
export class LolDetailsContentService {
  constructor(private store: Store<AppState>) {}

  getDisplayFavouriteChampions(userChampions: LolGameChampion[] = []): Observable<DisplayLolGameChampion[]> {
    return this.store.select(selectDicts, ['champions', 'gameApiVersion']).pipe(
      filter((results) => !!results.champions.length && !!results.gameApiVersion),
      take(1),
      switchMap(({ champions, gameApiVersion }) => {
        const displayChampions = userChampions.map((champ) => {
          const dictChamp = champions.find((c) => c.id === champ.championId);

          return {
            ...champ,
            name: dictChamp.name,
            imageUrl: `https://ddragon.leagueoflegends.com/cdn/${gameApiVersion.n.champion}/img/champion/${dictChamp.image.full}`,
          };
        });

        return of(displayChampions);
      })
    );
  }
}
