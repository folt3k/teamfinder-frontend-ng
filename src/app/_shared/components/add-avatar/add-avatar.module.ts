import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddAvatarComponent } from './add-avatar.component';
import { AddAvatarDialogComponent } from './dialog/dialog.component';
import { ButtonModule } from '../button/button.module';
import { HeadlineModule } from '../headline/headline.module';
import { ImageCropperModule } from '../image-cropper/image-cropper.module';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [AddAvatarComponent, AddAvatarDialogComponent],
  imports: [CommonModule, ButtonModule, HeadlineModule, ImageCropperModule, PipesModule],
  exports: [AddAvatarComponent],
})
export class AddAvatarModule {}
