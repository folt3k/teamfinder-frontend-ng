import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Actions, ofType } from '@ngrx/effects';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

import { User } from '../../../_shared/models';
import { BaseSelectOption } from '../../../_shared/interfaces';
import { AppState } from '../../../app.interface';
import { selectDicts } from '../../../_reducers/dictionaries';
import { loading, updateUser } from '../../../user/_reducer';
import * as actions from '../../../user/_reducer/actions';

@Component({
  selector: 'tf-my-profile-settings-account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.scss'],
})
export class MyProfileSettingsAccountDetailsComponent implements OnInit {
  @Input() currentUser: User;

  form: FormGroup;
  edit: boolean = false;
  options: { birthYears: BaseSelectOption[] } = { birthYears: [] };
  submitting$: Observable<boolean>;

  constructor(private fb: FormBuilder, private store: Store<AppState>, private actions: Actions) {}

  ngOnInit(): void {
    this.init();
    this.submitting$ = this.store.select(loading);
  }

  onCancelEdit(): void {
    this.disableEdit();
    this.form.setValue({
      age: this.currentUser.age,
    });
  }

  onSubmit(): void {
    this.store.dispatch(updateUser({ body: this.form.value }));

    this.actions.pipe(ofType(actions.updateUserSuccess), take(1)).subscribe((action) => {
      this.form.setValue({
        age: action.data.age,
      });
      this.disableEdit();
    });
  }

  enableEdit(): void {
    this.edit = true;
  }

  disableEdit(): void {
    this.edit = false;
  }

  private init(): void {
    this.form = this.fb.group({
      age: [this.currentUser.age, [Validators.required]],
    });
    this.store
      .select(selectDicts, ['birthYears'])
      .pipe(take(1))
      .subscribe((results) => {
        this.options = {
          birthYears: results.birthYears,
        };
      });
  }
}
