import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MyProfileSettingsComponent } from './settings.component';
import { SharedModule } from '../../_shared/shared.module';
import { MyProfileSettingsAccountDetailsComponent } from './account-details/account-details.component';
import { MyProfileSettingsChangePasswordComponent } from './change-password/change-password.component';
import { MyProfileSettingsMailNotificationsComponent } from './mail-notifications/mail-notifications.component';

@NgModule({
  declarations: [
    MyProfileSettingsComponent,
    MyProfileSettingsAccountDetailsComponent,
    MyProfileSettingsChangePasswordComponent,
    MyProfileSettingsMailNotificationsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: MyProfileSettingsComponent,
      },
    ]),
    SharedModule,
  ],
})
export class MyProfileSettingsModule {}
