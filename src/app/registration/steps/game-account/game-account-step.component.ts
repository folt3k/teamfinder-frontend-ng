import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  ChangeDetectorRef,
  OnDestroy,
} from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Dictionaries } from 'src/app/_shared/models';
import { GameOption } from 'src/app/_shared/common';
import { GameType } from 'src/app/_shared/interfaces';
import { distinctUntilChanged, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { GamesTranslationsService } from '../../../games/_services/games-translations.service';
import { GameTranslations } from '../../../games/_services/games-translations';

@Component({
  selector: 'tf-registration-game-account-step',
  templateUrl: './game-account-step.component.html',
  styleUrls: ['./game-account-step.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GameAccountStepComponent implements OnInit, OnDestroy {
  @Input() form: FormGroup;
  @Input() dictionaries: Partial<Dictionaries>;
  @Input() selectedGame: GameOption;

  canDisplayGameAccountField: boolean = false;
  translations: GameTranslations;

  private destroy = new Subject();

  constructor(private cdr: ChangeDetectorRef, private gameTranslationsService: GamesTranslationsService) {}

  get canDisplayGameServerField(): boolean {
    return this.selectedGame.value === GameType.LOL;
  }

  ngOnInit(): void {
    this.canDisplayGameAccountField =
      this.selectedGame.value === GameType.CS || this.selectedGame.value === GameType.FNT;

    this.gameTranslationsService.get().subscribe((results) => {
      this.translations = results;
    });

    this.form.controls.gameAccountServer.valueChanges
      .pipe(distinctUntilChanged(), takeUntil(this.destroy))
      .subscribe((value) => {
        this.canDisplayGameAccountField = value;
        this.form.controls.gameAccountName.setValue(null);
        this.cdr.detectChanges();
      });
  }

  ngOnDestroy(): void {
    this.destroy.next();
    this.destroy.complete();
  }
}
