import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import * as fromAnnouncements from './_reducer';

import { SharedModule } from '../_shared/shared.module';
import { AnnouncementsRoutingModule } from './announcements-routing.module';
import { AnnouncementCardModule } from './card/card.module';
import { AnnouncementFormComponent } from './form/form.component';
import { AddAnnouncementComponent } from './add/add.component';
import { EditAnnouncementComponent } from './edit/edit.component';

@NgModule({
  declarations: [AnnouncementFormComponent, AddAnnouncementComponent, EditAnnouncementComponent],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(fromAnnouncements.featureKey, fromAnnouncements.reducer),
    EffectsModule.forFeature([fromAnnouncements.AnnouncementsEffects]),
    AnnouncementsRoutingModule,
    AnnouncementCardModule,
  ],
  exports: [AnnouncementCardModule],
})
export class AnnouncementsModule {}
