import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';

import { AppState } from '../../app.interface';
import { PrivateChatRoom } from '../_models/chat-room.model';
import { addRoomWindow, selectRooms, startNewConversation } from '../_reducer';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'tf-chat-rooms-compact-list',
  templateUrl: './rooms-compact-list.component.html',
  styleUrls: ['./rooms-compact-list.component.scss'],
})
export class RoomsCompactListComponent implements OnInit, OnDestroy {
  items: PrivateChatRoom[] = [];
  private destroy$ = new Subject();

  constructor(private store: Store<AppState>, private cdr: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.store
      .select(selectRooms)
      .pipe(takeUntil(this.destroy$))
      .subscribe((results) => {
        this.items = [...results];
        this.cdr.detectChanges();
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  trackByFn(index: number, item: PrivateChatRoom): number {
    return item.id;
  }

  startNewConversation(): void {
    this.store.dispatch(startNewConversation({}));
  }

  onItemClick(item: PrivateChatRoom): void {
    this.store.dispatch(addRoomWindow({ roomId: item.id, chatType: item.chatType }));
  }
}
