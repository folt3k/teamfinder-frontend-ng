import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';

import { AppState } from '../../app.interface';
import * as actions from './actions';
import { FeedbacksService } from '../../feedbacks/feedbacks.service';
import { selectFeedbacks } from './selectors';

@Injectable()
export class CockpitEffects {
  fetchFeedbacks$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.fetchFeedbacks),
      withLatestFrom(this.store.select(selectFeedbacks).pipe(map((f) => f.pagination?.currentPage || 0))),
      switchMap(([_, currentPage]) => {
        return this.feedbacksService.fetchAll({ page: currentPage + 1 }).pipe(
          map((data) => actions.fetchFeedbacksSuccess(data)),
          catchError((err) => of(actions.fetchFeedbacksSuccess(err)))
        );
      })
    )
  );

  fetchLatestCommentFeedbacks$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.fetchLatestCommentFeedbacks),
      switchMap(() => {
        return this.feedbacksService.fetchLatestCommentFeeds().pipe(
          map((data) => actions.fetchLatestCommentFeedbacksSuccess(data)),
          catchError((err) => of(actions.fetchLatestCommentFeedbacksFailed(err)))
        );
      })
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private feedbacksService: FeedbacksService
  ) {}
}
