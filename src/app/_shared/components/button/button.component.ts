import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  ViewEncapsulation,
  Input,
  HostBinding,
} from '@angular/core';

import { ButtonTheme, ButtonSize, ButtonColor } from './button.interface';

@Component({
  selector: 'tf-btn',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
})
export class ButtonComponent implements OnInit {
  @Input() color: ButtonColor = 'primary';
  @Input() theme: ButtonTheme = 'normal';
  @Input() size: ButtonSize = 'medium';
  @Input() type: string = 'button';
  @Input() disabled: boolean = false;
  @Input() submitted: boolean = false;
  @Input() full: boolean = false;
  @Input() icon: string;

  constructor() {}

  @HostBinding('attr.disabled') get hostDisabled(): boolean {
    return this.disabled;
  }

  @HostBinding('class.tf-btn--full') get btnFull(): boolean {
    return this.full;
  }

  @HostBinding('class.tf-btn--submitted') get btnSubmitted(): boolean {
    return this.submitted;
  }

  get panelClasses(): { [key: string]: boolean } {
    return {
      [`tf-btn-${this.color}`]: true,
      [`tf-btn-${this.size}`]: true,
      [`tf-btn-${this.theme}`]: this.theme !== 'normal',
    };
  }

  ngOnInit(): void {}
}
