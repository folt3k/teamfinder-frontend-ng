import { ControlContainer, NG_VALUE_ACCESSOR, ControlValueAccessor, NG_VALIDATORS } from '@angular/forms';
import {
  Component,
  OnInit,
  ChangeDetectorRef,
  forwardRef,
  ChangeDetectionStrategy,
  OnDestroy,
  Input,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { map, filter, withLatestFrom, takeUntil } from 'rxjs/operators';
import { BaseSelectOption } from '../../interfaces';

import { AppState } from 'src/app/app.interface';
import { selectDicts } from 'src/app/_reducers/dictionaries';
import { CustomFormControl } from '../custom-form-control';
import { GameRole } from '../../models';
import { selectCurrentGame } from '../../../games/_reducer';
import { Subject } from 'rxjs';
import { GameOption } from '../../common';

interface DisplayedOption extends BaseSelectOption {
  id: number;
  imagePath: string;
  selected: boolean;
}

@Component({
  selector: 'tf-game-role-picker',
  templateUrl: './game-role-picker.component.html',
  styleUrls: ['./game-role-picker.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => GameRolePickerComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => GameRolePickerComponent),
      multi: true,
    },
  ],
})
export class GameRolePickerComponent extends CustomFormControl
  implements OnInit, OnDestroy, ControlValueAccessor {
  @Input() game: GameOption;

  constructor(private store: Store<AppState>, cdr: ChangeDetectorRef, controlContainer: ControlContainer) {
    super(cdr, controlContainer);
  }

  displayedOptions: DisplayedOption[] = [];
  private destroy$ = new Subject();

  ngOnInit(): void {
    this.updateOptionsOnCurrentGameChange();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  writeValue(values: number[]): void {
    this.displayedOptions = this.displayedOptions.map((opt) => ({
      ...opt,
      selected: !!(values || []).find((item) => item === opt.id),
    }));
    this.cdr.detectChanges();
  }

  registerOnChange(fn: () => {}): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: () => {}): void {
    this.propagateTouched = fn;
  }

  onItemClick(item: DisplayedOption): void {
    this.displayedOptions = this.displayedOptions.map((option) => {
      if (option.value === item.value) {
        return {
          ...option,
          selected: !option.selected,
        };
      }

      return option;
    });
    this.propagateChange(this.populateValue());
  }

  private updateOptionsOnCurrentGameChange(): void {
    this.store
      .select(selectCurrentGame)
      .pipe(
        withLatestFrom(this.store.select(selectDicts, ['roles'])),
        map(([game, dicts]) => dicts.roles[this.game?.key || game?.key]),
        filter((roles) => !!roles?.length),
        takeUntil(this.destroy$)
      )
      .subscribe((roles) => {
        this.setOptions(roles);
        this.cdr.detectChanges();
      });
  }

  private setOptions(roles: GameRole[]): void {
    this.displayedOptions = roles.map((role) => ({
      label: role.label,
      value: role.value,
      imagePath: role.iconUrl,
      selected: false,
      id: role.id,
    }));
  }

  private populateValue(): number[] {
    return this.displayedOptions.filter((opt) => opt.selected).map((opt) => opt.id);
  }

  private propagateChange(value: number[]): void {}

  private propagateTouched(): void {}
}
