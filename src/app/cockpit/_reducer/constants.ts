export const featureKey = 'cockpit';

export const types = {
  FetchFeedbacks: '[Cockpit] Fetch feedbacks',
  FetchFeedbacksSuccess: '[Cockpit] Fetch feedbacks success',
  FetchFeedbacksFailed: '[Cockpit] Fetch feedbacks failed',

  FetchLatestCommentFeedbacks: '[Cockpit] Fetch last comment feedbacks',
  FetchLatestCommentFeedbacksSuccess: '[Cockpit] Fetch last comment feedbacks success',
  FetchLatestCommentFeedbacksFailed: '[Cockpit] Fetch last comment feedbacks failed',

  ResetFeedbacks: '[Cockpit] Reset feedbacks',
};
