import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'tf-range-select',
  templateUrl: './range-select.component.html',
  styleUrls: ['./range-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RangeSelectComponent implements OnInit {
  @Input() parentFormGroup: FormGroup;
  @Input() formControlNames: [string, string];
  @Input() options: any[] = [];
  @Input() bindValue: string;
  @Input() label: string;

  constructor() {}

  ngOnInit(): void {}
}
