import { Pagination } from './pagination.interface';

export interface BaseListRO<T> {
  pagination: Pagination;
  results: T[];
}
