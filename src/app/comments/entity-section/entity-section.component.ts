import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { AppState } from '../../app.interface';
import { canPerformAuthorizedAction } from '../_reducer';

@Component({
  selector: 'tf-entity-comments-section',
  templateUrl: './entity-section.component.html',
  styleUrls: ['./entity-section.component.scss'],
})
export class EntityCommentsSectionComponent implements OnInit {
  canDisplayAddCommentSection$: Observable<boolean>;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    this.canDisplayAddCommentSection$ = this.store.select(canPerformAuthorizedAction);
  }
}
