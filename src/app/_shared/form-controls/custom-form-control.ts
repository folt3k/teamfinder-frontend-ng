import { Optional, Host, SkipSelf, Input, Directive, ChangeDetectorRef, OnInit } from '@angular/core';
import { AbstractControl, ControlContainer, FormControl } from '@angular/forms';

import { isRequired } from '../form-validation';

@Directive()
export abstract class CustomFormControl implements OnInit {
  @Input() formControlName: string;
  @Input() formControl: FormControl;
  @Input() disabled: boolean = false;
  @Input() placeholder: string = '';
  @Input() label: any;

  required: boolean = false;
  errors: { [key: string]: any } | null;
  control: any;

  protected constructor(
    protected cdr: ChangeDetectorRef,
    @Optional()
    @Host()
    @SkipSelf()
    protected controlContainer: ControlContainer
  ) {}

  ngOnInit(): void {
    this.control = this.formControl || this.controlContainer?.control.get(this.formControlName);
    this.required = this.control && isRequired(this.control);
  }

  validate(control: AbstractControl): void {}
}
