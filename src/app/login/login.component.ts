import { Store } from '@ngrx/store';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { STATIC_URLS } from '../_shared/common';
import * as fromAuth from '../auth/_reducer';
import { ErrorState } from '../_reducers';
import { error, loading, clearError, featureKey } from '../auth/_reducer';
import { AppState } from 'src/app/app.interface';
import { DialogService } from '../_shared/poppy/dialog';
import { RemindPasswordDialogComponent } from '../user/remind-password-dialog/remind-password-dialog.component';

@Component({
  selector: 'tf-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  STATIC_URLS = STATIC_URLS;
  AUTH_REDUCER_KEY = featureKey;

  form: FormGroup;
  error$: Observable<ErrorState>;
  submitted$: Observable<boolean>;

  constructor(
    private fb: FormBuilder,
    private store: Store<AppState>,
    private dialogService: DialogService
  ) {}

  ngOnInit(): void {
    this.initForm();
    this.error$ = this.store.select(error);
    this.submitted$ = this.store.select(loading);
  }

  ngOnDestroy(): void {
    this.store.dispatch(clearError());
  }

  initForm(): void {
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  onSubmit(): void {
    this.store.dispatch(fromAuth.login(this.form.value));
  }

  onRemindPassword(): void {
    this.dialogService.open(RemindPasswordDialogComponent);
  }
}
