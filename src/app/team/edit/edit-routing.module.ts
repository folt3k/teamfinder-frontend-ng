import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditTeamComponent } from './edit.component';
import { EditTeamResolver } from './edit.resolver';
import { AuthGuard } from '../../auth/_guards';

const routes: Routes = [
  {
    path: '',
    component: EditTeamComponent,
    canActivate: [AuthGuard],
    resolve: [EditTeamResolver],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditTeamRoutingModule {}
