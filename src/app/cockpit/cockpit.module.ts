import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CockpitComponent } from './cockpit.component';
import { StoreModule } from '@ngrx/store';
import * as fromCockpit from './_reducer';
import { EffectsModule } from '@ngrx/effects';
import { SharedModule } from '../_shared/shared.module';
import { FeedbacksModule } from '../feedbacks/feedbacks.module';

@NgModule({
  declarations: [CockpitComponent],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromCockpit.featureKey, fromCockpit.reducer),
    EffectsModule.forFeature([fromCockpit.CockpitEffects]),
    SharedModule,
    FeedbacksModule
  ],
  exports: [CockpitComponent],
})
export class CockpitModule {}
