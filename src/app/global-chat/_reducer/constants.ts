export const featureKey = 'globalChat';

export const types = {
  Init: '[Global Chat] Init',
  InitSuccess: '[Global Chat] Init success',
  InitFailed: '[Global Chat] Init failed',

  FetchMoreMessages: '[Global Chat] Fetch more messages',
  FetchMoreMessagesSuccess: '[Global Chat] Fetch more messages success',

  SendMessage: '[Global Chat] Send message',

  ReceiveMessage: '[Global Chat] Receive message',

  MarkAsRead: '[Global Chat] Mark as read',

  OpenChat: '[Global Chat] Open chat',
  MaximizeChat: '[Global Chat] Maximize chat',
  MinimizeChat: '[Global Chat] Minimize chat',
  CloseChat: '[Global Chat] Close chat',

  UpdateActiveUsers: '[Global Chat] Update active users',

  ToggleGroup: '[Global Chat] Toggle group',
  SelectChannel: '[Global Chat] Select channel',
};
