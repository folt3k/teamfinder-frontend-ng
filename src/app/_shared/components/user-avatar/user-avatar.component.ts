import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { UserProfile } from '../../models';
import { imageUrl } from '../../../_helpers';
import { currentProfile } from '../../../auth/_reducer';
import { openConversation } from '../../../chat/_reducer';

@Component({
  selector: 'tf-user-avatar',
  templateUrl: './user-avatar.component.html',
  styleUrls: ['./user-avatar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserAvatarComponent implements OnInit {
  @Input() user: UserProfile;
  @Input() size: 'sm' | 'md' | 'lg' | 'xl' = 'md';
  @Input() showCompactInfo: boolean = true;
  @Input() showRank: boolean = false;
  @Input() showSendMessage: boolean = false;
  @Input() showStatus: boolean = true;

  isCurrentUser$: Observable<boolean>;

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.isCurrentUser$ = this.store
      .select(currentProfile)
      .pipe(map((profile) => profile?.id === this.user?.id));
  }

  openConversation(): void {
    this.store.dispatch(openConversation({ receiver: this.user }));
  }

  get panelClasses(): { [key: string]: boolean } {
    return {
      [`user-avatar--${this.size}`]: true,
    };
  }

  get avatarUrl(): string {
    return imageUrl(this.user?.logoUrl);
  }
}
