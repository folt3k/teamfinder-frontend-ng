import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  EntityDetailsDisplayConditions,
  EntityDetailsHelpers,
  EntityType,
} from '../entity-details.interface';
import { EntityDetailsService } from '../entity-details.service';
import { UserProfile } from '../../_shared/models';

import { AppState } from '../../app.interface';
import { currentProfile } from '../../auth/_reducer';
import { selectCommentResults, selectComments } from '../../comments/_reducer';

@Component({
  selector: 'tf-entity-details-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss'],
})
export class EntityDetailsContentComponent implements OnInit {
  @Input() data: any;

  ENTITY_TYPE = EntityType;
  helpers: EntityDetailsHelpers;
  displayConditions: EntityDetailsDisplayConditions;
  currentUser$: Observable<UserProfile>;
  totalComments$: Observable<number>;

  constructor(private service: EntityDetailsService, private store: Store<AppState>) {}

  ngOnInit(): void {
    this.helpers = this.service.helpers();
    this.displayConditions = this.service.displayConditions();
    this.currentUser$ = this.store.select(currentProfile);
    this.totalComments$ = this.store
      .select(selectComments)
      .pipe(map((comments) => comments.pagination?.totalResults));
  }
}
