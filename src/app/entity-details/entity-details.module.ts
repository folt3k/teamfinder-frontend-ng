import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EntityDetailsComponent } from './entity-details.component';
import { SharedModule } from '../_shared/shared.module';
import { GamesModule } from '../games/games.module';
import { StoreModule } from '@ngrx/store';
import * as fromEntityDetails from './_reducer';
import { EffectsModule } from '@ngrx/effects';
import { EntityDetailsHeaderComponent } from './header/header.component';
import { EntityDetailsInfoComponent } from './info/info.component';
import { EntityDetailsContentComponent } from './content/content.component';
import {
  CsDetailsContentModule,
  LolDetailsContentModule,
  FntDetailsContentModule,
  TeamDetailsContentModule,
  AnnouncementDetailsContentModule,
} from './game-contents';
import { CommentsModule } from '../comments/comments.module';
import { NotFoundModule } from '../static-pages';

@NgModule({
  declarations: [
    EntityDetailsComponent,
    EntityDetailsHeaderComponent,
    EntityDetailsInfoComponent,
    EntityDetailsContentComponent,
  ],
  imports: [
    CommonModule,
    StoreModule.forFeature(fromEntityDetails.featureKey, fromEntityDetails.reducer),
    EffectsModule.forFeature([fromEntityDetails.EntityDetailsEffects]),
    SharedModule,
    GamesModule,
    LolDetailsContentModule,
    CsDetailsContentModule,
    FntDetailsContentModule,
    TeamDetailsContentModule,
    AnnouncementDetailsContentModule,
    CommentsModule,
    NotFoundModule,
  ],
  exports: [EntityDetailsComponent],
})
export class EntityDetailsModule {}
