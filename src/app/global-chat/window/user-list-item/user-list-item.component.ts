import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
} from '@angular/core';

import { UserProfile } from './../../../_shared/models';

@Component({
  selector: 'tf-global-chat-user-list-item',
  templateUrl: './user-list-item.component.html',
  styleUrls: ['./user-list-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserListItemComponent implements OnInit {
  @Input() user: UserProfile;

  constructor() {}

  ngOnInit(): void {}
}
