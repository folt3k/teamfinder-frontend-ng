import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { combineLatest, Subject } from 'rxjs';
import { filter, take, takeUntil } from 'rxjs/operators';

import { AppState } from '../../app.interface';
import { TileSelectOption } from '../../_shared/form-controls/tiles-select/tiles-select.component';
import { selectDicts } from '../../_reducers/dictionaries';
import { ageYears } from '../../_shared/common';
import { BaseSelectOption } from '../../_shared/interfaces';
import { selectCurrentGame } from '../../games/_reducer';
import { fieldsDisplayCondition, FiltersFieldsDisplayCondition } from './fields-display-condition';
import { EntitiesScreens, EntitiesScreenService } from '../entities-screen.service';

interface FormFieldOptions {
  roles: TileSelectOption[];
  modes: TileSelectOption[];
  servers: BaseSelectOption[];
  ranks: BaseSelectOption[];
  ageYears: BaseSelectOption[];
}

@Component({
  selector: 'tf-entities-screen-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.scss'],
})
export class FiltersComponent implements OnInit, OnDestroy {
  @Input() form: FormGroup;
  @Output() reset = new EventEmitter<void>();
  @Output() searched = new EventEmitter<void>();

  options: FormFieldOptions = { roles: [], modes: [], ranks: [], servers: [], ageYears: [] };
  fieldsDisplayCondition: FiltersFieldsDisplayCondition;
  private destroy$ = new Subject();

  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<AppState>,
    private service: EntitiesScreenService
  ) {}

  get searchFieldLabelText(): string {
    switch (this.service.getScreenType()) {
      case EntitiesScreens.USERS:
        return 'Nazwa gracza';
      case EntitiesScreens.TEAMS:
        return 'Nazwa drużyny';
      default:
        return '';
    }
  }

  ngOnInit(): void {
    this.initOptions();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onSearch(): void {
    this.searched.emit();
  }

  onReset(): void {
    this.reset.emit();
  }

  private initOptions(): void {
    combineLatest(
      this.store.select(selectDicts, ['roles', 'modes', 'servers', 'ranks']).pipe(take(1)),
      this.store.select(selectCurrentGame)
    )
      .pipe(
        filter(([_, game]) => !!game),
        takeUntil(this.destroy$)
      )
      .subscribe(([dicts, currentGame]) => {
        const gameKey = currentGame.key;

        this.fieldsDisplayCondition = fieldsDisplayCondition(currentGame.value, this.service.getScreenType());
        this.options = {
          roles: dicts.roles[gameKey],
          modes: dicts.modes[gameKey],
          servers: dicts.servers[gameKey],
          ranks: dicts.ranks[gameKey],
          ageYears,
        };
      });
  }
}
