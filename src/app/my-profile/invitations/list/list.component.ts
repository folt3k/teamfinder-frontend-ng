import { Component, Input, OnInit } from '@angular/core';

import { BaseListRO } from '../../../_shared/interfaces';
import { TeamInvitation, TeamInvitationStatus } from '../../../invitations/invitations.model';
import { TeamInvitationsScope, TeamRequestsScope } from '../../../invitations/invitations.interface';
import { Store } from '@ngrx/store';
import { AppState } from '../../../app.interface';
import {
  fetchInvitations,
  fetchRequests,
  manageInvitation,
  manageRequest,
} from '../../../invitations/_reducer';
import { TableActions } from '../../../_shared/components/table/table.interface';

@Component({
  selector: 'tf-my-profile-invitations-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class MyProfileInvitationsListComponent implements OnInit {
  @Input() data: BaseListRO<TeamInvitation>;
  @Input() scope: TeamInvitationsScope | TeamRequestsScope;

  INVITATION_STATUS = TeamInvitationStatus;

  constructor(private store: Store<AppState>) {}

  get columns(): string[] {
    switch (this.scope) {
      case 'sentInvitations':
        return ['Do', 'Drużyna', 'Data wysłania', 'Status'];
      case 'sentRequests':
        return ['Drużyna', 'Data wysłania', 'Status'];
      case 'receivedInvitations':
        return ['Drużyna', 'Data otrzymania', 'Status'];
      case 'receivedRequests':
        return ['Od', 'Drużyna', 'Data otrzymania', 'Status'];
    }
  }

  get canDisplayUserColumn(): boolean {
    return this.scope === 'sentInvitations' || this.scope === 'receivedRequests';
  }

  ngOnInit(): void {}

  getActions(element: TeamInvitation): TableActions<TeamInvitation> {
    switch (this.scope) {
      case 'sentInvitations':
      case 'sentRequests':
        if (element.status !== TeamInvitationStatus.PENDING) {
          return [];
        }
        return [
          {
            label: 'Anuluj',
            color: 'primary',
            callback: (item: TeamInvitation) => {
              if (this.scope === 'sentInvitations') {
                this.store.dispatch(manageInvitation({ id: item.id, action: 'cancel' }));
              } else {
                this.store.dispatch(manageRequest({ id: item.id, action: 'cancel' }));
              }
            },
          },
        ];

      case 'receivedRequests':
      case 'receivedInvitations':
        if (element.status !== TeamInvitationStatus.PENDING) {
          return [];
        }
        return [
          {
            label: 'Akceptuj',
            color: 'primary',
            callback: (item: TeamInvitation) => {
              if (this.scope === 'receivedInvitations') {
                this.store.dispatch(manageInvitation({ id: item.id, action: 'accept' }));
              } else {
                this.store.dispatch(manageRequest({ id: item.id, action: 'accept' }));
              }
            },
          },
          {
            label: 'Odrzuć',
            color: 'danger',
            callback: (item: TeamInvitation) => {
              if (this.scope === 'receivedInvitations') {
                this.store.dispatch(manageInvitation({ id: item.id, action: 'reject' }));
              } else {
                this.store.dispatch(manageRequest({ id: item.id, action: 'reject' }));
              }
            },
          },
        ];
    }
  }

  onPageChange(page: number): void {
    switch (this.scope) {
      case 'sentInvitations':
      case 'receivedInvitations':
        this.store.dispatch(fetchInvitations({ params: { page, scope: this.scope } }));
        break;
      case 'sentRequests':
      case 'receivedRequests':
        this.store.dispatch(fetchRequests({ params: { page, scope: this.scope } }));
        break;
    }
  }
}
