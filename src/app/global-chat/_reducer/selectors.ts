import { createSelector } from '@ngrx/store';

import { AppState } from './../../app.interface';
import { featureKey } from './constants';
import { featureKey as authFeatureKey } from '../../auth/_reducer';

const selectFeature = (state: AppState) => state[featureKey];
const authSelectFeature = (state: AppState) => state[authFeatureKey];

export const loadingMessages = (state: AppState): boolean => state.globalChat.loading;

export const isChatMaximized = (state: AppState): boolean => state.globalChat.isChatMaximized;

export const selectChannels = createSelector(selectFeature, (state) => state.channels);

export const selectGroups = createSelector(selectFeature, (state) => state.groups);

export const selectActiveChannel = createSelector(selectFeature, (state) => {
  const channel = state.channels.find((ch) => ch.active);

  if (channel) {
    return channel;
  }

  const group = state.groups.find((g) => g.channels.find((ch) => ch.active));

  if (group) {
    return group.channels.find((ch) => ch.active);
  }

  return null;
});

export const selectActiveChannelMessages = createSelector(selectActiveChannel, (channel) => {
  if (channel) {
    return channel.messages.results;
  }

  return [];
});

export const canCurrentUserSendChannelMessage = createSelector(
  authSelectFeature,
  selectActiveChannel,
  (auth, channel) => {
    return !auth.profile || !channel?.type || auth.profile.type === channel.type;
  }
);

export const selectNewMessagesCount = createSelector(selectFeature, (state) => {
  let count = 0;

  state.channels.forEach((ch) => {
    count += ch.unseenMessagesSinceLastDisplay;
  });

  state.groups.forEach((group) => {
    group.channels.forEach((ch) => {
      count += ch.unseenMessagesSinceLastDisplay;
    });
  });

  return count;
});

export const isChatInitialized = createSelector(selectFeature, (state) => state.isChatInitialized);
