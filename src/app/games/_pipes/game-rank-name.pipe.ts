import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gameRankName',
})
export class GameRankNamePipe implements PipeTransform {
  transform(value: string): string {
    switch (value) {
      case 'RANKED_FLEX_SR':
        return 'Flex';
      case 'RANKED_SOLO_5x5':
        return 'Solo Q';
      default:
        return value;
    }
  }
}
