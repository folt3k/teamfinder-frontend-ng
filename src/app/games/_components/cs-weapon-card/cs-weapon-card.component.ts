import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

import { CsGameAccountStatsBestWeapon } from '../../games.interface';

@Component({
  selector: 'tf-cs-weapon-card',
  templateUrl: './cs-weapon-card.component.html',
  styleUrls: ['./cs-weapon-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CsWeaponCardComponent implements OnInit {
  @Input() data: CsGameAccountStatsBestWeapon;
  @Input() maxKills: number;

  constructor() {}

  ngOnInit(): void {}
}
