import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { forkJoin } from 'rxjs';
import { filter, switchMap, take } from 'rxjs/operators';
import { Router } from '@angular/router';

import { AppState } from '../../app.interface';
import { TeamService } from '../../team/team.service';
import { Team, UserMemberTeam } from '../../team';
import { currentProfileId } from '../../auth/_reducer';
import { DYNAMIC_URLS, STATIC_URLS } from '../../_shared/common';
import { TableActions } from '../../_shared/components/table/table.interface';
import { leaveTeam, removeTeam } from '../../team/_reducer';
import { getGameRouterPrefixByType } from '../../games';
import { ConfirmationComponent } from '../../_shared/components/confirmation/confirmation.component';
import { DialogService } from '../../_shared/poppy';

@Component({
  selector: 'tf-my-profile-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss'],
})
export class MyProfileTeamsComponent implements OnInit {
  ownedTeams: Team[] = [];
  memberTeams: UserMemberTeam[] = [];
  loading: boolean = false;
  currentUserId: number;
  STATIC_URLS = STATIC_URLS;

  constructor(
    private store: Store<AppState>,
    private teamsService: TeamService,
    private router: Router,
    private dialogService: DialogService
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.store
      .select(currentProfileId)
      .pipe(
        take(1),
        switchMap((profileId) => {
          this.currentUserId = profileId;
          return forkJoin([
            this.teamsService.fetchUserOwnedTeams(profileId),
            this.teamsService.fetchUserMemberTeams(profileId),
          ]);
        })
      )
      .subscribe(([ownedTeams, memberTeams]) => {
        this.ownedTeams = ownedTeams.results;
        this.memberTeams = memberTeams.results;
        this.loading = false;
      });
  }

  getOwnedTeamActions(): TableActions<Team> {
    return [
      {
        label: 'Zarządzaj',
        color: 'primary',
        icon: 'edit',
        callback: (item: Team) => {
          this.router.navigate([
            DYNAMIC_URLS.TEAM_EDIT({
              identifier: item.slug,
              gamePrefix: getGameRouterPrefixByType(item.type),
            }).slashPath,
          ]);
        },
      },
      {
        label: 'Usuń',
        color: 'danger',
        icon: 'delete',
        callback: (item: Team) => {
          const dialogRef = this.dialogService.open(ConfirmationComponent, {
            data: {
              question: 'Czy na pewno chcesz usunąć tę druzynę? Jej przywrócenie nie będzie możliwe.',
            },
          });

          dialogRef.afterClose
            .pipe(
              take(1),
              filter((payload) => !!payload)
            )
            .subscribe(() => {
              this.store.dispatch(removeTeam({ id: item.id }));
              this.ownedTeams = this.ownedTeams.filter((team) => team.id !== item.id);
            });
        },
      },
    ];
  }

  getMemberTeamActions(): TableActions<Team> {
    return [
      {
        label: 'Opuść',
        color: 'primary',
        callback: (item: UserMemberTeam) => {
          this.store.dispatch(leaveTeam({ teamId: item.team.id, profileId: this.currentUserId }));
          this.memberTeams = this.memberTeams.filter((i) => i.id !== item.id);
        },
      },
    ];
  }
}
