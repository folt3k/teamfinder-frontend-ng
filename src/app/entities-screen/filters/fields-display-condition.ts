import * as memoizee from 'memoizee';

import { GameType } from '../../_shared/interfaces';
import { EntitiesScreens } from '../entities-screen.service';

export type FiltersFieldsDisplayCondition = (fieldKey: string) => boolean;

export const fieldsDisplayCondition = memoizee(
  (type: GameType, screen: EntitiesScreens): FiltersFieldsDisplayCondition => {
    let displayFields: string[] = [];

    switch (type) {
      case GameType.LOL:
        displayFields = ['role', 'rank', 'mode'];
        break;
      case GameType.CS:
        displayFields = ['role', 'rank', 'mode'];
        break;
      case GameType.FNT:
        displayFields = [];
        break;
    }

    if (screen === EntitiesScreens.USERS) {
      displayFields = [...displayFields, 'age'];
    }

    if (screen !== EntitiesScreens.GAME_ANNOUNCEMENTS) {
      displayFields = [...displayFields, 'gameServer', 'q'];
    }

    return (fieldKey: string) => displayFields.includes(fieldKey);
  }
);
