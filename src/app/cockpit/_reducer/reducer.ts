import { createReducer, on, Action } from '@ngrx/store';

import * as actions from './actions';
import { BaseReducerState, createBaseReducerState } from '../../_reducers';
import { Pagination } from '../../_shared/interfaces';
import { Feedback } from '../../_shared/models';
import { FEEDBACKS_PER_PAGE } from '../../feedbacks/feedbacks.service';

export interface FeedbacksState extends BaseReducerState {
  results: Feedback[];
  pagination: Pagination;
}

export interface FeedbacksCommentsState extends BaseReducerState {
  results: Feedback[];
}

export interface State {
  feedbacks: FeedbacksState;
  comments: FeedbacksCommentsState;
}

export const initialState: State = {
  feedbacks: createBaseReducerState<FeedbacksState>({
    results: [],
    pagination: null,
  }),
  comments: createBaseReducerState<FeedbacksCommentsState>({
    results: [],
  }),
};

export const _reducer = createReducer(
  initialState,
  on(actions.fetchFeedbacks, (state) => ({
    ...state,
    feedbacks: {
      ...state.feedbacks,
      error: null,
      loading: true,
    },
  })),
  on(actions.fetchFeedbacksSuccess, (state, payload) => ({
    ...state,
    feedbacks: {
      ...state.feedbacks,
      loading: false,
      results: state.feedbacks.pagination
        ? [...state.feedbacks.results, ...payload.results]
        : payload.results,
      pagination: payload.pagination,
    },
  })),
  on(actions.fetchLatestCommentFeedbacks, (state) => ({
    ...state,
    comments: {
      ...state.comments,
      error: null,
      loading: true,
    },
  })),
  on(actions.fetchLatestCommentFeedbacksSuccess, (state, payload) => ({
    ...state,
    comments: {
      ...state.comments,
      loading: false,
      results: payload.results,
    },
  })),
  on(actions.resetFeedbacks, (state) => ({
    ...state,
    feedbacks: {
      ...state.feedbacks,
      results: state.feedbacks.results.slice(0, FEEDBACKS_PER_PAGE),
      pagination: null,
    },
  }))
);

export function reducer(state: State, action: Action) {
  return _reducer(state, action);
}
