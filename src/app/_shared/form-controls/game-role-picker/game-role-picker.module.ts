import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GameRolePickerComponent } from './game-role-picker.component';
import { FormControlModule } from '../form-control/form-control.module';

@NgModule({
  declarations: [GameRolePickerComponent],
  imports: [CommonModule, FormControlModule],
  exports: [GameRolePickerComponent],
})
export class GameRolePickerModule {}
