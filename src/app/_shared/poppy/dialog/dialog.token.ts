import { InjectionToken } from '@angular/core';
import { DialogService } from './dialog.service';

export const DIALOG_CONFIG = new InjectionToken<DialogConfig>('dialog-config');

export interface DialogConfig {
  dialogId: number;
  service: DialogService;
}

export const DIALOG_DATA = new InjectionToken<any>('dialog-data');
