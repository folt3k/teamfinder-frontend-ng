import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MyProfileComponent } from './my-profile.component';
import { MyProfileRoutingModule } from './my-profile-routing.module';
import { SharedModule } from '../_shared/shared.module';
import { ProfileModule } from '../profile/profile.module';

@NgModule({
  declarations: [MyProfileComponent],
  imports: [CommonModule, SharedModule, MyProfileRoutingModule, ProfileModule],
})
export class MyProfileModule {}
