import { Component, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { filter, map, startWith, takeUntil } from 'rxjs/operators';
import { NavigationEnd, Router } from '@angular/router';

import { CurrentUser } from '../_shared/models';
import { AppState } from '../app.interface';
import { currentUser } from '../auth/_reducer';
import { CollapsableNavigationSchema } from '../_shared/components/collapsable-navigation';
import { navigationSchema } from './my-profile-navigation.schema';
import { MY_PROFILE_STATIC_URLS } from '../_shared/common';
import { SeoService } from '../_core/services/seo.service';

@Component({
  selector: 'tf-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.scss'],
})
export class MyProfileComponent implements OnInit, OnDestroy {
  currentUser: CurrentUser;
  navigationSchema: CollapsableNavigationSchema = navigationSchema;
  MY_PROFILE_STATIC_URLS = MY_PROFILE_STATIC_URLS;

  private destroy$ = new Subject();

  constructor(private store: Store<AppState>, private router: Router, private seo: SeoService) {}

  ngOnInit(): void {
    this.store
      .select(currentUser)
      .pipe(takeUntil(this.destroy$))
      .subscribe((profile) => {
        this.currentUser = profile;
      });

    this.router.events
      .pipe(
        filter((e) => e instanceof NavigationEnd),
        map((event: NavigationEnd) => event.url),
        startWith(this.router.url),
        takeUntil(this.destroy$)
      )
      .subscribe((url) => {
        this.setNavigationSchema(url);
      });

    this.seo.setSeo('my-profile');
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onNavigationChanged(schema: CollapsableNavigationSchema): void {
    this.navigationSchema = schema;
  }

  private setNavigationSchema(url: string): void {
    this.navigationSchema = this.navigationSchema.map((group) => {
      if (group.path === url) {
        return { ...group, selected: true, expanded: true };
      }

      if (group.children) {
        const hasMatchedChildPath = !!group.children.find((child) => child.path === url);
        return {
          ...group,
          selected: group.selected ? true : hasMatchedChildPath,
          expanded: group.expanded ? true : hasMatchedChildPath,
          children: group.children.map((child) => {
            return {
              ...child,
              selected: child.path === url,
            };
          }),
        };
      }

      return group;
    });
  }
}
