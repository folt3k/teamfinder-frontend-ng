import { Directive, ElementRef, NgZone, AfterViewInit, OnDestroy } from '@angular/core';
import { fromEvent, merge, interval, Subject } from 'rxjs';
import { filter, take, takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[tfResizeTextarea]',
})
export class ResizeTextareaDirective implements AfterViewInit, OnDestroy {
  private maxHeight: number = 300;
  private destroy$ = new Subject();

  constructor(private host: ElementRef, private ngZone: NgZone) {}

  ngAfterViewInit(): void {
    const element = this.host.nativeElement;

    if (element) {
      this.ngZone.runOutsideAngular(() => {
        merge<Event>(
          fromEvent(element, 'input'),
          fromEvent(element, 'change'),
          fromEvent(element, 'keypress')
        )
          .pipe(takeUntil(this.destroy$))
          .subscribe((event) => {
            if (event.type === 'change' || event.type === 'change') {
              this.clearHeightOnValueReset(element);
            }

            element.style.height = '1px';
            element.style.maxHeight = this.maxHeight + 'px';
            element.style.height = element.scrollHeight + 'px';

            if (element.scrollHeight >= this.maxHeight) {
              element.style.overflow = 'auto';
              element.style.borderTopRightRadius = 0;
              element.style.borderBottomRightRadius = 0;
            } else {
              element.style.overflow = 'hidden';
              element.style.removeProperty('border-top-right-radius');
              element.style.removeProperty('border-bottom-right-radius');
            }
          });
      });
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  clearHeightOnValueReset(element: HTMLInputElement): void {
    interval(1)
      .pipe(
        filter(() => !element.value),
        take(1),
        takeUntil(this.destroy$)
      )
      .subscribe(() => {
        element.style.removeProperty('height');
        element.style.removeProperty('border-top-right-radius');
        element.style.removeProperty('border-bottom-right-radius');
      });
  }
}
