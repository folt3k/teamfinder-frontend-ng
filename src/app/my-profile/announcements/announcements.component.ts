import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { filter, switchMap, take } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

import { Announcement } from '../../_shared/models';
import { AnnouncementsService } from '../../announcements/announcements.service';
import { AppState } from '../../app.interface';
import { currentProfileId } from '../../auth/_reducer';
import { TableActions } from '../../_shared/components/table/table.interface';
import { ConfirmationComponent } from '../../_shared/components/confirmation/confirmation.component';
import { removeAnnouncement } from '../../announcements/_reducer';
import { DialogService } from '../../_shared/poppy';
import { EntityLinkPipe } from '../../_shared/pipes';

@Component({
  selector: 'tf-my-profile-announcements',
  templateUrl: './announcements.component.html',
  styleUrls: ['./announcements.component.scss'],
})
export class MyProfileAnnouncementsComponent implements OnInit {
  announcements: Announcement[] = [];
  loading: boolean = false;

  constructor(
    private announcementsService: AnnouncementsService,
    private store: Store<AppState>,
    private route: ActivatedRoute,
    private router: Router,
    private dialogService: DialogService
  ) {}

  ngOnInit(): void {
    this.loading = true;
    this.store
      .select(currentProfileId)
      .pipe(
        take(1),
        switchMap((profileId) => this.announcementsService.fetchUserOwned(profileId))
      )
      .subscribe((data) => {
        this.announcements = data.results;
        this.loading = false;
      });
  }

  getActions(): TableActions<Announcement> {
    return [
      {
        label: 'Edytuj',
        callback: (item: Announcement) => {
          this.router.navigate([new EntityLinkPipe().transform(item)], { fragment: 'edycja' });
        },
        color: 'primary',
        icon: 'edit',
      },
      {
        label: 'Usuń',
        callback: (item: Announcement) => {
          const dialogRef = this.dialogService.open(ConfirmationComponent, {
            data: { question: 'Czy na pewno chcesz usunąć to ogłoszenie?' },
          });

          dialogRef.afterClose
            .pipe(
              filter((payload) => !!payload),
              take(1)
            )
            .subscribe(() => {
              this.store.dispatch(removeAnnouncement({ id: item.id }));
              this.announcements = this.announcements.filter((a) => a.id !== item.id);
            });
        },
        icon: 'delete',
        color: 'danger',
      },
    ];
  }
}
