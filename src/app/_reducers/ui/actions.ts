import { createAction, props } from '@ngrx/store';

import { types } from './constants';
import { State } from './reducer';

export const updateUI = createAction(types.UpdateUI, props<Partial<State>>());
