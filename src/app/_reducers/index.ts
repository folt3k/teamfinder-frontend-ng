import { ErrorRO } from '../_shared/interfaces';

export interface BaseReducerState {
  loading: boolean;
  error: ErrorState;
}

export interface ErrorState {
  occured: boolean;
  message: string | null;
  statusCode: number | null;
}

export const createBaseReducerState = <T extends BaseReducerState>(state: object): T & BaseReducerState =>
  ({
    ...state,
    loading: false,
    error: {
      occured: false,
    },
  } as T & BaseReducerState);

export const clearError = (): ErrorState => ({ occured: false, message: null, statusCode: null });

export const getErrorState = (error: ErrorRO): ErrorState => ({
  occured: true,
  message: error.message,
  statusCode: error.statusCode,
});
