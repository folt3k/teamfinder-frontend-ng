import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { SharedModule } from '../_shared/shared.module';
import { TeamRoutingModule } from './team-routing.module';
import { TeamCardModule } from './card/card.module';
import * as fromTeam from './_reducer';
import { TeamFormComponent } from './form/form.component';

@NgModule({
  declarations: [TeamFormComponent],
  imports: [
    CommonModule,
    SharedModule,
    StoreModule.forFeature(fromTeam.featureKey, fromTeam.reducer),
    EffectsModule.forFeature([fromTeam.TeamEffects]),
    TeamRoutingModule,
    TeamCardModule,
  ],
  exports: [TeamCardModule, TeamFormComponent],
})
export class TeamModule {}
