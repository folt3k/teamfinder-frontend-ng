import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { map, first } from 'rxjs/operators';

import { isLoggedIn } from '../_reducer';
import { AppState } from '../../app.interface';

@Injectable()
export class NotLoggedInGuard implements CanLoad, CanActivate {
  constructor(private router: Router, private store: Store<AppState>) {}

  canLoad(route: Route): Observable<boolean> {
    return this.isNotLoggedIn();
  }

  canActivate(): Observable<boolean> {
    return this.isNotLoggedIn();
  }

  private isNotLoggedIn(): Observable<boolean> {
    return this.store.select(isLoggedIn).pipe(
      first(),
      map((logged) => {
        if (logged) {
          this.router.navigate(['/']);
          return false;
        }
        return true;
      })
    );
  }
}
