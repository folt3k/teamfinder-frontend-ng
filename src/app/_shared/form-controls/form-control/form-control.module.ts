import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormControlComponent } from './form-control.component';

@NgModule({
  declarations: [FormControlComponent],
  imports: [CommonModule],
  exports: [FormControlComponent],
})
export class FormControlModule {}
