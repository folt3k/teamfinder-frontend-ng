import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddTeamComponent } from './add.component';

const routes: Routes = [
  {
    path: '',
    component: AddTeamComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddTeamRoutingModule {}
