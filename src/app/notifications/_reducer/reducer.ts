import { createReducer, on, Action } from '@ngrx/store';

import * as actions from './actions';
import { BaseReducerState, createBaseReducerState } from '../../_reducers';
import { meSuccess, logout } from '../../auth/_reducer/actions';
import { Notification } from '../notification.model';
import { Pagination } from '../../_shared/interfaces';

export interface State extends BaseReducerState {
  unreadCount: number;
  undisplayedCount: number;
  results: Notification[];
  pagination: Pagination;
}

export const initialState: State = createBaseReducerState({
  unreadCount: 0,
  undisplayedCount: 0,
  results: [],
  pagination: null,
});

export const _reducer = createReducer(
  initialState,
  on(meSuccess, (state, payload) => ({
    ...state,
    undisplayedCount: payload.metadata.counters.undisplayedNotifications,
  })),
  on(logout, () => initialState),
  on(actions.fetchAll, (state) => ({
    ...state,
    loading: true,
  })),
  on(actions.fetchAllSuccess, (state, payload) => ({
    ...state,
    loading: false,
    results: [...state.results, ...payload.results],
    pagination: payload.pagination,
  })),
  on(actions.markAsRead, (state, { id }) => ({
    ...state,
    unreadCount: state.unreadCount - 1,
    results: state.results.map((i) => (i.id === id ? { ...i, wasRead: true } : i)),
  })),
  on(actions.markAllAsRead, (state) => ({
    ...state,
    unreadCount: 0,
    results: state.results.map((i) => ({ ...i, wasRead: true })),
  })),
  on(actions.markAllAsDisplayed, (state) => ({
    ...state,
    undisplayedCount: 0,
  })),
  on(actions.receiveNewNotification, (state, { data }) => ({
    ...state,
    undisplayedCount: state.undisplayedCount + 1,
    pagination: {
      ...state.pagination,
      totalResults: state.pagination.totalResults + 1,
    },
    results: [data, ...state.results],
  }))
);

export function reducer(state: State, action: Action) {
  return _reducer(state, action);
}
