import { createAction, props } from '@ngrx/store';

import { types } from './constants';
import { ErrorRO } from '../../_shared/interfaces';
import { UpdateProfileDto } from '../profile.interface';
import { UserProfileMetadata } from '../../_shared/models';

export const updateProfile = createAction(types.Update, props<{ body: UpdateProfileDto }>());
export const updateProfileSuccess = createAction(types.UpdateSuccess);
export const updateProfileFailed = createAction(types.UpdateFailed, props<ErrorRO>());

export const updateProfileGameMetadata = createAction(types.UpdateGameMetadata);
export const updateProfileGameMetadataSuccess = createAction(
  types.UpdateGameMetadataSuccess,
  props<{ data: UserProfileMetadata }>()
);
export const updateProfileGameMetadataFailed = createAction(types.UpdateGameMetadataFailed, props<ErrorRO>());
