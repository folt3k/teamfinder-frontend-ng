import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, mergeMap, switchMap, take, tap, withLatestFrom } from 'rxjs/operators';

import { AppState } from '../../app.interface';
import {
  activateAccount,
  activateAccountFailed,
  activateAccountSuccess,
  init,
  login,
  loginFailed,
  loginSuccess,
  logout,
  logoutSuccess,
  me,
  meFailed,
  meSuccess,
} from './actions';
import { AuthService } from '../auth.service';
import { STATIC_URLS } from 'src/app/_shared/common';
import { LocalStorageService } from 'src/app/_core/services/local-storage.service';
import { UserSocketService } from '../../user/user-ws.service';
import { selectRouteParam } from '../../_reducers/router';
import { AlertService } from '../../_shared/components/alert/alert.service';
import { AlertTheme } from '../../_shared/components/alert/alert.interface';
import { currentProfileId } from './selectors';
import * as fromNotifications from '../../notifications/_reducer';
import * as fromChat from '../../chat/_reducer';
import * as fromGlobalChat from '../../global-chat/_reducer';
import * as fromGames from '../../games/_reducer';
import { selectUI, updateUI } from '../../_reducers/ui';
import { getGameByType } from '../../games';

@Injectable()
export class AuthEffects {
  init$ = createEffect(() =>
    this.actions$.pipe(
      ofType(init),
      map(() => me())
    )
  );

  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(login),
      switchMap((action) =>
        this.authService.login(action).pipe(
          map((data) => loginSuccess(data)),
          catchError((err) => of(loginFailed(err)))
        )
      )
    )
  );

  loginSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loginSuccess),
      mergeMap((action) => {
        this.authService.setTokenInLocalStorage(action.token);
        return [me(), fromGlobalChat.init()];
      })
    )
  );

  $logout = createEffect(() =>
    this.actions$.pipe(
      ofType(logout),
      withLatestFrom(this.store.select(currentProfileId)),
      tap(([action, profileId]) => {
        this.localStorage.set('token', null);
        this.userSocketService.emitUserOffline({ profileId });
        this.router.navigate(['/']);
        this.store.dispatch(updateUI({ isMainContainerFull: true, canDisplayGameSelect: true }));
      }),
      switchMap(() => {
        return this.authService.logout().pipe(map(() => logoutSuccess()));
      })
    )
  );

  me$ = createEffect(() =>
    this.actions$.pipe(
      ofType(me),
      switchMap(() =>
        this.authService.me().pipe(
          map((data) => meSuccess(data)),
          catchError((err) => of(meFailed(err)))
        )
      )
    )
  );

  meSuccess$ = createEffect(() =>
    this.actions$.pipe(
      ofType(meSuccess),
      withLatestFrom(
        this.store.select(selectRouteParam('gamePrefix')),
        this.store.select(selectUI, 'redirectPath')
      ),
      tap(([data, routerGamePrefix, redirectPath]) => {
        if (redirectPath) {
          this.router.navigate([redirectPath]);
          this.store.dispatch(updateUI({ redirectPath: null }));
        } else if (this.router.url === STATIC_URLS['login'].slashPath) {
          this.router.navigate(['/']);
        }
        if (!routerGamePrefix) {
          this.store.dispatch(fromGames.changeCurrentGame({ gameId: data.currentProfile.type }));
        }
        this.userSocketService.emitUserOnline(data.currentProfile);
      }),
      mergeMap(() => [
        fromNotifications.fetchAll({}),
        fromChat.fetchPrivateRooms(),
        fromChat.fetchTeamRooms(),
      ])
    )
  );

  activateAccount$ = createEffect(() =>
    this.actions$.pipe(
      ofType(activateAccount),
      withLatestFrom(this.store.select(selectRouteParam('token'))),
      switchMap(([action, routeToken]) =>
        this.authService.activateAccount({ token: routeToken }).pipe(
          map((data) => activateAccountSuccess(data)),
          catchError((err) => of(activateAccountFailed(err)))
        )
      )
    )
  );

  activateAccountSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(activateAccountSuccess),
        map((action) => {
          this.authService.setTokenInLocalStorage(action.token);
          this.store.dispatch(me());
          return action;
        }),
        switchMap((action) =>
          this.actions$.pipe(
            ofType(meSuccess),
            take(1),
            map((data) => {
              if (action.redirectUrl) {
                this.router.navigate([action.redirectUrl]);
                if (window.history.afterRedirectCb) {
                  window.history.afterRedirectCb();
                }
              } else {
                this.router.navigate(['/']);
              }
              this.store.dispatch(fromGlobalChat.init());
              this.alertService.show('Twoje konto zostało pomyślnie aktywowane');
            })
          )
        )
      ),
    { dispatch: false }
  );

  activateAccountFailed$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(activateAccountFailed),
        tap((err) => {
          this.alertService.show(err.message, { theme: AlertTheme.FAILED });
          this.router.navigate(['/logowanie']);
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private authService: AuthService,
    private router: Router,
    private localStorage: LocalStorageService,
    private userSocketService: UserSocketService,
    private alertService: AlertService
  ) {}
}
