import { Observable, Subject } from 'rxjs';
import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  AfterViewInit,
  ChangeDetectorRef,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { debounceTime, filter, first, takeUntil } from 'rxjs/operators';

import { AppState } from 'src/app/app.interface';
import {
  sendMessage,
  closeChat,
  isChatMaximized,
  maximizeChat,
  minimizeChat,
  canCurrentUserSendChannelMessage,
  GlobalChatStateChannel,
  selectActiveChannel,
  selectNewMessagesCount,
} from '../_reducer';
import { isMobile } from '../../_helpers';
import { isLoggedIn } from '../../auth/_reducer';

@Component({
  selector: 'tf-global-chat-window',
  templateUrl: './window.component.html',
  styleUrls: ['./window.component.scss'],
})
export class WindowComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('wrapperEl') wrapperEl: ElementRef;

  activeUsersCount$: Observable<number>;
  unseenMessagesCount$: Observable<number>;
  isLoggedIn$: Observable<boolean>;
  canCurrentUserSendChannelMessage$: Observable<boolean>;
  activeChannel$: Observable<GlobalChatStateChannel>;
  isChatMaximized: boolean;
  wasFirstOpen: boolean = false;

  private destroy$ = new Subject();

  constructor(private store: Store<AppState>, private cdr: ChangeDetectorRef) {
    this.activeUsersCount$ = this.store.select((state) => state.globalChat.activeUsers.length);
    this.isLoggedIn$ = this.store.select(isLoggedIn);
    this.canCurrentUserSendChannelMessage$ = this.store.select(canCurrentUserSendChannelMessage);
    this.activeChannel$ = this.store.select(selectActiveChannel);
    this.unseenMessagesCount$ = this.store.select(selectNewMessagesCount);
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.listenOnOpenWindowFirstTime();
    this.listenOnOpenWindow();
    this.listenOnMaximizeWindowChange();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
  }

  onSubmitMessage(value: string): void {
    this.store.dispatch(sendMessage({ content: value }));
  }

  onClose(): void {
    this.store.dispatch(closeChat());
  }

  onMaximize(): void {
    if (this.isChatMaximized) {
      this.store.dispatch(minimizeChat());
    } else {
      this.store.dispatch(maximizeChat());
    }
  }

  private listenOnOpenWindowFirstTime(): void {
    this.store
      .select((state) => state.globalChat.isChatOpen)
      .pipe(
        filter((isOpen) => !!isOpen),
        debounceTime(0),
        first()
      )
      .subscribe(() => {
        this.wasFirstOpen = true;
        this.cdr.detectChanges();
      });
  }

  private listenOnMaximizeWindowChange(): void {
    this.store
      .select(isChatMaximized)
      .pipe(takeUntil(this.destroy$))
      .subscribe((maximized) => {
        this.onMaximizeChange(maximized);
      });
  }

  private listenOnOpenWindow(): void {
    this.store
      .select((state) => state.globalChat.isChatOpen)
      .pipe(debounceTime(0), takeUntil(this.destroy$))
      .subscribe((isOpen) => {
        if (isMobile()) {
          if (isOpen) {
            document.body.classList.add('block-scroll');
          } else {
            document.body.classList.remove('block-scroll');
          }
        }
      });
  }

  private onMaximizeChange(maximized: boolean): void {
    if (maximized) {
      const headerEl = document.getElementById('header');
      const headerHeight = headerEl ? headerEl.getBoundingClientRect().height : 0;
      this.wrapperEl.nativeElement.style.height = `calc(100vh - ${headerHeight}px)`;
      this.isChatMaximized = true;
    } else {
      this.wrapperEl.nativeElement.style.height = `500px`;
      this.isChatMaximized = false;
    }
  }
}
