import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { EmojiPickerModule } from './../../components';
import { SendMessageComponent } from './send-message.component';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [SendMessageComponent],
  imports: [CommonModule, ReactiveFormsModule, DirectivesModule, EmojiPickerModule],
  exports: [SendMessageComponent],
})
export class SendMessageModule {}
