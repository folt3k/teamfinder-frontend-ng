import { BaseSelectOption } from '../_shared/interfaces';
import { UserMailNotificationSettings } from '../_shared/models';

export interface RegistrationDataAvailability {
  isUsernameAvailable: boolean;
  isEmailAvailable: boolean;
  isSummonerExists: boolean;
  isSummonerAvailable: boolean;
  isGameAccountExists: boolean;
  isSteamAccountPublic: boolean;
}

export interface RegistrationDataAvailabilityRO extends Partial<RegistrationDataAvailability> {}

export interface RegistrationDataAvailabilityParams {
  [key: string]: string;
  type: 'username' | 'email' | 'gameAccountName';
  value: string;
  gameType: string;
  gameAccountServer?: string;
}

export interface CreateUserDto {
  username: string;
  email: string;
  password: string;
  terms: boolean;
  age: BaseSelectOption;
  gameAccountName: string;
  modes: BaseSelectOption[];
  gameAccountServer?: BaseSelectOption;
  desc?: string;
}

export interface UpdateUserDto {
  age: number;
}

export interface ChangePasswordDto {
  currentPassword: string;
  newPassword: string;
  repeatedPassword: string;
}

export type UpdateMailNotificationSettingsDto = UserMailNotificationSettings;

export interface RemindPasswordDto {
  email: string;
}

export interface UnsubscribeFromMailNotificationsDto {
  token: string;
}

export interface RecoverPasswordDto {
  password: string;
  passwordRepeat: string;
  token: string;
}
