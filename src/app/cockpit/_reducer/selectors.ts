import { createSelector } from '@ngrx/store';

import { AppState } from '../../app.interface';
import { featureKey } from './constants';
import { State } from './reducer';

const selectFeature = (state: AppState) => state[featureKey];

export const selectFeedbacks = createSelector(selectFeature, (state: State) => state.feedbacks);
export const selectLatestCommentFeedbacks = createSelector(selectFeature, (state: State) => state.comments);
