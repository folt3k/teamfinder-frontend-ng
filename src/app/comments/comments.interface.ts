import { EntityType } from '../_shared/interfaces';

export interface CreateCommentDto {
  content: string;
  entityId: number;
  entityType: EntityType;
}

export interface UpdateCommentDto {
  content: string;
  entityId: number;
}

export interface CreateSubcommentDto {
  content: string;
  commentId: number;
}

export interface UpdateSubcommentDto {
  content: string;
  commentId: number;
  subcommentId: number;
}
