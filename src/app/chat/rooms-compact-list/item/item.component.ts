import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { PrivateChatRoom, TeamChatRoom } from '../../_models/chat-room.model';
import { ChatMessage } from '../../_models/chat-message.model';
import { ChatType } from '../../chat.inerface';
import { UserProfile } from '../../../_shared/models';

@Component({
  selector: 'tf-chat-rooms-compact-list-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
})
export class RoomsCompactListItemComponent implements OnInit {
  @Input() item: PrivateChatRoom | TeamChatRoom;
  @Output() clicked = new EventEmitter<void>();

  constructor() {}

  get panelClasses(): { [key: string]: boolean } {
    const unreadCount =
      this.item.chatType === ChatType.TEAM
        ? this.item.unreadMessages
        : this.item.getCurrentProfileUnreadCount();
    return {
      'rooms-compact-list-item--active': unreadCount > 0,
    };
  }

  get lastMessage(): ChatMessage {
    const messages = this.item.messages.results;

    return messages[messages.length - 1];
  }

  get isPrivateChat(): boolean {
    return this.item.chatType === ChatType.PRIVATE;
  }

  get isTeamChat(): boolean {
    return this.item.chatType === ChatType.TEAM;
  }

  get title(): string {
    switch (this.item.chatType) {
      case ChatType.PRIVATE:
        return this.item.memberProfile.username;
      case ChatType.TEAM:
        return `Czat drużynowy - ${(this.item as TeamChatRoom).team.name}`;
    }
  }

  ngOnInit(): void {}

  onClick(): void {
    this.clicked.emit();
  }
}
