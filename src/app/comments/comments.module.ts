import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { AddCommentComponent } from './forms/add/add.component';
import { SharedModule } from '../_shared/shared.module';
import { EntityCommentsSectionComponent } from './entity-section/entity-section.component';
import { StoreModule } from '@ngrx/store';
import * as fromComments from './_reducer';
import { EffectsModule } from '@ngrx/effects';
import { CommentsListComponent } from './list/list.component';
import { CommentsItemComponent } from './item/item.component';
import { EditCommentComponent } from './forms/edit/edit.component';

@NgModule({
  declarations: [
    EntityCommentsSectionComponent,
    AddCommentComponent,
    EditCommentComponent,
    CommentsListComponent,
    CommentsItemComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    StoreModule.forFeature(fromComments.featureKey, fromComments.reducer),
    EffectsModule.forFeature([fromComments.CommentsEffects]),
  ],
  exports: [EntityCommentsSectionComponent],
})
export class CommentsModule {}
