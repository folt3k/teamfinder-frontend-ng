import { Store } from '@ngrx/store';
import {
  Component,
  OnInit,
  ViewEncapsulation,
  ChangeDetectionStrategy,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { Observable } from 'rxjs';
import { filter, first } from 'rxjs/operators';

import { AppState } from './../app.interface';
import * as globalChatActions from './_reducer/actions';
import { isChatInitialized, selectNewMessagesCount } from './_reducer';
import { isAuthorizationCompleted } from '../auth/_reducer';

@Component({
  selector: 'tf-global-chat',
  templateUrl: './global-chat.component.html',
  styleUrls: ['./global-chat.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GlobalChatComponent implements OnInit {
  showChat$: Observable<boolean>;
  activeUsersCount$: Observable<number>;
  unseenMessagesCount$: Observable<number>;
  isChatInitialized$: Observable<boolean>;

  constructor(private store: Store<AppState>, @Inject(PLATFORM_ID) private platformId: object) {
    this.showChat$ = this.store.select((state) => state.globalChat.isChatOpen);
    this.activeUsersCount$ = this.store.select((state) => state.globalChat.activeUsers.length);
    this.unseenMessagesCount$ = this.store.select(selectNewMessagesCount);
    this.isChatInitialized$ = this.store.select(isChatInitialized);
  }

  ngOnInit(): void {
    if (isPlatformBrowser(this.platformId)) {
      this.init();
    }
  }

  openChat(): void {
    this.store.dispatch(globalChatActions.openChat());
  }

  private init(): void {
    this.store
      .select(isAuthorizationCompleted)
      .pipe(filter(Boolean), first())
      .subscribe(() => {
        this.store.dispatch(globalChatActions.init());
      });
  }
}
