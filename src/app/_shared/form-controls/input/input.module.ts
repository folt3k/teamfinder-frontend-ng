import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InputComponent } from './input.component';
import { FormControlModule } from '../form-control/form-control.module';
import { LoaderModule } from '../../components';
import { DirectivesModule } from '../../directives/directives.module';

@NgModule({
  declarations: [InputComponent],
  imports: [CommonModule, FormControlModule, LoaderModule, DirectivesModule],
  exports: [InputComponent],
})
export class InputModule {}
