import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_INITIALIZER, ErrorHandler, Inject, NgModule, PLATFORM_ID } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Router, RouterModule } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreRouterConnectingModule, routerReducer } from '@ngrx/router-store';
import { ReactiveFormsModule } from '@angular/forms';
import * as Sentry from '@sentry/angular';
import { isPlatformBrowser } from '@angular/common';

import { environment } from '../environments/environment';
import { AlertModule } from './_shared/components/alert/alert.module';
import { AppComponent } from './app.component';
import { GlobalChatModule } from './global-chat/global-chat.module';
import {
  ApiEndpointInterceptor,
  GameTypeInterceptor,
  ApiErrorInterceptor,
  HttpServerCacheInterceptor,
} from './_core/interceptors';
import { AuthInterceptor, AuthService } from './auth';
import { CoreModule } from './_core/core.module';
import { AppRoutingModule } from './app-routing.module';
import { AuthEffects } from './auth/_reducer';
import {
  LocalStorageService,
  ScrollPositionRestorationService,
  WindowFocusDetectorService,
  SocketListenerService,
  SentryErrorHandler,
} from './_core';
import { SharedModule } from './_shared/shared.module';
import { UserModule } from './user/user.module';
import { NotificationsModule } from './notifications/notifications.module';
import { ChatModule } from './chat/chat.module';

import * as fromUI from './_reducers/ui';
import * as fromDicts from './_reducers/dictionaries';
import * as fromAuth from './auth/_reducer';
import * as fromRouter from './_reducers/router';
import { AppInitService } from './app-init.service';
import { TeamInvitationsModule } from './invitations/invitations.module';
import { NotFoundModule } from './static-pages';
import { Dictionaries } from './_shared/models';
import { EmojiService } from './_shared/components/emoji-picker/emoji.service';

export function initApp(initService: AppInitService): () => Promise<Partial<Dictionaries>> {
  return () => initService.init();
}

const INTERCEPTORS = [
  ApiEndpointInterceptor,
  ApiErrorInterceptor,
  AuthInterceptor,
  GameTypeInterceptor,
  HttpServerCacheInterceptor,
];

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    // TransferHttpCacheModule,
    BrowserTransferStateModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot([], { relativeLinkResolution: 'legacy' }),
    ReactiveFormsModule,
    StoreModule.forRoot(
      {
        router: routerReducer,
        [fromDicts.featureKey]: fromDicts.reducer,
        [fromUI.featureKey]: fromUI.reducer,
        [fromAuth.featureKey]: fromAuth._reducer,
      },
      {
        runtimeChecks: {
          strictStateImmutability: true,
          strictActionImmutability: true,
          strictStateSerializability: true,
          strictActionSerializability: true,
        },
      }
    ),
    EffectsModule.forRoot([fromRouter.RouterEffects, AuthEffects, fromUI.UiEffects]),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
    StoreRouterConnectingModule.forRoot(),
    SharedModule,
    CoreModule,
    GlobalChatModule,
    ChatModule,
    UserModule,
    NotificationsModule,
    AlertModule,
    TeamInvitationsModule,
    NotFoundModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initApp,
      deps: [AppInitService, Sentry.TraceService],
      multi: true,
    },
    ...INTERCEPTORS.map((interceptor) => ({
      provide: HTTP_INTERCEPTORS,
      useClass: interceptor,
      multi: true,
    })),
    {
      provide: ErrorHandler,
      useClass: SentryErrorHandler,
    },
    {
      provide: Sentry.TraceService,
      deps: [Router],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(
    @Inject(PLATFORM_ID) private platformId: object,
    private socketListener: SocketListenerService,
    private localStorage: LocalStorageService,
    private authService: AuthService,
    private scrollPositionRestorationService: ScrollPositionRestorationService,
    private windowFocusDetector: WindowFocusDetectorService,
    private emojiService: EmojiService
  ) {
    this.socketListener.init();
    this.localStorage.init();
    this.authService.init();
    this.scrollPositionRestorationService.init();
    this.emojiService.inti();

    if (isPlatformBrowser(this.platformId)) {
      this.windowFocusDetector.init();
    }
  }
}
