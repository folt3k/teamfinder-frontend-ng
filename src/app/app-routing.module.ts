import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; // CLI imports router

import { AuthGuard, AuthModule, NotLoggedInGuard } from './auth';
import { DYNAMIC_URLS, STATIC_URLS } from './_shared/common';
import { MyProfileResolver } from './my-profile/myprofile.resolver';
import { NotFoundModule } from './static-pages';
import { NotFoundComponent } from './static-pages/not-found/not-found.component';

const routes: Routes = [
  {
    path: STATIC_URLS['login'].path,
    loadChildren: () => import('./login/login.module').then((m) => m.LoginModule),
    canLoad: [NotLoggedInGuard],
    canActivate: [NotLoggedInGuard],
  },
  {
    path: STATIC_URLS['sign-up'].path,
    loadChildren: () => import('./registration/registration.module').then((m) => m.RegistrationModule),
    canLoad: [NotLoggedInGuard],
    canActivate: [NotLoggedInGuard],
  },
  {
    path: DYNAMIC_URLS.ACCOUNT_ACTIVATION().symbolPath,
    loadChildren: () =>
      import('./auth/_pages/account-activation/account-activation.module').then(
        (m) => m.AccountActivationModule
      ),
  },
  {
    path: STATIC_URLS['add-team'].path,
    loadChildren: () => import('./team/add/add.module').then((m) => m.AddTeamModule),
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
  },
  {
    path: STATIC_URLS['my-profile'].path,
    loadChildren: () => import('./my-profile/my-profile.module').then((m) => m.MyProfileModule),
    canLoad: [AuthGuard],
    canActivate: [AuthGuard],
    resolve: [MyProfileResolver],
  },
  {
    path: STATIC_URLS['policy'].path,
    loadChildren: () => import('./static-pages/terms/terms.module').then((m) => m.TermsModule),
  },
  {
    path: STATIC_URLS['contact'].path,
    loadChildren: () => import('./static-pages/contact/contact.module').then((m) => m.ContactModule),
  },
  {
    path: DYNAMIC_URLS.UNSUBSCRIBE_FROM_MAIL_NOTIFICATIONS().symbolPath,
    loadChildren: () =>
      import(
        './static-pages/unsubcribe-from-mail-notifications/unsubcribe-from-mail-notifications.module'
      ).then((m) => m.UnsubcribeFromMailNotificationsModule),
  },
  {
    path: DYNAMIC_URLS.CHANGE_PASSWORD().symbolPath,
    loadChildren: () =>
      import('./user/_pages/change-password/change-password.module').then((m) => m.ChangePasswordModule),
  },
  {
    path: '',
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
  },
  {
    path: ':gamePrefix',
    loadChildren: () => import('./profile/profile.module').then((m) => m.ProfileModule),
  },
  {
    path: ':gamePrefix',
    loadChildren: () => import('./team/team.module').then((m) => m.TeamModule),
  },
  {
    path: ':gamePrefix',
    loadChildren: () => import('./announcements/announcements.module').then((m) => m.AnnouncementsModule),
  },
  {
    path: '**',
    component: NotFoundComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
    initialNavigation: 'enabled',
    relativeLinkResolution: 'legacy'
}),
    AuthModule,
    NotFoundModule,
  ],
  exports: [RouterModule],
  providers: [MyProfileResolver],
})
export class AppRoutingModule {}
