import { updateUserNetworkStatus } from './_reducer/actions';
import { Injectable } from '@angular/core';

import { SocketService } from './../_core/services/socket.service';
import { UserProfile } from '../_shared/models';
import { Store } from '@ngrx/store';
import { AppState } from '../app.interface';
import { WithListeners } from '../_core/services';

export enum UserSocketEvents {
  EmitUserOnline = 'EMIT_USER_ONLINE',
  EmitUserOffline = 'EMIT_USER_OFFLINE',
  NewUserOnline = 'BROADCAST_ONLINE_USER',
  NewUserOffline = 'BROADCAST_OFFLINE_USER',
}

@Injectable({
  providedIn: 'root',
})
export class UserSocketService implements WithListeners {
  constructor(private socket: SocketService, private store: Store<AppState>) {}

  emitUserOnline(body: UserProfile): void {
    this.socket.emit(UserSocketEvents.EmitUserOnline, body);
  }

  emitUserOffline(body: { profileId: number }): void {
    this.socket.emit(UserSocketEvents.EmitUserOffline, body.profileId);
  }

  // tslint:disable-next-line:no-any
  listeners(): { [p: string]: (data: any) => void } {
    return {
      [UserSocketEvents.NewUserOnline]: (data: UserProfile) => {
        this.store.dispatch(updateUserNetworkStatus({ profile: data, isOnline: true }));
      },
      [UserSocketEvents.NewUserOffline]: (data: number) => {
        this.store.dispatch(updateUserNetworkStatus({ profile: { id: data }, isOnline: false }));
      },
    };
  }
}
