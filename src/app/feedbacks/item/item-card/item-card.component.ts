import { ChangeDetectionStrategy, Component } from '@angular/core';

import { FeedbackItemComponent } from '../item.component';
import { FeedbackAction } from '../../../_shared/models';

@Component({
  selector: 'tf-feedback-card-item',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FeedbackCardItemComponent extends FeedbackItemComponent {
  get isCommentAction(): boolean {
    return (
      this.data.action === FeedbackAction.TEAM_COMMENTED ||
      this.data.action === FeedbackAction.GAME_ANNOUNCEMENT_COMMENTED
    );
  }
}
