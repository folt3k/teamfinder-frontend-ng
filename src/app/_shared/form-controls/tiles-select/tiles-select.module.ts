import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TilesSelectComponent } from './tiles-select.component';
import { FormControlModule } from '../form-control/form-control.module';

@NgModule({
  declarations: [TilesSelectComponent],
  imports: [CommonModule, FormControlModule],
  exports: [TilesSelectComponent],
})
export class TilesSelectModule {}
