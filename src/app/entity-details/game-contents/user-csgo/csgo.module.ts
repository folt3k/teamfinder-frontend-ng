import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CsDetailsContentComponent } from './csgo.component';
import { CsDetailsContentService } from './csgo.service';
import { SharedModule } from '../../../_shared/shared.module';
import { GamesModule } from '../../../games/games.module';

@NgModule({
  declarations: [CsDetailsContentComponent],
  imports: [CommonModule, SharedModule, GamesModule],
  exports: [CsDetailsContentComponent],
  providers: [CsDetailsContentService],
})
export class CsDetailsContentModule {}
