import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { EntityDetailsActionsCreator, IEntityDetailsActionsCreator } from '../actions-creator';
import { filter, map, take } from 'rxjs/operators';
import { EDIT_ANNOUNCEMENT_DIALOG_UNIQUE_ID } from '../../../announcements';
import { addAnnouncementMember, removeAnnouncement } from '../../../announcements/_reducer';
import { EditAnnouncementComponent } from '../../../announcements/edit/edit.component';
import { ConfirmationComponent } from '../../../_shared/components/confirmation/confirmation.component';
import { EntityDetailsAction } from '../../entity-details.interface';
import { Announcement, UserProfile } from '../../../_shared/models';
import { openConversation } from '../../../chat/_reducer';

@Injectable({
  providedIn: 'root',
})
export class EntityDetailsAnnouncementActionsCreator extends EntityDetailsActionsCreator
  implements IEntityDetailsActionsCreator {
  getActions$(): Observable<EntityDetailsAction[]> {
    return this.data$.pipe(
      filter(([currentUser]) => !!currentUser),
      map(([currentUser, data]) => {
        if (this.isPlatformServer()) {
          return [];
        }
        if (currentUser.id === data.author.id) {
          return this.getActionsForAuthor(data as Announcement);
        } else {
          return this.getActionsForNonAuthor(data as Announcement, currentUser);
        }
      })
    );
  }

  private getActionsForAuthor(data: Announcement): EntityDetailsAction[] {
    return [
      {
        label: 'Edytuj',
        callback: () => {
          this.dialogService.open(EditAnnouncementComponent, {
            uniqueId: EDIT_ANNOUNCEMENT_DIALOG_UNIQUE_ID,
          });
        },
        icon: 'edit',
      },
      {
        label: 'Usuń',
        callback: () => {
          const dialogRef = this.dialogService.open(ConfirmationComponent, {
            data: { question: 'Czy na pewno chcesz usunąć to ogłoszenie?' },
          });

          dialogRef.afterClose
            .pipe(
              filter((payload) => !!payload),
              take(1)
            )
            .subscribe(() => {
              this.store.dispatch(removeAnnouncement({ id: data.id, redirectFromCurrentUrl: true }));
            });
        },
        icon: 'delete',
        btnType: 'danger',
      },
    ];
  }

  private getActionsForNonAuthor(data: Announcement, currentUser: UserProfile): EntityDetailsAction[] {
    const canUserJoin =
      this.isCurrentUserGameEqualsToEntityGame(currentUser, data) &&
      !data.members.find((m) => m.member.id === currentUser.id);

    return [
      {
        label: 'Wyślij wiadomość',
        callback: () => {
          this.store.dispatch(openConversation({ receiver: data.author }));
        },
        icon: 'mail_outline',
      },
      ...(canUserJoin
        ? [
            {
              label: 'Dołącz',
              callback: () => {
                this.store.dispatch(addAnnouncementMember({ id: data.id }));
              },
              icon: 'whatshot',
            },
          ]
        : []),
    ];
  }
}
