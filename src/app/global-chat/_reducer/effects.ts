import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';

import { GlobalChatSocketService } from '../global-chat-ws.service';
import { GlobalChatService } from '../global-chat.service';
import { AppState } from '../../app.interface';
import { types } from './constants';
import {
  fetchMoreMessages,
  initFailed,
  initSuccess,
  markAsRead,
  receiveMessage,
  selectChannel,
  sendMessage,
} from './actions';
import { selectActiveChannel } from './selectors';
import { currentProfile, currentUser } from 'src/app/auth/_reducer';
import { LocalStorageService, SoundService, SoundType } from 'src/app/_core/services';
import { AlertService } from '../../_shared/components/alert/alert.service';

@Injectable()
export class GlobalChatEffects {
  init$ = createEffect(() =>
    this.actions$.pipe(
      ofType(types.Init),
      withLatestFrom(this.store.select(currentProfile)),
      switchMap(([_, user]) => {
        return this.globalChatService.init().pipe(
          map((data) => initSuccess({ ...data, currentUser: user })),
          catchError((err) => of(initFailed(err)))
        );
      })
    )
  );

  fetchMoreMessages$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fetchMoreMessages),
        withLatestFrom(this.store.select(selectActiveChannel)),
        switchMap(([_, channel]) => {
          this.globalChatSocket.fetchMoreMessages({
            page: channel.messages.results.length,
            channelId: channel.id,
          });

          return of([]);
        })
      ),
    { dispatch: false }
  );

  sendMessage$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(sendMessage),
        withLatestFrom(this.store.select(currentUser), this.store.select(selectActiveChannel)),
        map(([action, user, channel]) => {
          if (user.user.banned) {
            this.alertService.definedAlerts.userBanned.show();
          } else {
            return this.globalChatSocket.sendMessage({
              senderId: user.profile.id,
              content: action.content,
              channelId: channel.id,
            });
          }
        })
      ),
    { dispatch: false }
  );

  markAsRead$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(markAsRead),
        withLatestFrom(this.store.select(currentUser)),
        tap(([{ channelId }, user]) => {
          if (user.user) {
            this.globalChatSocket.markAsRead({ userId: user.user.id, channelId });
          }
        })
      ),
    { dispatch: false }
  );

  receiveMessage$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(receiveMessage),
        tap(() => {
          this.soundService.play(SoundType.NEW_MESSAGE);
        })
      ),
    { dispatch: false }
  );

  selectChannel$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(selectChannel),
        withLatestFrom(this.store.select(selectActiveChannel)),
        tap(([_, channel]) => {
          if (!channel.wasInitialDataFetched) {
            this.store.dispatch(fetchMoreMessages({ channelId: channel.id }));
          }
        })
      ),
    { dispatch: false }
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private globalChatService: GlobalChatService,
    private globalChatSocket: GlobalChatSocketService,
    private localStorage: LocalStorageService,
    private alertService: AlertService,
    private soundService: SoundService
  ) {}
}
