import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { first, map, tap } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';

import { GameOption } from '../_shared/common';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Dictionaries } from '../_shared/models';
import { AppState } from '../app.interface';
import { selectDicts } from '../_reducers/dictionaries';
import {
  EmailAvailableAsyncValidator,
  GameAccountAvailableAsyncValidator,
  noSpecialCharactersValidator,
  UsernameAvailableAsyncValidator,
} from '../_shared/form-validation';
import { register, types } from '../user/_reducer';
import { changeCurrentGame } from '../games/_reducer';
import { GameType } from '../_shared/interfaces';
import { scrollToTop } from '../_helpers';

@Component({
  selector: 'tf-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
  providers: [
    UsernameAvailableAsyncValidator,
    EmailAvailableAsyncValidator,
    GameAccountAvailableAsyncValidator,
  ],
})
export class RegistrationComponent implements OnInit {
  selectedGame: GameOption;
  steps: string[] = [];
  form: FormGroup;
  activeStep: number = 1;
  dictionaries$: Observable<Partial<Dictionaries>>;
  submitting$: Observable<boolean>;
  isRegisterSuccess$: Observable<boolean>;

  constructor(
    private fb: FormBuilder,
    private store: Store<AppState>,
    private actions: Actions,
    private usernameAsyncValidator: UsernameAvailableAsyncValidator,
    private emailAsyncValidator: EmailAvailableAsyncValidator,
    private gameAccountAsyncValidator: GameAccountAvailableAsyncValidator
  ) {
    this.dictionaries$ = this.store.select(selectDicts, ['servers', 'birthYears', 'ranks', 'modes']);
    this.submitting$ = this.store.select((state) => state.user.loading);
  }

  ngOnInit(): void {
    this.initSteps();
    this.initForm();
    this.listenOnRegisterStatusChange();
  }

  onSubmit(): void {
    this.store.dispatch(
      register({
        body: {
          ...this.form.value,
          gameType: this.selectedGame.value,
        },
      })
    );
  }

  onGameSelect(game: GameOption): void {
    this.selectedGame = game;
    this.store.dispatch(changeCurrentGame({ gameId: this.selectedGame.value }));
    this.setValidators();
  }

  canNextStep(): boolean {
    switch (this.activeStep) {
      case 1:
        return this.isBaseStepValid();
      case 2:
        return this.isGameAccountStepValid();
      case 3:
        return this.form.valid;
      default:
        return false;
    }
  }

  goNextStep(): void {
    this.activeStep = this.activeStep + 1;
    scrollToTop();
  }

  goBackStep(): void {
    this.activeStep = this.activeStep - 1;
  }

  private isBaseStepValid(): boolean {
    const { username, email, password, terms } = this.form.controls;
    return username.valid && email.valid && password.valid && terms.valid;
  }

  private isGameAccountStepValid(): boolean {
    const { gameAccountName, gameAccountServer } = this.form.controls;
    return gameAccountName.valid && gameAccountServer.valid;
  }

  private initForm(): void {
    this.form = this.fb.group({
      username: [
        '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(50),
          noSpecialCharactersValidator(),
        ],
        [this.usernameAsyncValidator],
      ],
      email: ['', [Validators.email, Validators.required], [this.emailAsyncValidator]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(50)]],
      terms: [false, [Validators.requiredTrue]],
      gameAccountName: ['', [Validators.required], [this.gameAccountAsyncValidator]],
      gameAccountServer: ['', []],
      age: [null, [Validators.required]],
      modes: [[]],
      roles: [[]],
      desc: [''],
      avatar: [null],
      csRank: [null],
    });
  }

  private initSteps(): void {
    this.steps = ['Podstawowe dane', 'Twoje konto w grze', 'Dodatkowe informacje'];
  }

  private listenOnRegisterStatusChange(): void {
    this.isRegisterSuccess$ = this.actions.pipe(
      ofType(types.RegisterSuccess, types.RegisterFailed),
      first(),
      map((action) => action.type === types.RegisterSuccess),
      tap(() => {
        this.activeStep = this.activeStep + 1;
      })
    );
  }

  private setValidators(): void {
    if (this.selectedGame.value === GameType.LOL) {
      this.form.controls['gameAccountServer'].setValidators([Validators.required]);
    }
    if (this.selectedGame.value === GameType.CS) {
      this.form.controls['csRank'].setValidators([Validators.required]);
    }
  }
}
