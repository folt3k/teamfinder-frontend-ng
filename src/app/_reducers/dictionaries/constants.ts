export const featureKey = 'dicts';

export const types = {
  FetchDictionaries: '[Dictionaries] Fetch dictionaries',
  FetchDictionariesSuccess: '[Dictionaries] Fetch dictionaries success',
};
