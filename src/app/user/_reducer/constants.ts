export const featureKey = 'user';

export const types = {
  UpdateUserNetworkStatus: '[User] Update user network status',

  Register: '[User] Register',
  RegisterSuccess: '[User] Register success',
  RegisterFailed: '[User] Register failed',

  Update: '[User] Update user',
  UpdateSuccess: '[User] Update user success',
  UpdateFailed: '[User] Update user failed',

  ChangePassword: '[User] Change user password',
  ChangePasswordSuccess: '[User] Change user password success',
  ChangePasswordFailed: '[User] Change user password failed',

  UpdateUserMailNotificationSettings: '[User] Update user mail notification settings ',
  UpdateUserMailNotificationSettingsSuccess: '[User] Update user mail notification settings success',
  UpdateUserMailNotificationSettingsFailed: '[User] UpdateU user mail notification settings failed',
};
