import { ButtonColor } from '../button/button.interface';

export interface TableAction<T> {
  label: string;
  callback: (T) => void;
  color: ButtonColor;
  icon?: string;
}

export type TableActions<T> = TableAction<T>[];
