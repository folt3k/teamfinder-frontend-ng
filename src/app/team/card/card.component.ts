import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';

import { Team } from '../team.model';
import { GameCheckers, getGameCheckers } from '../../games';

@Component({
  selector: 'tf-team-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TeamCardComponent implements OnInit {
  @Input() team: Team;

  gameCheckers: GameCheckers;

  constructor() {}

  ngOnInit(): void {
    this.gameCheckers = getGameCheckers(this.team.type);
  }

  get gameRoles(): string[] {
    return (this.team.roles || []).map((role) => role.name);
  }

  get gameModes(): string {
    return (this.team.modes || []).map((mode) => mode.name).join(', ');
  }
}
