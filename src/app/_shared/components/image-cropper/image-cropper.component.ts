import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  EventEmitter,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import Cropper from 'cropperjs';

@Component({
  selector: 'tf-image-cropper',
  templateUrl: './image-cropper.component.html',
  styleUrls: ['./image-cropper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageCropperComponent implements OnInit {
  @ViewChild('cropperImg') cropperImg: ElementRef;

  @Output() saved = new EventEmitter<string>();

  isImageLoaded: boolean = false;
  cropper: Cropper;

  constructor(private cdr: ChangeDetectorRef) {}

  ngOnInit(): void {}

  onFileChange(event: FileEvent): void {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = (e) => {
      this.onImageLoaded();
      this.cdr.detectChanges();

      if (this.cropperImg?.nativeElement) {
        this.cropperImg.nativeElement.src = e.target.result;
        this.initCropper();
      }
    };

    reader.readAsDataURL(file);
  }

  back(event: Event): void {
    event.stopPropagation();
    this.isImageLoaded = false;
    this.cdr.detectChanges();
  }

  save(): void {
    const croppedImage = this.cropper.getCroppedCanvas({ width: 200, height: 200 });

    if (croppedImage) {
      this.saved.emit(croppedImage.toDataURL('image/jpeg', 1));
    }
  }

  private onImageLoaded(): void {
    this.isImageLoaded = true;
  }

  private initCropper(): void {
    this.cropper = new Cropper(this.cropperImg.nativeElement, {
      aspectRatio: 1,
      zoomable: false,
      scalable: false,
    });
  }
}
