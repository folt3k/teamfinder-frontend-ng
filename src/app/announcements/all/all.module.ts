import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AllAnnouncementsRoutingModule } from './all-routing.module';
import { AllAnnouncementsComponent } from './all.component';
import { EntitiesScreenModule } from '../../entities-screen/entities-screen.module';
import { SharedModule } from '../../_shared/shared.module';

@NgModule({
  declarations: [AllAnnouncementsComponent],
  imports: [CommonModule, AllAnnouncementsRoutingModule, EntitiesScreenModule, SharedModule],
})
export class AllAnnouncementsModule {}
