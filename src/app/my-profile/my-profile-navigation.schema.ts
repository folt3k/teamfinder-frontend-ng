import { CollapsableNavigationSchema } from '../_shared/components/collapsable-navigation';
import { MY_PROFILE_STATIC_URLS } from '../_shared/common';

export const navigationSchema: CollapsableNavigationSchema = [
  {
    key: 'my-profile',
    label: 'Mój profil',
    children: [
      {
        key: 'profile-preview',
        label: 'Podgląd profilu',
        path: MY_PROFILE_STATIC_URLS['preview-profile'].slashPath,
      },
      {
        key: 'edit-profile',
        label: 'Edycja profilu',
        path: MY_PROFILE_STATIC_URLS['edit-profile'].slashPath,
      },
    ],
  },
  {
    key: 'settings',
    label: 'Ustawienia',
    path: MY_PROFILE_STATIC_URLS['settings'].slashPath,
  },
  {
    key: 'invitations',
    label: 'Zaproszenia do drużyn',
    children: [
      {
        key: 'my-invitations',
        label: 'Moje zaproszenia',
        path: MY_PROFILE_STATIC_URLS['my-invitations'].slashPath,
      },
      {
        key: 'team-invitations',
        label: 'Moje drużyny',
        path: MY_PROFILE_STATIC_URLS['team-invitations'].slashPath,
      },
    ],
  },
  {
    key: 'teams',
    label: 'Drużyny',
    path: MY_PROFILE_STATIC_URLS['teams'].slashPath,
  },
  {
    key: 'announcements',
    label: 'Moje ogłoszenia',
    path: MY_PROFILE_STATIC_URLS['announcements'].slashPath,
  },
];
