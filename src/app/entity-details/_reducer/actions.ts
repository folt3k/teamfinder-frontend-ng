import { createAction, props } from '@ngrx/store';

import { types } from './constants';
import { BaseEntity, ErrorRO } from '../../_shared/interfaces';
import { EntityType } from '../entity-details.interface';

export const fetchEntityDetails = createAction(
  types.FetchDetails,
  props<{ identifier: string; entityType: EntityType }>()
);
export const fetchEntityDetailsSuccess = createAction(
  types.FetchDetailsSuccess,
  props<{ data: BaseEntity }>()
);
export const fetchEntityDetailsFailed = createAction(types.FetchDetailsFailed, props<ErrorRO>());
