import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MyProfileTeamInvitationsComponent } from './team-invitations.component';
import { SharedModule } from '../../../_shared/shared.module';
import { MyProfileInvitationListModule } from '../list/list.module';

@NgModule({
  declarations: [MyProfileTeamInvitationsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        component: MyProfileTeamInvitationsComponent,
      },
    ]),
    SharedModule,
    MyProfileInvitationListModule,
  ],
})
export class MyProfileTeamInvitationsModule {}
