import { Injectable } from '@angular/core';

import { FntGameAccountStats, FntGameModeStat } from '../../../games';

@Injectable()
export class FntDetailsContentService {
  private stats: FntGameAccountStats;
  private controller: string;

  constructor() {}

  getStats(stats: FntGameAccountStats, controller: string): any {
    this.stats = stats;
    this.controller = controller;

    return {
      tileStats: [
        {
          label: 'Zagranych meczy',
          value: this.getAggregateValue('matchesplayed'),
        },
        {
          label: 'Zagranych godzin',
          value: Math.round(this.getAggregateValue('minutesplayed') / 60) + 'h',
        },
        {
          label: 'Współczynnik zwycięstw',
          value: this.getAverageValuePerGameMode('winrate', true) + '%',
        },
        {
          label: 'Liczba zabójstw',
          value: this.getAggregateValue('kills'),
        },
        {
          label: 'Współczynnik zabójstw/śmierci',
          value: this.getAverageValuePerGameMode('kd'),
        },
        {
          label: 'Score',
          value: this.getAggregateValue('score').toFixed(0),
        },
      ],
      gameModeStats: this.getGameModeStats(),
    };
  }

  private getAggregateValue(fieldKey: string): any {
    const stats = this.stats.per_input;
    const controllerKey = this.controller;

    return (
      ((stats &&
        stats[controllerKey] &&
        stats[controllerKey]['solo'] &&
        stats[controllerKey]['solo'][fieldKey]) ||
        0) +
      ((stats &&
        stats[controllerKey] &&
        stats[controllerKey]['duo'] &&
        stats[controllerKey]['duo'][fieldKey]) ||
        0) +
      ((stats &&
        stats[controllerKey] &&
        stats[controllerKey]['squad'] &&
        stats[controllerKey]['squad'][fieldKey]) ||
        0)
    );
  }

  private getAverageValuePerGameMode(fieldKey: string, percent: boolean = false): string {
    const valueSum = this.getAggregateValue(fieldKey);
    const stats = this.stats.per_input;
    const numberOfModes = stats ? Object.keys(stats).length : 1;

    return ((valueSum / numberOfModes) * (percent ? 100 : 1)).toFixed(2);
  }

  private getGameModeStats(): any {
    const stats = this.stats.per_input && this.stats.per_input[this.controller];

    return [
      {
        mode: 'solo',
        label: 'Solo',
        stats: stats && stats['solo'] && this.getGameModeStat(stats['solo']),
      },
      {
        mode: 'duo',
        label: 'Duo',
        stats: stats && stats['duo'] && this.getGameModeStat(stats['duo']),
      },
      {
        mode: 'squad',
        label: 'Squad',
        stats: stats && stats['squad'] && this.getGameModeStat(stats['squad']),
      },
    ];
  }

  private getGameModeStat(stats: FntGameModeStat): { matches: number; stats: any[] } {
    return {
      matches: stats.matchesplayed,
      stats: [
        {
          label: 'Wygranych',
          value: (stats.winrate * stats.matchesplayed).toFixed(0),
        },
        {
          label: 'Liczba zabójstw',
          value: stats.kills,
        },
        {
          label: 'Współczynnik zwycięstw',
          value: (stats.winrate * 100).toFixed(2) + '%',
        },
        {
          label: 'K/D',
          value: stats.kd,
        },
        {
          label: 'Zagranych godzin',
          value: Math.round(stats.minutesplayed / 60) + 'h',
        },
        {
          label: 'Zabójstw/mecz',
          value: (stats.kills / stats.minutesplayed).toFixed(2),
        },
        {
          label: 'Score',
          value: stats.score,
        },
      ],
    };
  }
}
