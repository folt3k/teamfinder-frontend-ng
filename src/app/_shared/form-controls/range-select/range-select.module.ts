import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { RangeSelectComponent } from './range-select.component';
import { FormControlModule } from '../form-control/form-control.module';
import { SelectModule } from '../select/select.module';

@NgModule({
  declarations: [RangeSelectComponent],
  imports: [CommonModule, ReactiveFormsModule, SelectModule, FormControlModule],
  exports: [RangeSelectComponent],
})
export class RangeSelectModule {}
