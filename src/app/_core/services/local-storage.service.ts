import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  private STORAGE_KEY: string = 'data';
  private storage: Storage;

  constructor(@Inject(PLATFORM_ID) private platformId: object) {
    if (isPlatformBrowser(this.platformId)) {
      this.storage = localStorage;
    }
  }

  init(): void {
    if (this.storage) {
      const storage = this.storage.getItem(this.STORAGE_KEY);

      if (!storage) {
        this.storage.setItem(this.STORAGE_KEY, JSON.stringify({}));
      }
    }
  }

  // tslint:disable-next-line: no-any
  set(fieldKey: string, value: any): void {
    if (this.storage) {
      const storage = this.storage.getItem(this.STORAGE_KEY);
      const currentData = storage && JSON.parse(storage);
      const data = {
        ...currentData,
        [fieldKey]: value,
      };

      this.storage.setItem(this.STORAGE_KEY, JSON.stringify(data));
    }
  }

  // tslint:disable-next-line: no-any
  get(fieldKey: string): any {
    if (this.storage) {
      const storage = this.storage.getItem(this.STORAGE_KEY);

      if (!!storage) {
        return JSON.parse(storage)[fieldKey];
      }

      return null;
    }
  }
}
