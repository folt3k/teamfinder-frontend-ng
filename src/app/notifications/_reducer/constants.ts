export const featureKey = 'notifications';

export const types = {
  FetchAll: '[Notifications] Fetch all',
  FetchAllSuccess: '[Notifications] Fetch all success',
  FetchAllFailed: '[Notifications] Fetch all failed',

  MarkAsRead: '[Notifications] Mark as read',
  MarkAsReadSuccess: '[Notifications] Mark as read success',
  MarkAsReadFailed: '[Notifications] Mark as read failed',

  MarkAllAsRead: '[Notifications] Mark all as read',
  MarkAllAsReadSuccess: '[Notifications] Mark all as read success',
  MarkAllAsReadFailed: '[Notifications] Mark all as read failed',

  MarkAllAsDisplayed: '[Notifications] Mark as displayed',
  MarkAllAsDisplayedSuccess: '[Notifications] Mark as displayed success',
  MarkAllAsDisplayedFailed: '[Notifications] Mark as displayed failed',

  ReceiveNewNotification: '[Notification] Receive new notification',
};
