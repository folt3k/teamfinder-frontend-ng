import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { combineLatest, Observable, Subject } from 'rxjs';
import { filter, take, takeUntil } from 'rxjs/operators';

import { TeamFormFieldsDisplayCondition } from '../../team/form/fields-display-condition';
import { TileSelectOption } from '../../_shared/form-controls/tiles-select/tiles-select.component';
import { BaseSelectOption } from '../../_shared/interfaces';
import { AppState } from '../../app.interface';
import { selectDicts } from '../../_reducers/dictionaries';
import { selectCurrentGame } from '../../games/_reducer';
import { fieldsDisplayCondition } from './filed-display-conditions';
import { createLoading } from '../_reducer';
import { CreateAnnouncementDto } from '../announcements.interface';
import { selectDetails } from '../../entity-details/_reducer';

interface FormFieldOptions {
  roles: TileSelectOption[];
  modes: TileSelectOption[];
  ranks: BaseSelectOption[];
}

@Component({
  selector: 'tf-announcement-form',
  templateUrl: './form.component.html',
})
export class AnnouncementFormComponent implements OnInit, OnDestroy {
  @Output() submitted = new EventEmitter<CreateAnnouncementDto>();
  @Input() edit: boolean = false;

  form: FormGroup;
  options: FormFieldOptions = { roles: [], modes: [], ranks: [] };
  fieldsDisplayCondition: TeamFormFieldsDisplayCondition;
  submitting$: Observable<boolean>;
  private destroy$ = new Subject();

  constructor(private fb: FormBuilder, private store: Store<AppState>) {}

  ngOnInit(): void {
    this.init();
    this.submitting$ = this.store.select(createLoading);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  onSubmit(): void {
    this.submitted.emit(this.form.value);
  }

  private init(): void {
    combineLatest(
      this.store.select(selectDicts, ['roles', 'modes', 'ranks']).pipe(take(1)),
      this.store.select(selectCurrentGame)
    )
      .pipe(
        filter(([_, currentGame]) => !!currentGame),
        takeUntil(this.destroy$)
      )
      .subscribe(([dicts, currentGame]) => {
        const gameKey = currentGame.key;

        this.fieldsDisplayCondition = fieldsDisplayCondition(currentGame.value);
        this.options = {
          roles: dicts.roles[gameKey],
          modes: dicts.modes[gameKey],
          ranks: dicts.ranks[gameKey],
        };

        this.initForm();
      });
  }

  private initForm(): void {
    this.form = this.fb.group({
      desc: ['', [Validators.required]],
      rank: [null],
      modes: [[]],
      roles: [[]],
    });

    if (this.edit) {
      this.setInitialValues();
    }
  }

  private setInitialValues(): void {
    this.store
      .select(selectDetails)
      .pipe(take(1))
      .subscribe((details) => {
        this.form.patchValue({
          desc: details.desc,
          rank: details.rank?.id,
          modes: details.modes.map((mode) => mode.id),
          roles: details.roles.map((role) => role.id),
        });
      });
  }
}
