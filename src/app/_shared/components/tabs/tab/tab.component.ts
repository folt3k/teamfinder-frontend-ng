import { ChangeDetectionStrategy, Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'tf-tab',
  templateUrl: './tab.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TabComponent implements OnInit {
  @ViewChild(TemplateRef) content: TemplateRef<any>;

  @Input() label: string;
  @Input() active: boolean = false;

  constructor() {}

  ngOnInit(): void {}
}
