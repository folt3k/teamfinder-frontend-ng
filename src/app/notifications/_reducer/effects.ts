import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { AppState } from './../../app.interface';
import * as actions from './actions';
import { NotificationsService } from '../notifications.service';

@Injectable()
export class NotificationsEffects {
  fetchAll$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.fetchAll),
      switchMap(({ page, perPage }) =>
        this.notificationService.fetchAll({ page, perPage }).pipe(
          map((data) => actions.fetchAllSuccess(data)),
          catchError((err) => of(actions.fetchAllFailed(err)))
        )
      )
    )
  );

  markAsRead$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.markAsRead),
      switchMap(({ id }) =>
        this.notificationService.markAsRead(id).pipe(
          map((data) => actions.markAsReadSuccess()),
          catchError((err) => of(actions.markAsReadFailed(err)))
        )
      )
    )
  );

  markAllAsRead$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.markAllAsRead),
      switchMap(() =>
        this.notificationService.markAllAsRead().pipe(
          map(() => actions.markAllAsReadSuccess()),
          catchError((err) => of(actions.markAllAsReadFailed(err)))
        )
      )
    )
  );

  markAllAsDisplayed$ = createEffect(() =>
    this.actions$.pipe(
      ofType(actions.markAllAsDisplayed),
      switchMap(() =>
        this.notificationService.markAllAsDisplayed().pipe(
          map(() => actions.markAllAsDisplayedSuccess()),
          catchError((err) => of(actions.markAllAsDisplayedFailed(err)))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private notificationService: NotificationsService
  ) {}
}
