import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'tf-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StepperComponent implements OnInit {
  @Input() steps: string[] = [];
  @Input() activeStep: number;

  constructor() {}

  ngOnInit(): void {}

  isStepDone(stepIndex: number): boolean {
    return this.activeStep > stepIndex + 1;
  }

  isStepActive(stepIndex: number): boolean {
    return this.activeStep === stepIndex+ 1;
  }
}
